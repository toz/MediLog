<!--
  ~     This file is part of MediLog.
  ~
  ~     MediLog is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU Affero General Public License as published by
  ~     the Free Software Foundation.
  ~
  ~     MediLog is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU Affero General Public License for more details.
  ~
  ~     You should have received a copy of the GNU Affero General Public License
  ~     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~     Copyright (c) 2018 - 2024 by Zell-MBC.com
  -->

<!--
  -->

<PreferenceScreen xmlns:android="http://schemas.android.com/apk/res/android">
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <PreferenceCategory android:title="@string/general">
        <SwitchPreference
            android:defaultValue="@string/BLENDINITEMS_DEFAULT"
            android:key="swBloodPressureBlendInDiary"
            android:summary= "@string/blendInDiaryItemsSummary"
            android:title="@string/blendInDiaryItems"/>

        <EditTextPreference
            android:defaultValue="@string/BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW_DEFAULT"
            android:key="etMultiMeasurementsWindow"
            android:inputType="number"
            android:summary="@string/multipleMeasurementsSummary"
            android:selectAllOnFocus="true"
            android:title="@string/multipleMeasurements" />

        <SwitchPreference
            android:defaultValue="@string/SHOWTIME_DEFAULT"
            android:key="swBloodPressureShowtime"
            android:summary= "@string/showTimeSummary"
            android:title="@string/showTime"/>

        <SwitchPreference
            android:defaultValue="@string/BLOODPRESSURE_HIGHLIGHT_VALUES_DEFAULT"
            android:key="cbBloodPressureHighlightValues"
            android:summary="@string/highlightValuesSummaryRange"
            android:title="@string/highlightValues" />

        <EditTextPreference
            android:defaultValue="mmHg"
            android:key="etBloodPressureUnit"
            android:selectAllOnFocus="true"
            android:singleLine="true"
            android:text="mmHg"
            android:summary="@string/bloodPressureUnitSummary"
            android:title="@string/unit"/>

        <SwitchPreference
            android:defaultValue="@string/LOG_HEARTRHYTHM_DEFAULT"
            android:key="cbLogHeartRhythm"
            android:summary="@string/logHeartRhythmSummary"
            android:title="@string/logHeartRhythm"/>
    </PreferenceCategory>

    <PreferenceCategory android:title="@string/thresholds">
        <EditTextPreference
            android:defaultValue="@string/BLOODPRESSURE_GRADE1_VALUES"
            android:digits="0123456789,"
            android:key="grade1"
            android:selectAllOnFocus="true"
            android:summary="@string/preferencesGrade1Summary"
            android:text="@string/BLOODPRESSURE_GRADE1_VALUES"
            android:title="@string/grade1" />

        <EditTextPreference
            android:defaultValue="@string/BLOODPRESSURE_GRADE2_VALUES"
            android:key="grade2"
            android:summary="@string/preferencesGrade2Summary"
            android:selectAllOnFocus="true"
            android:text="@string/BLOODPRESSURE_GRADE2_VALUES"
            android:title="@string/grade2" />

        <EditTextPreference
            android:defaultValue="@string/BLOODPRESSURE_GRADE3_VALUES"
            android:key="grade3"
            android:summary="@string/preferencesGrade3Summary"
            android:selectAllOnFocus="true"
            android:text="@string/BLOODPRESSURE_GRADE3_VALUES"
            android:title="@string/grade3" />

        <EditTextPreference
            android:defaultValue="@string/BLOODPRESSURE_HYPOTENSION_VALUES"
            android:key="etHypotension"
            android:summary="@string/hypotensionSummary"
            android:selectAllOnFocus="true"
            android:text="@string/BLOODPRESSURE_HYPOTENSION_VALUES"
            android:title="@string/hypotension" />
    </PreferenceCategory>

    <PreferenceCategory android:title="@string/actionPDF">
        <SwitchPreference
            android:defaultValue="false"
            android:key="cbBloodPressureLandscape"
            android:summary="@string/landscapeOrientationSummary"
            android:title="@string/landscapeOrientation"/>

        <SwitchPreference
            android:defaultValue="@string/DAY_SEPARATOR_LINE_DEFAULT"
            android:key="swBloodPressurePdfDaySeparatorLines"
            android:summary="@string/pdfShowDaySeparatorSummary"
            android:title="@string/pdfShowDaySeparator"/>

        <ListPreference
            android:defaultValue="A4"
            android:key="liBloodPressurePaperSize"
            android:entries="@array/paperSizes"
            android:entryValues="@array/paperSizes"
            android:summary="@string/paperSizeSummary"
            android:title="@string/paperSize" />

        <EditTextPreference
            android:defaultValue="@string/BLOODPRESSURE_TIMEZONE_DEFAULT"
            android:key="etTimezones"
            android:summary="@string/timezonesSummary"
            android:title="@string/timezones"/>

    </PreferenceCategory>

    <PreferenceCategory android:title="@string/chart">
               <SwitchPreference
                   android:defaultValue="false"
                   android:key="swBloodPressureRibbonChart"
                   android:summary="@string/bloodPressureRibbonChartSummary"
                   android:title="@string/bloodPressureRibbonChart" />

               <SwitchPreference
                   android:defaultValue="false"
                   android:key="cbShowPulse"
                   android:summary="@string/showPulseSummary"
                   android:title="@string/showPulse" />

               <SwitchPreference
                   android:defaultValue="@string/BLOODPRESURE_SHOW_THRESHOLD_DEFAULT"
                   android:key="cbShowBloodPressureThreshold"
                   android:title="@string/showThresholds"
                   android:summary="@string/showThresholdsSummary" />

               <SwitchPreference
                   android:defaultValue="true"
                   android:key="cbShowBloodPressureGrid"
                   android:title="@string/showGrid"
                   android:summary="@string/showGridSummary" />

               <SwitchPreference
                   android:defaultValue="@string/BLOODPRESURE_SHOW_LEGEND_DEFAULT"
                   android:key="cbShowBloodPressureLegend"
                   android:summary="@string/showLegendSummary"
                   android:title="@string/showLegend" />

                <SwitchPreference
                    android:defaultValue="false"
                    android:key="cbBloodPressureLinearTrendline"
                    android:title="@string/showLinearTrendline"
                    android:summary="@string/showLinearTrendlineSummary" />

                <SwitchPreference
                   android:defaultValue="false"
                   android:key="cbBloodPressureShowMovingAverage"
                   android:title="@string/showMovingAverage"
                   android:summary="@string/showMovingAverageSummary" />

               <EditTextPreference
                   android:defaultValue="6"
                   android:key="etBloodPressureMovingAverageSize"
                   android:inputType="number"
                   android:summary="@string/movingAverageTrendlineSizeSummary"
                   android:selectAllOnFocus="true"
                   android:title="@string/movingAverageTrendLineSize" />

           </PreferenceCategory>

       </PreferenceScreen>

