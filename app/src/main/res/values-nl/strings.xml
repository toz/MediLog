<?xml version="1.0" encoding="utf-8"?>
<!--
  ~     This file is part of MediLog.
  ~
  ~     MediLog is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU Affero General Public License as published by
  ~     the Free Software Foundation.
  ~
  ~     MediLog is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU Affero General Public License for more details.
  ~
  ~     You should have received a copy of the GNU Affero General Public License
  ~     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~     Copyright (c) 2018 - 2024 by Zell-MBC.com
  --><!--
  --><resources>
    <string name="word_deleted">verwijderd!</string>
    <string name="action_restore">Herstel</string>
    <string name="action_dataManagement">Gegevensbeheer</string>
    <string name="actionPDF">PDF-verslag</string>
    <string name="actionOpenPDF">PDF openen</string>
    <string name="actionSendPDF">Stuur</string>
    <string name="actionSendZIP">Stuur ZIP</string>
    <string name="fluidMissing">Vochtwaarde ontbreekt of is ongeldig!</string>
    <string name="bloodPressure">Bloeddruk</string>
    <string name="diary">Dagboek</string>
    <string name="comment">Opmerking</string>
    <string name="weightMissing">Gewichtswaarde ontbreekt of is ongeldig!</string>
    <string name="pulseMissing">Hartslag ontbreekt of is ongeldig!</string>
    <string name="sysMissing">Systolische waarde ontbreekt of is ongeldig!</string>
    <string name="diaryMissing">Dagboekaantekening ontbreekt of is ongeldig!</string>
    <string name="time">Tijd</string>
    <string name="agree">Akkoord</string>
    <string name="cancel">Annuleer</string>
    <string name="delete">Verwijder</string>
    <string name="unit">Eenheid</string>
    <string name="weight">Gewicht</string>
    <string name="action_about">Over</string>
    <string name="action_filter">Filter</string>
    <string name="fluid">Vocht</string>
    <string name="systolic">Systolisch</string>
    <string name="diastolic">Diastolisch</string>
    <string name="date">Datum</string>
    <string name="yes">Ja</string>
    <string name="save">Bewaar</string>
    <string name="edit">Bewerk</string>
    <string name="state">Staat</string>
    <string name="delimiter">Scheidingsteken</string>
    <string name="general">Algemeen</string>
    <string name="barChart">Staafdiagram</string>
    <string name="barChartSummary">Omschakeling van lijngrafiek naar staafdiagram</string>
    <string name="Copyright2">Deze toepassing maakt gebruik van modules van derden:</string>
    <string name="listTextSize">Tekstgrootte</string>
    <string name="eReadError">Fout: Kan de inhoud van het bestand niet lezen in de regel</string>
    <string name="eCreateFile">Fout: Kan geen bestand aanmaken</string>
    <string name="eOpenZipFile">Fout: Kan ZIP-bestand niet openen!
\n</string>
    <string name="eShareError">Fout: Kan geen bestand verzenden</string>
    <string name="showLinearTrendline">Lineaire trendlijnen</string>
    <string name="quickEntry">Snelle gegevensinvoer</string>
    <string name="quickEntrySummary">Voer de gegevens rechtstreeks in op het hoofdtabblad</string>
    <string name="highlightValues">Markeer verhoogde waarden</string>
    <string name="listTextSizeSummary">Tekstgrootte in gegevenslijsten</string>
    <string name="threshold">Drempelwaarde</string>
    <string name="grade1">Hypertensie graad 1</string>
    <string name="grade2">Hypertensie graad 2</string>
    <string name="grade3">Hypertensie graad 3</string>
    <string name="preferencesGrade2Summary">Drempels voor hypertensie graad 2
\nFormaat: Systolische drempelwaarde / Diastolische drempelwaarde</string>
    <string name="preferencesGrade1Summary">Drempels voor hypertensie graad 1
\nFormaat: Systolische drempelwaarde / Diastolische drempelwaarde</string>
    <string name="grade3Error">Fout in Bloeddruk graad 3 drempelwaarde:</string>
    <string name="grade1Error">Fout in Bloeddruk graad 1 drempelwaarde:</string>
    <string name="movingAverageTrendLineSize">Bewegend gemiddelde grootte</string>
    <string name="notEnoughDataForChart">Minstens 2 datapunten zijn nodig om een grafiek te tekenen!</string>
    <string name="noDataToExport">Geen gegevens om te exporteren!</string>
    <string name="noActiveTab">Geen tabblad geselecteerd? Schakel ten minste één tabblad in de instellingen in.</string>
    <string name="afternoon">Namiddag</string>
    <string name="evening">Avond</string>
    <string name="eSelectDirectory">"Niet in staat om de map in te stellen. Fout: "</string>
    <string name="warning">Waarschuwing!</string>
    <string name="thisIsGoing">Dit gaat alle(!) gegevens verwijderen! %1$d regels.</string>
    <string name="doYouReallyWantToContinue">Wil je echt doorgaan\?</string>
    <string name="morning">Ochtend</string>
    <string name="selectDirectory">Selecteer back-up directory</string>
    <string name="selectFile">Selecteer bestand</string>
    <string name="eNoMediLogFile">Fout: Niet in staat om te importeren. Is dit een MediLog-bestand\?
\nGevonden:</string>
    <string name="data">gegevens</string>
    <string name="showPulse">Toon Hartslag</string>
    <string name="showPulseSummary">Toon Hartslag in Bloeddrukgrafiek</string>
    <string name="weightEditTitle">Gewicht item bewerken</string>
    <string name="diaryEditTitle">Dagboekitem bewerken</string>
    <string name="bloodPressureEditTitle">Bloeddrukpunt bewerken</string>
    <string name="zipPassword">Wachtwoord</string>
    <string name="enterPassword">Geef wachtwoord voor ZIP-bestand</string>
    <string name="password">Wachtwoord</string>
    <string name="sendText">Gemaakt door MediLog</string>
    <string name="userNameDefault">Uw naam</string>
    <string name="showLegend">Legenda tonen</string>
    <string name="showLegendSummary">Toon legenda in gewichts- en bloeddrukgrafieken</string>
    <string name="showGrid">Toon rooster</string>
    <string name="showGridSummary">Toon rooster in gewichts- en bloeddrukgrafieken</string>
    <string name="showThresholds">Toon drempelwaarden</string>
    <string name="showThresholdsSummary">Toon drempelwaarden in gewichts- en bloeddrukgrafieken</string>
    <string name="invalidInput">Ongeldige invoer</string>
    <string name="emptySeparator">Het scheidingsteken kan niet leeg zijn!</string>
    <string name="setFilter">Filter instellen:</string>
    <string name="filteredOut">Gegevens succesvol opgeslagen. Maar nieuwe waarde wordt niet getoond vanwege een actief filter!</string>
    <string name="disclaimerText">Deze app is geen vervanging voor medisch advies. Gebruikers van de applicatie dienen hun zorgverlener te raadplegen voordat ze een gezondheids-, medische of andere beslissing nemen op basis van de gegevens die in de applicatie zijn opgenomen. Voor licentie- en garantievoorwaarden raadpleegt u het licentiedocument.</string>
    <string name="allowedDelimiters">Toegestane scheidingstekens:</string>
    <string name="errorInCSVHeader">Fout in CSV header!
\nVerwachte</string>
    <string name="noCsvDelimiterFound">Fout: Kon het CSV-scheidingsteken op lijn 1 van het importbestand niet identificeren!\nGeldige scheidingstekens zijn</string>
    <string name="entries">inzendingen</string>
    <string name="security">Beveiliging</string>
    <string name="databaseEncryption">Database Gecodeerd</string>
    <string name="biometricProtection">Biometrische bescherming</string>
    <string name="protectedBackup">Wachtwoordbeveiliging voor back-ups</string>
    <string name="noBiometricHardware">Er zijn geen biometrische kenmerken beschikbaar op dit apparaat.</string>
    <string name="biometricHardwareUnavailable">Biometrische kenmerken zijn momenteel niet beschikbaar.</string>
    <string name="appDescription">MediLog - Gezondheidsgegevenslogger</string>
    <string name="action_settings">Instellingen</string>
    <string name="diaMissing">Diastolische waarde ontbreekt of is ongeldig!</string>
    <string name="weightUnitSummary">Gewichtseenheid</string>
    <string name="preferencesDelimiterSummary">Teken dat wordt gebruikt om de velden voor de invoer/uitvoer van gegevens te scheiden (CSV).
\nToegestane tekens: , ; |</string>
    <string name="showLinearTrendlineSummary">Toon lineaire trendlijnen in grafieken</string>
    <string name="movingAverageTrendlineSizeSummary">Aantal records gebruikt voor de berekening van het bewegend gemiddelde. Hoe hoger de waarde hoe platter de trendlijn. Minimumwaarde is 2</string>
    <string name="protectedZipFileCreated">Met succes gecreëerd beschermd</string>
    <string name="disclaimer">Ontkenning</string>
    <string name="unprotectedZipFileCreated">Met succes onbeschermde</string>
    <string name="zipPasswordSummary">Standaard wachtwoord instellen voor back-ups</string>
    <string name="preferencesGrade3Summary">Drempels voor hypertensie graad 3
\nFormaat: Systolische drempelwaarde / Diastolische drempelwaarde</string>
    <string name="grade2Error">Fout in Bloeddruk graad 2 drempelwaarde:</string>
    <string name="biometricLoginDescription">Bescherm de toegang tot de applicatie met biometrische authenticatie.</string>
    <string name="noCredentials">Biometrische beveiliging staat aan, maar u heeft geen biometrische gegevens aan uw account gekoppeld.</string>
    <string name="today">Vandaag</string>
    <string name="word_item">punt</string>
    <string name="action_backup">Back-up</string>
    <string name="pulse">Hartslag</string>
    <string name="submit">Indienen</string>
    <string name="startDate">Startdatum</string>
    <string name="endDate">Einddatum</string>
    <string name="retryActionText">Probeer het opnieuw!</string>
    <string name="biometricLogin">Biometrische login</string>
    <string name="retryAction">OPNIEUW</string>
    <string name="fluidUnitSummary">Vochteenheid</string>
    <string name="emptyTable">Tabel is leeg.</string>
    <string name="tabTransition">Tabbladovergangen</string>
    <string name="tabTransitionSummary">Animatie die wordt gebruikt om tussen tabbladen te schakelen</string>
    <string name="preferencesShowTabIconSummary">Pictogrammen tonen in tab-koppen</string>
    <string name="preferencesShowTabTextSummary">Tekst tonen in tab header sectie</string>
    <string name="scrollableTabsText">Controleerbare Tabbladen</string>
    <string name="preferencesScrollableTabsSummary">Tabs horizontaal laten scrollen</string>
    <string name="chart">Grafiek</string>
    <string name="showTabIcon">Tab-Pictogrammen Tonen</string>
    <string name="showTabText">Tabtekst Tonen</string>
    <string name="tabs">Tabblad</string>
    <string name="preferencesFluidThresholdSummary">Dagelijkse vochtinname limiet</string>
    <string name="fluidFormula">Formule = (gewicht in kg * 30 / 1000)</string>
    <string name="recommendedDailyWaterIntake">Aanbevolen dagelijkse vochtinname:</string>
    <string name="measurementsInDB">Metingen in database:</string>
    <string name="heightSummary">Voer de lichaamshoogte in cm/ in om MediLog in staat te stellen de Body Mass Index (BMI) te berekenen</string>
    <string name="fluidInfoTitle">Info vochtinname</string>
    <string name="noDataToShow">Tabel is leeg, geen gegevens te zien!</string>
    <string name="thresholds">Drempelwaarden</string>
    <string name="diaryInfoTitle">Dagboek Info</string>
    <string name="glucoseInfoTitle">Bloedsuiker Info</string>
    <string name="bloodPressureInfoTitle">Info over bloeddruk</string>
    <string name="weightInfoTitle">Gewichten Info</string>
    <string name="value">waarde</string>
    <string name="invalid">Ongeldig</string>
    <string name="invalidBodyHeight">Ongeldige lichaamslengte waarde. Controleer uw instellingen!</string>
    <string name="timePeriod">Tijdsperiode:</string>
    <string name="minMaxValues">Minimum/maximum:</string>
    <string name="measurementsInFilter">Metingen in filter:</string>
    <string name="height">Lichaamslengte</string>
    <string name="bmiActual">Body Mass Index (BMI) :</string>
    <string name="bloodPressureUnitSummary">Bloeddruk eenheid, standaard is mmHg</string>
    <string name="glucoseUnitSummary">Selecteer mg/dL of mmol/L</string>
    <string name="glucoseEditTitle">Bewerk bloedsuiker item</string>
    <string name="activeTabsSummary">Kies de gegevens die u wilt bijhouden</string>
    <string name="activeTabs">Actieve tabbladen</string>
    <string name="glucose">Bloedsuiker</string>
    <string name="glucoseKetoneIndex">GKI</string>
    <string name="size">Maat</string>
    <string name="database">Databank</string>
    <string name="allowedInput">Toegestane invoer:</string>
    <string name="enterComment">Voeg opmerking toe:</string>
    <string name="backupOverdue">Backup is te laat!</string>
    <string name="backupWarningSummary">Waarschuw als de laatste back-up ouder is dan de hier opgegeven waarde (Dagen). Laat het veld leeg om uit te schakelen</string>
    <string name="backupWarning">Waarschuwing voor back-up</string>
    <string name="autoBackup">Automatisch back-uppen</string>
    <string name="lastBackup">Laatste Backup:</string>
    <string name="unknown">Onbekend</string>
    <string name="autoBackupSummary">Maak automatisch een back-up als de laatste back-up ouder is dan hier opgegeven (Dagen). Laat het veld leeg om uit te schakelen</string>
    <string name="logHeartRhythm">Log Hartritme</string>
    <string name="backupLocation">Back-up locatie:</string>
    <string name="invalidBackupLocation">Kan automatische back-uplocatie niet gebruiken! Voer een handmatige back-up opnieuw uit.</string>
    <string name="missingBackupLocation">Standaard back-uplocatie ontbreekt!</string>
    <string name="logHeartRhythmSummary">Log hartritmestatus aangeboden door sommige meettoestellen</string>
    <string name="fluidEditTitle">Bewerk Vocht Item</string>
    <string name="eUnableToWriteToFolder">Kon niet naar map schrijven</string>
    <string name="eLostAccessRights">Geen toegangsrechten tot back-upmap! Back-uplocatie bijwerken in back-upinstellingen</string>
    <string name="paperSize">Papierformaat</string>
    <string name="landscapeOrientationSummary">Gebruik liggende afdrukstand</string>
    <string name="landscapeOrientation">Liggend</string>
    <string name="paperSizeSummary">Selecteer papierformaat</string>
    <string name="themeLight">Licht</string>
    <string name="themeSystem">Zelfde als systeem</string>
    <string name="appTheme">Thema</string>
    <string name="themeOptionsSummary">Thema-opties instellen</string>
    <string name="bodyFatMinMax">Lichaamsvet min / max waarden</string>
    <string name="logBodyFat">Lichaamsvet</string>
    <string name="bodyFatMinMaxSummary">Bepaal het lichaamsvetwaarschuwingsniveau (min/max)</string>
    <string name="logBodyFatSummary">Schakelaar logging lichaamsvet functie aan/uit</string>
    <string name="bodyFat">lichaamsvet</string>
    <string name="themeAutoBattery">Automatische Batterij</string>
    <string name="themeDark">Donker</string>
    <string name="avg">gem.</string>
    <string name="includeOtherTabs">Inclusief andere tabbladen</string>
    <string name="includeOtherTabsSummary">Laat ook opmerkingen van andere tabbladen zien</string>
    <string name="blendedItem">Dit item is afkomstig van een ander tabblad, u kunt het niet bewerken hier!</string>
    <string name="addTimestampSummary">Voeg tijd toe aan backup bestandsnaam</string>
    <string name="addTimestamp">Tijdstempel toevoegen</string>
    <string name="quantity">Hoeveelheid</string>
    <string name="rowPadding">Rij opvulling</string>
    <string name="rowPaddingSummary">Spatie tussen rijen in gegevenslijst</string>
    <string name="reportTitle">MediLog - %1$s Verslag voor %2$s</string>
    <string name="timeframeLabel">Periode (dagen)</string>
    <string name="linesRead">regels gelezen</string>
    <string name="checkFileHeader">Controleer bestand header</string>
    <string name="monthLabel">1 maand</string>
    <string name="deleteExistingData">Wis bestaande gegevens</string>
    <string name="loadRawData">Ruwe gegevens laden</string>
    <string name="addRecords">Records toevoegen aan databank</string>
    <string name="found">Gevonden</string>
    <string name="hypotension">Hypotensie</string>
    <string name="max">max</string>
    <string name="min">min</string>
    <string name="openZipFile">Open ZIP bestand</string>
    <string name="importCanceled">Import geannuleerd</string>
    <string name="hypotensionError">Fout in bloeddruk hypotensie drempelwaarde:</string>
    <string name="done">Gedaan</string>
    <string name="statistics">Statistieken</string>
    <string name="measurementLabel">Metingen (#)</string>
    <string name="hypotensionSummary">Drempels voor lage bloeddruk
\nFormaat: Systolische drempelwaarde | Diastolische drempelwaarde</string>
    <string name="invalidTextSize">Ongeldige waarde voor Tekstgrootte.</string>
    <string name="choseValue">Kies een waarde tussen %1$s en %2$s</string>
    <string name="annualLabel">1 Jaar</string>
    <string name="totalLabel">Totaal</string>
    <string name="temperature">Temperatuur</string>
    <string name="temperatureInfoTitle">Info Temperatuur</string>
    <string name="temperatureEditTitle">Temperatuur bewerken</string>
    <string name="tab_temperature">Temperatuur</string>
    <string name="temperatureMissing">Temperatuurwaarde ontbreekt of is ongeldig!</string>
    <string name="temperatureUnitSummary">Temperatuur eenheid</string>
    <string name="invalidUpperThreshold">Ongeldige bovengrens.</string>
    <string name="invalidThresholds">Ongeldige drempels</string>
    <string name="invalidLowerThreshold">Ongeldige ondergrens.</string>
    <string name="notGood">Ik voel me niet lekker</string>
    <string name="bad">Ik voel me slecht</string>
    <string name="good">Ik voel me lekker</string>
    <string name="stateGood">Lekker</string>
    <string name="pdfTextSizeSummary">Tekstgrootte in gegevensrapporten</string>
    <string name="backupStarted">Backup gestart…</string>
    <string name="invalidRowHeight">Ongeldige waarde voor Rijhoogte. Kies een waarde &gt;= %1$s</string>
    <string name="tareSummary">Deze waarde automatisch aftrekken nadat een nieuwe gewichtswaarde werd toegevoegd</string>
    <string name="tare">Tarreren</string>
    <string name="deleteItem">Verwijder item</string>
    <string name="stateBad">Slecht</string>
    <string name="stateNoValue">Geen waarde</string>
    <string name="filter_static">Statisch</string>
    <string name="filter_off">Uit</string>
    <string name="filter_rolling">Rollen</string>
    <string name="timeframe">Tijdsframe</string>
    <string name="years">Jaar</string>
    <string name="months">Maanden</string>
    <string name="weeks">Weken</string>
    <string name="days">Dagen</string>
    <string name="timezonesSummary">Uren definiëren Ochtend-Middag-Evening kolommen</string>
    <string name="invalidTimezoneSetting">is een ongeldige tijdzone waarde. Terugzetten naar standaard 0–12–18</string>
    <string name="timezones">Tijdzones</string>
    <string name="never">Nooit</string>
    <string name="featureNotAvailable">Deze functie vereist Android 8 of nieuwer!</string>
    <string name="unknownError">Onbekende fout!</string>
    <string name="actionOpenCSV">CSV openen</string>
    <string name="actionSendCSV">CSV zenden</string>
    <string name="authenticationTimeout">Authenticatie Timeout</string>
    <string name="authenticationTimeoutSummary">Minuten van inactiviteit waarna herauthenticatie vereist is</string>
    <string name="otherFeedbackMeans">Andere manieren om feedback te geven:</string>
    <string name="serverResponse">Antwoord server:</string>
    <string name="dataSent">Data verzonden:</string>
    <string name="lastFeedback">Laatste terugkoppeling:</string>
    <string name="whyUserFeedback">Waarom gebruikersfeedback\?</string>
    <string name="userFeedback">Feedback van gebruikers</string>
    <string name="nothing">Niets</string>
    <string name="instantFeedback">Directe terugkoppeling</string>
    <string name="feedbackEnabled">Dagelijkse terugkoppeling:</string>
    <string name="logKetone">Ketonen</string>
    <string name="ketone">Ketonen</string>
    <string name="logKetoneSummary">Keton loggen aan/uit zetten</string>
    <string name="valuesMissing">Waarden ontbreken of zijn ongeldig!</string>
    <string name="oximetryInfoTitle">Oximetrie-Informatie</string>
    <string name="oximetryEditTitle">Bewerk Oximetrie Item</string>
    <string name="days7">7 dagen</string>
    <string name="thresholdSummaryUpper">Maximumdrempel definiëren</string>
    <string name="thresholdSummaryLower">Minimumdrempel definiëren</string>
    <string name="thresholdSummaryRange">Minimum-en maximumwaarden definiëren</string>
    <string name="highlightValuesSummaryRange">Markeer waarden lager of hoger dan de drempelwaarde</string>
    <string name="highlightValuesSummaryHigh">Markeer waarden hoger dan drempelwaarde</string>
    <string name="highlightValuesSummaryLow">Markeer waarden onder de drempelwaarde</string>
    <string name="oximetry">Oximetrie</string>
    <string name="days14">14 dagen</string>
    <string name="days31">31 dagen</string>
    <string name="days21">21 dagen</string>
    <string name="privacy">Privacy</string>
    <string name="blockScreenshots">Blokkeer Schermafdrukken</string>
    <string name="blockScreenshotsDescription">Voorkom of sta schermafdrukken toe</string>
    <string name="invalidTemplateIndex">Sjabloon %1$s bestaat niet!</string>
    <string name="emptyTemplate">Sjabloon %1$s is leeg!</string>
    <string name="pulseUnit">BPM</string>
    <string name="tareApplied">Tarra toegepast</string>
    <string name="average">Gemiddelde</string>
    <string name="invalidTreshold">Ongeldige waarde voor Drempelwaarde.</string>
    <string name="textTemplates">Sjablonen voor tekst</string>
    <string name="showEmojiSummary">Weergave van emoji aan/uit schakelen</string>
    <string name="showBadEmoji">Toon 🙁</string>
    <string name="showNotGoodEmoji">Toon 😐︎</string>
    <string name="showGoodEmoji">Toon 😀︎</string>
    <string name="showTimeSummary">Geef de tijd weer in datumkolom</string>
    <string name="showTime">Toon tijd</string>
    <string name="bmiChart">https://nl.wikipedia.org/wiki/Queteletindex</string>
    <string name="action_profiles">Profielen</string>
    <string name="attachmentsImported">Geïmporteerde %1$s bijlagen</string>
    <string name="onSwitch">Aan</string>
    <string name="offSwitch">Uit</string>
    <string name="count">Graaf</string>
    <string name="settingsEncryption">Instellingen versleuteld</string>
    <string name="attachmentEncryption">Bijlagen versleuteld</string>
    <string name="bmiNormal">Normale BMI (18,5 – 24,9):</string>
    <string name="bmiDifference">Verschil:</string>
    <string name="notAvailableShort">N/A</string>
    <string name="openDataFile">Open gegevensbestand</string>
    <string name="profiles">Profielen</string>
    <string name="no">Geen</string>
    <string name="readLines">Lijnen lezen</string>
    <string name="openProfilesFile">Profielen openen</string>
    <string name="openTextTemplatesFile">Tekstsjablonenbestand openen</string>
    <string name="extractCSV">CSV-bestand uitpakken</string>
    <string name="grade">Rang</string>
    <string name="emptyDatabase">Database is leeg</string>
    <string name="protected_">beschermd</string>
    <string name="unprotected">onbeschermd</string>
    <string name="attention">Attentie!</string>
    <string name="invalidGrade">Je invoer moet twee numerieke waarden bevatten, gescheiden door een \\nBijv. 140/80</string>
    <string name="eReadZipFile">Fout bij lezen ZIP-bestand!
\n</string>
    <string name="values">Waarden</string>
    <string name="bloodPressureRibbonChart">Lintgrafiek</string>
    <string name="bloodPressureRibbonChartSummary">Toon lintgrafiek in plaats van lijngrafiek (Experimenteel)</string>
    <string name="ok">OKÉ</string>
    <string name="add">Toevoegen</string>
    <string name="stateNotGood">Niet goed</string>
    <string name="action_import">Importeren</string>
    <string name="dataDeleted">Bestaande gegevens verwijderd</string>
    <string name="checkCsvHeader">Controleer de koptekst van CSV-bestanden</string>
    <string name="importAttachments">Bestandsbijlagen importeren</string>
    <string name="createdNewDefaultProfile">Standaardprofiel aangemaakt</string>
    <string name="color">Kleur</string>
    <string name="cancelImport1">Het importeren in de database is al begonnen, de database zal inconsistent zijn!</string>
    <string name="fasting">Vasten</string>
    <string name="preMeal">Pre-maaltijd</string>
    <string name="postMeal">Na de maaltijd</string>
    <string name="profileSpecific">Profiel specifieke instellingen</string>
    <string name="blendInDiaryItems">Voeg dagboek opmerkingen toe</string>
    <string name="blendInDiaryItemsSummary">Toon dagboek opmerkingen in dit tabblad (alleen lezen)</string>
    <string name="multipleMeasurements">Meerdere metingen</string>
    <string name="multipleMeasurementsSummary">Samenvatten of vervangen van metingen die binnen x minuten zijn gedaan. Gebruik 0 om deze instelling uit te schakelen</string>
    <string name="ignore">Negeer</string>
    <string name="keepAllRecords">en houd alle gegevens bij</string>
    <string name="replaceWith">Vervangen door</string>
    <string name="previousMeasurements">oude metingen</string>
    <string name="enableAttachments">Bijlagen inschakelen</string>
    <string name="pdfDataOrder">Sortering</string>
    <string name="pdfDataOrderSummary">Selecteer of gegevens in PDF-documenten oplopend of aflopend worden geordend</string>
    <string name="ascending">Oudste waarden eerst</string>
    <string name="descending">Meest recente waarden eerst</string>
    <string name="dynamicColor">Dynamische kleur</string>
    <string name="dynamicColorSummary">Material3 dynamisch kleurenthema inschakelen</string>
    <string name="pdfShowDaySeparator">Scheidingslijn per dag</string>
    <string name="pdfShowDaySeparatorSummary">Geeft een lijn weer wanneer de dag verandert</string>
    <string name="textTemplatesDialogDescription">Kies een tekst:</string>
    <string name="attachmentDialogTitle">Bijlage toevoegen</string>
    <string name="attachmentDialogText">Kies een bestand of maak een nieuwe foto</string>
    <string name="photo">Foto</string>
    <string name="file">Bestand</string>
    <string name="permissionDenied">Toestemming geweigerd</string>
    <string name="attachments">Bijlagen</string>
    <string name="errorDuringAttachmentDecryption">Fout: Kan bijlage niet decoderen, probeert lokaal te openen</string>
    <string name="recordsAdded">%1$s records toegevoegd</string>
    <string name="search">Zoek op</string>
    <string name="textTemplate">Tekst sjabloon</string>
    <string name="noTextTemplate">Sjabloon is leeg</string>
    <string name="waitForUIUpdate">Wacht tot de lijst vernieuwd is</string>
    <string name="activeProfileName">Actief profiel</string>
    <string name="addProfileDialog">Profiel toevoegen</string>
    <string name="profileName">Naam profiel</string>
    <string name="editProfilesDialog">Profielen bewerken</string>
    <string name="editProfileDialog">Profiel bewerken</string>
    <string name="deleteProfileDialog">Profiel(en) verwijderen</string>
    <string name="noProfileName">Profielnaam is leeg</string>
    <string name="memoryConsumption">Geheugengebruik</string>
    <string name="moreThanOneProfileSelected">Meer dan één profiel geselecteerd!</string>
    <string name="noProfileSelected">Geen profiel gekozen!</string>
    <string name="defaultProfileNeedsToRemain">Laatste profiel kan niet worden verwijderd!</string>
    <string name="details">Bijzonderheden:</string>
    <string name="importProfiles">Profielgegevens importeren</string>
    <string name="importCSV">CSV-bestand importeren</string>
    <string name="overwriteData">Bestaande gegevens overschrijven</string>
    <string name="wrongUnitWarning">Waarschuwing: app is ingesteld op mmol/L, maar waarde lijkt in mg/dL te zijn?</string>
    <string name="importFormat">Zorg ervoor dat het importbestand overeenkomt met de vereiste structuur! Kijk hier voor meer informatie:</string>
    <string name="cancelImport2">Het importeren van de database is nog niet gestart, annuleren is veilig!</string>
    <string name="enableAttachmentsSummary">Bijlagen zijn verouderd en worden mogelijk verwijderd in een toekomstige versie van MediLog. Neem contact met me op als je denkt dat je een use case hebt die afhankelijk is van deze functie</string>
    <string name="biometricProtectionOff">Biometrische bescherming uit</string>
    <string name="unsavedData">Niet-opgeslagen waarden gevonden. Wilt u dit scherm echt sluiten?</string>
    <string name="multipleMeasurementsFound">Gevonden %1$d meting opgenomen in de laatste %2$s minuten.</string>
    <string name="trendline">Trendlijn</string>
    <string name="deleteProfiles">Dit gaat %1$d profielen met in totaal %2$d data regels verwijderen.</string>
    <string name="crashMessageEmail">Helaas is MediLog gecrasht! Deze email bevat de crash log als bijlage.\nControleer dit a.u.b.</string>
    <string name="crashContext">Wat was u aan het doen toen de crash plaatsvond?</string>
    <string name="crashLogs">Email crash logs</string>
    <string name="crashLogsSummary">Sta toe dat crash logs naar de ontwikkelaars worden gestuurd. Voor elke email zal explicite toestemming worden gevraagd.</string>
    <string name="crashMessageDialog">Helaas crashte MediLog door een ernstige fout. Indien u dit de ontwikkelaars wilt laten weten zodat zij het probleem kunnen onderzoeken, overweeg dan om nu een crash rapport te emailen.</string>
    <string name="openSettingsFile">Open Instellingen bestand</string>
    <string name="settings">Instellingen</string>
    <string name="pdfCantCreate">Kon geen PDF bestand aanmaken!</string>
    <string name="crashTitle">MediLog is gecrasht</string>
    <string name="backupOverdueMsg1">%1$d records welke nog nooit werden opgeslagen!</string>
    <string name="backupOverdueMsg2">%1$d records, laatste backup is %2$d dagen oud!</string>
    <string name="showMovingAverage">Voortschrijdend Gemiddelde</string>
    <string name="showMovingAverageSummary">Geef voortschrijdend gemiddelde weer i.p.v. waarden</string>
    <string name="fluidShowDaily">Dagelijkse samenvattingen</string>
    <string name="fluidShowDailySummary">Laat dagelijkse samenvattingen zien i.p.v. afzonderlijke waarden</string>
    <string name="pickTag">Kies een label:</string>
    <string name="addTag">Voeg een label toe:</string>
    <string name="emptyTag">Label is leeg!</string>
    <string name="openTagsFile">Open Label bestand</string>
    <string name="tag">Label</string>
    <string name="tags">Labels</string>
    <string name="description">Beschrijving</string>
</resources>