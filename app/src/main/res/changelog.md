# Changelog
 
## v2.5.11, build 5427

### New 

- Nothing

### Fixed/Changed

- BloodPressure: Fixed text scrolling issue: https://codeberg.org/toz/MediLog/issues/321


### Known issues

- None
## v2.5.10, build 5426

### New 

- Added page numbers to PDF reports. Seem to have missed this all the years and no one dared to complain :-), https://codeberg.org/toz/MediLog/issues/313

### Fixed/Changed

- Fixed corner case in which the keyboard wouldn't show a decimal separator, https://codeberg.org/toz/MediLog/issues/314
- Fixed visual glitch when QuickEntry is disabled and icon + text enabled, https://codeberg.org/toz/MediLog/issues/316
- BloodPressure: Some clean up of the PDF statistics page
- Updated dependencies and translations


### Known issues

- None
## v2.5.9, build 5425

### New 

- Added support for Android Themed App Icons, https://codeberg.org/toz/MediLog/issues/304
- Diary: Allow to move the + Button, https://codeberg.org/toz/MediLog/issues/300

### Fixed/Changed

- New language: Punjabi (pa) by bgo-eiu
- Some Turkish and Polish language updates by (abfreeman and Erik Michalak)

### Known issues

- None
## v2.5.8, build 5424

### New 

- Nothing

### Fixed/Changed

- Changed default dark mode setting to "Follow System" https://codeberg.org/toz/MediLog/issues/303
- Unable to select/change backup location, https://codeberg.org/toz/MediLog/issues/305
- Lost unsaved text if navigating away from app, https://codeberg.org/toz/MediLog/issues/301 and https://codeberg.org/toz/MediLog/issues/299
- Some language updates

### Known issues

- None
## v2.5.7, build 5423

### New 

- Nothing

### Fixed/Changed

- Export: Fixed visual glitch, https://codeberg.org/toz/MediLog/issues/288
- Glucose: Stats don't show in dark mode, https://codeberg.org/toz/MediLog/issues/296
- Glucose: Fixed distorted PDF output, https://codeberg.org/toz/MediLog/issues/297
- Water: Fixed a bug when filtering data for the PDF report
- Some language updates

### Known issues

- None
## v2.5.6, build 5422

### New 

- All tabs: Jump to first entry field after record is entered to allow rapid entry of multiple data sets, https://codeberg.org/toz/MediLog/issues/283

### Fixed/Changed

- All: Issue where the changelog splashscreen wasn't updated, https://codeberg.org/toz/MediLog/issues/276
- All: Bug which made the app crash after deleting an entry and immediately selecting a new one, https://codeberg.org/toz/MediLog/issues/279
- All: Translation issue in filter dialog, https://codeberg.org/toz/MediLog/issues/285
- All: Removed time for filter start date to make sure all entries of the start date are included in filter, https://codeberg.org/toz/MediLog/issues/286
- All: Language updates: Swedish by tyhg, Chinese simplified by hamburger1024
- Weight: Issue where "," isn't treated appropriately, https://codeberg.org/toz/MediLog/issues/277
- Water: Selecting the fluid chart would crash the app, https://codeberg.org/toz/MediLog/issues/282
- BloodPressure: Added rudimentary function line break long comments in PDF report, https://codeberg.org/toz/MediLog/issues/287

### Known issues

- None
### New 

- All tabs: Jump to first entry field after record is entered to allow rapid entry of multiple data sets, https://codeberg.org/toz/MediLog/issues/283

### Fixed/Changed

- All: Issue where the changelog splashscreen wasn't updated, https://codeberg.org/toz/MediLog/issues/276
- All: Bug which made the app crash after deleting an entry and immediately selecting a new one, https://codeberg.org/toz/MediLog/issues/279
- All: Translation issue in filter dialog, https://codeberg.org/toz/MediLog/issues/285
- All: Removed time for filter start date to make sure all entries of the start date are included in filter, https://codeberg.org/toz/MediLog/issues/286
- All: Language updates: Swedish by tyhg, Chinese simplified by hamburger1024
- Weight: Issue where "," isn't treated appropriately, https://codeberg.org/toz/MediLog/issues/277
- Water: Selecting the fluid chart would crash the app, https://codeberg.org/toz/MediLog/issues/282
- BloodPressure: Added rudimentary function line break long comments in PDF report, https://codeberg.org/toz/MediLog/issues/287

### Known issues

- None
## v2.5.5, build 5421

### New 

- All tabs: Added rudimentary text search functionality, https://codeberg.org/toz/MediLog/issues/271
- Turkish translation by https://translate.codeberg.org/user/abfreeman/

### Fixed/Changed

- All tabs: Don't show attachment info if there are none: https://codeberg.org/toz/MediLog/issues/274

### Known issues

- None
## v2.5.4, build 5420

### New 

- All tabs: Added possibility to capture a photo / add attachments to a record, https://codeberg.org/toz/MediLog/issues/267

### Fixed/Changed

- WeightInfo: Fixed BMI calculation with imperial values. https://codeberg.org/toz/MediLog/issues/270
- BloodPressureInfo: Stats cut off at the bottom in some cases. https://codeberg.org/toz/MediLog/issues/269
- Some language updates in NO and BR
- Switch to AGPL license

### Known issues

- None
### New 

- All tabs: Added possibility to capture a photo / add attachments to a record, https://codeberg.org/toz/MediLog/issues/267

### Fixed/Changed

- WeightInfo: Fixed BMI calculation with imperial values. https://codeberg.org/toz/MediLog/issues/270
- BloodPressureInfo: Stats cut off at the bottom in some cases. https://codeberg.org/toz/MediLog/issues/269
- Some language updates in NO and BR
- Switch to AGPL license

### Known issues

- None
## v2.5.3, build 5418

### New 

- Nothing

### Fixed/Changed

- Some language updates by @mondstern@mastodon.green
- Weight: Fixed bug in threshold check, https://gitlab.com/toz12/medilog/-/issues/254
- Fixed a bug which prevented error message in settings dialogs from showing

### Known issues

- None
## v2.5.2, build 5417

### New 

- Nothing

### Fixed/Changed

- Weight: Increased max Weight to 999 <unit>
- BloodPressure: Change Zoom behaviour to zoom only x-axis: https://gitlab.com/toz12/medilog/-/issues/250

### Known issues

- None
## v2.5.1, build 5416

### New 

- All: Provide more space for comment text, https://gitlab.com/toz12/medilog/-/issues/245
- Diary: Allow customizing the default text associated with state emojis, https://gitlab.com/toz12/medilog/-/issues/244
- Diary: Allow to configure which emojis are shown to make either bad or good stand out (App and PDF), https://gitlab.com/toz12/medilog/-/issues/246
- Oximetry: Added statistics page to PDF report, https://gitlab.com/toz12/medilog/-/issues/247

### Fixed/Changed

- BloodPressure: Info screen incomplete when set to dark mode, https://gitlab.com/toz12/medilog/-/issues/242
- Chinese language updates by monyxie

### Known issues

- None
## v2.5.0, build 5415

### New 

- All tabs: Added feature to automatically enter text templates in comment fields, https://gitlab.com/toz12/medilog/-/wikis/Text-templates

### Fixed/Changed

- BloodPressure: Pulse not mandatory any longer, required changes which made the app a little more resilient
- About Dialog: Wrong format for email address, https://gitlab.com/toz12/medilog/-/issues/238
- BloodPressure: Large / bold diastolic/systolic values will create an unnecessary line break, https://gitlab.com/toz12/medilog/-/issues/239 & https://gitlab.com/toz12/medilog/-/issues/230
- WhatsNew screen: Fixed legibility issue in dark mode: https://gitlab.com/toz12/medilog/-/issues/240
- Added MediLog build to user feedback data, https://gitlab.com/toz12/medilog/-/issues/241

### Known issues

- None
## v2.4.8, build 5414

### New 

- Nothing

### Fixed/Changed

- Temperature: Use drop down box instead of text field to select temperature unit
- Temperature: Fix max temperature for Farenheit. https://gitlab.com/toz12/medilog/-/issues/234
- Fixed issue where default threshold values getting automatically changed by Android 

### Known issues

- None
## v2.4.7, build 5413

### New 

- Nothing

### Fixed/Changed

- Fixed application crash when entering data with "Quick Data Entry" setting disabled, https://gitlab.com/toz12/medilog/-/issues/232

### Known issues

- None
## v2.4.6, build 5412

### New 

- Nothing

### Fixed/Changed

- Glucose: Fixed issue preventing to enter Ketone in Glucose tab, https://gitlab.com/toz12/medilog/-/issues/231
- Glucose: Fixed some minor ui issues when displaying Ketone (Field length)

### Known issues

- None
### New 

- Nothing

### Fixed/Changed

- Glucose: Fixed issue preventing to enter Ketone in Glucose tab, https://gitlab.com/toz12/medilog/-/issues/231
- Glucose: Fixed some minor ui issues when displaying Ketone (Field length)

### Known issues

- None
## v2.4.5, build 5411

### New 

- Added possibility to block screenshots, https://gitlab.com/toz12/medilog/-/issues/226
- Added showing Changelog whenever a new build gets installed, https://gitlab.com/toz12/medilog/-/issues/130
- Diary: Added more stats to Info screen, https://gitlab.com/toz12/medilog/-/issues/173
- Diary: Added month separators to chart, https://gitlab.com/toz12/medilog/-/issues/218
- Compiled/tested with Android 12 / API 32

### Fixed/Changed

- Created separate privacy settings category
- Glucose: Fix crash in PDF and chart, https://gitlab.com/toz12/medilog/-/issues/227
- Glucose: Fixed text overlap in PDF title when Ketone isn't used
- Improved resilience of number type conversion for thresholds across the app
- Show and allow "," as decimal separator, https://gitlab.com/toz12/medilog/-/issues/221
- Fixed some date format inconsistencies for locale en_DE
- Fixed a crash when trying to backup an empty database
- Fixed: LastBackup string isn't updated when backup is launched from within the ABout screen

### Known issues

- None
## v2.4.4, build 5410
New 

- Added statistics page to PDF report for Temperature and Weight

Fixed/Changed

- NOne

Known issues

- Oximetry: No highlight in PDF report
- A few new strings not yet translated


## v2.4.3, build 5409
New 

- Oximetry: Added support for tracking Oximetry

Fixed/Changed

- Made "highlight values" a tab individual setting

Known issues

- Oximetry: No highlight in PDF report
- A few new strings not yet translated


## v2.4.1, build 5407
New 

- BloodPressure: Allow to change the timezones for morning-midday-evening in PDF report
- BloodPressure: Added posibility to show only trendlines and no actual data values in chart, new setting ShowValues
- Added support for CSV export (Open + Send CSV file)
- Added setting to specify the minutes of inactivity after which reauthentication is required
- Added **Opt-In** user feedback setting

Fixed/Changed

- Fixed crash when creating a pdf reported with empty table
- Fixed data duplication in PDF report when report is opened multiple times: https://gitlab.com/toz12/medilog/-/issues/209
- Fixed dark mode color issues, https://gitlab.com/toz12/medilog/-/issues/207
- Entries sometimes don't get emptied after hitting enter, https://gitlab.com/toz12/medilog/-/issues/192


Known issues

- A few new strings not yet translated


## v2.3.3, build 5349
New 

Fixed/Changed

- Fixed a regression introduced by v2.3.2 which would backup only the active tab instead of all tabs

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.3.2, build 5348
New 

- Added Temperature as a new value to log

Fixed/Changed

- Fixed a bug which led to body fat field always showing in weight edit dialog no matter if on or off
- Fixed some German string inconsistencies in BloodSugar settings
- Fixed a crash when opening blood sugar chart
- Fixed an uncought condition when bogux BloodSugar tresholds values were entered
- Fixed issue where heart rhythm icon was inverted when dark mode was on
- Fixed a bug in BloodPressureInfoFragment.kt and pdf report which led to wrong annual timeframe
- Fixed support for line breaks in comment fields during import/export
- Fixed bug which sometimes prevented comments to show list views

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.3.1, build 5345
New 

- Nothing

Fixed/Changed

- Fix spanish string error preventing F-Droid from compiling
- Some Android libraries upgraded

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.3.0, build 5344
New 

- Totally revamped data import screen
- BloodPressure: Added support for highlighting low blood pressure
- BloodPressure: Added statistics to PDF and info screen
- Additional stats in Info screen and PDF report
- Diary: Switched from warning sign to a more approbriate Emoji for health state 
- Diary: Added experimental chart to reflect health state. Not sure if it will stay.
- Added Chinese Simplified translation
- Some language updates and minor bug fixes here and there


Fixed/Changed

- Warning signs shown as Emoji instead of proper text, https://gitlab.com/toz12/medilog/-/issues/168
- Write Access for Nextcloud lost after App restart, https://gitlab.com/toz12/medilog/-/issues/149

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.2.5, build 5341
New 

- First iteration of progress bar to show progress during data imports

Fixed/Changed

- Changed somewhat misleading import error message with more precise language
- Fixed a regresion introduced in v2.2.4 which prevented setting the row padding for the weight tab
- Substantial update to Finish translation


Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered
- Some older Android versions can't show the warning sign ISO character and will display a strange Ü instead

## v2.2.4, build 5340
New 

- New general RowPadding setting change the height of the data rows (0 no padding, 10 = default)

Fixed/Changed

- Language "bug" in Water, header showed "Weight" instead of "Quantity"
- Fixed inconsistency in German translation for pdf report, it's now "Bericht" everywhere
- Added separation lines in Diary tab for better visibility


Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.2.3, build 5339
New 

- Nothing

Fixed/Changed

- Fixed bug which prevented the Water chart from showing data

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.2.2, build 5338
New 

- Nothing

Fixed/Changed

- Fixed bug which prevented the weight comment field to be hidden when QuickEntry is off  
- Fixed bug which prevented tabs from showing when migrating from 2.1 to 2.2.
- Fixed the colour of the overflow menue which turned to black with dark mode support, now white again
- Fixed a bug which showed the wrong fluid unit when quick edit is turned off

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.2.1, build 5337
New 

- Make backup file name configurable

Fixed/Changed

- Fixed wrong default setting for dark mode
- Fixed bug which prevented tabs from showing when migrating from 2.1 to 2.2.

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

## v2.2.0, build 5336
New 

- Allow to select paper size (A4 & Letter)
- Allow to select Landscape/Portrait paper orientation for PDF reports
- Added MediLog Logo to PDF reports
- Added support for Dark Mode 
- Added ability to log Body Fat to Weight tab
- Added Weight/BloodSugar average to info screens
- Added possibility to blend tab comments into Diary, https://gitlab.com/toz12/medilog/-/issues/150

Fixed/Changed

- Improved blood pressure pdf report
- Use warning sign for heart rhythm issues throughout the app
- Fixed Weight Chart issues https://gitlab.com/toz12/medilog/-/issues/141
- Fixed a few tab related issues: https://gitlab.com/toz12/medilog/-/issues/142, https://gitlab.com/toz12/medilog/-/issues/122


Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered


## v2.1.2, build 5335
New 

Fixed/Changed

- Fixed bug which led to customTab code being executed. Present an error message instead

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered


## v2.1.1, build 5334
New 

Fixed/Changed

- Fixed bug which prevented entering decimals in Weight tab

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered


## v2.1.0, build 5333
New 

- New AutoBackup capability
- Added support for Cloud Backups / StorageProviders (Tested with Nextcloud, others should work too)
- Allow to start backup from About Dialog
- Faster data input. Focus automatically changes to next field if required number of chars is entered
- Redesigned report menu (Removed sending CSV, added opening PDF)

Fixed/Changed

- Changed BackupCheck from once per launch to once per day
- Fixed a glitch in data import which sometimes led to the last value to be ignored
- CSV file output is now always MediLog.csv
- Fixed uri persistence for autoBackup
- Removed Delete Data Menu option
- Refactored import/export routines
- Dropped support for v1.x data imports/restore (Workaround is importing + exporting with v2.0.x, followed by an upgrade to 2.1 and final import)
- Fixed send file (csv, pdf, zip) options. Now working as expected and not via file save.
- Fixed small quirk while entering systolic values where the focus would jump to diastolic if a 2 digit value is entered
- Increased resilience to bogus data when creating the dairy pdf report
- #134, Use Enter to jump between fields / save new record

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered


## v2.0.6, build 5332
New 

-  Added time to lastBackup in About and BackupSettings

Fixed

- Fix bug which prevented Systolic value to show in PDF report when HighlightValues was set to off
- Changed PDF report so it respects the log-heart-rhythm-issues setting.

Known issues

- None

## v2.0.5, build 5331
New 

-  Nothing

Fixed

- Replaced images in info dialogs because they were creating issues with low resolution devices
- Cleaned up diary info dialog
- Set backup check off by default

Known issues

- None


## v2.0.4, build 5330
New 

-  Added: Make HeartRythm logging configurable

Fixed

- Remove leftover BloodPressure comment button if quick edit is off

Known issues

- None


## v2.0.2, build 5328
New 

- Added signing certificate info to About screen
- Capture and show date of last backup in About dialog
- New translation: Norwegian Bokmål
- Split out backup preferences into separate screen
- Added backup overdue check
- Added autoBackup setting (disabled until 2.1.0)
- Automatically close keyboard after an item is added in quick entry mode

Fixed

- Fixed weight data showing too many digits after migrating to v2.0
- Fixed crash during entries/changes of the diary if quick entry is disabled.
- Changed charts so all available space is used
- Fixed bug where the fluid chart/pdf would show individual values instead of a days summary
- Changed highlighting values in pdf from > to >=
- v2 weight chart: Zoom only timeline (x-axis), not the values (y-axis)

Known issues

- None

## v2.0.1, build 5327
New 

- No code changes

Fixed

- Crash of F-Droid signed app during launch. Changed AndroidStudio minifiedEnabled compiler flag to false.

Known issues

- None

## v2.0, build 5326
New 

- Support for older devices, Lollipop (Android 5) and upwards
- Added support for Blood Sugar logging (experimental)
- Added Pan&Zoom for charts
- New logo
- New Info section for each logged value
- Allow to reorder tabs
- Moved BloodPressure Comment field to separate input Dialog to save space
- Better looking charts
- Significant internal code changes to support future addition of additional values to be logged

Fixed

- None

Known issues

- None


## v1.9.5, build 5325
New 

- Added Polish and Swedish language support

Fixed

- None

Known issues

- None


## v1.9.4, build 5323
New 

- Added Basque language support

Fixed

- None

Known issues

- None

## v1.9.3, build 5322
New 

- Added Finnish language support

Fixed

- Fixed bug which distorted Weight and BloodPressure charts with many values

Known issues

- None


## v1.9.2, build 5321
New 

- Changed line width of grade thresholds in Blood Pressure chart to be more subtle
- Added tab icons and settings to select text/icon/text+icon to support devices with little horizontal real estate
- Changed to hierarchical settings structure
- Show Grid, show legend and show thresholds are now individual settings for each tab instead of application global
- Added setting to configure horizontal tab scrolling
- Added setting to switch between summary (daily) and detail view for Water Intake pdf report

Fixed

- Fixed bug where during editing time was off by 12 hours
- Fixed bug where unfiltered calls to the database weren't working
- Changed stepping size in Water Chart from 100 to 500 to make things look less busy 
- Fixed bug which prevented threshold to show in bar charts
- Changed Legend order in BloodPressure chart, pulse is now last
- Fixed visual glitch in chart legend icon, removed black background
- Check text size in Water chart to prevent y-axis label being cut off
- Fixed bug where chart button color doesn't match the theme after fresh install

Known issues

- None


## v1.9.1.0, build 5320
New 

- Translations (thank you https://mastodon.technology/@mondstern)
    + Hungarian
    + Russian
    +Portuguese (Brazil)

Fixed

- Changed threshold lines in blood pressure view to same colour dashed lines for better visibility

Known issues

- Water Intake report not (yet) showing summary


## v1.9.0.3, build 5319
New 

- Translations (thank you @mondstern@mastodon.technology)
    + Started with spanish translation
    + Started with danish translation
    + Dutch complete
    + Italian complete
    + German complete
    + French complete

Fixed

- Added state highlighting to Diary PDF report. "Not good" = Italics, Bad = bold italics
- Fixed a crash when trying to filter an empty table
- Fixed an issue where the filter icon is not reset after a data import

Known issues

- Water Intake report not (yet) showing summary

## v1.9.0.2, build 5318

New 

- Nothing

Fixed

- Fixed a nasty bug in fluid tab which would crash the app if no data was compiled for today

Known issues
- Water Intake report not (yet) showing summary
- Diary PDF report not showing state

## v1.9.0.1, build 5317

New 

- Initial support for Dutch language (thank you @mondstern@mastodon.technology)
- Initial support for Italian language (thank you @mondstern@mastodon.technology)
- Finishing off most of the missing fluid features
- Added Today row to Water intake screen

Fixed

- Nothing

Known issues

- Water Intake settings not (yet) exposed to user
- Water Intake report not (yet) showing summary


## v1.9.0.0 Beta, build 5316

New 

- Added Heart Rhythm logging
- Added Water Intake logging
- Added basic Water Intake report

Fixed

- Fixed bug where Weight edit wouldn't load the comment
- Setting/Changing the filter is now reflected in the data tabs right away.

Known issues

- Water Intake settings not (yet) exposed to user
- Water Intake report not (yet) showing summary
- Water Intake main screen not (yet) showing summary of daily intake


## v1.8.1.3, build 5315

New 

- Nothing

Fixed

- Fixed bug where Weight edit wouldn't load the comment
- Setting/Changing the filter is now reflected in the data tabs right away.

Known issues

- None


## v1.8.1.2, build 5314
New 
- Nothing

Fixed
- Fixed a bug where "send CSV" would lead to a "PDF not found" error message


## v1.8.1.1, build 5313
New 

- Nothing

Fixed

- De-branded screenshots
- Introduced 4th digit in version number to properly reflect bug fix releases for F-Droid

## v1.8.1, build 5311
New 
- Added French tr
anslation, thanks to https://gitlab.com/djelouze
- Added | as acceptable CSV field delimiter

## v1.8.0, build 5310
New

- Added state (good/notGood/bad) / (no colour/amber/red) to Diary tab
- Cleaned up filter dialog
- Accept all valid CSV delimiters during import/restore
- Added restore from ZIP backup file
- Enabled pin authentication as fallback for biometric logon for API levels > 29 (Android 11)

Fixed

- Changed colour of Floating action buttons from black to white to increase contrast
- "Send raw data" crashed when data set was too large.
- Significant re-architecture of the filter section
- Should have fixed a bug where new data added wasn't showing without a ViewModel reload
- Fixed bug which prevent setting , as CSV delimiter
- More secure handling of ZIP password (Hide password, ask for pwd if none i set, etc.)

## v1.7.6, build 5309
New

- Added | as acceptable CSV field delimiter
- Added check for empty CSV field delimiter in settings dialog
- Added pin authentication as fallback for biometric logon (disabled for now until bug https://issuetracker.google.com/issues/142740104 is fixed)
- More delimiter checking and better feedback in case of errors during import

Fixed
- Fixed some documentation inconsistencies in regards to version numbers, build number, etc.
- Disable biometric logon instead of hiding if biometric is not available/set up

## v1.7.5, build 5308
New

- Added Database encryption 
- Added feature to activate first edit field whenever an edit screen is opened, saves one click for each entry :-)
- Added "Show Pulse" setting to further clean up Blood Pressure chart
- Added database size to About screen
- Added number of items exported to backup success messages
- Added security section to About dialog

Fixed

- Brought back changelog.md, will drop adding notes to version tags from now on
- Fixed race condition during import which may have led to partial imports with large imports (> 1000 rows)
- Fixed calculating max and min values to properly place the charts in the middle of the screen
- Removed "item saved" messages after an item got added, looked like an error and didn't serve a purpose because the new value is visible at the top of the list anyway.
- Enlarged the blood pressure quickedit fields somewhat to make the hint message fit in german language
- Made sure chart origins starts on a 10er number instead of uneven numbers

## v1.7.4
New

* Added quick data entry mode, on: same behaviour as before, off: open data entry form which also allows to change date and time upon entry.
* Added legends to the charts
* Added switch to turn chart legend on/off

* Fixed an issue where the app would hang during import if a filter was active
* Fixed a little glitch which mixed up date and time in dataEdit Dialogs
* Changed chart grid colour to gray to make it more subtle
* Align chart y-axis to multiples of 10
* Set chart stepping to 5 (Weight) and 10 (blood pressure)
* Fixed visual glitch in Weight Edit dialog

## 1.7.3
Fixed

- Removed the type selector for DeleteData menu. Not needed any longer 

## 1.7.2
Fixed

- Some bugfixes and code simplifcations 

## 1.7.1
Fixed

- Fixed crash in Diary module

## 1.7.0
New/improved

- Added date filter capability
- Improved usability when handling authentication errors
- Added disclaimer dialog for first launch
- Moved data import to separate thread to cater for large imports
- Large scale testing. 10k records per category = 27 years worth of one entry per day

Fixes

- Weight value was not showing when highlighting values was off
- Fixed a racing condition where clearing the db before a data import would lead to unpredictable results

## 1.6.0
App Internal - Significant refactoring 

- Switched to Google recommended architecture
- Utilizing Room, Data Migrations, Repositories, ViewModels and LiveData
- Migrated from Java to Kotlin
- Switched from fingerprint to Biometric libraries 
- Upgraded to AndroidX

New/improved

- Protect app with biometric (fingerprint) logon
- Cleaner PDF reports, no more hard to see colours but bold and underline for elevated values
- New default color theme blue, added ability to switch colour themes
- Added MovingAverage Trendlines
- Added proper edit dialogs / fragments

Fixes

- Fixed bug when highlighting elevated values, #2
- Fixed bug where deleted values remained in chart, #49
- Fixed a bug in BloodPressure PDF report. Diastolic value didn't show

## 1.5.0
Export your data before upgrading!!!
New/improved

- Major overhaul of applications inner working. Code clean up, more code reuse through class inheritance, more robust database interface
- Allow to save the Backup wherever the user has write access
- Backup as ZIP or individual files
- Added posibility to change colour theme (Green, Blue, Red)
- Added proper data edit dialogs

Fixes

- Fixed a bug in blood pressure pdf, diastolic value didn't show

## 1.3.2
New/improved
- Moved away from export/import to Backup/Restore. Backup all 3 data areas into a ZIP File. Restore can read individual files from ZIP or file system
- Allow to save the Backup wherever the user has write access

Fixes
- More error handling during file IO

## 1.3.1
New/improved
- Added ability to add name (any text really) to PDF reports via new "User name" setting
- Color code / highlight individual escalated values in Blood Pressure view, instead of the whole row
- Blood pressure PDF report now sectioned into morning /afternoon / evening to allow for quicker assessment of the situation
- Added capability to change the formating of the date/time columns via new "Date/Time" format setting. See here for syntax: https://developer.android.com/reference/java/text/SimpleDateFormat

Fixes
- More error handling during file IO
- Made sure read/write access is requested before writing to default Download folder

## Version 1.2.3
- Fixed PDFs won't open after file was created.
- Fixed crash when saving on SD-Card
- Fixed crash when creating PDF files
- Fixed crash when importing a non existing file
- Removed code signing code
- Check for empty tables before launching charts and exporting data
- Updated manifest to be able to install on SD-Card

## Version 1.2.2
- Added possibility to specify import/export directory

## Version 1.2.1
- Can't remember what I added :-)

## Version 1.2.0
- Added human readable reports
- Realigned the menu structure
- New Send menu for creating/sending reports and raw (CSV) data
- New Data Management menu for data import/export

## Version 1.0.5
- Added charts

## Version 1.0.0
- Added colour thresholds
