<?xml version="1.0" encoding="utf-8"?>
<!--
  ~     This file is part of MediLog.
  ~
  ~     MediLog is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU Affero General Public License as published by
  ~     the Free Software Foundation.
  ~
  ~     MediLog is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU Affero General Public License for more details.
  ~
  ~     You should have received a copy of the GNU Affero General Public License
  ~     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~     Copyright (c) 2018 - 2024 by Zell-MBC.com
  -->

<!--
  -->

<resources xmlns:tools="http://schemas.android.com/tools">
    <string name="zellMBC" translatable="false">Zell-MBC.com</string>
    <string name="email" translatable="false">medilog@zell-mbc.com</string>
    <string name="appName" translatable="false">MediLog</string>
    <string name="bpEntryHint" translatable="false">000</string>
    <string name="bodyFatEntryHint" translatable="false">00.0</string>
    <string name="ketoneEntryHint" translatable="false">0.0</string>
    <string name="csvSeparators" translatable="false">,;|</string>
    <string name="warningSign" translatable="false">\u26A0︎</string>
    <string name="goodSmileyEmojy" translatable="false">🙂︎</string>
    <string name="badSmileyEmojy" translatable="false">🙁</string>
    <string name="notGoodSmileyEmojy" translatable="false">😐︎</string>
    <string name="userFeedbackUrl" translatable="false"> https://medilog.zell-mbc.com/</string>
    <string name="glucoseHint" translatable="false">0.0</string>
    <string name="glucoseHintUS" translatable="false">000</string>
    <string name="oximetryHint" translatable="false">00</string>
    <string name="onSwitch">On</string>
    <string name="offSwitch">Off</string>
    <string name="bmiChart">https://en.wikipedia.org/wiki/Body_mass_index</string>
    <string name="good">Feeling good</string>
    <string name="notGood">Not feeling good</string>
    <string name="bad">Feeling bad</string>
    <string name="stateGood">Good</string>
    <string name="stateNotGood">Not good</string>
    <string name="stateNoValue">No value</string>
    <string name="stateBad">Bad</string>
    <string name="state">State</string>
    <string name="appDescription">MediLog - Health Data Logger</string>
    <string name="word_item">item</string>
    <string name="word_deleted">deleted!</string>
    <string name="action_settings">Settings</string>
    <string name="action_profiles">Profiles</string>
    <string name="actionPDF">PDF Report</string>
    <string name="actionOpenPDF">Open PDF</string>
    <string name="actionSendPDF">Send</string>
    <string name="actionSendZIP">Send ZIP</string>
    <string name="actionOpenCSV">Open CSV</string>
    <string name="actionSendCSV">Send CSV</string>
    <string name="action_about">About</string>
    <string name="statistics">Statistics</string>
    <string name="action_restore">Restore</string>
    <string name="action_backup">Backup</string>
    <string name="action_import">Import</string>
    <string name="action_dataManagement">Data Management</string>
    <string name="action_filter">Filter</string>
    <string name="fluid">Fluid</string>
    <string name="fluidHint" translatable="false">0000</string>
    <string name="fluidMissing">Fluid value missing or invalid!</string>
    <string name="today">Today</string>
    <string name="bloodPressure">Blood Pressure</string>
    <string name="weight">Weight</string>
    <string name="diary">Diary</string>
    <string name="systolic">Systolic </string>
    <string name="diastolic">Diastolic </string>
    <string name="pulse">Pulse</string>
    <string name="sysShort" translatable="false">Sys</string>
    <string name="diaShort" translatable="false">Dia</string>
    <string name="comment">Comment</string>
    <string name="description">Description</string>
    <string name="valuesMissing">Values missing or invalid!</string>
    <string name="weightMissing">Weight value missing or invalid!</string>
    <string name="diaMissing">Diastolic value missing or invalid!</string>
    <string name="pulseMissing">Pulse value missing or invalid!</string>
    <string name="sysMissing">Systolic value missing or invalid!</string>
    <string name="diaryMissing">Diary entry missing or invalid!</string>
    <string name="date">Date</string>
    <string name="time">Time</string>
    <string name="yes">Yes</string>
    <string name="no">No</string>
    <string name="save">Save</string>
    <string name="ok">OK</string>
    <string name="agree">Agree</string>
    <string name="disclaimer">Disclaimer</string>
    <string name="add">Add</string>
    <string name="submit">Submit</string>
    <string name="cancel">Cancel</string>
    <string name="delete">Delete</string>
    <string name="edit">Edit</string>
    <string name="unit">Unit</string>
    <string name="weightUnitSummary">Weight unit</string>
    <string name="fluidUnitSummary">Fluid unit</string>
    <string name="delimiter">Delimiter</string>
    <string name="general">General</string>
    <string name="preferencesDelimiterSummary">Character used to separate fields for data import/export (CSV).\nAllowed characters:  , ; |</string>
    <string name="barChart">Bar Chart</string>
    <string name="barChartSummary">Switch from line chart to bar chart</string>
    <string name="Copyright1" translatable="false">Copyright 2019–%1$s, zell-mbc.com.  Licensed under AGPL:</string>
    <string name="Copyright2">This application utilizes 3rd party modules:</string>
    <string name="listTextSize">Text size</string>
    <string name="listTextSizeSummary">Text size in data lists</string>
    <string name="pdfTextSizeSummary">Text size in data reports</string>
    <string name="AppURL" translatable="false">https://codeberg.org/toz/MediLog</string>
    <string name="UserFeedbackUrl" translatable="false" tools:ignore="TypographyDashes">https://codeberg.org/toz/MediLog/wiki/User-Feedback</string>
    <string name="grade">Grade</string>
    <string name="grade1">Hypertension grade 1</string>
    <string name="grade2">Hypertension grade 2</string>
    <string name="grade3">Hypertension grade 3</string>
    <string name="hypotension">Hypotension</string>
    <string name="preferencesGrade3Summary">Thresholds for hypertension grade 3\nFormat: Systolic threshold value / Diastolic threshold value</string>
    <string name="preferencesGrade2Summary">Thresholds for hypertension grade 2\nFormat: Systolic threshold value / Diastolic threshold value</string>
    <string name="preferencesGrade1Summary">Thresholds for hypertension grade 1\nFormat: Systolic threshold value / Diastolic threshold value</string>
    <string name="hypotensionSummary">Thresholds for low blood pressure\nFormat: Systolic threshold value | Diastolic threshold value</string>
    <string name="grade3Error">Error in BloodPressure grade 3 threshold value:</string>
    <string name="grade2Error">Error in BloodPressure grade 2 threshold value:</string>
    <string name="grade1Error">Error in BloodPressure grade 1 threshold value:</string>
    <string name="invalidGrade">Your entry needs to contain two numeric values separated by a \/\nE.g. 140/80</string>
    <string name="hypotensionError">Error in BloodPressure hypotension threshold value:</string>
    <string name="threshold">Threshold</string>
    <string name="thresholds">Thresholds</string>
    <string name="eReadError">Error: Cannot read file content in line</string>
    <string name="eCreateFile">Error: Cannot create file</string>
    <string name="eOpenZipFile">Error: Cannot open ZIP file!\n</string>
    <string name="eReadZipFile">Error reading ZIP file!\n</string>
    <string name="eShareError">Error: Cannot send file</string>
    <string name="showLinearTrendline">Linear Trend line</string>
    <string name="trendline">Trend line</string>
    <string name="quickEntry">Quick data entry</string>
    <string name="quickEntrySummary">Enter data directly on main tab</string>
    <string name="showLinearTrendlineSummary">Show linear trend line</string>
    <string name="movingAverageTrendLineSize">Moving Average size</string>
    <string name="movingAverageTrendlineSizeSummary">Number of records used to calculate the MA. The higher the value the flatter the trendline. Minimum value is 2</string>
    <string name="reportTitle">MediLog - %1$s Report for %2$s</string>
    <string name="notEnoughDataForChart">Need at least 2 datapoints to draw a chart!</string>
    <string name="noDataToShow">Table is empty, no data to show!</string>
    <string name="noDataToExport">No data to export!</string>
    <string name="noActiveTab">No Tab selected? Please enable at least one tab in settings.</string>
    <string name="morning">Morning</string>
    <string name="afternoon">Afternoon</string>
    <string name="evening">Evening</string>
    <string name="selectDirectory">Select backup directory</string>
    <string name="emptyDatabase">Database is empty</string>
    <string name="selectFile">Select file</string>
    <string name="eSelectDirectory">Unable to set folder. Error: "</string>
    <string name="protectedZipFileCreated">Successfully created protected</string>
    <string name="unprotectedZipFileCreated">Successfully created unprotected</string>
    <string name="protected_">protected</string>
    <string name="unprotected">unprotected</string>
    <string name="attention">Attention!</string>
    <string name="warning">Warning!</string>
    <string name="thisIsGoing">This is going to delete all(!) existing data! %1$d rows.</string>
    <string name="deleteProfiles">This is going to delete %1$d profiles and %2$d associated data records.</string>
    <string name="doYouReallyWantToContinue">Do you really want to continue?</string>
    <string name="eNoMediLogFile">Error: Unable to import. Is this a MediLog file? \nFound:</string>
    <string name="data">data</string>
    <string name="showPulse">Show Pulse</string>
    <string name="showPulseSummary">Show Pulse values in Blood Pressure chart</string>
    <string name="invalid">Invalid</string>
    <string name="value">value</string>
    <string name="values">Values</string>
    <string name="fluidEditTitle">Edit Fluid Item</string>
    <string name="weightEditTitle">Edit Weight Item</string>
    <string name="diaryEditTitle">Edit Diary Item</string>
    <string name="bloodPressureEditTitle">Edit Blood Pressure Item</string>
    <string name="oximetryEditTitle">Edit Oximetry Item</string>
    <string name="glucoseEditTitle">Edit Glucose Item</string>
    <string name="weightInfoTitle">Weight Info</string>
    <string name="bloodPressureInfoTitle">Blood Pressure Info</string>
    <string name="oximetryInfoTitle">Oximetry Info</string>
    <string name="glucoseInfoTitle">Glucose Info</string>
    <string name="fluidInfoTitle">Fluid Intake Info</string>
    <string name="diaryInfoTitle">Diary Info</string>
    <string name="zipPassword">Password</string>
    <string name="zipPasswordSummary">Set default password for backups</string>
    <string name="enterPassword">Enter password for ZIP file</string>
    <string name="password">Password</string>
    <string name="sendText">Created by MediLog</string>
    <string name="userNameDefault">Your name</string>
    <string name="chart">Chart</string>
    <string name="showLegend">Show Legend</string>
    <string name="showLegendSummary">Show legend in chart</string>
    <string name="showGrid">Show Grid</string>
    <string name="showGridSummary">Show grid in chart</string>
    <string name="showThresholds">Show Thresholds</string>
    <string name="showThresholdsSummary">Show threshold lines in chart</string>
    <string name="invalidInput">Invalid Input</string>
    <string name="emptySeparator">Delimiter value can not be empty!</string>
    <string name="setFilter">Set filter:</string>
    <string name="startDate">Start Date</string>
    <string name="endDate">End Date</string>
    <string name="filteredOut">Data saved successfully. But new value will not show because of active filter!</string>
    <string name="disclaimerText">This application is not a substitute for medical advice. Users of the application should consult their healthcare professional before making any health, medical or other decisions based upon the data contained herein.
For license and waranty terms consult the licensing document.</string>
    <string name="retryAction">RETRY</string>
    <string name="retryActionText">Try again!</string>
    <string name="allowedDelimiters">Allowed delimiters:</string>
    <string name="allowedInput">Allowed input:</string>
    <string name="weightHint" translatable="false">000.0</string>
    <string name="errorInCSVHeader">Error in CSV header!\nExpected</string>
    <string name="noCsvDelimiterFound">Error: Unable to identify CSV delimiter in line 1 of import file!\nValid delimiters are"</string>
    <string name="entries">entries</string>
    <string name="database">Database</string>
    <string name="size">Size</string>
    <string name="count">Count</string>
    <string name="security">Security</string>
    <string name="databaseEncryption">Database encryption</string>
    <string name="settingsEncryption">Settings encrypted</string>
    <string name="attachmentEncryption">Attachments encrypted</string>
    <string name="biometricProtection">Biometric protection</string>
    <string name="biometricProtectionOff">Biometric Protection Off</string>
    <string name="protectedBackup">Password protected backups</string>
    <string name="mediLogLicense" translatable="false">https://codeberg.org/toz/MediLog/src/branch/master/LICENSE</string>
    <string name="thirdparty" translatable="false">https://codeberg.org/toz/MediLog#libraries</string>
    <string name="biometricLogin">Biometric login</string>
    <string name="biometricLoginDescription">Protect access to the application with biometric authentication.</string>
    <string name="noBiometricHardware">No biometric features available on this device.</string>
    <string name="biometricHardwareUnavailable">Biometric features are currently unavailable.</string>
    <string name="noCredentials">Biometric protection is on, but you have not associated any biometric credentials with your account.</string>
    <string name="emptyTable">Table is empty.</string>
    <string name="showTabIcon">Show Tab Icons</string>
    <string name="preferencesShowTabIconSummary">Show Icons in tab headers</string>
    <string name="showTabText">Show Tab Text</string>
    <string name="preferencesShowTabTextSummary">Show Text in tab header section</string>
    <string name="scrollableTabsText">Scrollable Tabs</string>
    <string name="preferencesScrollableTabsSummary">Allow tabs to scroll horizontally</string>
    <string name="tabs">Tabs</string>
    <string name="tabTransition">Tab Transitions</string>
    <string name="tabTransitionSummary">Animation used to switch between tabs</string>
    <string name="glucose">Glucose</string>
    <string name="activeTabs">Active Tabs</string>
    <string name="activeTabsSummary">Select the values to be logged</string>
    <string name="glucoseUnitSummary">Select mg/dL or mmol/L</string>
    <string name="bloodPressureUnitSummary">Blood Pressure unit, default is mmHg</string>
    <string name="bmiActual">Body Mass Index (BMI):</string>
    <string name="bmiNormal">Normal BMI (18.5 – 24.9): </string>
    <string name="bmiDifference">Difference: </string>
    <string name="height">Body Height</string>
    <string name="notAvailableShort">N/A</string>
    <string name="heightSummary">Enter body height in cm/in to allow MediLog to calculate the Body Mass Index (BMI)</string>
    <string name="measurementsInDB">Measurements in Database:</string>
    <string name="measurementsInFilter">Measurements in Filter:</string>
    <string name="minMaxValues">Minimal/Maximal:</string>
    <string name="timePeriod">Time period:</string>
    <string name="invalidBodyHeight">Invalid Body Height value. Check your settings!</string>
    <string name="recommendedDailyWaterIntake">Recommended Daily Water Intake:</string>
    <string name="fluidFormula">Formula = (weight in kg * 30 / 1000)</string>
    <string name="invalidTextSize">Invalid value for Text Size.</string>
    <string name="invalidTreshold">Invalid value for Threshold.</string>
    <string name="choseValue">Choose a value between %1$s and %2$s</string>
    <string name="invalidRowHeight">Invalid value for Row Height. Choose a value &gt;= %1$s</string>
    <string name="enterComment">Enter Comment: </string>
    <string name="found">Found</string>
    <string name="temperatureHint" translatable="false">00.0</string>
    <string name="temperature">Temperature</string>
    <string name="temperatureInfoTitle">Temperature Info</string>
    <string name="temperatureEditTitle">Edit Temperature Item</string>
    <string name="tab_temperature">Temperature</string>
    <string name="temperatureMissing">Temperature value missing or invalid!</string>
    <string name="temperatureUnitSummary">Temperature unit</string>
    <string name="lastBackup">Last Backup: </string>
    <string name="unknown">Unknown</string>
    <string name="autoBackup">Automatic Backup</string>
    <string name="autoBackupSummary">Backup automatically if the last backup is older than the specified here (Days). Leave field empty to turn off</string>
    <string name="backupWarning">Backup Warning</string>
    <string name="backupWarningSummary">Warn if the last backup is older than the specified here (Days). Leave field empty to turn off</string>
    <string name="backupOverdue">Backup is overdue!</string>
    <string name="backupOverdueMsg1">%1$d records which were never saved!</string>
    <string name="backupOverdueMsg2">"%1$d records, last backup is %2$d days old!"</string>
    <string name="backupLocation">Backup Location:</string>
    <string name="backupStarted">Backup started…</string>
    <string name="logHeartRhythm">Log Heart Rhythm</string>
    <string name="logHeartRhythmSummary">Log heart rhythm state offered by some measurement devices</string>
    <string name="exception" translatable="false">Exception:</string>
    <string name="missingBackupLocation">Default backup location is missing!</string>
    <string name="invalidBackupLocation">Unable to use auto backup location! Run a manual backup again.</string>
    <string name="eUnableToWriteToFolder">Unable to write to folder</string>
    <string name="eLostAccessRights">No access rights to backup folder! Update backup location in backup settings</string>
    <string name="landscapeOrientation">Landscape</string>
    <string name="landscapeOrientationSummary">Landscape page orientation</string>
    <string name="paperSize">Paper Size</string>
    <string name="paperSizeSummary">Select paper size</string>
    <string name="themeLight">Light</string>
    <string name="themeDark">Dark</string>
    <string name="themeSystem">Follow System</string>
    <string name="themeAutoBattery">Auto Battery</string>
    <string name="appTheme">Theme</string>
    <string name="themeOptionsSummary">Set Theme options</string>
    <string name="bodyFat">Body Fat</string>
    <string name="logBodyFat">Body Fat</string>
    <string name="logBodyFatSummary">Switch logging Body Fat feature on/off</string>
    <string name="bodyFatMinMax">Body Fat Min-Max values</string>
    <string name="bodyFatMinMaxSummary">Define the Body Fat warning levels (min-max)</string>
    <string name="includeOtherTabs">Include other Tabs</string>
    <string name="includeOtherTabsSummary">Blend in comments of other tabs</string>
    <string name="blendedItem">"This item is blended in, you can't edit here!"</string>
    <string name="average">Average</string>
    <string name="avg">avg.</string>
    <string name="min">min</string>
    <string name="max">max</string>
    <string name="addTimestamp">Add timestamp</string>
    <string name="addTimestampSummary">Add time to backup file name</string>
    <string name="quantity">Quantity</string>
    <string name="rowPadding">Row Padding</string>
    <string name="rowPaddingSummary">Space between rows in data list</string>
    <string name="importCanceled">Import canceled</string>
    <string name="readLines">Read lines</string>
    <string name="linesRead">lines read</string>
    <string name="openZipFile">Open ZIP file</string>
    <string name="openDataFile">Open data file</string>
    <string name="openProfilesFile">Open Profiles file</string>
    <string name="openTextTemplatesFile">Open Text Templates file</string>
    <string name="openTagsFile">Open Tags file</string>
    <string name="openSettingsFile">Open Settings file</string>
    <string name="checkFileHeader">Check file header</string>
    <string name="loadRawData">Load raw data</string>
    <string name="deleteExistingData">Delete existing data</string>
    <string name="dataDeleted">Existing data deleted</string>
    <string name="deleteItem">Delete item</string>
    <string name="addRecords">Add records to database</string>
    <string name="extractCSV">Extract CSV file</string>
    <string name="checkCsvHeader">Check CSV file header</string>
    <string name="importAttachments">Import file attachments</string>
    <string name="createdNewDefaultProfile">Created default profile</string>
    <string name="done">Done</string>
    <string name="timeframeLabel">Timeframe (Days)</string>
    <string name="totalLabel">Total</string>
    <string name="annualLabel">1 Year</string>
    <string name="monthLabel">1 Month</string>
    <string name="measurementLabel">Measurements (#)</string>
    <string name="invalidLowerThreshold">Invalid lower threshold.</string>
    <string name="invalidUpperThreshold">Invalid upper threshold.</string>
    <string name="invalidThresholds">Invalid thresholds</string>
    <string name="tare">Tare</string>
    <string name="tareSummary">Automatically subtract this value after a new weight value was added</string>
    <string name="tareApplied">Tare applied</string>
    <string name="days">Days</string>
    <string name="weeks">Weeks</string>
    <string name="months">Months</string>
    <string name="years">Years</string>
    <string name="timeframe">Timeframe</string>
    <string name="filter_rolling">Rolling</string>
    <string name="filter_off">Off</string>
    <string name="filter_static">Static</string>
    <string name="featureNotAvailable">This feature requires Android 8 or newer!</string>
    <string name="unknownError">Unknown error!</string>
    <string name="never">Never</string>
    <string name="nothing">Nothing</string>
    <string name="invalidTimezoneSetting">is an invalid timezone value. Reverting to default 0–12–18</string>
    <string name="timezones">Timezones</string>
    <string name="timezonesSummary">Hours defining Morning-Midday-Evening columns</string>
    <string name="showMovingAverage">Moving Average</string>
    <string name="showMovingAverageSummary">Show the Moving Average instead of values</string>
    <string name="authenticationTimeout">Authentication Timeout</string>
    <string name="authenticationTimeoutSummary">Minutes of inactivity after which reauthentication is required</string>
    <string name="userFeedback">User Feedback</string>
    <string name="whyUserFeedback">Why user feedback?</string>
    <string name="feedbackEnabled">Daily Feedback :</string>
    <string name="lastFeedback">Last feedback: </string>
    <string name="dataSent">Data sent: </string>
    <string name="serverResponse">Server response: </string>
    <string name="otherFeedbackMeans">Other means to provide feedback:</string>
    <string name="instantFeedback">Instant feedback</string>
    <string name="ketone">Ketone</string>
    <string name="logKetone">Ketone</string>
    <string name="logKetoneSummary">Switch logging Ketone on/off</string>
    <string name="glucoseKetoneIndex">GKI</string>
    <string name="oximetry">Oximetry</string>
    <string name="pulseUnit">bpm</string>
    <string name="highlightValues">Highlight values</string>
    <string name="highlightValuesSummaryLow">Highlight values lower than threshold</string>
    <string name="highlightValuesSummaryHigh">Highlight values higher than threshold</string>
    <string name="highlightValuesSummaryRange">Highlight values lower or higher than threshold</string>
    <string name="thresholdSummaryRange">Define minimum and maximum threshold</string>
    <string name="thresholdSummaryLower">Define lower threshold</string>
    <string name="thresholdSummaryUpper">Define upper threshold</string>
    <string name="preferencesFluidThresholdSummary">Targeted daily fluid intake</string>
    <string name="days7">7 days</string>
    <string name="days14">14 days</string>
    <string name="days21">21 days</string>
    <string name="days31">31 days</string>
    <string name="privacy">Privacy</string>
    <string name="blockScreenshots">Block Screenshots</string>
    <string name="blockScreenshotsDescription">Prevent or allow taking screenshots</string>
    <string name="emptyTemplate">Template %1$s is empty!</string>
    <string name="invalidTemplateIndex">Template %1$s does not exist!</string>
    <string name="textTemplate">Text template</string>
    <string name="textTemplates">Text templates</string>
    <string name="textTemplatesDialogDescription">Pick a text:</string>
    <string name="noTextTemplate">Template is empty</string>
    <string name="tag">Tag</string>
    <string name="tags">Tags</string>
    <string name="settings">Settings</string>
    <string name="pickTag">Pick a tag:</string>
    <string name="addTag">Add a tag:</string>
    <string name="color">Color</string>
    <string name="emptyTag">Tag is empty!</string>
    <string name="showTime">Show time</string>
    <string name="showTimeSummary">Show time in date column</string>
    <string name="showGoodEmoji">Show 😀︎</string>
    <string name="showNotGoodEmoji">Show 😐︎</string>
    <string name="showBadEmoji">Show 🙁</string>
    <string name="showEmojiSummary">Toggle display of emoji on/off</string>
    <string name="attachmentDialogTitle">Add attachment</string>
    <string name="attachmentDialogText">Pick any file or take a new photo</string>
    <string name="photo">Photo</string>
    <string name="file">File</string>
    <string name="permissionDenied">Permission Denied</string>
    <string name="attachments">Attachments</string>
    <string name="errorDuringAttachmentDecryption">Error: Unable to decrypt attachment, trying to open natively</string>
    <string name="attachmentsImported">Imported %1$s attachments</string>
    <string name="recordsAdded">%1$s records added</string>
    <string name="search">Search</string>
    <string name="waitForUIUpdate">Please wait for the list to be refreshed</string>
    <string name="wrongUnitWarning">Warning: app is set to mmol/L, but value appears to be in mg/dL?</string>
    <string name="profiles">Profiles</string>
    <string name="activeProfileName">Active profile</string>
    <string name="addProfileDialog">Add profile</string>
    <string name="profileName">Profile name</string>
    <string name="editProfilesDialog">Edit profiles</string>
    <string name="editProfileDialog">Edit profile</string>
    <string name="deleteProfileDialog">Delete profile(s)</string>
    <string name="noProfileName">Profile name is empty</string>
    <string name="memoryConsumption">Memory consumption</string>
    <string name="moreThanOneProfileSelected">More than one profile selected!</string>
    <string name="noProfileSelected">No profile selected!</string>
    <string name="defaultProfileNeedsToRemain">Last profile can not be deleted!</string>
    <string name="details">Details:</string>
    <string name="importProfiles">Import profile data</string>
    <string name="importCSV">Import CSV file</string>
    <string name="importFormat">Make sure the import file matches the required structure! See here for more details:</string>
    <string name="importLink" translatable="false">https://codeberg.org/toz/MediLog/wiki/Import-Export</string>
    <string name="overwriteData">Overwrite existing data</string>
    <string name="cancelImport1">"Import into database has already begun, database will be inconsistent!"</string>
    <string name="cancelImport2">"Database import has not yet started, canceling is safe!"</string>
    <string name="fasting">Fasting</string>
    <string name="preMeal">Pre-Meal</string>
    <string name="postMeal">Post-Meal</string>
    <string name="profileSpecific">Profile specific settings</string>
    <string name="blendInDiaryItems">Blend in Diary items</string>
    <string name="blendInDiaryItemsSummary">Show diary entries in this tab (Read only)</string>
    <string name="unsavedData">Unsaved values found. Do you really want to close this screen?</string>
    <string name="multipleMeasurements">Multiple Measurements</string>
    <string name="multipleMeasurementsSummary">Summarize or replace measurement taken within x minutes. Use 0 to disable this setting</string>
    <string name="ignore">Ignore</string>
    <string name="multipleMeasurementsFound">Found %1$d measurement recorded in the last %2$s minutes.</string>
    <string name="keepAllRecords">and keep all records</string>
    <string name="replaceWith">Replace with</string>
    <string name="previousMeasurements">old measurements</string>
    <string name="enableAttachments">Enable Attachments</string>
    <string name="enableAttachmentsSummary">Attachments are deprecated and may be removed in a future version of MediLog. Please contact me if you think you got a use case which depends on this feature</string>
    <string name="pdfDataOrder">Data order</string>
    <string name="pdfDataOrderSummary">Select if data in PDF documents is ordered descending or ascending</string>
    <string name="ascending">Oldest values first</string>
    <string name="descending">Most recent values first</string>
    <string name="dynamicColor">Dynamic Color</string>
    <string name="dynamicColorSummary">Enable Material3 Dynamic color theme</string>
    <string name="pdfShowDaySeparator">Show day separator line</string>
    <string name="pdfShowDaySeparatorSummary">Prints a line whenever the day changes</string>
    <string name="pdfCantCreate">Unable to create PDF document!</string>
    <string name="bloodPressureRibbonChart">Ribbon chart</string>
    <string name="bloodPressureRibbonChartSummary">Show ribbon chart instead of line chart (Experimental)</string>
    <string name="crashLogs">Email crash logs</string>
    <string name="crashLogsSummary">Allow crash logs to be emailed to the developers. Each email will require explicit user confirmation.</string>
    <string name="crashMessageDialog">Unfortunately MediLog crashed due to a severe error. If you want the developers to know and be able to troubleshoot, please consider to email a crash report now.</string>
    <string name="crashMessageEmail">Unfortunately MediLog crashed! This email contains the crash log as attachment.\nCheck it out please.</string>
    <string name="crashContext">What did you do when this crash happened?</string>
    <string name="crashTitle">"MediLog crashed</string>
    <string name="fluidShowDaily">"Daily summaries</string>
    <string name="fluidShowDailySummary">"Show daily summaries instead of individual values</string>
    <string name="documentsEditTitle">Edit Document Item</string>
    <string name="documents">Documents</string>
    <string name="documentMissing">Document is missing</string>
    <string name="glucoseWrongUnit">The value you just added looks like a mg/dL measurement. However, your configuration is mmol/L!\n\nPlease check your input or change the unit in Settings/Glucose.</string>
    <string name="receivedDiaryEntry">Received new Diary entry</string>
    <string name="receivedDocument">Received new Document</string>

    <string name="OXIMETRY_UNIT" translatable="false">\% SpO2</string>
    <string name="OXIMETRY_THRESHOLDS_DEFAULT" translatable="false">95</string>
    <string name="OXIMETRY_SHOW_THRESHOLDS_DEFAULT" translatable="false">true</string>
    <string name="OXIMETRY_SHOW_GRID_DEFAULT" translatable="false">true</string>
    <string name="OXIMETRY_SHOW_LEGEND_DEFAULT" translatable="false">false</string>
    <string name="OXIMETRY_LANDSCAPE_DEFAULT" translatable="false">false</string>
    <string name="OXIMETRY_PAPER_SIZE_DEFAULT" translatable="false">A4</string>
    <string name="PULSE_HINT" translatable="false">000</string>
    <string name="OXIMETRY_HIGHLIGHT_VALUES_DEFAULT" translatable="false">true</string>
    <string name="BLENDINITEMS_DEFAULT" translatable="false">false</string>

    <string name="BLOODPRESSURE_HIGHLIGHT_VALUES_DEFAULT" translatable="false">true</string>
    <string name="BLOODPRESSURE_GRADE1_VALUES" translatable="false">140/90</string>
    <string name="BLOODPRESSURE_GRADE2_VALUES" translatable="false">160/100</string>
    <string name="BLOODPRESSURE_GRADE3_VALUES" translatable="false">180/110</string>
    <string name="BLOODPRESSURE_HYPOTENSION_VALUES" translatable="false">90/60</string>
    <string name="BLOODPRESSURE_MOVING_AVERAGE_SIZE_DEFAULT" translatable="false">6</string>
    <string name="BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW_DEFAULT" translatable="false">0</string>
    <string name="BLOODPRESSURE_TIMEZONE_DEFAULT" translatable="false" tools:ignore="TypographyDashes">0-12-18</string>
    <string name="BLOODPRESSURE_UNIT_DEFAULT" translatable="false">mmHg</string>
    <string name="BLOODPRESURE_SHOW_THRESHOLD_DEFAULT" translatable="false">true</string>
    <string name="BLOODPRESSURE_LANDSCAPE_DEFAULT" translatable="false">false</string>
    <string name="BLOODPRESSURE_PAPER_SIZE_DEFAULT" translatable="false">A4</string>
    <string name="BLOODPRESURE_SHOW_LEGEND_DEFAULT" translatable="false">false</string>

    <string name="WEIGHT_HIGHLIGHT_VALUES_DEFAULT" translatable="false">true</string>
    <string name="TEMPERATURE_HIGHLIGHT_VALUES_DEFAULT" translatable="false">true</string>
    <string name="GLUCOSE_HIGHLIGHT_VALUES_DEFAULT" translatable="false">true</string>
    <string name="LOG_KETONE_DEFAULT" translatable="false">false</string>
    <string name="FAT_MIN_MAX_DEFAULT" translatable="false" tools:ignore="TypographyDashes">14-25</string>
    <string name="LOG_FAT_DEFAULT" translatable="false">false</string>
    <string name="WEIGHT_UNIT_DEFAULT" translatable="false">kg</string>
    <string name="WEIGHT_THRESHOLD_DEFAULT" translatable="false">0-80</string>
    <string name="SHOW_WEIGHT_THRESHOLD_DEFAULT" translatable="false">true</string>
    <string name="SHOW_WEIGHT_GRID_DEFAULT" translatable="false">false</string>
    <string name="SHOW_WEIGHT_LEGEND_DEFAULT" translatable="false">false</string>
    <string name="WEIGHT_LINEAR_TRENDLINE_DEFAULT" translatable="false">false</string>
    <string name="WEIGHT_MOVING_AVERAGE_TRENDLINE_DEFAULT" translatable="false">false</string>
    <string name="WEIGHT_MOVING_AVERAGE_SIZE_DEFAULT" translatable="false">6</string>
    <string name="KEY_PREF_WEIGHT_DAY_STEPPING_DEFAULT" translatable="false">false</string>
    <string name="KEY_PREF_WEIGHT_BAR_CHART_DEFAULT" translatable="false">false</string>
    <string name="WEIGHT_LANDSCAPE_DEFAULT" translatable="false">false</string>
    <string name="WEIGHT_PAPER_SIZE_DEFAULT" translatable="false">A4</string>
    <string name="FLUID_UNIT_DEFAULT" translatable="false">ml</string>
    <string name="FLUID_THRESHOLD_DEFAULT" translatable="false">2000</string>
    <string name="SHOW_FLUID_THRESHOLD_DEFAULT" translatable="false">true</string>
    <string name="FLUID_SHOW_PDF_SUMMARY_DEFAULT" translatable="false">true</string>
    <string name="FLUID_LANDSCAPE_DEFAULT" translatable="false">false</string>
    <string name="FLUID_PAPER_SIZE_DEFAULT" translatable="false">A4</string>
    <string name="DIARY_LANDSCAPE_DEFAULT" translatable="false">false</string>
    <string name="DIARY_PAPER_SIZE_DEFAULT" translatable="false">A4</string>
    <string name="DIARY_SHOWALLTABS_DEFAULT" translatable="false">false</string>

    <string name="GLUCOSE_UNIT_MMOL_L" translatable="false">mmol/L</string>
    <string name="GLUCOSE_UNIT_MG_DL" translatable="false">mg/dL</string>
    <string name="GLUCOSE_THRESHOLDS_DEFAULT_MG_DL" translatable="false" tools:ignore="TypographyDashes">60-100</string>
    <string name="GLUCOSE_THRESHOLDS_DEFAULT_MMOL_L" translatable="false" tools:ignore="TypographyDashes">3.3-5.6</string>
    <string name="GLUCOSE_SHOW_THRESHOLDS_DEFAULT" translatable="false">true</string>
    <string name="GLUCOSE_SHOW_GRID_DEFAULT" translatable="false">true</string>
    <string name="GLUCOSE_SHOW_LEGEND_DEFAULT" translatable="false">false</string>
    <string name="GLUCOSE_LANDSCAPE_DEFAULT" translatable="false">false</string>
    <string name="GLUCOSE_PAPER_SIZE_DEFAULT" translatable="false">A4</string>
    <string name="KETON_UNIT" translatable="false">mmol/L</string>

    <string name="TEMPERATURE_UNIT_DEFAULT" translatable="false">°C</string>
    <string name="TEMPERATURE_THRESHOLDS_CELSIUS_DEFAULT" translatable="false" tools:ignore="TypographyDashes">36.5-37.5</string>
    <string name="TEMPERATURE_THRESHOLDS_FAHRENHEIT_DEFAULT" translatable="false" tools:ignore="TypographyDashes">97.7-99.5</string>
    <string name="TEMPERATURE_SHOW_THRESHOLDS_DEFAULT" translatable="false">true</string>
    <string name="TEMPERATURE_SHOW_GRID_DEFAULT" translatable="false">true</string>
    <string name="TEMPERATURE_SHOW_LEGEND_DEFAULT" translatable="false">false</string>
    <string name="TEMPERATURE_LANDSCAPE_DEFAULT" translatable="false">false</string>
    <string name="TEMPERATURE_PAPER_SIZE_DEFAULT" translatable="false">A4</string>
    <string name="TEMPERATURE_CELSIUS" translatable="false">°C</string>
    <string name="TEMPERATURE_FAHRENHEIT" translatable="false">°F</string>

    <string name="LOG_HEARTRHYTHM_DEFAULT" translatable="false">false</string>
    <string name="BACKUP_WARNING_DEFAULT" translatable="false">7</string>
    <string name="AUTO_BACKUP_DEFAULT" translatable="false">1</string>
    <string name="ADD_TIMESTAMP_DEFAULT" translatable="false">true</string>
    <string name="DARK_MODE_DEFAULT" translatable="false">MODE_NIGHT_FOLLOW_SYSTEM</string>
    <string name="DELIMITER_DEFAULT" translatable="false">,</string>
    <string name="SHOW_TABICONS_DEFAULT" translatable="false">true</string>
    <string name="SHOW_TABTEXT_DEFAULT" translatable="false">false</string>
    <string name="SCROLL_TABS_DEFAULT" translatable="false">false</string>
    <string name="TAB_TRANSITIONS_DEFAULT" translatable="false">Horizontal Slide</string>
    <string name="TEXT_SIZE_DEFAULT" translatable="false">12</string>
    <string name="PDF_TEXT_SIZE_DEFAULT" translatable="false">10</string>
    <string name="PDF_DATA_ORDER_DEFAULT" translatable="false">DESC</string>
    <string name="ROW_PADDING_DEFAULT" translatable="false">0</string>
    <string name="HIGHLIGHT_VALUES_DEFAULT" translatable="false">false</string>
    <string name="QUICKENTRY_DEFAULT" translatable="false">true</string>
    <string name="BIOMETRICS_DEFAULT" translatable="false">false</string>
    <string name="ROLLING_FILTER_TIMEFRAME_DEFAULT" translatable="false">3</string>
    <string name="ROLLING_FILTER_VALUE_DEFAULT" translatable="false">1</string>
    <string name="FILTER_MODE_DEFAULT" translatable="false">0</string>
    <string name="FORCE_REAUTHENTICATION_DEFAULT" translatable="false">5</string>
    <string name="SHOWTIME_DEFAULT" translatable="false">true</string>
    <string name="DAY_SEPARATOR_LINE_DEFAULT" translatable="false">true</string>
</resources>