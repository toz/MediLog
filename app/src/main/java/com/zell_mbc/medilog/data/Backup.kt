/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.documentfile.provider.DocumentFile
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DatePattern
import com.zell_mbc.medilog.MainActivity.Companion.settingsViewModel
import com.zell_mbc.medilog.MainActivity.Companion.textTemplatesViewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.*
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.BufferedOutputStream
import java.io.File
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

val backupFileNames = arrayOf("MediLog-Data.csv", "MediLog-Profiles.csv", "MediLog-TextTemplates.csv", "MediLog-Tags.csv", "MediLog-Settings.csv")

class Backup(application: Application, private val uri: Uri, private val zipPassword: String) {
    private val app = application
    private val preferences = PreferenceManager.getDefaultSharedPreferences(app)
    private val datePrefix = preferences.getBoolean(SettingsActivity.KEY_PREF_ADD_TIMESTAMP, app.getString(R.string.ADD_TIMESTAMP_DEFAULT).toBoolean())
    private var dataViewModel = MainActivity.viewModel

    // Create,write and protect backup ZIP file to backup location
    @SuppressLint("SimpleDateFormat")
    fun exportZIPFile(autoBackup: Boolean) {

        // persist permission to access backup folder across reboots
        val takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        try {
            app.grantUriPermission(app.packageName, uri, takeFlags)
            app.contentResolver.takePersistableUriPermission(uri, takeFlags)
        } catch (_: SecurityException) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eLostAccessRights), Toast.LENGTH_LONG).show() }
            return
        }

        //val timeStamp = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) android.icu.text.SimpleDateFormat(MainActivity.DATE_PATTERN).format(Date().time)
        //else UUID.randomUUID().toString()

        // Create empty Zip file
        var zipFile = app.getString(R.string.appName) + "-Backup" + if (datePrefix) "-" + SimpleDateFormat(DatePattern.DATE_TIME).format(Date().time) else ""
        zipFile = zipFile.replace(":","-") // German time format prevents a valid filename
        zipFile += ".zip"
        var dFile: DocumentFile? = null
        val dFolder: DocumentFile?
        try {
            dFolder = DocumentFile.fromTreeUri(app, uri)
        } catch (e: SecurityException) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString(), Toast.LENGTH_LONG).show() }
            return
        }
        if (dFolder == null) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eCreateFile) + ": " + dFile, Toast.LENGTH_LONG).show() }
            return
        }

        if (!dFolder.canWrite()) { // lost access rights, needs a manual backup
            Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eLostAccessRights) + ": " + dFolder.toString(), Toast.LENGTH_LONG).show() }
            return
        }

        try {
            // Check if file exists, delete if yes
            if (dFolder.findFile(zipFile) != null) dFolder.findFile(zipFile)?.delete()
            dFile = dFolder.createFile("application/zip", zipFile)
            if (dFile == null) {
                Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eUnableToWriteToFolder) + ": " + dFolder.toString(), Toast.LENGTH_LONG).show() }
                return
            }
        } catch (e: IllegalArgumentException) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString(), Toast.LENGTH_LONG).show() }
            return
        }
        val attachments = listAttachments(app,  ATTACHMENT_LABEL)

        // Add files to ZIP file
        val dest: OutputStream?
        val out: ZipOutputStream?

        try {
            dest = app.contentResolver.openOutputStream(dFile.uri)
            if (dest == null) {
                Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eCreateFile) + " " + dFile, Toast.LENGTH_LONG).show() }
                return
            }
            out = if (zipPassword.isEmpty()) ZipOutputStream(BufferedOutputStream(dest))
            else ZipOutputStream(BufferedOutputStream(dest), zipPassword.toCharArray())

        } catch (_: Exception) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app,app.getString(R.string.eCreateFile) + " " + dFile, Toast.LENGTH_LONG).show() }
            return
        }
        val zipParameters = ZipParameters()
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        // First we add the data file
        val dbTables = backupFileNames.count()
        var curFile = 0
        Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.backupStarted), Toast.LENGTH_LONG).show() }
        val zipData = arrayOfNulls<String>(dbTables)
        zipData[curFile] = dataViewModel.dataToCSV(dataViewModel.backup())
        try {
            zipParameters.fileNameInZip = backupFileNames[curFile]
            out.putNextEntry(zipParameters)
            out.write(zipData[curFile]!!.toByteArray())
            out.closeEntry()
        } catch (_: Exception) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + zipParameters.fileNameInZip, Toast.LENGTH_LONG).show() }
            return
        }

        // Next the profiles table
        val profilesViewModel =  MainActivity.profilesViewModel
        curFile++
        zipData[curFile] = profilesViewModel.dataToCSV(profilesViewModel.backup())
        try {
            zipParameters.fileNameInZip = backupFileNames[curFile]
            out.putNextEntry(zipParameters)
            out.write(zipData[curFile]!!.toByteArray())
            out.closeEntry()
        } catch (_: Exception) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + zipParameters.fileNameInZip, Toast.LENGTH_LONG).show() }
            return
        }

        // The text templates table
        curFile++
        val templateViewModel =  textTemplatesViewModel
        if (templateViewModel.count() > 0) {
            zipData[curFile] = templateViewModel.dataToCSV(templateViewModel.backup())
            try {
                zipParameters.fileNameInZip = backupFileNames[curFile]
                out.putNextEntry(zipParameters)
                out.write(zipData[curFile]!!.toByteArray())
                out.closeEntry()
            } catch (_: Exception) {
                Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + zipParameters.fileNameInZip, Toast.LENGTH_LONG).show() }
                return
            }
        }

        // The Tags table
        val tagsViewModel =  MainActivity.tagsViewModel
        curFile++
        if (tagsViewModel.count() > 0) {
            zipData[curFile] = tagsViewModel.dataToCSV(tagsViewModel.backup())
            try {
                zipParameters.fileNameInZip = backupFileNames[curFile]
                out.putNextEntry(zipParameters)
                out.write(zipData[curFile]!!.toByteArray())
                out.closeEntry()
            } catch (_: Exception) {
                Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + zipParameters.fileNameInZip, Toast.LENGTH_LONG).show() }
                return
            }
        }

        // The Settings table
        curFile++
        // Make sure the current profile is current before we pull the database
        profilesViewModel.preferenceToActiveProfile()
        val tmp = settingsViewModel.backup()
        if (!tmp.isNullOrEmpty()) {
            zipData[curFile] = settingsViewModel.dataToCSV(tmp)
            try {
                zipParameters.fileNameInZip = backupFileNames[curFile]
                out.putNextEntry(zipParameters)
                out.write(zipData[curFile]!!.toByteArray())
                out.closeEntry()
            } catch (_: Exception) {
                Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + zipParameters.fileNameInZip, Toast.LENGTH_LONG).show() }
                return
            }
        }

        // And now the attachments
        val attachmentFolder = getAttachmentFolder(app)
        if (attachments.isNotEmpty()) {
            var i = 0
            for (fileName in attachments) {
                try {
                    zipParameters.fileNameInZip = fileName
                    out.putNextEntry(zipParameters)

                    //val f = decryptAttachment(app, fileName)
                    val filePath = File(attachmentFolder, "")
                    val f = File(filePath, fileName)
                    out.write(f.readBytes())
                    out.closeEntry()
                    i++
                } catch (_: Exception) {
                    Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + fileName, Toast.LENGTH_LONG).show() }
                    return
                }
            }
        }

        // CLose ZIP file
        try {
            out.close()
        } catch (_: Exception) {
            Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.eCreateFile) + " 2 " + dFile, Toast.LENGTH_LONG).show() }
            return
        }
        var msg = if (zipPassword.isEmpty()) app.getString(R.string.unprotectedZipFileCreated)
        else app.getString(R.string.protectedZipFileCreated)

        val size = dataViewModel.backup().size
        val sizeString = "\n($size " + app.getString(R.string.entries) + ")" //+ ", " + attachments.size.toString() + " " + app.getString(R.string.attachments) + ")"
        if (autoBackup) msg = "AutoBackup: $msg"
        Handler(Looper.getMainLooper()).post { Toast.makeText(app, msg + " " + dFile.name + sizeString, Toast.LENGTH_LONG).show() }

        // Capture last backup date
        val editor = preferences.edit()
        editor.putLong("LAST_BACKUP", Date().time)
        editor.putString(SettingsActivity.KEY_PREF_BACKUP_URI, uri.toString())
        editor.apply()
    }
}