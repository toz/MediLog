/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.data

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.room.*
import androidx.room.migration.Migration
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import androidx.sqlite.db.SupportSQLiteDatabase
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.profiles.ProfilesDao
import com.zell_mbc.medilog.settings.SettingsDao
import com.zell_mbc.medilog.support.SQLCipherUtils
import com.zell_mbc.medilog.support.SQLCipherUtils.getDatabaseState
import com.zell_mbc.medilog.support.getVersionName
import com.zell_mbc.medilog.tags.TagsDao
import com.zell_mbc.medilog.texttemplates.TextTemplatesDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory
import java.util.*


@Entity(tableName = "data", indices = [Index(value = ["timestamp"]), Index(value = ["type"]), Index(value = ["profile_id"])])
data class Data(
    @PrimaryKey(autoGenerate = true) val _id: Int,
    var timestamp: Long,
    var comment: String = "",
    var type: Int,
    var value1: String = "",
    var value2: String = "",
    var value3: String = "",
    var value4: String = "",
    var attachment: String,
    var profile_id: Int,
    var category_id: Int,
    var tags: String = ""
)


// Per user data and settings
@Entity(tableName = "profiles", indices = [Index(value = ["_id"])])
data class Profiles(
        @PrimaryKey(autoGenerate = true) var _id: Int,
        var name: String  = "",
        var description: String = ""
)

//@Entity(tableName = "settings", indices = [Index(value = ["_id","profile_id", "_key" ], unique = true)])
@Entity(tableName = "settings", indices = [Index(value = ["profile_id"]), Index(value = ["_key"])])
data class Settings(
    @PrimaryKey(autoGenerate = true) var _id: Long,
    var profile_id: Int,
    var _key: String,
    var value: String
)

@Entity(tableName = "texttemplates", indices = [Index(value = ["type"])])
data class TextTemplates(
    @PrimaryKey(autoGenerate = true) var _id: Int,
    var type: Int,
    var template: String
)

@Entity(tableName = "tags", indices = [Index(value = ["type"])])
data class Tags(
    @PrimaryKey(autoGenerate = true) var _id: Int,
    var type: Int,
    var tag: String,
    var color: String
)

// should we sort in ascending or descending order
enum class Order {
    ASC,
    DESC
}

fun getTableColumnNames(tableName: String, context: Context): List<String> {
    val rv = arrayListOf<String>()
        val csr = MediLogDB.getDatabase(context).query("SELECT name FROM pragma_table_info('${tableName}')", emptyArray())
    while (csr.moveToNext()) {
        rv.add(csr.getString(0))
    }
    csr.close()
    return rv.toList()
}

@Database(entities = [Data::class, Profiles::class, TextTemplates::class, Tags::class, Settings::class], version = 18, exportSchema = false)
abstract class MediLogDB : RoomDatabase() {
    abstract fun dataDao(): DataDao
    abstract fun profilesDao(): ProfilesDao
    abstract fun TextTemplatesDao(): TextTemplatesDao
    abstract fun TagsDao(): TagsDao
    abstract fun SettingsDao(): SettingsDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(db: SupportSQLiteDatabase) {
                // Create the new table
                db.execSQL("CREATE TABLE IF NOT EXISTS `weight_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `weight` REAL NOT NULL)")
                db.execSQL("INSERT INTO weight_new (_id, timestamp, comment, weight) SELECT _id, timestamp, comment, weight FROM weight;")
                db.execSQL("DROP TABLE weight;")// Change the table name to the correct one
                db.execSQL("ALTER TABLE weight_new RENAME TO weight;")

                db.execSQL("CREATE TABLE IF NOT EXISTS `diary_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `diary` TEXT NOT NULL)")
                db.execSQL("INSERT INTO diary_new (_id, timestamp, comment, diary) SELECT _id, timestamp, '', diary FROM diary;")
                db.execSQL("DROP TABLE diary;")// Change the table name to the correct one
                db.execSQL("ALTER TABLE diary_new RENAME TO diary;")

                db.execSQL("CREATE TABLE IF NOT EXISTS `bp_new` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `sys` INTEGER NOT NULL, `dia` INTEGER NOT NULL, `pulse` INTEGER NOT NULL)")
                db.execSQL("INSERT INTO bp_new (_id, timestamp, comment, sys, dia, pulse) SELECT _id, timestamp, comment, systolic, diastolic, pulse FROM bloodpressure")
                db.execSQL("DROP TABLE bloodpressure")// Change the table name to the correct one
                db.execSQL("ALTER TABLE bp_new RENAME TO bloodpressure;")
            }
        }

        private val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(db: SupportSQLiteDatabase) {
                // Add the index
                db.execSQL("CREATE INDEX index_weight_timestamp on weight(timestamp)")
                db.execSQL("CREATE INDEX index_bloodpressure_timestamp on bloodpressure(timestamp)")
                db.execSQL("CREATE INDEX index_diary_timestamp on diary(timestamp)")
            }
        }

        private val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("ALTER TABLE diary ADD COLUMN state INTEGER NOT NULL  DEFAULT '0'")
            }
        }

        private val MIGRATION_4_5: Migration = object : Migration(4, 5) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("CREATE TABLE IF NOT EXISTS `water` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `water` INTEGER NOT NULL, `day` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_water_timestamp on water(timestamp)")
                db.execSQL("ALTER TABLE bloodpressure ADD COLUMN state INTEGER NOT NULL  DEFAULT '0'")
            }
        }

        private val MIGRATION_5_6: Migration = object : Migration(5, 6) {
            override fun migrate(db: SupportSQLiteDatabase) {
                // Migrate
                db.execSQL("CREATE TABLE IF NOT EXISTS `data` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timestamp` INTEGER NOT NULL, `comment` TEXT NOT NULL, `type` INTEGER NOT NULL, `value1` TEXT NOT NULL, `value2` TEXT NOT NULL, `value3` TEXT NOT NULL, `value4` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_data_type on data(type)")
                db.execSQL("CREATE INDEX index_data_timestamp on data(timestamp)")

                // Move measurement data
                db.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, ${Tabs.WEIGHT}, weight, '', '', '' FROM weight")
                db.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, ${Tabs.BLOODPRESSURE}, sys, dia, pulse, state FROM bloodpressure")
                db.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, ${Tabs.DIARY}, diary, state, '', '' FROM diary")
                db.execSQL("INSERT INTO data (timestamp, comment, type, value1, value2, value3, value4) SELECT timestamp, comment, ${Tabs.FLUID}, water, day, '', '' FROM water")

                db.execSQL("CREATE TABLE IF NOT EXISTS `settings` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `type` INTEGER NOT NULL, `_key` TEXT NOT NULL, `value` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_settings_type on settings(type)")
                db.execSQL("CREATE INDEX index_settings__key on settings(_key)")

                // Do not drop the old tables until the next DB upgrade is due, to provide a safety net in case something goes wrong during migration
            }
        }

        private val MIGRATION_6_7: Migration = object : Migration(6, 7) {
            override fun migrate(db: SupportSQLiteDatabase) {
                // We have kept the old tables until next DB upgrade is due to provide a safety net in case something goes wrong during migration
                db.execSQL("DROP TABLE IF EXISTS weight")
                db.execSQL("DROP TABLE IF EXISTS bloodpressure")
                db.execSQL("DROP TABLE IF EXISTS diary")
                db.execSQL("DROP TABLE IF EXISTS water")
                db.execSQL("UPDATE data SET comment = value1 WHERE type = '3'")
            }
        }

        private val MIGRATION_7_8: Migration = object : Migration(7, 8) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("ALTER TABLE data ADD COLUMN photo TEXT NOT NULL DEFAULT ''")
            }
        }

        private val MIGRATION_8_9: Migration = object : Migration(8, 9) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("ALTER TABLE data RENAME COLUMN photo TO attachment")
            }
        }

        private val MIGRATION_9_10: Migration = object : Migration(9, 10) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("ALTER TABLE data ADD COLUMN profile_id INTEGER NOT NULL DEFAULT '1'")
                db.execSQL("CREATE INDEX index_data_profile_id on data(profile_id)")
                db.execSQL("CREATE TABLE IF NOT EXISTS `profiles` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `comment` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_profiles__id on profiles(_id)")
            }
        }

        private val MIGRATION_10_11: Migration = object : Migration(10, 11) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("ALTER TABLE profiles ADD COLUMN height INTEGER NOT NULL DEFAULT '0'")
                db.execSQL("ALTER TABLE profiles ADD COLUMN gender TEXT NOT NULL DEFAULT ''")
                db.execSQL("ALTER TABLE profiles ADD COLUMN dob INTEGER NOT NULL DEFAULT '0'")
            }
        }

        private val MIGRATION_11_12: Migration = object : Migration(11, 12) {
            override fun migrate(db: SupportSQLiteDatabase) {
                /* Can't use this because Room can't handle conditional schemas :-(
                if (dropColumnAvailable())
                    db.execSQL("ALTER TABLE profiles DROP COLUMN gender")
                */
                db.execSQL("ALTER TABLE profiles ADD COLUMN sex TEXT NOT NULL DEFAULT ''")
                db.execSQL("ALTER TABLE profiles ADD COLUMN weight_threshold INTEGER NOT NULL DEFAULT '0'")
                db.execSQL("ALTER TABLE profiles ADD COLUMN water_threshold INTEGER NOT NULL DEFAULT '0'")
                db.execSQL("ALTER TABLE profiles ADD COLUMN active_tabs TEXT NOT NULL DEFAULT ''")
            }
        }

        private val MIGRATION_12_13: Migration = object : Migration(12, 13) {
            override fun migrate(db: SupportSQLiteDatabase) {
                /* Can't use this because Room can't handle conditional schemas :-(
                if (dropColumnAvailable()) {
                    db.execSQL("ALTER TABLE profiles DROP COLUMN weight_threshold")
                    db.execSQL("ALTER TABLE profiles DROP COLUMN water_threshold")
                }*/
                db.execSQL("ALTER TABLE profiles ADD COLUMN weight_thresholds TEXT NOT NULL DEFAULT ''")
                db.execSQL("ALTER TABLE profiles ADD COLUMN water_thresholds TEXT NOT NULL DEFAULT ''")
                db.execSQL("ALTER TABLE profiles ADD COLUMN weight_tare INTEGER NOT NULL DEFAULT(0)")
                db.execSQL("ALTER TABLE profiles ADD COLUMN log_body_fat INTEGER NOT NULL DEFAULT(0)")
                db.execSQL("ALTER TABLE profiles ADD COLUMN fat_min_max TEXT NOT NULL DEFAULT ''")
                db.execSQL("ALTER TABLE profiles ADD COLUMN glucose_thresholds TEXT NOT NULL DEFAULT ''")
                db.execSQL("ALTER TABLE profiles ADD COLUMN temperature_thresholds TEXT NOT NULL DEFAULT ''")
                db.execSQL("ALTER TABLE profiles ADD COLUMN oximetry_thresholds TEXT NOT NULL DEFAULT ''")
            }
        }

        private val MIGRATION_13_14: Migration = object : Migration(13, 14) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("CREATE TABLE IF NOT EXISTS `texttemplates` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `type` INTEGER NOT NULL, `template` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_texttemplates_type on texttemplates(type)")
            }
        }

        private val MIGRATION_14_15: Migration = object : Migration(14, 15) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("DROP TABLE settings")
                db.execSQL("CREATE TABLE IF NOT EXISTS `categories` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `type` INTEGER NOT NULL, `category` TEXT NOT NULL, `color` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_categories_type on categories(type)")
            }
        }

        private val MIGRATION_15_16: Migration = object : Migration(15, 16) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("ALTER TABLE data ADD COLUMN category_id INTEGER NOT NULL DEFAULT '-1'")
                db.execSQL("CREATE INDEX index_data_category_id on data(category_id)")
            }
        }

        private val MIGRATION_16_17: Migration = object : Migration(16, 17) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("CREATE TABLE IF NOT EXISTS `tags` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `type` INTEGER NOT NULL, `tag` TEXT NOT NULL, `color` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_tags_type on tags(type)")
                db.execSQL("INSERT INTO tags (_id, type, tag, color) SELECT _id, type, category, color FROM categories")
                db.execSQL("DROP TABLE categories")

                db.execSQL("ALTER TABLE data ADD COLUMN tags TEXT NOT NULL DEFAULT ''")
                db.execSQL("UPDATE data SET tags = CAST(category_id as TEXT)")
                db.execSQL("DROP INDEX index_data_category_id")

                // Can't use this because Room can't handle conditional schemas :-(
                // No DROP COLUMN before 3.35
                //if (dropColumnAvailable())
                //    db.execSQL("ALTER TABLE data DROP COLUMN category_id")
                // Todo: Rename to tmp, create new, copy content function
            }
        }

        private val MIGRATION_17_18: Migration = object : Migration(17, 18) {
            override fun migrate(db: SupportSQLiteDatabase) {
                // Clean up profiles table
                val TABLE_NAME       = "profiles"
                val TABLE_NAME_TEMP = TABLE_NAME+"_tmp"

                // 1. Create new table
                db.execSQL("CREATE TABLE IF NOT EXISTS `$TABLE_NAME_TEMP` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `description` TEXT NOT NULL)")

                // 2. Copy the data
                db.execSQL("INSERT INTO $TABLE_NAME_TEMP (name, description) SELECT name, comment FROM $TABLE_NAME")

                // 3. Remove the old table
                db.execSQL("DROP TABLE $TABLE_NAME")

                // 4. Change the table name to the correct one
                db.execSQL("ALTER TABLE $TABLE_NAME_TEMP RENAME TO $TABLE_NAME")
                db.execSQL("CREATE INDEX index_profiles_id on profiles(_id)")

                // New settings key store table
                db.execSQL("CREATE TABLE IF NOT EXISTS `settings` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `profile_id` INTEGER NOT NULL, `_key` TEXT NOT NULL, `value` TEXT NOT NULL)")
                db.execSQL("CREATE INDEX index_settings_profile_id on settings(profile_id)")
                db.execSQL("CREATE INDEX index_settings_key on settings(_key)")
                //db.execSQL("CREATE INDEX index_settings_fields on settings(_key, profile_id, key)")
            }
        }

        @Volatile
        private var INSTANCE: MediLogDB? = null

        fun getDatabase(
            context: Context
        ) : MediLogDB {
            val dbName = "MediLogDatabase"
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance

            // Check if DB is already encrypted
            SQLiteDatabase.loadLibs(context)
            val dbFile = context.getDatabasePath(dbName)

            // #################
            //dbFile.delete()
            // #################

            val state = getDatabaseState(dbFile)
            var databasePin = ""

            //state = SQLCipherUtils.State.ENCRYPTED
            //val preferences = Preferences.getSharedPreferences(context)
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            //val crashOnEncryption = preferences.getBoolean("crashOnEncryption",false) // Returns the unencrypted pin or a new one
            //if (crashOnEncryption) state = SQLCipherUtils.State.UNENCRYPTED

            synchronized(this) {
                lateinit var instance: MediLogDB
                val versionName = getVersionName(context)

                val encryptDB = when(state) {
                    SQLCipherUtils.State.UNENCRYPTED -> false // If recognized as unencrypted stay with that, no matter the other options
                    SQLCipherUtils.State.DOES_NOT_EXIST -> !versionName.contains("debug") // Do not encrypt if this is a dev/debug situation, otherwise encrypt
                    else -> true // Default case in production = State.ENCRYPTED
                }

                instance = if (!encryptDB) {
                    Room.databaseBuilder(context.applicationContext, MediLogDB::class.java, dbName)
                        .addMigrations(MIGRATION_1_2)
                        .addMigrations(MIGRATION_2_3)
                        .addMigrations(MIGRATION_3_4)
                        .addMigrations(MIGRATION_4_5)
                        .addMigrations(MIGRATION_5_6)
                        .addMigrations(MIGRATION_6_7)
                        .addMigrations(MIGRATION_7_8)
                        .addMigrations(MIGRATION_8_9)
                        .addMigrations(MIGRATION_9_10)
                        .addMigrations(MIGRATION_10_11)
                        .addMigrations(MIGRATION_11_12)
                        .addMigrations(MIGRATION_12_13)
                        .addMigrations(MIGRATION_13_14)
                        .addMigrations(MIGRATION_14_15)
                        .addMigrations(MIGRATION_15_16)
                        .addMigrations(MIGRATION_16_17)
                        .addMigrations(MIGRATION_17_18)
                        .build()
                }
                else {  // Encrypted, new db's for non debug builds will always be encrypted
                    // Trying to move away from encrypted preferences, hence look here first
                    databasePin = "" + preferences.getString("databasePin","") // Returns the unencrypted pin or a new one

                    // Get pin from encrypted preferences
                    // Consider a db_migration which reads encrypted and writes unencrypted prefs
                    if (databasePin.isEmpty()) {
                        val masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
                        val encryptedSharedPreferences = EncryptedSharedPreferences.create(context, "medilog_prefs", masterKey, EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV, EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM)
                        databasePin = "" + encryptedSharedPreferences.getString("databasePin", UUID.randomUUID().toString())
                    }

                    // If nothing is in encrypted prefs either, (first time launch) create a new pin
                    if (databasePin.isEmpty()) {
                        databasePin = UUID.randomUUID().toString() // In case any of the above returns an empty string
                    }

                    //val editor = encryptedSharedPreferences.edit()
                    //editor?.putString("databasePin", databasePin)?.apply()

                    val passphrase: ByteArray = SQLiteDatabase.getBytes(databasePin.toCharArray())
                    val factory = SupportFactory(passphrase)

                    Room.databaseBuilder(context.applicationContext, MediLogDB::class.java, dbName)
                        .openHelperFactory(factory)
                        .addMigrations(MIGRATION_1_2)
                        .addMigrations(MIGRATION_2_3)
                        .addMigrations(MIGRATION_3_4)
                        .addMigrations(MIGRATION_4_5)
                        .addMigrations(MIGRATION_5_6)
                        .addMigrations(MIGRATION_6_7)
                        .addMigrations(MIGRATION_7_8)
                        .addMigrations(MIGRATION_8_9)
                        .addMigrations(MIGRATION_9_10)
                        .addMigrations(MIGRATION_10_11)
                        .addMigrations(MIGRATION_11_12)
                        .addMigrations(MIGRATION_12_13)
                        .addMigrations(MIGRATION_13_14)
                        .addMigrations(MIGRATION_14_15)
                        .addMigrations(MIGRATION_15_16)
                        .addMigrations(MIGRATION_16_17)
                        .addMigrations(MIGRATION_17_18)
                        .build()
                    //.fallbackToDestructiveMigration()
                }

                INSTANCE = instance

                val editor = preferences.edit()
                editor?.putBoolean("encryptedDB", encryptDB)
                if (encryptDB) editor?.putString("databasePin", databasePin)?.apply() // Prepare to move away from encrypted preferences
                editor?.apply()
                return instance
            }
        }

        // SQLite Version is OS dependent. Find out which version we got available
        // https://developer.android.com/reference/android/database/sqlite/package-summary
        // https://www.sqlite.org/changes.html
        // https://stackoverflow.com/questions/2421189/version-of-sqlite-used-in-android

        // 3.35.0 No DROP COLUMN before 3.35
        /*fun dropColumnAvailable(): Boolean {
            val version = android.database.sqlite.SQLiteDatabase.create(null).use {
                android.database.DatabaseUtils.stringForQuery(it, "SELECT sqlite_version()", null)
            }
            val v = version.split(".")
            val major = try { v[0].toInt() } catch (_: NumberFormatException) { 0 }
            val minor = try { v[1].toInt() } catch (_: NumberFormatException) { 0 }
            return (major >= 3 && minor >= 35)
        }*/

        // Not used right now
        /*
        fun encryptDB(context: Context, dbFile: File, databasePin: String) {
//            if (state == SQLCipherUtils.State.UNENCRYPTED) {
            // Migrate dbFile to encrypted DB
            val tmpDB = File.createTempFile("migration", "", context.cacheDir)
            var database = SQLiteDatabase.openOrCreateDatabase(dbFile, "", null)
            database.rawExecSQL(java.lang.String.format("ATTACH DATABASE '%s' AS encrypted KEY '%s';", tmpDB.absolutePath, databasePin))
            database.rawExecSQL("SELECT sqlcipher_export('encrypted')")
            database.rawExecSQL("DETACH DATABASE encrypted;")
            val version = database.version
            database.close()
            database = SQLiteDatabase.openDatabase(tmpDB.absolutePath, databasePin, null, SQLiteDatabase.OPEN_READWRITE)
            database.version = version
            database.close()
            dbFile.delete()
            tmpDB.renameTo(dbFile)
        }*/
    }

    suspend fun resetDb(context: Context) = withContext(Dispatchers.IO){
        val db = getDatabase(context)
            db.runInTransaction{
            runBlocking {
                db.clearAllTables()
                db.dataDao().resetPrimaryKey()
                db.profilesDao().resetPrimaryKey()
                db.SettingsDao().resetPrimaryKey()
                db.TextTemplatesDao().resetPrimaryKey()
                db.TagsDao().resetPrimaryKey()
            }
        }
    }

}
