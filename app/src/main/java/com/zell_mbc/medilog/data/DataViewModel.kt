/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.app.Application
import android.content.SharedPreferences
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.compose.runtime.State
import androidx.core.content.FileProvider
import androidx.lifecycle.*
import androidx.preference.PreferenceManager
import androidx.sqlite.db.SimpleSQLiteQuery
import com.zell_mbc.medilog.MainActivity.Companion.DatePattern
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_DELIMITER
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import com.zell_mbc.medilog.MainActivity.Companion.Filter
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import kotlinx.coroutines.flow.Flow
import java.text.DecimalFormatSymbols

abstract class DataViewModel(application: Application, val dataType: Int): AndroidViewModel(application) {
    val app = application

    abstract val filterStartPref: String
    abstract val filterEndPref: String
    abstract val timeFilterModePref: String
    abstract val rollingFilterValuePref: String
    abstract val rollingFilterTimeframePref: String
    val tagFilterPref = "DIARYTAGFILTER" // Categories are currently only supported for Diary tab

    abstract val itemName: String

    // Retrieve decimal separator for current language
    val decimalSeparator = DecimalFormatSymbols.getInstance().decimalSeparator
    val modifyDecimalSeparator = (decimalSeparator.compareTo('.') != 0)

    var commentSearchCriteria = ""
    private var filterString = "" // Holds string representation of the active filter
    lateinit var dataList: State<List<Data>>

    private var dao     = MediLogDB.getDatabase(app).dataDao()

    private val authority = app.applicationContext.packageName + ".provider"
    private val lineSeparator = System.lineSeparator()
    private val fileName = "MediLog.csv"

    // Filter variables
    var filterStart = 0L
    var filterEnd = 0L
    var timeFilterMode = 0
    var rollingTimeframe = 0
    var rollingValue = 0
    var tagFilter = ""

    var highlightValues = true

    //var editItemIndex = -1 // Position of the item which the edit dialog will lookup

    // Blend in tabs
    abstract var blendInItems: Boolean

    private lateinit var itemList: MutableList<Data>

    // Store data entry fields here so they survive an orientation change
    var value1 = ""
    var value2 = ""
    var value3 = ""
    var value4 = ""
    var comment = ""
    //var category_id = ""
    var tags = ""

    @JvmField
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)
    @JvmField
    val separator = preferences.getString(KEY_PREF_DELIMITER, ",")

    @SuppressLint("SimpleDateFormat")
    var csvPattern = SimpleDateFormat(DatePattern.DATE_TIME)

    // ++++++++++++++++++++++
    // https://medium.com/@jpmtech/intro-to-room-using-jetpack-compose-38d078cdb43d
    fun getAllRecords(dataType: Int = -1): Flow<List<Data>> {
        return if (dataType > 0) dao.getAllRecords(dataType) else dao.getAllRecords()
    }

    // viewModelScope is ok because dao.upsert is a suspend function, w/o suspend -> crash
    fun upsert(data: Data) = viewModelScope.launch(Dispatchers.IO) { dao.upsert(data) }
    fun delete(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        dao.delete(id)
        itemList = getItems("DESC",true) // itemList is not mutable, hence we need to reload
    }

    fun upsertBlocking(item: Data):Long {
        var itemId = -1L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                itemId = dao.upsert(item)
            }
            j.join()
        }
        return itemId
    }

    fun getItem(id: Int): Data? {
        var item: Data? = null
        runBlocking {
            val j = launch(Dispatchers.IO) {
                item = dao.getItem(id)
            }
            j.join()
        }
        return item
    }

    fun getItem(string: String): Data? {
        val query = SimpleSQLiteQuery(string)
        var item: Data? = null
        runBlocking {
            val j = launch(Dispatchers.IO) {
                item = dao.getItem(query)
            }
            j.join()
        }
        return item
    }

    fun backup():List<Data> {
        var items = emptyList<Data>()
        runBlocking {
            val j = launch(Dispatchers.IO) {
                items = dao.backup()
            }
            j.join()
        }
        return items
    }


    // Run query which returns an integer result
    fun getInt(string: String): Int {
        val query = SimpleSQLiteQuery(string)
        var i = 0
        runBlocking {
            val j = launch(Dispatchers.IO) {
                i = dao.getInt(query)
            }
            j.join()
        }
        return i
    }

    // Run query which returns a float result
    private fun getFloat(string: String): Float {
        val query = SimpleSQLiteQuery(string)
        var f = 0f
        runBlocking {
            val j = launch(Dispatchers.IO) {
                f = dao.getFloat(query)
            }
            j.join()
        }
        return f
    }

    // Run query which returns a float result
    fun getFloatAsString(string: String, digits: Int = 1): String {
        val f = getFloat(string)
        val format = "%." + digits + "f"
        return format.format(f)
    }
    //**********************************************
    //Viewmodel specific function
    fun query(
        profile: String = activeProfileId.toString(),
        order: Order = Order.DESC
    ): Flow<List<Data>> {
        buildFilterString(profile, order.name )
        return dao.query(SimpleSQLiteQuery(filterString))
    }

    private fun buildFilterString(profile: String= activeProfileId.toString(), order: String = "DESC" ): String {
        // Profile
        var queryText = "SELECT * FROM data WHERE profile_id = $profile"

        // Type
        // Dual purpose flag, for Diary we blend in all others, for any other tab we blend in diary data
        queryText += if (dataType == Tabs.DIARY && blendInItems) " AND comment!=''"
        else {
            if (blendInItems) " AND (type = $dataType OR type = ${Tabs.DIARY})"
            else " AND type = $dataType"
        }
        queryText += compileFilter()

        // Active text search
        // Case-independent
        if (commentSearchCriteria.isNotEmpty()) queryText += " AND comment LIKE '%$commentSearchCriteria%'"

/*        if (dataType == DIARY && tagFilter.isNotEmpty()) {
            val tagArray = tagFilter.split(Delimiter.TAG)
            var tagString = ""
            tagArray.forEach { tagString += if (tagString.isEmpty()) "tags='$it'" else " OR tags='$it'" }
            queryText += " AND ($tagString)"
        }*/

        // Order
        queryText += " ORDER BY timestamp $order"
        filterString = queryText

        return queryText
    }

    fun searchQuery(): Flow<List<Data>> {
        // Profile
        var queryText = "SELECT * FROM data WHERE profile_id = " + activeProfileId.toString()

        // Type
        // Dual purpose flag, for Diary we blend in all others, for any other tab we blend in diary data
        queryText += " AND comment!='' AND type = $dataType"

        // Time filter
        queryText += compileFilter()

        // Active text search
        // Case-independent
        queryText += " AND comment LIKE '%$commentSearchCriteria%'"

        // Order
        queryText += " ORDER BY timestamp DESC"

        return dao.query(SimpleSQLiteQuery(queryText))
    }
    // ++++++++++++++++++++++


    fun filterActive(): Boolean = (timeFilterMode > Filter.OFF) || tagFilter.isNotEmpty()

    // Compile filter string
    fun compileFilter(): String {
        getFilter() // Make sure filter vars are loaded
        var  queryText = ""
        if ((filterStart + filterEnd) > 0L) {
//        if (filterActive) {
            if ((filterStart != 0L) && (filterEnd != 0L)) queryText += " AND timestamp >= $filterStart AND timestamp <= $filterEnd"
            if ((filterStart == 0L) && (filterEnd != 0L)) queryText += " AND timestamp <= $filterEnd"
            if ((filterStart != 0L) && (filterEnd == 0L)) queryText += " AND timestamp >= $filterStart"
        }

        if (dataType == Tabs.DIARY && tagFilter.isNotEmpty()) {
            val tagArray = tagFilter.split(Delimiter.TAG)
            var tagString = ""
            //tagArray.forEach { tagString += if (tagString.isEmpty()) "tags like \"'% $it %'\"" else " OR tags like \"'% $it %'\"" }
            tagArray.forEach { tagString += if (tagString.isEmpty()) "tags like '%$it%'" else " OR tags like '%$it%'" }
            queryText += " AND ($tagString)"
        }

        return queryText
    }



    /////
    // Return value1 summed up by day
    @SuppressLint("SimpleDateFormat")
    fun getToday():Int {
        val today = SimpleDateFormat("yyyyMMdd").format(Date().time)
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = dao.getDay(activeProfileId, dataType, today)
            }
            j.join()
        }
        return i
    }

    // Higher level functions

    open fun getItems(order: String, filtered: Boolean): MutableList<Data> {
        // Dual purpose flag, for Diary we blend in all others, for any other tab we blend in diary data
        var string: String = if (dataType == Tabs.DIARY && blendInItems) "SELECT * FROM data WHERE profile_id = $activeProfileId AND comment!=''"
        else {
            if (blendInItems) "SELECT * FROM data WHERE profile_id = $activeProfileId AND (type = $dataType OR type = ${Tabs.DIARY})"
            else "SELECT * FROM data WHERE profile_id = $activeProfileId AND type = $dataType"
        }
        if (filtered) string += compileFilter()
        string += " ORDER BY timestamp $order"
        //Log.d("GetItems:", "Reloading List $dataType")
        //string = buildFilterString("data", profilesViewModel.activeProfileId.toString(), "timestamp", order )
        return getDataList(string)
    }


    fun getDataList(string: String): MutableList<Data> {
        val query = SimpleSQLiteQuery(string)
        lateinit var items: MutableList<Data>
        runBlocking {
            val j = launch(Dispatchers.IO) {
                items = dao.getDataList(query)
            }
            j.join()
        }
        return items
    }

    fun getFirst(filtered: Boolean = true): Data? {
        var string = "SELECT * from data WHERE profile_id = $activeProfileId AND type = $dataType"
        val order = "ASC"
        if (filtered) string += compileFilter()
        string += " ORDER BY timestamp $order LIMIT 1"
        return getItem(string)
    }

    fun getLast(filtered: Boolean = true): Data? {
        var string = "SELECT * from data WHERE profile_id = $activeProfileId AND type = $dataType"
        val order = "DESC"
        if (filtered) string += compileFilter()
        string += " ORDER BY timestamp $order LIMIT 1"
        return getItem(string)
    }

    fun toStringDate(l: Long): String { return DateFormat.getDateInstance().format(l) }

    @SuppressLint("SimpleDateFormat")
    fun toStringTime(l: Long): String { return SimpleDateFormat("HH:mm").format(l) }

    // Deletes all records
    fun deleteAll() {
        runBlocking {
            val j = launch(Dispatchers.IO) {
                MediLogDB.getDatabase(app).runInTransaction {
                    dao.deleteAll()
                    dao.resetPrimaryKey()
                }
            }
            j.join()
        }
    }

    fun count(): Int {
        val string = "SELECT COUNT(*) FROM data"
        return getInt(string)
    }

    // Get # of records for a datatype
    fun getSize(filtered: Boolean): Int {
        var string = "SELECT COUNT(*) FROM data WHERE profile_id = $activeProfileId AND type= $dataType"
        if (filtered) string += compileFilter()
        return getInt(string)
    }

    // Get # of records for a specific data type
    // Used by user feedback function
    fun getSize(type: Int): Int {
        val string = "SELECT COUNT(*) FROM data WHERE profile_id = $activeProfileId AND type= $type"
        return getInt(string)
    }

    // Min Max functions
    private fun getMinValue(field: String, filtered: Boolean): Float{
        var string = "SELECT MIN(CAST($field as float)) as $field from data WHERE profile_id = $activeProfileId AND type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getFloat(string)
    }

    private fun getMaxValue(field: String, filtered: Boolean): Float{
        var string = "SELECT MAX(CAST($field as float)) as $field from data WHERE profile_id = $activeProfileId AND type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getFloat(string)
    }

    private fun getMinValue(field: String, timeframe: Int): Float {
        val today = Calendar.getInstance()
        today.add(Calendar.DATE, -timeframe)
        val from = today.timeInMillis
        return getFloat("SELECT MAX(CAST($field as float)) as $field from data WHERE profile_id = $activeProfileId AND type=$dataType  AND timestamp>=$from")
    }

    private fun getMaxValue(field: String, timeframe: Int): Float {
        val today = Calendar.getInstance()
        today.add(Calendar.DATE, -timeframe)
        val from = today.timeInMillis
        return getFloat("SELECT MAX(CAST($field as float)) as $field from data WHERE profile_id = $activeProfileId AND type=$dataType  AND timestamp>=$from")
    }

    fun getAvgFloat(field: String, timeframe: Int): Float {
        val today = Calendar.getInstance()
        today.add(Calendar.DATE, -timeframe)
        val from = today.timeInMillis
        val string = "SELECT AVG(CAST($field as float)) as $field from data WHERE profile_id = $activeProfileId AND type=$dataType AND timestamp>=$from"
        return getFloat(string)
    }

    fun getAvgFloat(field: String, filtered: Boolean): Float {
        var string = "SELECT AVG(CAST($field as float)) as $field from data WHERE profile_id = $activeProfileId AND type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getFloat(string)
    }

    fun getAvgInt(field: String, filtered: Boolean): Int {
        var string = "SELECT AVG(CAST($field as int)) as $field from data WHERE profile_id = $activeProfileId AND type=$dataType"

        if (filtered) {
            getFilter()
            string += compileFilter()
        }
        return getInt(string)
    }

    fun getMinValue1(filtered: Boolean): Float = getMinValue("value1", filtered)
    //fun getMinValue2(filtered: Boolean): Float = getMinValue("value2", filtered)
    fun getMaxValue1(filtered: Boolean): Float = getMaxValue("value1", filtered)
    //fun getMaxValue2(filtered: Boolean): Float = getMaxValue("value2", filtered)
    fun getMinValue1(timeframe: Int): Float = getMinValue("value1", timeframe)
    fun getMaxValue1(timeframe: Int): Float = getMaxValue("value1", timeframe)

    // End Min Max Functions

    fun getFilter() = loadFilter()

    fun loadFilter() {
        timeFilterMode = preferences.getInt(timeFilterModePref, Filter.OFF)
        when (timeFilterMode) {
            Filter.OFF -> {
                filterStart = 0L
                filterEnd = 0L
            }
            Filter.STATIC -> {
                filterStart = preferences.getLong(filterStartPref, 0L)
                filterEnd = preferences.getLong(filterEndPref, 0L)
            }
            Filter.ROLLING -> {
                rollingTimeframe = preferences.getInt(rollingFilterTimeframePref, 2)
                rollingValue = preferences.getInt(rollingFilterValuePref, 1)
                filterEnd = 0

                val today = Calendar.getInstance()
                when (rollingTimeframe) {
                    0 -> today.add(Calendar.DATE,-rollingValue)
                    1 -> today.add(Calendar.DATE,-rollingValue * 7)
                    2 -> today.add(Calendar.DATE,-rollingValue * 30)
                    3 -> today.add(Calendar.DATE,-rollingValue * 365)
                }
                filterStart = today.timeInMillis
            }
        }
        tagFilter = preferences.getString(tagFilterPref, "").toString()

    }

    fun setFilter(start: Long, end: Long, tagIds: String = "") {
        val editor = preferences.edit()
        editor.putInt(timeFilterModePref, timeFilterMode)
        editor.putInt(rollingFilterTimeframePref, rollingTimeframe)
        editor.putInt(rollingFilterValuePref,rollingValue)

        editor.putLong(filterStartPref, start)
        editor.putLong(filterEndPref, end)
        editor.putString(tagFilterPref, tagIds)
        editor.apply()

        when (timeFilterMode) {
            Filter.OFF -> {
                filterStart = 0L
                filterEnd = 0L
            }
            Filter.ROLLING -> {
                val today = Calendar.getInstance()
                when (rollingTimeframe) {
                    0 -> today.add(Calendar.DATE,-rollingValue)
                    1 -> today.add(Calendar.DATE,-rollingValue * 7)
                    2 -> today.add(Calendar.DATE,-rollingValue * 30)
                    3 -> today.add(Calendar.DATE,-rollingValue * 365)
                }
                filterStart = today.timeInMillis
                filterEnd = 0L
            }
            Filter.STATIC -> {
                filterStart = start
                filterEnd = end
            }
        }

        tagFilter = tagIds
    }


    // Write temporary CSV file
    @SuppressLint("SimpleDateFormat")
    fun writeCsvFile(filtered: Boolean): Uri? {
        val filePath = File(app.cacheDir, "")
        val fn = fileName.replace(".csv", "") + "-" + SimpleDateFormat(DatePattern.DATE).format(Date().time) + ".csv"
        val newFile = File(filePath, fn)

        val uri = FileProvider.getUriForFile(app, authority, newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                val data = dataToCSV(getItems("ASC", filtered))
                out.run {
                    write(data.toByteArray(Charsets.UTF_8))
                    flush()
                    close()
                }
            }
        } catch (_: IOException) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }
        return uri
    }

    fun dataToCSV(items: List<Data>): String {
        if (items.isEmpty()) {
            // This will always be called from a coroutine, hence the looper is required
            Handler(Looper.getMainLooper()).post {Toast.makeText(app, app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show() }
            return ""
        }

        var line = ""
        val sb = StringBuilder()
        val tableFields = getTableColumnNames("data", app)
        for (field in tableFields) {
            if (field == "_id") continue // skip _id
            line += field + separator
        }
        line = line.removeSuffix(",")
        line += lineSeparator

        // Create header
        // Compile header line
        //var line = "timestamp$separator profile_id$separator type$separator category_id$separator type$separator value1$separator value2$separator value3$separator value4$separator comment$separator attachment$lineSeparator"
        sb.append(line)

        for (item in items) {
            line = ""
            for (field in tableFields) {
                when (field) {
                    "_id" -> {}
                    "comment"
                    -> {
                        if (item.comment.contains("\n")) item.comment = item.comment.replace("\n","\\n")  // Look for line breaks
                        if (item.comment.contains(Delimiter.STRING)) item.comment = item.comment.replace(Delimiter.STRING.toString(),"&quot;")  // Look for "
                        line += Delimiter.STRING + item.comment + Delimiter.STRING + separator
                    }
                    "timestamp" -> line += csvPattern.format(item.timestamp) + separator
                    "type"        -> line += item.type.toString() + separator
                    "value1"      -> line += item.value1 + separator
                    "value2"      -> line += item.value2 + separator
                    "value3"      -> line += item.value3 + separator
                    "value4"      -> line += item.value4 + separator
                    "attachment"  -> line += item.attachment + separator
                    "profile_id"  -> line += item.profile_id.toString() + separator
                    "tags"        -> line += item.tags + separator
                    "category_id" -> line += "" + separator // ToDo: Legacy, can go once DB schema drops this column

                    // Should never happen! If this shows we are dealing with a bug
                    else -> {
                        Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Field $field not handled?!", Toast.LENGTH_LONG).show() }
                        return sb.toString()
                    }
                }
            }
            line = line.removeSuffix(",")
            line += lineSeparator
            sb.append(line)

//            sb.append(line)
        }
        return sb.toString()
    }

    @SuppressLint("SimpleDateFormat")
    fun savePdfDocument(document: PdfDocument?): Uri? {
        if (document == null) {
            return null
        }
        val filePath = File(app.cacheDir, "")
        val fn = fileName.replace(".csv", "") + "-" + SimpleDateFormat(DatePattern.DATE).format(Date().time) + ".pdf"
        val newFile = File(filePath, fn)
        val uri = FileProvider.getUriForFile(app, authority, newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.run { writeTo(out) }
                out.run {
                    flush()
                    close()
                }
            }
        } catch (_: IOException) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }
        return uri
    }

    fun writeBackup(uri: Uri, zipPassword: String, autoBackup: Boolean = false) {
        if (count() == 0) {
            Toast.makeText(app, app.getString(R.string.emptyDatabase) , Toast.LENGTH_LONG).show()
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            Backup(app, uri, zipPassword).exportZIPFile(autoBackup)
        }
    }

    @SuppressLint("SimpleDateFormat")
    // https://github.com/srikanth-lingala/zip4j
    fun getZIP(document: PdfDocument?, zipPassword: String): Uri? {
        if (document == null) {
            Handler(Looper.getMainLooper()).post {Toast.makeText(app, fileName.replace(".csv", "") + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show() }
            return null
        }

        val uri: Uri?

        val outputStream: OutputStream?
        val zipOutputStream: ZipOutputStream?

        val zipParameters = ZipParameters()
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        // Create empty Zip file
        val zipFileName = app.getString(R.string.appName) + "-" + SimpleDateFormat(DatePattern.DATE).format(Date().time) + ".zip"
        try {
            val filePath = File(app.cacheDir, "")
            val newFile = File(filePath, zipFileName)
            uri = FileProvider.getUriForFile(app, authority, newFile)
            outputStream = app.contentResolver.openOutputStream(uri)
            if (outputStream == null) {
                Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + zipFileName, Toast.LENGTH_LONG).show()
                return null
            }

            zipOutputStream = if (zipPassword.isEmpty()) ZipOutputStream(BufferedOutputStream(outputStream))
            else ZipOutputStream(BufferedOutputStream(outputStream), zipPassword.toCharArray())

        } catch (_: Exception) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + zipFileName, Toast.LENGTH_LONG).show()
            return null
        }

        // File 1
        val fileNames = arrayOfNulls<String>(1)
        fileNames[0] = app.getString(R.string.appName) + "-" + SimpleDateFormat(DatePattern.DATE).format(Date().time) + ".pdf"

        val zipContentOut = ByteArrayOutputStream()
        document.run {
            writeTo(zipContentOut)
            close()
        }

        zipParameters.fileNameInZip = fileNames[0]

        try {
            zipOutputStream.run {
                putNextEntry(zipParameters)
                write(zipContentOut.toByteArray())
                closeEntry()
            }
        } catch (_: Exception) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + fileNames[0], Toast.LENGTH_LONG).show()
            return null
        }

        zipOutputStream.close()

        return uri
    }


    // ------------ Functions to check value validity before saving to the DB
    fun validValueF(value: String, threshold: Float = Threshold.NO_THRESHOLD_FLOAT): Float {
        val checkString = value.replace(decimalSeparator, '.')
        val checkValue = try { checkString.toFloat() } catch (_: Exception) { -1f }

        // Check if value is reasonable
        return when {
            checkValue <= 0f -> -1f
            checkValue > threshold -> -1f
            else -> checkValue
        }
    }

    fun validValueI(value: String, threshold: Int = Threshold.NO_THRESHOLD_INT): Int {
        val checkValue = try { value.toInt() } catch (_: Exception) { -1 }
        // Check if value is reasonable
        return when {
            checkValue <= 0 -> -1
            checkValue > threshold -> -1
            else -> checkValue
        }
    }
}