/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.data

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import kotlinx.coroutines.flow.Flow

@Dao
abstract class DataDao {

    @Upsert
    abstract suspend fun upsert(data: Data): Long

    @Query("DELETE from data where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("SELECT * FROM data ORDER BY timestamp DESC")
    abstract fun getAllRecords(): Flow<List<Data>>

    @Query("SELECT * FROM data WHERE type=:type ORDER BY timestamp DESC")
    abstract fun getAllRecords(type: Int): Flow<List<Data>>

    @Query("DELETE FROM data WHERE profile_id =:profile_id")
    abstract fun deleteProfile(profile_id: Int): Int

    @Query("DELETE FROM data")
    abstract fun deleteAll()

    @Query("DELETE FROM sqlite_sequence WHERE name = 'data'")
    abstract fun resetPrimaryKey()

    @Query("DELETE FROM data WHERE type=:dataType")
    abstract fun deleteAll(dataType: Int)

    @Query("DELETE FROM data WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)

    @Query("SELECT * from data where _id = :id")
    abstract fun getItem(id: Int): Data

    @RawQuery(observedEntities = [Data::class])
    abstract fun query(query: SupportSQLiteQuery): Flow<List<Data>>

    // ------------------------

   // @Insert(onConflict = OnConflictStrategy.REPLACE)
   // abstract suspend fun insert(obj: Data): Long

    @Update
    abstract suspend fun update(obj: Data)

    @Query("SELECT SUM(CAST(value1 as int)) as value1 from data WHERE profile_id =:profile and type=:dataType and value2 = :day")
    abstract fun getDay(profile: Int, dataType: Int, day: String): Int

    // ##########################
    @Query("SELECT * from data") // Used by Backup routine
    abstract fun backup(): List<Data>

    @RawQuery
    abstract fun getDataList(query: SimpleSQLiteQuery): MutableList<Data>

    @RawQuery
    abstract fun getInt(query: SimpleSQLiteQuery): Int

    @RawQuery
    abstract fun getFloat(query: SimpleSQLiteQuery): Float

    @RawQuery
    abstract fun getItem(query: SimpleSQLiteQuery): Data

}
