/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.data

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.storage.StorageManager
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withLink
import androidx.compose.ui.unit.dp
import androidx.core.content.getSystemService
import androidx.core.net.toUri
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import androidx.room.Transaction
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKey
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DatePattern
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.MainActivity.Companion.LINK_COLOR
import com.zell_mbc.medilog.MainActivity.Companion.NOT_SET
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.dataFields
import com.zell_mbc.medilog.MainActivity.Companion.profileFields
import com.zell_mbc.medilog.MainActivity.Companion.profilesViewModel
import com.zell_mbc.medilog.MainActivity.Companion.settingsFields
import com.zell_mbc.medilog.MainActivity.Companion.settingsViewModel
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter.STRING
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.tagFields
import com.zell_mbc.medilog.MainActivity.Companion.tagsViewModel
import com.zell_mbc.medilog.MainActivity.Companion.templateFields
import com.zell_mbc.medilog.MainActivity.Companion.textTemplatesViewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_BODY_HEIGHT
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_FAT_MIN_MAX
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_FLUID_THRESHOLD
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_GLUCOSE_THRESHOLDS
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_LOG_FAT
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_OXIMETRY_THRESHOLDS
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_WEIGHT_TARE
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_WEIGHT_THRESHOLD
import com.zell_mbc.medilog.support.MedilogTheme
import com.zell_mbc.medilog.support.determineAvailableTabs
import com.zell_mbc.medilog.support.emptyAttachmentFolder
import com.zell_mbc.medilog.support.getAttachmentFolder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.lingala.zip4j.io.inputstream.ZipInputStream
import net.lingala.zip4j.model.LocalFileHeader
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.Locale
import java.util.Objects

class ImportActivity : AppCompatActivity() {
    data class TableField(var name: String, var pos: Int = NOT_SET)

    var tableFields = arrayListOf<TableField>()

    private lateinit var dataViewModel: DataViewModel
    private lateinit var uri: Uri
    private lateinit var job: Job

    private var showImportDialog = mutableStateOf(true)
    private var headerColumns = 0
    private var pointOfNoReturn = false
    var zipPassword = ""
    private var importDelimiter = ""
    var attachmentsImported = ""
    private var finished = false
    private var canceled = false

    private var messages = mutableStateListOf("")

    private var errorString = mutableStateOf("")
    private var buttonString = mutableStateOf("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataViewModel = MainActivity.viewModel

        val intent: Intent = intent
        val b = intent.extras

        val uriString = b?.getString("URI")
        if (uriString != null) uri = uriString.toUri()

        val pwdString = b?.getString("PWD")
        zipPassword = if (pwdString != null) pwdString else ""

        //supportActionBar?.title = getString(R.string.impor)
        setContent {
            MedilogTheme {
                ImportUI()
                if (pwdString.equals("SIDELOAD")) CheckImportDetails()
            }
        }
        if (!pwdString.equals("SIDELOAD"))
            job = CoroutineScope(Dispatchers.IO).launch { importBackup() }
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            val dir = cacheDir
            deleteDir(dir)
        } catch (_: Exception) { // DO nothing
        }
    }

    fun deleteDir(dir: File): Boolean {
        if (dir.isDirectory()) {
            val children = dir.list()
            if (children != null) {
                children.forEach {
                    val success = deleteDir(File(dir, it))
                    if (!success) return false
                }
            }
            return dir.delete()
        } else if(dir.isFile) {
            //Log.d("Delete Cache: ", dir.name.toString())
            return dir.delete()
        }
        else return false
    }


    @Composable
    fun ImportUI() {
        val state = remember { messages }

        buttonString.value = getString(R.string.cancel)
        val buttonText by remember { buttonString }

        val error by remember { errorString }
        val listState = rememberLazyListState()


        Column(modifier = Modifier.fillMaxWidth().fillMaxHeight().padding(horizontal = 8.dp), horizontalAlignment = Alignment.CenterHorizontally ) {
            LazyColumn(state = listState, modifier = Modifier.fillMaxWidth().padding(horizontal = 8.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                item {
                    Text("")
                    Surface(modifier = Modifier.size(100.dp), color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0f)) {
                        Image(painter = painterResource(id = R.mipmap.ic_launcher_inverted), contentDescription = null)
                    }
                    Text("")
                }
                items(items = state) {
                    Text(text = it, color = MaterialTheme.colorScheme.primary, style = MaterialTheme.typography.bodySmall)
                }
                item {
                    Text("")
                    Button(onClick = { done() }) { Text(buttonText) }
                    Text("")
                    Text(text = error, color = MaterialTheme.colorScheme.error)
                }
            }
        }
    }

    fun messageUpdate(m: String, add: Boolean = true) {
        if (add) messages.add(m)
        else messages[messages.size - 1] = m
        //Log.d("Messages: ", messages.toString())
    }


    private fun importBackup() {
        var localFileHeader: LocalFileHeader?
        var tmpDataItems = ArrayList<Data>()
        var tmpProfileItems = ArrayList<Profiles>()
        var tmpTemplateItems = ArrayList<TextTemplates>()
        var tmpTagItems = ArrayList<Tags>()
        var tmpSettingsItems = ArrayList<Settings>()
        var cleanAttachmentFolder = true
        var attachmentCounter = 0
        val isEncrypted: Boolean?

        messageUpdate(getString(R.string.openZipFile), false) // False, becaue there's already an empty item due to definition

        val zipIn= openZIPFile()

        if (zipIn == null) {
            errorString.value = getString(R.string.eOpenZipFile) + " zipIn == null"
            buttonString.value = getString(R.string.done)
            return
        }

        // Parse through files
        try {
            localFileHeader = zipIn.nextEntry
            isEncrypted = localFileHeader?.isEncrypted()
        } catch (e: Exception) {
            errorString.value = getString(R.string.eOpenZipFile) + e.message
            buttonString.value = getString(R.string.done)
            return
        }

        if (localFileHeader == null) {
            val msg = getString(R.string.eOpenZipFile) + " localFileHeader == null"
            errorString.value = msg
            buttonString.value = getString(R.string.done)
            return
        }
        messageUpdate("✓ " + messages[messages.size-1] + "(" + if (isEncrypted == true) getString(R.string.protected_) + ")" else getString(R.string.unprotected) + ")", false)

        var attachmentMessage = 0
        var dataFileName = localFileHeader.fileName

        if (localFileHeader.fileName.contains(regex = Regex("MediLog-([0-9]{4})-([0-9]{2})-([0-9]{2}).csv")))
            dataFileName = backupFileNames[0] // Make sure legacy data file is processed as well
        messageUpdate("-")

        while (localFileHeader != null) {
            when (dataFileName) {
                backupFileNames[0] -> {
                    messageUpdate(getString(R.string.openDataFile))
                    val inputStream = getInputStream(zipIn, localFileHeader) ?: return
                    messageUpdate("✓ " + messages[messages.size - 1], false)

                    messageUpdate(getString(R.string.checkCsvHeader))
                    val reader: BufferedReader = checkDataHeader(inputStream) ?: return
                    messageUpdate("✓ " + messages[messages.size - 1], false)

                    messageUpdate(getString(R.string.readLines))
                    tmpDataItems = readData(reader)
                    val size = tmpDataItems.size
                    messageUpdate(" ✓ " + size.toString() + " " + getString(R.string.linesRead), false)

                    // Should never happen, but just in case
                    if (size == 0) {
                        showError("Imported 0 items = something's wrong, aborting…")
                        return
                    }
                    messageUpdate("-")
                }

                backupFileNames[1] -> {
                    messageUpdate(getString(R.string.openProfilesFile))
                    val inputStream = getInputStream(zipIn, localFileHeader) ?: return
                    messageUpdate("✓     " + messages[messages.size - 1], false)

                    messageUpdate(getString(R.string.checkCsvHeader))
                    val reader: BufferedReader = checkHeader("profiles", inputStream) ?: return
                    messageUpdate("✓ " + messages[messages.size - 1], false)

                    messageUpdate(getString(R.string.readLines))
                    tmpProfileItems = readProfiles(reader)
                    //if (tmpProfileItems.size == 0) return
                    val size = tmpProfileItems.size
                    messageUpdate("✓ " + size.toString() + " " + getString(R.string.linesRead), false)
                    messageUpdate("-")
                }

                backupFileNames[2] -> {
                    messageUpdate(getString(R.string.openTextTemplatesFile))
                    val inputStream = getInputStream(zipIn, localFileHeader) // ?: return
                    messageUpdate("✓ " + messages[messages.size - 1], false)
                    if (inputStream != null) {
                        messageUpdate(getString(R.string.checkCsvHeader))
                        val reader = checkHeader("texttemplates", inputStream) //?: return
                        if (reader != null) {
                            messageUpdate("✓ " + messages[messages.size - 1], false)
                            messageUpdate(getString(R.string.readLines))
                            tmpTemplateItems = readTemplates(reader)
                            //if (tmpTemplateItems.size == 0) return
                            val size = tmpTemplateItems.size
                            messageUpdate(" ✓ " + size.toString() + " " + getString(R.string.linesRead), false)
                        } else messageUpdate("Empty header! Nothing imported", false)
                    } else messageUpdate("No Template file in backup", false)
                    messageUpdate("-")
                }

                backupFileNames[3] -> {
                    messageUpdate(getString(R.string.openTagsFile))
                    val inputStream = getInputStream(zipIn, localFileHeader) //?: return
                    if (inputStream != null) {
                        messageUpdate("✓ " + messages[messages.size - 1], false)

                        messageUpdate(getString(R.string.checkCsvHeader))
                        val reader = checkHeader("tags", inputStream) //?: return
                        if (reader != null) {
                            messageUpdate("✓ " + messages[messages.size - 1], false)

                            messageUpdate(getString(R.string.readLines))
                            tmpTagItems = readTags(reader)
                            //if (tmpTagItems.size == 0) return
                            val size = tmpTagItems.size
                            messageUpdate(" ✓ " + size.toString() + " " + getString(R.string.linesRead), false)
                        }
                        else messageUpdate("Empty header! Nothing imported", false)
                    }
                    else messageUpdate("No Tags file in backup", false)
                    messageUpdate("-")
                }

                backupFileNames[4] -> {
                    messageUpdate(getString(R.string.openSettingsFile))
                    val inputStream = getInputStream(zipIn, localFileHeader) //?: return
                    if (inputStream != null) {
                        messageUpdate("✓ " + messages[messages.size - 1], false)

                        messageUpdate(getString(R.string.checkCsvHeader))
                        val reader = checkHeader("settings", inputStream) //?: return
                        if (reader != null) {
                            messageUpdate("✓ " + messages[messages.size - 1], false)

                            messageUpdate(getString(R.string.readLines))
                            tmpSettingsItems = readSettings(reader)
                            //if (tmpTagItems.size == 0) return
                            val size = tmpSettingsItems.size
                            messageUpdate(" ✓ " + size.toString() + " " + getString(R.string.linesRead), false)
                        }
                        else messageUpdate("Empty header! Nothing imported", false)
                    }
                    else messageUpdate("No Tags file in backup", false)
                    messageUpdate("-")
                }

                else -> { // Attachment
                    if (attachmentMessage == 0) {
                        messageUpdate("-")
                        messageUpdate(getString(R.string.importAttachments) + " " + (attachmentCounter++).toString())
                        attachmentMessage = messages.size - 1
                    }
                    else messageUpdate(getString(R.string.importAttachments) + " " + (attachmentCounter++).toString(), false)

                    if (cleanAttachmentFolder) {
                        emptyAttachmentFolder(application)
                        cleanAttachmentFolder = false
                    }
                    copyAttachment(zipIn, localFileHeader)
                }
            }
            localFileHeader = zipIn.nextEntry
            dataFileName = if (localFileHeader != null) localFileHeader.fileName else ""
        }
        if (attachmentCounter > 0) messageUpdate(" ✓ " + getString(R.string.attachmentsImported, attachmentCounter.toString()), false)

        // Point of no return
        pointOfNoReturn = true
        //messageUpdate("-")
        messageUpdate(getString(R.string.deleteExistingData))
/*        dataViewModel.deleteAll()
        profilesViewModel.deleteAll()
        textTemplatesViewModel.deleteAll()
        tagsViewModel.deleteAll()
        settingsViewModel.deleteAll()
*/
        // ************
        runBlocking {
            val j = launch(Dispatchers.IO) {
                MediLogDB.getDatabase(application).resetDb(application)
            }
            j.join()
        }
        //********

        messageUpdate("✓ " + getString(R.string.dataDeleted), false)
        val dataString = getString(R.string.data).replaceFirstChar { it.titlecase(Locale.ROOT) }
        messageUpdate("$dataString: " + getString(R.string.addRecords))
        if (tmpDataItems.size > 0) bulkDataInsert(tmpDataItems)
        messageUpdate("✓ $dataString: " + getString(R.string.recordsAdded, tmpDataItems.size.toString()), false)

        messageUpdate(getString(R.string.profiles) + ": " + getString(R.string.addRecords))

        // Profile handling
        if (tmpProfileItems.size == 0) { // No profiles in backup
            messageUpdate("✓ " + getString(R.string.createdNewDefaultProfile),false)
            val newDefaultProfile = Profiles(_id = 0, name = getString(R.string.userNameDefault)) // 0 will make Room create a new record/key
            profilesViewModel.preferenceToProfile(newDefaultProfile) // Create new default profile based on active preferences
            tmpProfileItems.add(newDefaultProfile)
        }
        bulkProfileInsert(tmpProfileItems)
        messageUpdate("✓ " + getString(R.string.profiles) + ": " + getString(R.string.recordsAdded, tmpProfileItems.size.toString()), false)

        //  profilesViewModel.setActiveProfile(activeProfile)

        // Text Templates
        messageUpdate( getString(R.string.textTemplates) + ": " + getString(R.string.addRecords))
        bulkTemplateInsert(tmpTemplateItems)
        messageUpdate("✓ " + getString(R.string.textTemplates) + ": " + getString(R.string.recordsAdded, tmpTemplateItems.size.toString()), false)

        // Tags
        messageUpdate( getString(R.string.tags) + ": " + getString(R.string.addRecords))
        bulkTagInsert(tmpTagItems)
        messageUpdate("✓ " + getString(R.string.tags) + ": " + getString(R.string.recordsAdded, tmpTagItems.size.toString()), false)

        // Settings
        messageUpdate( getString(R.string.settings) + ": " + getString(R.string.addRecords))
        bulkSettingsInsert(tmpSettingsItems)
        messageUpdate("✓ " + getString(R.string.settings) + ": " + getString(R.string.recordsAdded, tmpSettingsItems.size.toString()), false)

        // Can't work with db below because the db updates may not be completed yet
        // Set active profile -
        var temp = ""
        for (item in tmpSettingsItems) {
            if (item.profile_id == 0 && item._key == "activeProfile") {
                temp = item.value
                break
            }
        }
        activeProfileId = try { temp.toInt() } catch (e: NumberFormatException) { tmpProfileItems[0]._id }

        // Update preferences with profile data
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = preferences.edit()

        for (item in tmpSettingsItems) {
            if (item.profile_id == activeProfileId) {
                when (item._key) {
                    KEY_PREF_LOG_FAT -> editor.putBoolean(KEY_PREF_LOG_FAT, item.value.toBoolean())
                    KEY_PREF_BODY_HEIGHT -> editor.putString(KEY_PREF_BODY_HEIGHT, item.value)
                    KEY_PREF_WEIGHT_THRESHOLD -> editor.putString(KEY_PREF_WEIGHT_THRESHOLD, item.value)
                    KEY_PREF_WEIGHT_TARE -> editor.putString(KEY_PREF_WEIGHT_TARE, item.value)
                    KEY_PREF_FAT_MIN_MAX -> editor.putString(KEY_PREF_FAT_MIN_MAX, item.value)
                    KEY_PREF_GLUCOSE_THRESHOLDS -> editor.putString(KEY_PREF_GLUCOSE_THRESHOLDS, item.value)
                    KEY_PREF_OXIMETRY_THRESHOLDS -> editor.putString(KEY_PREF_OXIMETRY_THRESHOLDS, item.value)
                    KEY_PREF_FLUID_THRESHOLD -> editor.putString(KEY_PREF_FLUID_THRESHOLD, item.value)
                    "activeTabs" -> editor.putString("activeTabs", item.value)
                }
            }
        }
        editor.apply()
        // ------------- Done with import -------------
        
        // Set activeTabs based on imported data per profile
        determineAvailableTabs(tmpDataItems, tmpProfileItems)

        // Check imported Glucose data for default unit
        var sampleValue = ""
        for (item in tmpDataItems)
            if (item.type == Tabs.GLUCOSE) {
                sampleValue = item.value1
                break
            }

        if (sampleValue.isNotEmpty()) {
            val u = if (sampleValue.toFloat() > 10) getString(R.string.GLUCOSE_UNIT_MG_DL) else getString(R.string.GLUCOSE_UNIT_MMOL_L)
            editor.putString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, u).apply()
        }

        buttonString.value = getString(R.string.done) //+ " Size: " + messages.size.toString()
        finished = true
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun CheckImportDetails() {
        val existingRecords = dataViewModel.count()
        var importDialogTrigger by remember { showImportDialog }
        var checkedState by remember { mutableStateOf(false) }
        val uriHandler = LocalUriHandler.current

        if (importDialogTrigger)
            BasicAlertDialog(onDismissRequest = { importDialogTrigger = false }) {
                Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.95f)) {
                    Column(modifier = Modifier.padding(16.dp)) {
//                        Text(getString(R.string.action_import), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))
                        Text(getString(R.string.action_import), style = MaterialTheme.typography.headlineMedium)
                        Text("")
                        Text(getString(R.string.importFormat), style = MaterialTheme.typography.bodyMedium)
                        Text(buildAnnotatedString {
                            append("")
                            withLink(LinkAnnotation.Url(url = stringResource(id = R.string.importLink), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(stringResource(id = R.string.importLink)) }
                        }, style = MaterialTheme.typography.bodyMedium, textDecoration = TextDecoration.Underline)

                        Text("")
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Checkbox(checked = checkedState, colors = CheckboxDefaults.colors(), onCheckedChange = {
                                checkedState = it
                                //Log.d("Debug ", selectedProfileIds.size.toString())
                            })
                            Text(getString(R.string.overwriteData), style = MaterialTheme.typography.bodyMedium)
                        }

                        if (checkedState) Text(resources.getString(R.string.thisIsGoing, existingRecords))
                        Text("")
                        Row(modifier = Modifier.align(Alignment.End)) {
                            TextButton(onClick = {
                                importDialogTrigger = false
                                finish()
                            }) { Text(getString(R.string.cancel)) }
                            TextButton(onClick = {
                                importDialogTrigger = false
                                job = CoroutineScope(Dispatchers.IO).launch { importCSV(checkedState) }
                            }) { Text(getString(R.string.action_import)) }
                        }
                    }
                }
            }
    }

    private fun importCSV(deleteData: Boolean) {
        val tmpDataItems: ArrayList<Data>

        messageUpdate(getString(R.string.importCSV))
        messageUpdate("Open Data file")
        val resolver = contentResolver
        val inputStream: InputStream? = resolver.openInputStream(uri)
        messageUpdate("✓ " + messages[messages.size-1], false)

        if (inputStream == null) {
            showError("Imported 0 items = something's wrong, aborting…")
            return
        }

        messageUpdate(getString(R.string.checkCsvHeader))
        val reader: BufferedReader = checkDataHeader(inputStream) ?: return
        messageUpdate("✓ " + messages[messages.size-1], false)

        messageUpdate(getString(R.string.readLines))
        tmpDataItems = readData(reader)

        // Should never happen, but just in case
        if (tmpDataItems.size == 0) {
            showError( "Imported 0 items = something's wrong, aborting…")
            return
        }

        // If profile_id is invalid -> change profile of imported records to whatever the current profile is
        tmpDataItems.forEach { item -> if (item.profile_id <= 0) item.profile_id = activeProfileId }

        messageUpdate(" ✓ " + tmpDataItems.size.toString() + " " + getString(R.string.linesRead), false)
        if (canceled) return

        // Point of no return
        pointOfNoReturn = true
        if (deleteData) {
            messageUpdate(getString(R.string.deleteExistingData))
            dataViewModel.deleteAll()
            messageUpdate("✓ " + getString(R.string.dataDeleted), false)
        }

        messageUpdate(getString(R.string.addRecords))

        if (tmpDataItems.size > 0) {
            bulkDataInsert(tmpDataItems)
            messageUpdate("✓ " + getString(R.string.recordsAdded, tmpDataItems.size.toString()), false)
        }

        // Todo: Check tmpData for types and make sure the tabs are set to active

        buttonString.value = getString(R.string.done)
        finished = true
    }


    // Creates a file handle to be used in import functions
    private fun openZIPFile(): ZipInputStream? {
        val inputStream = contentResolver.openInputStream(uri)
        return try {
            ZipInputStream(inputStream, zipPassword.toCharArray())
        } catch (e: Exception) {
            showError( getString(R.string.eOpenZipFile) + e.message)
            null
        }
    }


    private fun getInputStream(zipIn: ZipInputStream, localFileHeader:LocalFileHeader?): InputStream? {
        var readLen: Int
        val readBuffer = ByteArray(4096)

        var csvStream: InputStream? = null

        // Store file in temporary folder
        //val filePath = File(externalCacheDir, "")
        val filePath = File(getCacheDir(), "") // Using app specific cache dir

        // Check if disk is full
        // https://developer.android.com/training/data-storage/app-specific
        val availableBytes: Long
        val NUM_BYTES_NEEDED = 1024 * 1024 * 1L
        val storageManager = applicationContext.getSystemService<StorageManager>()!!
        val appSpecificInternalDirUuid = storageManager.getUuidForPath(filePath)
        availableBytes = storageManager.getAllocatableBytes(appSpecificInternalDirUuid)

        if (availableBytes < NUM_BYTES_NEEDED) {
            val s = "Insufficient space left on local filesystem! Please free up some space\nAvailable Bytes = $availableBytes"
            showError(s)
            return null
        }

        // Check if external storage is writable
        //val rw = Environment.getExternalStorageState() in setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED)

        val extractedFile: File?
        if (localFileHeader != null) {
            // Catch cases where externalCacheDir is readonly for some reason
            try {
                extractedFile = File(filePath, localFileHeader.fileName)
                // Check extension, if not csv no need to bother
                if (!extractedFile.name.endsWith(".csv")) {
                    showError(getString(R.string.eNoMediLogFile) + localFileHeader.fileName)
                    return null
                }
            }
            catch (e: IOException)
            {
                var availableBytes = 0L
                val storageManager = applicationContext.getSystemService<StorageManager>()!!
                val appSpecificInternalDirUuid= storageManager.getUuidForPath(filePath)
                availableBytes = storageManager.getAllocatableBytes(appSpecificInternalDirUuid)
                val s = "Unable to create temporary file: " + localFileHeader.fileName +
                        if (availableBytes > 0) "\nAvailable Bytes = $availableBytes" else "\n" + e.toString()
                showError(s)
                return null
            }

            val outputStream: OutputStream?
            try {
                outputStream = FileOutputStream(extractedFile)
            }
            catch (e: IOException) {
                // https://developer.android.com/training/data-storage/app-specific
                var availableBytes = 0L
                val NUM_BYTES_NEEDED = 1024 * 1024 * 1L

                val storageManager = applicationContext.getSystemService<StorageManager>()!!
                val appSpecificInternalDirUuid= storageManager.getUuidForPath(filePath)
                availableBytes = storageManager.getAllocatableBytes(appSpecificInternalDirUuid)
                if (availableBytes < NUM_BYTES_NEEDED) {
                    val s = "Insufficient space left in application cache! Please remove temporary files " + localFileHeader.fileName +
                            if (availableBytes > 0) "\nAvailable Bytes = $availableBytes" else "\n" + e.toString()
                    showError(s)
                    /*val storageIntent = Intent().apply {
                        // To request that the user remove all app cache files instead, set
                        // "action" to ACTION_CLEAR_APP_CACHE. ACTION_MANAGE_STORAGE
                        action = ACTION_MANAGE_STORAGE
                    }*/
                    return null
                }

                val s = "Unable to write to temporary file: " + localFileHeader.fileName +
                        if (availableBytes > 0) "\nAvailable Bytes = $availableBytes" else "\n" + e.toString()
                showError(s)
                return null
            }

            readLen = zipIn.read(readBuffer)
            while (readLen != -1) {
                outputStream.write(readBuffer, 0, readLen)
                readLen = zipIn.read(readBuffer)
            }
            outputStream.close()

            csvStream = FileInputStream(extractedFile)
        }
        return csvStream
    }

    private fun checkDataHeader(inStream: InputStream): BufferedReader? {
        val reader = BufferedReader(InputStreamReader(Objects.requireNonNull(inStream)))

        // Valid header?
        val line: String?
        try {
            line = reader.readLine()
        } catch (e: IOException) {
            showError(getString(R.string.eReadError))
            return null
        }

        // Empty file?
        if (line == null) {
            showError(getString(R.string.eNoMediLogFile) + " line == null")
            return null
        }

        // Validate header line
        val headerLine = line.replace("\\s".toRegex(), "") // Remove potential blanks

        for (testDelimiter in getString(R.string.csvSeparators)) {
            if (headerLine.indexOf(testDelimiter) > 0) importDelimiter = testDelimiter.toString()
        }

        // Check if separator exists in headerLine
        if (importDelimiter.isEmpty()) {
            var filterHint = ""
            for (c in getString(R.string.csvSeparators)) filterHint = "$filterHint$c " // Add blanks for better visibility
            showError(getString(R.string.noCsvDelimiterFound) + " $filterHint")
            return null
        }

        mapHeader("data", headerLine)

        return reader
    }

    @SuppressLint("SimpleDateFormat")
    private fun readData(reader: BufferedReader?): ArrayList<Data> {
        val csvPattern = SimpleDateFormat(DatePattern.DATE_TIME)
        var printCounter = 0

        val linesReadString = getString(R.string.linesRead)
        // Load data into buffer
        var lineNo = 0
        var tmpLine = ""
        val tmpItems = ArrayList<Data>()
        if (reader == null) return tmpItems
        try {
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                lineNo++
                if (printCounter == 10) {
                    messageUpdate( "$linesReadString: $lineNo", false)
                    printCounter = 0
                }
                else printCounter++

                // New item with failsave defaults
                // profile_id = 1 in case this backup is from pre-profile times
                val item = Data(0, 0, "", 0, "", "", "", "", "", 1, category_id = -1, tags = NO_TAGS) // Start with empty item
                tmpLine = line.toString()
                if (!tmpLine.endsWith(importDelimiter)) tmpLine += importDelimiter // Add a delimiter to make sure the last value item is not overlooked

                val data = parseCSVLine(tmpLine) //.split(importDelimiter.toRegex()).toTypedArray()

                // If this triggers, a record might still import ok. If not, the other import checks will ring the alert
                if (data.count() < headerColumns)
                    Log.d("MediLog Warning", getString(R.string.warning) + " CSV data columns (" + data.count() + " columns) do not match columns in file header ($headerColumns columns) in line $lineNo: ${System.err}, '$tmpLine'" )
                    //errorString.value = getString(R.string.warning) + " CSV data columns (" + data.count() + " columns) do not match columns in file header ($headerColumns columns) in line $lineNo: ${System.err}, '$tmpLine'"

                for (field in tableFields) { //dataFields) {
                    if ((field.pos == NOT_SET) || (field.pos >= data.size)) continue // Otherwise field.pos will crash below

                    when (field.name) {
                        "timestamp" -> {
                            try {
                                val date: Date? = csvPattern.parse(data[field.pos].trim())
                                if (date != null) item.timestamp = date.time
                            } catch (e: ParseException) {
                                errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "profile_id" -> {
                            try {
                                item.profile_id = data[field.pos].trim().toInt()
                                //if (item.profile_id < 0) item.profile_id = 1// Should never happen, but hey…
                            } catch (e: NumberFormatException) {
                                // Default to profile id = 1
                                item.profile_id = 1
                                //errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                //tmpItems.clear()
                                //return tmpItems
                            }
                        }

                        "tags" -> {
                            try {
                                item.tags = data[field.pos].trim()
                            } catch (e: Exception) { item.tags = NO_TAGS }
                        }

                        "type" -> {
                            try {
                                item.type = data[field.pos].trim().toInt()
                            } catch (e: NumberFormatException) {
                                errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                //Log.d("Debug", "$tmpLine")
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "value1" -> {
                            try {
                                item.value1 = data[field.pos].trim()
                            } catch (e: ParseException) {
                                errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "value2" -> {
                            try {
                                if (item.type == Tabs.FLUID) item.value2 = SimpleDateFormat("yyyyMMdd").format(item.timestamp)
                                else item.value2 = data[field.pos].trim()
                            } catch (e: ParseException) {
                                errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "value3" -> {
                            try {
                                item.value3 = data[field.pos].trim()
                            } catch (e: ParseException) {
                                errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "value4" -> {
                            try {
                                item.value4 = data[field.pos].trim()
                            } catch (e: ParseException) {
                                errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "attachment" -> {
                            try {
                                item.attachment = data[field.pos].trim()
                            } catch (e: ParseException) {
                                errorString.value = "Error setting " + field.name + " in line $lineNo\n${System.err}, '$tmpLine'"
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "comment" -> {
                            val c = data[field.pos].trim()
                            // Look for line breaks
                            if (c.contains("\\n")) item.comment = c.replace("\\n", "\n")
                            else item.comment = c

                            if (c.contains("&quot;")) item.comment = item.comment.replace("&quot;", Delimiter.STRING.toString())  // Look for "
                        }
                    }
                }

                tmpItems.add(item)
            }
        } catch (e: IOException) {
            errorString.value = "IO Exception in line $lineNo\n${System.err}, '$tmpLine'"
            tmpItems.clear() // Delete all items collected so far to prevent loading half a database
            return tmpItems
        }
        return tmpItems
    }

    @Transaction
    fun bulkDataInsert(items: ArrayList<Data>) {
        var percentage = 0
        var count = 0
        val records = items.size
        val div = records / 100

        for (item in items) {
            //job.ensureActive()
            dataViewModel.upsertBlocking(item)
         //   val xx = dataViewModel.backup()
            if (++count >= div) {
                if (percentage < 100) percentage++
                count = 0
                val msg = "$percentage%"
                messageUpdate(getString(R.string.recordsAdded,msg), false)
            }
        }
    }

    fun countOccurrences(s: String, ch: Char): Int {
        return s.filter { it == ch }.count()
    }


    // Parser which respects quotes -> To allow text entry with ,
    fun parseCSVLine(csvLine: String): Array<String> {
        val separator = importDelimiter[0]
        val ARRAY_SIZE = countOccurrences(csvLine,separator)
        val lf = '\n'
        val cr = '\r'
        var quoteOpen = false
        val a = Array(ARRAY_SIZE) { "" }
        var token = ""
        var column = 0
        for (c in csvLine.toCharArray()) when (c) {
            lf,
            cr -> {
                // not required as we are already read line
                quoteOpen = false
                a[column++] = token
                token = ""
            }
            Delimiter.STRING -> quoteOpen = !quoteOpen
            separator -> {
                if (!quoteOpen) {
                    a[column++] = token
                    token = ""
                } else token += c
            }
            else -> token += c
        }
        return a
    }


    private fun copyAttachment(zipIn:ZipInputStream, localFileHeader: LocalFileHeader?): Int {
        var readLen: Int
        val readBuffer = ByteArray(4096)

        // Encryption section
        //val masterKey = MasterKey.Builder(application, MasterKey.DEFAULT_MASTER_KEY_ALIAS).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()

        // Store files in local folder
        val filePath = File(getAttachmentFolder(application), "")
        try {
            val outputStream: OutputStream?

            val newAttachment = File(filePath, localFileHeader!!.fileName)
//    Log.d("Debug", localFileHeader.fileName)
            //outputStream = EncryptedFile.Builder(application, newAttachment, masterKey, EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB).build().openFileOutput()
            outputStream = newAttachment.outputStream()

            readLen = zipIn.read(readBuffer)
            while (readLen != -1) {
                outputStream.write(readBuffer, 0, readLen)
                readLen = zipIn.read(readBuffer)
            }
            outputStream.close()
        } catch (e: Exception) {
            showError(getString(R.string.eReadZipFile) + e.message)
            return -1
        }
        return 0
    }

    // Profiles + Templates + Tags
    private fun checkHeader(table: String, inStream: InputStream): BufferedReader? {
        val reader = BufferedReader(InputStreamReader(Objects.requireNonNull(inStream)))

        // Valid header?
        val line: String?
        try {
            line = reader.readLine()
        } catch (e: IOException) {
            showError(getString(R.string.eReadError))
            return null
        }

        // Empty file?
        if (line == null) {
        //    showError(getString(R.string.eNoMediLogFile) + " line == null")
            return null
        }

        // Validate header line
        val headerLine = line.replace("\\s".toRegex(), "") // Remove potential blanks

        for (testDelimiter in getString(R.string.csvSeparators)) {
            if (headerLine.indexOf(testDelimiter) > 0) importDelimiter = testDelimiter.toString()
        }

        // Check if separator exists in headerLine
        if (importDelimiter.isEmpty()) {
            var filterHint = ""
            for (c in getString(R.string.csvSeparators)) filterHint = "$filterHint$c " // Add blanks for better visibility
            showError(getString(R.string.noCsvDelimiterFound) + " $filterHint")
            return null
        }

        mapHeader(table, headerLine)
        return reader
    }

    // Have we got all fields we need?
    fun mapHeader(table: String, header: String) {
        //val dbTableFields = getTableColumnNames(table, this)
        val dbTableFields = when (table) {
            "profiles" -> profileFields
            "texttemplates" -> templateFields
            "tags" -> tagFields
            "settings" -> settingsFields
            else -> dataFields
        }

        tableFields.clear()

        // Make sure there are no trailing delimiters leading to wrong column count
        val newHeader =
            if (header.endsWith(importDelimiter)) header.substring(0, header.length - 1)
            else header

        val receivedColumns = newHeader.split(importDelimiter)
        headerColumns = receivedColumns.count()
        for (field in dbTableFields) {
            tableFields.add(TableField(pos = receivedColumns.indexOf(field.name), name = field.name))
        }
    }

    private fun readProfiles(reader: BufferedReader?): ArrayList<Profiles> {
        // Load data into buffer
        val tmpItems = ArrayList<Profiles>()
        if (reader == null) return tmpItems
        var line: String?
        var lineNo = 0

        try {
            var tmpLine: String
            while (reader.readLine().also { line = it } != null) {
                //yield()
                lineNo++

                val item = Profiles(_id = 0, name = "") // Start with empty item

                tmpLine = line.toString()
                if (!tmpLine.endsWith(importDelimiter))
                    tmpLine = line.toString() + importDelimiter // Add a delimiter to make sure the last value item is not overlooked

                val profile = parseCSVLine(tmpLine) //.split(importDelimiter.toRegex()).toTypedArray()

                // Further integrity checks
                /*if (profile.count() != headerColumns) { // Header don't match actual data
                    showError("CSV file header ($headerColumns columns) do not match data columns (" + profile.count() + " columns) in line $lineNo: ${System.err}, '$tmpLine'")
                    tmpItems.clear()
                    return tmpItems
                }*/

                for (field in tableFields) {
                    if ((field.pos == NOT_SET) || (field.pos >= profile.size)) continue // Otherwise field.pos will crash below
                    //Log.d("Debug", field.name)
                    when(field.name) {
                        "_id" -> {
                            try {
                                item._id = profile[field.pos].trim().toInt()
                            } catch (e: NumberFormatException) {
                                showError("Error setting " + field.name + " in line $lineNo: ${System.err}, '$tmpLine'")
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "name" -> {
                            try {
                                item.name = profile[field.pos].trim()
                            } catch (e: ParseException) {
                                showError("Error setting " + field.name + " in line $lineNo: ${System.err}, '$tmpLine'")
                                tmpItems.clear()
                                return tmpItems
                            }
                        }

                        "description" -> {
                            try {
                                item.description = profile[field.pos].trim()
                            } catch (e: ParseException) {
                                showError("Error setting " + field.name + " in line $lineNo: ${System.err}, '$tmpLine'")
                                tmpItems.clear()
                                return tmpItems
                            }
                        }
                    } // End when
                } // End for field
                tmpItems.add(item)
            } // End while
        } catch (e: IOException) {
            showError("IO exception while reading file line $lineNo: ${System.err}")
            tmpItems.clear()
            return tmpItems
        }
        return tmpItems
    }


    private fun readTemplates(reader: BufferedReader?): ArrayList<TextTemplates> {
        // Load data into buffer
        val tmpItems = ArrayList<TextTemplates>()
        if (reader == null) return tmpItems
        var line: String?
        var lineNo = 0

        try {
            var tmpLine: String
            while (reader.readLine().also { line = it } != null) {
                //yield()
                lineNo++

                val item = TextTemplates(_id = 0, type = 0, template = "") // Start with empty item

                tmpLine = line.toString()
                if (!tmpLine.endsWith(importDelimiter))
                    tmpLine = line.toString() + importDelimiter // Add a delimiter to make sure the last value item is not overlooked

                val template = parseCSVLine(tmpLine) //.split(importDelimiter.toRegex()).toTypedArray()

                for (field in tableFields) {
                    if ((field.pos == NOT_SET) || (field.pos >= template.size)) continue // Otherwise field.pos will crash below
                    when(field.name) {
                        "type" ->
                            try { item.type = template[field.pos].trim().toInt() }
                            catch (e: NumberFormatException) { item.type = Tabs.WEIGHT } // Default to weight
                        "template" -> item.template = template[field.pos].trim()
                    } // End when
                } // End for field
                tmpItems.add(item)
            } // End while
        } catch (e: IOException) {
            showError("IO exception while reading file line $lineNo: ${System.err}")
            tmpItems.clear()
            return tmpItems
        }
        return tmpItems
    }

    private fun readTags(reader: BufferedReader?): ArrayList<Tags> {
        // Load data into buffer
        val tmpItems = ArrayList<Tags>()
        if (reader == null) return tmpItems
        var line: String?
        var lineNo = 0

        try {
            var tmpLine: String
            while (reader.readLine().also { line = it } != null) {
                //yield()
                lineNo++

                val item = Tags(_id = 0, type = 0, tag = NO_TAGS, color = "") // Start with empty item

                tmpLine = line.toString()
                if (!tmpLine.endsWith(importDelimiter))
                    tmpLine = line.toString() + importDelimiter // Add a delimiter to make sure the last value item is not overlooked

                val tag = parseCSVLine(tmpLine) //.split(importDelimiter.toRegex()).toTypedArray()

                for (field in tableFields) {
                    if ((field.pos == NOT_SET) || (field.pos >= tag.size)) continue // Otherwise field.pos will crash below
                    //Log.d("Debug", field.name)
                    when(field.name) {
                        "type" -> {
                            try { item.type = tag[field.pos].trim().toInt() }
                            catch (e: NumberFormatException) { item.type = 1 } // Fallback to weight
                        }
                        "tag" -> item.tag = tag[field.pos].trim()
                        "color" -> item.color = tag[field.pos].trim()
                    } // End when
                } // End for field
                tmpItems.add(item)
            } // End while
        } catch (e: IOException) {
            showError("IO exception while reading file line $lineNo: ${System.err}")
            tmpItems.clear()
            return tmpItems
        }
        return tmpItems
    }

    private fun readSettings(reader: BufferedReader?): ArrayList<Settings> {
        // Load data into buffer
        val tmpItems = ArrayList<Settings>()
        if (reader == null) return tmpItems
        var line: String?
        var lineNo = 0

        try {
            var tmpLine: String
            while (reader.readLine().also { line = it } != null) {
                //yield()
                lineNo++

                val item = Settings(_id = 0, profile_id = 0, _key = "", value = "") // Start with empty item

                tmpLine = line.toString()
                if (!tmpLine.endsWith(importDelimiter))
                    tmpLine = line.toString() + importDelimiter // Add a delimiter to make sure the last value item is not overlooked

                val setting = parseCSVLine(tmpLine) //.split(importDelimiter.toRegex()).toTypedArray()

                for (field in tableFields) {
                   // if (item._key == "activeTabs")
                   //     Log.d("Medilog", item.value)
                    if ((field.pos == NOT_SET) || (field.pos >= setting.size)) continue // Otherwise field.pos will crash below
                    //Log.d("Debug", field.name)
                    when(field.name) {
                        "profile_id" -> {
                            try { item.profile_id = setting[field.pos].trim().toInt() }
                            catch (e: NumberFormatException) { item.profile_id = activeProfileId } // Fallback to activeProfile
                        }
                        "_key" -> item._key = setting[field.pos].trim()
                        "value" -> item.value = setting[field.pos].trim()
                    } // End when
                } // End for field
                tmpItems.add(item)
            } // End while
        } catch (e: IOException) {
            showError("IO exception while reading file line $lineNo: ${System.err}")
            tmpItems.clear()
            return tmpItems
        }
        return tmpItems
    }


    @Transaction
    fun bulkProfileInsert(items: ArrayList<Profiles>) {
 //       var firstId = -1
        // Handle the fact that Profiles will get new _ids upon getting added to the database
        for (item in items) {
            //yield()
            val newId = profilesViewModel.upsertBlocking(item)
            if (newId == -1L) Log.e("MediLog error", "Unable to add new profile id = $newId")
            val sql = "UPDATE data SET profile_id = $newId WHERE profile_id = " + item._id
            dataViewModel.getInt(sql)
        }
    }

    @Transaction
    fun bulkTemplateInsert(items: ArrayList<TextTemplates>) {
        for (item in items) {
            val newId = textTemplatesViewModel.upsertBlocking(item)
            if (newId == -1L) Log.e("MediLog error", "new TextTemplate id = $newId")
        }
    }

    @Transaction
    fun bulkTagInsert(items: ArrayList<Tags>) {
        // Handle the fact that categories will get new _ids upon getting added to the database!!
        for (item in items) {
            val newId = tagsViewModel.upsertBlocking(item)
            if (newId == -1L) Log.e("MediLog error", "new tag id = $newId")
        }
    }

    @Transaction
    fun bulkSettingsInsert(items: ArrayList<Settings>) {
        // Handle the fact that categories will get new _ids upon getting added to the database!!
        for (item in items) {
            val newId = settingsViewModel.upsertBlocking(item)
            if (newId == -1L) Log.e("MediLog error", "new settings id = $newId")
            if (item._key == "activeTabs") Log.e("MediLog", item.profile_id.toString() + ", " + item._key + ", " + item.value)
        }
    }

    fun done() {
        if (!finished) {
            if (pointOfNoReturn) {
                showError(resources.getString(R.string.cancelImport1))
            } else {
                showError(resources.getString(R.string.cancelImport2))
                //startActivity(Intent(this, MainActivity::class.java)) // Relaunch MainActivity so new data gets picked up
            }
            job.cancel()
        } else {
            finish()
            startActivity(Intent(this, MainActivity::class.java)) // Relaunch MainActivity so new data gets picked up
        }
    }

    fun showError(s: String) {
        Handler(Looper.getMainLooper()).post {
            Toast.makeText(this, s, Toast.LENGTH_LONG).show()
        }
    }
}

