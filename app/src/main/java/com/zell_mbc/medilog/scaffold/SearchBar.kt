/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.scaffold

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.unit.dp
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.support.getCorrectedDateFormat
import java.text.DateFormat
import java.util.Locale

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun MediLogSearchBar(setShowDialog: (Boolean) -> Unit, context: Context, setSelected: (Int) -> Unit) {
    val dateFormat  = getCorrectedDateFormat(context)
    val timeFormat  = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())
    fun formatDateString(m: Long) = dateFormat.format(m) + " - " + timeFormat.format(m)

    var searchBarActive by rememberSaveable { mutableStateOf(false) }
    var searchText by rememberSaveable { mutableStateOf("") }
    val dataList = MainActivity.viewModel.searchQuery().collectAsState(initial = emptyList())
    val focusRequester = remember { FocusRequester() }
    MainActivity.viewModel.commentSearchCriteria = ""

    SearchBar(
        modifier = Modifier.fillMaxWidth().focusRequester(focusRequester),
        leadingIcon = { IconButton(onClick = { setShowDialog(false) }) { Icon(imageVector = Icons.AutoMirrored.Filled.ArrowBack, contentDescription = "Close Search") }  },
        query = searchText,
        onQueryChange = {
            searchText = it
            MainActivity.viewModel.commentSearchCriteria = searchText
        },
        placeholder = { context.getString(R.string.search) },
        onSearch = {
            searchBarActive = false
            setShowDialog(false)
            MainActivity.viewModel.commentSearchCriteria = "" // reset filter
        },
        active = searchBarActive,
        onActiveChange = { searchBarActive = it }
    ) {
        // Search result shown when active
        LazyColumn(
            modifier = Modifier.fillMaxWidth(),
            contentPadding = PaddingValues(16.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            items(dataList.value) { item ->
                Row(modifier = Modifier.height(IntrinsicSize.Min).fillMaxWidth().width(IntrinsicSize.Max).clickable {
                    MainActivity.viewModel.commentSearchCriteria = ""
                    setSelected(item._id)
                    setShowDialog(false)
                })
                {
                    Text(formatDateString(item.timestamp), modifier = Modifier.padding(end = 8.dp), style = MaterialTheme.typography.bodyMedium)
                    Text(item.comment, style = MaterialTheme.typography.bodyMedium)
                }
            }
        }
    }
    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }
}

/*
@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun MediLogSearchBar(setShowDialog: (Boolean) -> Unit, context: Context, setSelected: (Int) -> Unit) {
    val dateFormat  = getCorrectedDateFormat(context)
    val timeFormat  = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())
    fun formatDateString(m: Long) = dateFormat.format(m) + " - " + timeFormat.format(m)

    var searchBarActive by rememberSaveable { mutableStateOf(false) }
    var searchText by rememberSaveable { mutableStateOf("") }
    val dataList = MainActivity.viewModel.searchQuery().collectAsState(initial = emptyList())
    val focusRequester = remember { FocusRequester() }
    viewModel.commentSearchCriteria = ""
    var expanded by rememberSaveable { mutableStateOf(false) }

    SearchBar(
        inputField = {
            SearchBarDefaults.InputField(
                query = searchText,
                onQueryChange = {
                    searchText = it
                    viewModel.commentSearchCriteria = it
                },
                onSearch = {
                    searchBarActive = false
                    setShowDialog(false)
                    viewModel.commentSearchCriteria = "" // reset filter
                },
                expanded = expanded,
                onExpandedChange = { expanded = it },
                placeholder = { context.getString(R.string.search) },
                leadingIcon = { IconButton(onClick = { setShowDialog(false) }) { Icon(imageVector = Icons.AutoMirrored.Filled.ArrowBack, contentDescription = "Close Search") }  },
                //trailingIcon = { Icon(Icons.Default.MoreVert, contentDescription = null) },
            )
        },
        modifier = Modifier.fillMaxWidth().focusRequester(focusRequester),
        expanded = expanded,
        onExpandedChange = { expanded = it }
    ) {
        // Search result shown when active
        LazyColumn(
            modifier = Modifier.fillMaxWidth(),
            contentPadding = PaddingValues(16.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            items(dataList.value) { item ->
                Row(modifier = Modifier.height(IntrinsicSize.Min).fillMaxWidth().width(IntrinsicSize.Max).clickable {
                    MainActivity.viewModel.commentSearchCriteria = ""
                    setSelected(item._id)
                    setShowDialog(false)
                })
                {
                    Text(formatDateString(item.timestamp), modifier = Modifier.padding(end = 8.dp), style = MaterialTheme.typography.bodyMedium)
                    Text(item.comment, style = MaterialTheme.typography.bodyMedium)
                }
            }
        }
    }
    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }
}
*/
