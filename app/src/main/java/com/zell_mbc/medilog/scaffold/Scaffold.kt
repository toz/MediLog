/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.scaffold

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.zell_mbc.medilog.MainActivity.Companion.availableTabs
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.support.MedilogTheme

// Class to capture the individual tabs
class DataTab(
    val id: Int,
    val label: String,
    var active: Boolean = false,
    var iconActive: Int = R.drawable.ic_weight_outlined,
    var iconInactive: Int = R.drawable.ic_weight_outlined,
    var route: String
)

sealed class Screens(val route : String) {
    data object Weight : Screens("weight_route")
    data object BloodPressure : Screens("bloodpressure_route")
    data object Diary : Screens("diary_route")
    data object Fluid: Screens("fluid_route")
    data object Glucose: Screens("glucose_route")
    data object Temperature: Screens("temperature_route")
    data object Oximetry: Screens("oximetry_route")
    data object Documents: Screens("documents_route")
}

//initializing the data class with default parameters
data class BottomNavigationItem(
    val label : String = "",
    var iconActive : Int  = R.drawable.ic_weight_outlined,
    var iconInactive : Int = R.drawable.ic_weight_outlined,
    val route : String = ""
) {
    //function to get the list of bottomNavigationItems
    fun bottomNavigationItems() : List<BottomNavigationItem> {
        val array = arrayListOf<BottomNavigationItem>()
        for (tab in availableTabs) if (tab.active) array.add(BottomNavigationItem(
                label = tab.label,
                iconActive = tab.iconActive,
                iconInactive = tab.iconInactive,
                route = tab.route))
        return array
    }
}


@Composable
fun NotYetAuthorized() {
    MedilogTheme {
        Column(horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.fillMaxWidth()) {
            Spacer(modifier = Modifier.height(40.dp))
            Surface(
                modifier = Modifier.size(50.dp),
                //shape = CircleShape,
                color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0f)
            ) {
                Image(
                    painter = painterResource(id = R.mipmap.ic_launcher_inverted),
                    contentDescription = null
                )
            }
            Spacer(modifier = Modifier.height(40.dp))
            Text("Waiting for Authentication!",
                style = MaterialTheme.typography.headlineSmall,
                color = MaterialTheme.colorScheme.primary,)
        }
    }
}
/*
fun scaleIntoContainer(
    direction: ScaleTransitionDirection = ScaleTransitionDirection.INWARDS,
    initialScale: Float = if (direction == ScaleTransitionDirection.OUTWARDS) 0.9f else 1.1f
): EnterTransition {
    return scaleIn(
        animationSpec = tween(220, delayMillis = 90),
        initialScale = initialScale
    ) + fadeIn(animationSpec = tween(220, delayMillis = 90))
}

fun scaleOutOfContainer(
    direction: ScaleTransitionDirection = ScaleTransitionDirection.OUTWARDS,
    targetScale: Float = if (direction == ScaleTransitionDirection.INWARDS) 0.9f else 1.1f
): ExitTransition {
    return scaleOut(
        animationSpec = tween(
            durationMillis = 220,
            delayMillis = 90
        ), targetScale = targetScale
    ) + fadeOut(tween(delayMillis = 90))
}

    // Required to move the icons to the end
    // See https://stackoverflow.com/questions/74536496/how-to-specify-arrangement-spaceevenly-in-a-bottomappbar
    @Composable
    fun ModifiedBottomAppBar(
        modifier: Modifier = Modifier,
        containerColor: Color = BottomAppBarDefaults.containerColor,
        contentColor: Color = contentColorFor(containerColor),
        tonalElevation: Dp = BottomAppBarDefaults.ContainerElevation,
        contentPadding: PaddingValues = BottomAppBarDefaults.ContentPadding,
        windowInsets: WindowInsets = BottomAppBarDefaults.windowInsets,
        content: @Composable RowScope.() -> Unit
    ) {
        Surface(
            color = containerColor, contentColor = contentColor, tonalElevation = tonalElevation,
            //shape = ShapeKeyTokens.CornerNone,
            modifier = modifier
        ) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .windowInsetsPadding(windowInsets)
                    //.height(50.0.dp)
                    .padding(contentPadding),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically,
                content = content
            )
        }
    }*/

