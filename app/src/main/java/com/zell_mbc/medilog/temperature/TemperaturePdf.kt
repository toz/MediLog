/*
 *     This file is part of MediLog.
 *
 *     MediLog is free softwar_: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.temperature

import android.app.Activity
import android.graphics.pdf.PdfDocument
import android.widget.Toast
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.checkThresholds

class TemperaturePdf(activity: Activity): BasePdf(activity) {
    // Base class overrides
    override var itemName = activity.getString(R.string.temperature)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_LANDSCAPE, activity.getString(R.string.TEMPERATURE_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_PAPER_SIZE, activity.getString(R.string.TEMPERATURE_PAPER_SIZE_DEFAULT))
    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_BLENDINITEMS, activity.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())
    override var printDaySeparatorLines = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_PDF_DAYSEPARATOR, activity.getString(R.string.DAY_SEPARATOR_LINE_DEFAULT).toBoolean())
    override var highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_TEMPERATURE_HIGHLIGHT_VALUES, false)

    // Temperature specific
    private val temperatureUnit = preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT, activity.getString(R.string.TEMPERATURE_UNIT_DEFAULT))
    private val quantity = "$temperatureUnit  "
    private val warningSign = activity.getString(R.string.warningSign)

    private var warningTab = 0f
    var itemUnit = ""

    override fun createPdfDocument(): PdfDocument? {
        // Check here to be able to return right away in case of error
        if (viewModel.getSize(true) == 0) {
            Toast.makeText(activity, activity.getString(R.string.bloodPressure) + ": " + activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        super.createPdfDocument()

        itemUnit = "" + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT,activity.getString(R.string.TEMPERATURE_CELSIUS))
        val defaultThresholds = if (itemUnit == activity.getString(R.string.TEMPERATURE_CELSIUS)) activity.getString(R.string.TEMPERATURE_THRESHOLDS_CELSIUS_DEFAULT) else activity.getString(R.string.TEMPERATURE_THRESHOLDS_FAHRENHEIT_DEFAULT)
        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_THRESHOLDS, defaultThresholds)
        val th = checkThresholds(activity, s, defaultThresholds, R.string.temperature)
        val lowerThreshold = try { th[0].toFloat() } catch  (_: NumberFormatException) { 0f }
        val upperThreshold = try { th[1].toFloat() } catch  (_: NumberFormatException) { 0f }
    
        setColumns()
        initDataPage()
        var f = pdfDataTop + pdfLineSpacing
        commentWidth = measureColumn(pdfRightBorder - commentTab)
    
        var currentDay = toStringDate(pdfItems[0].timestamp)
    
        for (item in pdfItems) {
            if (printDaySeparatorLines) {
                val itemDay = toStringDate(item.timestamp)
                if (currentDay != itemDay) {
                    f -= (pdfLineSpacing - pdfDaySeparatorLineSpacer) // Go back one line and add half a daySeparatorLineHeight
                    canvas.drawLine(pdfLeftBorder, f, pdfRightBorder, f, pdfPaintDaySeparator)
                    f += (pdfLineSpacing - pdfDaySeparatorLineSpacer)
                    currentDay = itemDay
                }
            }
            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)
            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)
    
            // If this is a blended item, draw comment and be done
            if (item.type != MainActivity.temperatureViewModel.dataType) {
                f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
                continue
            }
    
            val value = try {
                item.value1.toFloat()
            } catch  (_: NumberFormatException) {
                0f
            }
            if (value > upperThreshold || value < lowerThreshold) canvas.drawText(warningSign, warningTab, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
        }
        // finish the page
        document.finishPage(page)
        // Add statistics page
        createPage()
        drawStatsPage()
    
        document.finishPage(page)

        /* Add chart page
        createPage()
        addChartPage()
        document.finishPage(page) */

        return document
    }

/*    fun addChartPage() {
        // Draw individual chart
        val chart = TemperatureChart(activity, chartWidth = pageWidth)
        //…and let super do the bitmap handling
        super.addChartPage(chart)
    }*/

    override fun setColumns() {
        val warningSignWidth = pdfPaintHighlight.measureText(activity.getString(R.string.warningSign))
        val temperatureWidth = pdfPaintHighlight.measureText(quantity)
    
        warningTab = pdfLeftBorder + dateTabWidth + padding
        dataTab = warningTab + warningSignWidth + space
        commentTab = dataTab + temperatureWidth + padding
    }
    
    override fun initDataPage() {
        drawHeader() // VieModel function doesn't need to highlight
    
        // Data section
        canvas.drawText(activity.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(quantity, dataTab, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(warningTab, pdfHeaderBottom, warningTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }
    
    fun drawStatsPage() {
        super.drawStatsPage(R.drawable.ic_outline_device_thermostat_24)

        val timeframeLabel = activity.getString(R.string.timeframeLabel)
        val totalLabel = activity.getString(R.string.totalLabel)
        val annualLabel = activity.getString(R.string.annualLabel)
        val monthLabel = activity.getString(R.string.monthLabel)

        val space = 25
        val monthColumn = pdfPaintHighlight.measureText(timeframeLabel) + space
        val yearColumn = monthColumn + pdfPaintHighlight.measureText(totalLabel) + space
        val totalColumn = yearColumn + pdfPaintHighlight.measureText(annualLabel) + space

        var f = pdfDataTop + pdfLineSpacing
        canvas.drawText(activity.getString(R.string.statistics) + ":", pdfLeftBorder, f, pdfPaintHighlight)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(monthLabel, monthColumn, f, pdfPaint)
        canvas.drawText(annualLabel, yearColumn, f, pdfPaint)
        canvas.drawText(totalLabel, totalColumn, f, pdfPaint)

        collectData(activity, totalData, itemUnit).forEach {
            val row = it.split(";")
            f += pdfLineSpacing
            canvas.drawText(row[0], pdfLeftBorder, f, pdfPaint)
            canvas.drawText(row[1], monthColumn, f, pdfPaint)
            canvas.drawText(row[2], yearColumn, f, pdfPaint)
            canvas.drawText(row[3], totalColumn, f, pdfPaint)
        }
    }
}