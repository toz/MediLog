/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.temperature

import android.app.Activity
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.util.ArrayList
import java.util.Calendar

fun collectData(activity: Activity, arr: ArrayList<String>, unit: String = ""): ArrayList<String> {

    val totalMeasurements = viewModel.getSize(false)

    val now = Calendar.getInstance().time
    val aYearAgo = now.time - 31557600000
    var sql = "SELECT count(*) FROM data WHERE timestamp >= $aYearAgo and type=${viewModel.dataType}"
    val annualMeasurements = viewModel.getInt(sql)

    val aMonthAgo = now.time - 2629800000
    sql = "SELECT count(*) FROM data WHERE timestamp >= $aMonthAgo and type=${viewModel.dataType}"
    val monthlyMeasurements = viewModel.getInt(sql)

    arr.add(activity.getString(R.string.measurementLabel) + ";" +
            monthlyMeasurements.toString() + ";" +
            annualMeasurements.toString() + ";" +
            totalMeasurements.toString()
    )

    arr.add(" ; ; ; ;") // Blank row

    // Total
    sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE type=${viewModel.dataType}"
    val minT = viewModel.getFloatAsString(sql)
    sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE type=${viewModel.dataType}"
    val maxT = viewModel.getFloatAsString(sql)
    sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE type=${viewModel.dataType}"
    val avgT = viewModel.getFloatAsString(sql)

    // Annual
    sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${viewModel.dataType}"
    val minA = viewModel.getFloatAsString(sql)
    sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${viewModel.dataType}"
    val maxA = viewModel.getFloatAsString(sql)
    sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE timestamp >= $aYearAgo and type=${viewModel.dataType}"
    val avgA = viewModel.getFloatAsString(sql)

    // Monthly
    sql = "SELECT MIN(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${viewModel.dataType}"
    val minM = viewModel.getFloatAsString(sql)
    sql = "SELECT MAX(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${viewModel.dataType}"
    val maxM = viewModel.getFloatAsString(sql)
    sql = "SELECT AVG(CAST(value1 as float)) FROM data WHERE timestamp >= $aMonthAgo and type=${viewModel.dataType}"
    val avgM = viewModel.getFloatAsString(sql)

    arr.add(activity.getString(R.string.temperature) + " " + activity.getString(R.string.min) + ";$minM;$minA;$minT $unit")
    arr.add(activity.getString(R.string.max) + ";$maxM;$maxA;$maxT $unit")
    arr.add(activity.getString(R.string.avg) + ";$avgM;$avgA;$avgT $unit")

    return arr
}
