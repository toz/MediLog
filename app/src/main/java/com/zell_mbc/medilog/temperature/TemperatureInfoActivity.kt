/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.temperature

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.base.InfoActivity
import com.zell_mbc.medilog.preferences.SettingsActivity

class TemperatureInfoActivity: InfoActivity() {
    val column1Weight = .4f
    val column2Weight = .2f
    val column3Weight = .2f
    val column4Weight = .2f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[TemperatureViewModel::class.java]
        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT, getString(R.string.TEMPERATURE_UNIT_DEFAULT))

        totalData = collectData(this, totalData, itemUnit)
        setContent { StartCompose() }
    }

    @Composable
    fun ShowBlock() {
        Text(measurementsIn, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
        Text(avgString,      modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
        Text(minMaxString, modifier = Modifier.padding(start = leftPadding.dp),color = MaterialTheme.colorScheme.primary)
        Text(timePeriod, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(8.dp)) {
            HeaderBlock(R.drawable.ic_outline_device_thermostat_24)

            Row(Modifier.background(MaterialTheme.colorScheme.secondaryContainer)) {
                TableCell(text = "", weight = column1Weight)
                TableCell(text = getString(R.string.monthLabel), weight = column2Weight)
                TableCell(text = getString(R.string.annualLabel), weight = column3Weight)
                TableCell(text = getString(R.string.totalLabel), weight = column4Weight)
            }

            LazyColumn(modifier = Modifier.fillMaxSize(1F)) {
                items(totalData) { item ->
                    ShowRow(item)
                }
            }

            /*
            if ((viewModel.filterStart + viewModel.filterStart) > 0L) {
                gatherData(true)
                ShowBlock()
                Text("")
                HorizontalDivider(color = MaterialTheme.colorScheme.secondary, thickness = 1.dp)
                Text("")
            }
            gatherData(false)
            ShowBlock()
            ShowAttachments()*/
            }
    }

    @Composable
    fun ShowRow(s: String) {
        val row = s.split(";")
        Row(Modifier.fillMaxWidth()) {
            TableCell(text = row[0], weight = column1Weight)
            TableCell(text = row[1], weight = column2Weight)
            TableCell(text = row[2], weight = column3Weight)
            TableCell(text = row[3], weight = column4Weight)
        }
    }

    @Composable
    fun RowScope.TableCell(text: String, weight: Float, color: Color = MaterialTheme.colorScheme.primary) {
        Text(text = text, Modifier.weight(weight).padding(cellPadding.dp), color = color)
    }

    private fun gatherData(filtered: Boolean) {
        count = viewModel.getSize(filtered)
        var item: Data? = viewModel.getFirst(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val startDate = dateFormat.format(item.timestamp)

        item = viewModel.getLast(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val endDate = dateFormat.format(item.timestamp)

        measurementsIn = if (!filtered) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"

        val avg = viewModel.getAvgFloat("value1",filtered)
        var tmp = avg.toString()
        if (tmp.length > 4) tmp = tmp.substring(0,4)
        if (modifyDecimalSeparator) tmp = tmp.replace('.', decimalSeparator)
        avgString = getString(R.string.average) + ": $tmp $itemUnit"

        val min = viewModel.getMinValue1(filtered)
        val max = viewModel.getMaxValue1(filtered)
        minMaxString = getString(R.string.minMaxValues) + " $min - $max $itemUnit"
        if (modifyDecimalSeparator) minMaxString = minMaxString.replace('.', decimalSeparator)

        timePeriod = getString(R.string.timePeriod) + " $startDate - $endDate"
    }
}
