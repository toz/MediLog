/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.profiles

import androidx.room.*
import com.zell_mbc.medilog.data.Profiles
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ProfilesDao {
    // ############################################################
    // profiles table
    // ############################################################
    @Upsert
    abstract suspend fun upsert(profiles: Profiles): Long

    @Query("SELECT * FROM profiles ")
    abstract fun getAllRecords(): Flow<List<Profiles>>

    @Query("SELECT * from profiles where _id = :id")
    abstract fun getItem(id: Int): Profiles

    @Query("SELECT count(*) FROM profiles")
    abstract fun count(): Long

    @Query("DELETE from profiles where _id = :id")
    abstract fun delete(id: Int)

    @Query("DELETE FROM sqlite_sequence WHERE name = 'profiles'")
    abstract fun resetPrimaryKey()

    @Query("DELETE from profiles")
    abstract fun deleteAll()

//    @Query("UPDATE profiles SET name=:name, comment=:comment, height=:height, sex=:sex, dob=:dob, weight_thresholds=:weightThresholds, fluid_thresholds=:fluidThresholds, active_tabs=:activeTabs where _id=:id")
    @Query("UPDATE profiles SET name=:name, description=:description where _id=:id")
    abstract suspend fun updateProfile(id: Int, name: String, description: String) //, height: Int, sex: String, dob: Long, weightThresholds: String, fluidThresholds: String, activeTabs: String)

    @Query("SELECT * from profiles ORDER BY _id ASC") // Used by Backup routine
    abstract fun backup(): List<Profiles>

    @Query("SELECT * from profiles LIMIT 1") // Get first profile
    abstract fun getFirst(): Profiles?
}
