/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.profiles

import android.app.Application
import android.content.SharedPreferences
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.profileFields
import com.zell_mbc.medilog.MainActivity.Companion.settingsViewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.MediLogDB
import com.zell_mbc.medilog.data.Profiles
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_BODY_HEIGHT
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_FAT_MIN_MAX
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_GLUCOSE_THRESHOLDS
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_LOG_FAT
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_OXIMETRY_THRESHOLDS
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_TEMPERATURE_THRESHOLDS
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_TEMPERATURE_UNIT
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_FLUID_THRESHOLD
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_WEIGHT_TARE
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_WEIGHT_THRESHOLD
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

const val UNSET = -1

class ProfilesViewModel(application: Application): AndroidViewModel(application) {
    val app = application

    @JvmField
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    @JvmField
    val separator = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, ",")
    private val lineSeparator = System.lineSeparator()

    private val dao = MediLogDB.getDatabase(app).profilesDao()

    fun setActiveProfile(id: Int): Profiles? {
        if (id <= UNSET) return null
        preferenceToProfile(get(activeProfileId)) // Save current preferences
        val newActive = profileToPreference(id)
        if (newActive != null) settingsViewModel.setValue(profile_id = 0,key = "activeProfile",value = id.toString())
        return newActive
    }

    // viewModelScope is ok because dao.upsert is a suspend function, w/o suspend -> crash
    fun upsert(profile: Profiles) = viewModelScope.launch(Dispatchers.IO) { dao.upsert(profile) }
    //fun update(p: Profiles)       = viewModelScope.launch(Dispatchers.IO) { dao.updateProfile(p._id, p.name, p.description) }

    fun delete(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        val dataDao = MediLogDB.getDatabase(app).dataDao()
        val db = MediLogDB.getDatabase(app)

        db.runInTransaction {
            dao.delete(id) // Delete from profiles table
            dataDao.deleteProfile(id) // Delete all data entries associated with this profile
            // The other tables are profile independent
        }
    }

    // DB functions
    fun getAllRecords(): Flow<List<Profiles>> = dao.getAllRecords()

    fun get(id: Int): Profiles? {
        var p: Profiles? = null
        runBlocking {
            val j = launch(Dispatchers.IO) { p = dao.getItem(id) }
            j.join()
        }
        return p
    }


    fun count(): Long {
        var count = 0L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                count = dao.count()
            }
            j.join()
        }
        return count
    }

    // Wait for the new id to be returned
    fun upsertBlocking(p: Profiles): Long {
        var id = -2L
        runBlocking {
            val j = launch(Dispatchers.IO) { id = dao.upsert(p) }
            j.join()
        }
        if (id == -2L) id = p._id.toLong() // If this is an update return current id

        return id
    }

    // Deletes all records
    fun deleteAll() {
        runBlocking {
            val j = launch(Dispatchers.IO) {
                MediLogDB.getDatabase(app).runInTransaction {
                    dao.deleteAll()
                    dao.resetPrimaryKey()
                }
            }
            j.join()
        }
    }

    fun backup():List<Profiles> {
        var items = emptyList<Profiles>()
        runBlocking {
            val j = launch(Dispatchers.IO) {
                items = dao.backup()
            }
            j.join()
        }
        return items
    }

    // Return Id of first profile
    fun getFirst(): Int {
        var profileId = -1
        runBlocking {
            val j = launch(Dispatchers.IO) {
                val item = dao.getFirst()
                if (item != null) profileId = item._id
            }
            j.join()
        }

        return profileId // > 0) profileId else -1
    }

    //--------------------------------
    // Profile specific

    // Called after the settings dialog has ended
    fun preferenceToActiveProfile() {
        preferenceToProfile(get(activeProfileId))
    }

    // Update profile with current preferences and sync with database
    // If this is a new profile, new default values will be set
    // Return: id of profile
    fun preferenceToProfile(profile: Profiles?) { //}: Int {
        val app: Application = getApplication()

        if (profile == null) {
            Toast.makeText(getApplication(), "Error: Unable to save settings changes to profile, activeProfile is null!", Toast.LENGTH_LONG).show()
            return
        }

        settingsViewModel.setValue(key = KEY_PREF_WEIGHT_THRESHOLD, value = "" + preferences.getString(KEY_PREF_WEIGHT_THRESHOLD, app.getString(R.string.WEIGHT_THRESHOLD_DEFAULT)))
        settingsViewModel.setValue(key = KEY_PREF_LOG_FAT, value = (preferences.getBoolean(KEY_PREF_LOG_FAT, app.getString(R.string.LOG_FAT_DEFAULT).toBoolean())).toString())
        settingsViewModel.setValue(key = KEY_PREF_BODY_HEIGHT, value = "" + preferences.getString(KEY_PREF_BODY_HEIGHT, "0"))
        settingsViewModel.setValue(key = KEY_PREF_WEIGHT_TARE, value = "" + preferences.getString(KEY_PREF_WEIGHT_TARE, "0"))
        settingsViewModel.setValue(key = KEY_PREF_FAT_MIN_MAX, value = "" + preferences.getString(KEY_PREF_FAT_MIN_MAX, app.getString(R.string.FAT_MIN_MAX_DEFAULT)))

        settingsViewModel.setValue(key = KEY_PREF_TEMPERATURE_THRESHOLDS, value = "" + preferences.getString(KEY_PREF_TEMPERATURE_THRESHOLDS, app.getString(R.string.TEMPERATURE_THRESHOLDS_CELSIUS_DEFAULT)))
        val defaultUnit = if (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, "") == app.getString(R.string.GLUCOSE_UNIT_MMOL_L)) app.getString(R.string.GLUCOSE_UNIT_MMOL_L) else app.getString(R.string.GLUCOSE_UNIT_MG_DL)
        settingsViewModel.setValue(key = KEY_PREF_GLUCOSE_THRESHOLDS, value = "" + preferences.getString(KEY_PREF_GLUCOSE_THRESHOLDS, defaultUnit))
        settingsViewModel.setValue(key = KEY_PREF_OXIMETRY_THRESHOLDS, value = "" + preferences.getString(KEY_PREF_OXIMETRY_THRESHOLDS, app.getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT)))
        settingsViewModel.setValue(key = KEY_PREF_FLUID_THRESHOLD, value = "" + preferences.getString(KEY_PREF_FLUID_THRESHOLD, app.getString(R.string.FLUID_THRESHOLD_DEFAULT)))

        settingsViewModel.setValue(key = "activeTabs", value = "" + preferences.getString("activeTabs",Tabs.ALL_TABS).toString())
    }

    // Copy profile settings to preferences in order for the user to interact with the preference screens rather than building my own
    fun profileToPreference(id: Int): Profiles? {
        val profile = get(id)
        //val xx = settingsViewModel.backup()

        if (profile == null) {
            Toast.makeText(getApplication(), "Severe error. Profile $id not found!", Toast.LENGTH_LONG).show()
            return null
        }

        // Update preferences with profile data
        val editor = preferences.edit()
        editor.putBoolean(KEY_PREF_LOG_FAT, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_LOG_FAT).toBoolean())
        editor.putString(KEY_PREF_BODY_HEIGHT, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_BODY_HEIGHT))
        editor.putString(KEY_PREF_WEIGHT_THRESHOLD, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_WEIGHT_THRESHOLD))
        editor.putString(KEY_PREF_WEIGHT_TARE, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_WEIGHT_TARE))
        editor.putString(KEY_PREF_FAT_MIN_MAX, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_FAT_MIN_MAX))

        editor.putString(KEY_PREF_GLUCOSE_THRESHOLDS, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_GLUCOSE_THRESHOLDS))
        editor.putString(KEY_PREF_OXIMETRY_THRESHOLDS, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_OXIMETRY_THRESHOLDS))
        editor.putString(KEY_PREF_FLUID_THRESHOLD, settingsViewModel.getValue(profile_id = id, key = KEY_PREF_FLUID_THRESHOLD))

        //val t = settingsViewModel.backup()
        editor.putString("activeTabs", settingsViewModel.getValue(profile_id = id, key = "activeTabs"))
        //val tt = settingsViewModel.getValue(profile_id = id, key = "activeTabs")
        editor.apply()
        return profile
    }

    // Create new profile with default values
    fun createNewProfile(name: String = app.getString(R.string.userNameDefault), description: String = ""): Profiles {
        val profile = Profiles(_id = 0) //

        profile.name = name
        profile.description = description
        val newId = upsertBlocking(profile).toInt()
        profile._id = newId

        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_LOG_FAT, value = app.getString(R.string.LOG_FAT_DEFAULT))
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_BODY_HEIGHT, value = "0")
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_WEIGHT_TARE, value = "0")
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_FAT_MIN_MAX, value = app.getString(R.string.FAT_MIN_MAX_DEFAULT))
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_WEIGHT_THRESHOLD, value = app.getString(R.string.WEIGHT_THRESHOLD_DEFAULT))

        val defaultThreshold = if (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, "") == app.getString(R.string.GLUCOSE_UNIT_MMOL_L)) app.getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT_MMOL_L) else app.getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT_MG_DL)
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_GLUCOSE_THRESHOLDS, value = defaultThreshold)
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_OXIMETRY_THRESHOLDS, value = app.getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT))
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_FLUID_THRESHOLD, value = app.getString(R.string.FLUID_THRESHOLD_DEFAULT))

        // Different defaults for different units
        val itemUnit = "" + preferences.getString(KEY_PREF_TEMPERATURE_UNIT,app.getString(R.string.TEMPERATURE_CELSIUS))
        val defaultTemp = if (itemUnit == app.getString(R.string.TEMPERATURE_CELSIUS)) app.getString(R.string.TEMPERATURE_THRESHOLDS_CELSIUS_DEFAULT) else app.getString(R.string.TEMPERATURE_THRESHOLDS_FAHRENHEIT_DEFAULT)
        settingsViewModel.setValue(profile_id = newId, key = KEY_PREF_TEMPERATURE_UNIT, value = defaultTemp)
        settingsViewModel.setValue(profile_id = newId, key = "activeTabs", value = Tabs.ALL_TABS)

        return profile
    }

    fun dataToCSV(items: List<Profiles>): String {
        if (items.isEmpty()) {
            // This will always be called from a coroutine, hence the looper is required
            Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show() }
            return ""
        }

        val sb = StringBuilder()

        val tableFields = profileFields  //getTableColumnNames("profiles", app)

    // Compile header line
        var line = ""
        for (field in tableFields) {
            line += field.name + separator
        }
        line += lineSeparator

       // val line2 = "_id$separator name$separator height$separator sex$separator dob$separator weight_threshold$separator fluid_threshold$separator active_tabs$separator comment$separator$lineSeparator"
        sb.append(line)
        //Log.d("Backup", line)

        for (item in items) {
            line = ""
            // Not elegant but this makes sure the field order always matches the header line
            for (field in tableFields) {
                when (field.name) {
                    "_id"                    -> line += item._id.toString() + separator
                    "name"                   -> line += item.name + separator
                    "description"            -> {
                        if (item.description.contains("\n")) item.description = item.description.replace("\n","\\n")  // Look for line breaks
                        if (item.description.contains(Delimiter.STRING)) item.description = item.description.replace(Delimiter.STRING.toString(),"&quot;")  // Look for "
                        line += Delimiter.STRING + item.description + Delimiter.STRING + separator
                    }
/*                    "active_tabs"            -> {
                        // active_tabs are separated by , which conflicts with the CSV delimiter, hence:
                        val tmpActiveTabs = settingsViewModel.getValue(profile_id = item._id, key= field.name).replace(",","_")
                        line += tmpActiveTabs + separator
                    }*/
                    else -> line += settingsViewModel.getValue(profile_id = item._id, key= field.name) + separator

                    // Should never happen! If this shows we are dealing with a bug
//                    else -> Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Field " + field + " not handled?!", Toast.LENGTH_LONG).show() }
                }
            }
            line += lineSeparator
            sb.append(line)
        }
        return sb.toString()
    }
}