/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.profiles

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Profiles
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.settings.SettingsViewModel
import com.zell_mbc.medilog.support.MedilogTheme

class ProfilesActivity: AppCompatActivity() {
    private var selectedItems  = mutableListOf<Int>()
    private var selectedProfileName = ""
    private var selectedProfileId = -1
    private val editMode = mutableStateOf(false)
    private val resetCheckBoxes = mutableStateOf(false)
    
    private var topPadding = 0 // Padding between tab selector and data part, will be calculated later on
    private val cellPadding = 5 // Padding before and after a text in the grid
    private var rowPadding = 1 // Padding tab list rows (top & bottom)
    private val nameFieldWidth = 100.dp
    private var fontSize = 0
    private var showEditDialog = mutableStateOf(false)
    private var showAddDialog = mutableStateOf(false)
    private val showDeleteDialog = mutableStateOf(false)

    private lateinit var listState: LazyListState
    lateinit var preferences: SharedPreferences

    lateinit var profilesViewModel: ProfilesViewModel
    lateinit var settingsViewModel: SettingsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModelProvider = ViewModelProvider(this)
        profilesViewModel      = viewModelProvider[ProfilesViewModel::class.java]
        settingsViewModel      = viewModelProvider[SettingsViewModel::class.java]

        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        //preferences = Preferences.getSharedPreferences(this)

        // Nothing can be done without an active profile, error message already displayed by getActiveProfile()
        val profileObject = profilesViewModel.get(activeProfileId) ?: return

        selectedProfileName = profileObject.name + ""
        selectedProfileId   = profileObject._id
        fontSize = getString(R.string.TEXT_SIZE_DEFAULT).toInt() // Set defined value in case the below fails
        val checkValue = preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, getString(R.string.TEXT_SIZE_DEFAULT))
        if (!checkValue.isNullOrEmpty()) {
            var tmp: Int
            try {
                tmp = checkValue.toInt()
            } catch (_: NumberFormatException) {
                tmp = getString(R.string.TEXT_SIZE_DEFAULT).toInt()
                Toast.makeText(this, "Invalid Font Size value: $checkValue", Toast.LENGTH_LONG).show()
            }
            if (tmp > 0) fontSize = tmp
        }

        supportActionBar?.title = getString(R.string.editProfilesDialog)
        setContent { MedilogTheme { Profiles() } }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(this)
    }


    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun Profiles() {
        val dataList = profilesViewModel.getAllRecords().collectAsState(initial = emptyList())

        val activity = this
        listState = rememberLazyListState()
        var addDialogTrigger by remember { showAddDialog }
        var editDialogTrigger by remember { showEditDialog }
        var deleteDialogTrigger by remember { showDeleteDialog }
        val activateControl = remember { editMode }

        var selection: Profiles? by remember { mutableStateOf(null) }

        Column {
            Row {
                // Show active profile
                Column(modifier = Modifier.padding(start = 16.dp, top = 16.dp)) {
                    Text(getString(R.string.activeProfileName) +": $selectedProfileName", color = MaterialTheme.colorScheme.primary)
                    Text("")
                }
            }
            Row {
                Box {
                    LazyColumn(
                        //state = listState,
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier.padding(top = topPadding.dp, start = 8.dp, end = 8.dp) //.fillMaxHeight() //.verticalScroll(rememberScrollState())
                    ) {
                        //items(profilesViewModel.itemList, { listItem: Profiles -> listItem._id } ) { item ->
                        stickyHeader {
                            Row(modifier = Modifier
                                .fillMaxWidth()
                                .width(IntrinsicSize.Min)
                                .height(IntrinsicSize.Min)
                                .background(MaterialTheme.colorScheme.surface)
                                .clickable { /* */ }, verticalAlignment = Alignment.CenterVertically
                            )
                            { ShowHeader() }
                            HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, thickness = 1.dp)
                        }
                        //items(profileList, { listItem: Profiles -> listItem._id }) { item ->
                        items(dataList.value) { item ->
                            Row(modifier = Modifier
                                .fillMaxWidth()
                                .width(IntrinsicSize.Min)
                                .height(IntrinsicSize.Min)
                                .clickable { selection = item }, verticalAlignment = Alignment.CenterVertically
                            )
                            { ShowRow(item) }

                            if (selection != null) {
                                // Only waste time updating if a different id is selected
                                if (activeProfileId != selection!!._id) {
                                    profilesViewModel.setActiveProfile(selection!!._id)
                                    activeProfileId = selection!!._id
                                    selection = null
                                }
                                activity.finish() // Close current window / activity
                                startActivity(Intent(activity, MainActivity::class.java)) // Relaunch MainActivity so new profile gets picked up
                            }
                        }
                    }
                    Column( modifier = Modifier.fillMaxWidth().align(Alignment.BottomCenter)) {
                        // Bottom buttons
                        Row(modifier = Modifier.fillMaxHeight().align(Alignment.End)) {
                            Column(modifier = Modifier.align(Alignment.Bottom).padding(end = 16.dp, bottom = 16.dp)) {
                                Row(modifier = Modifier.background(MaterialTheme.colorScheme.background)) {
                                    // Add button
                                    TextButton(modifier = Modifier.padding(end = 8.dp), onClick = { addDialogTrigger = true }) { Text(getString(R.string.add)) }
                                    // Edit button
                                    TextButton(modifier = Modifier.padding(end = 8.dp), enabled = activateControl.value,
                                        onClick = {
                                            when (selectedItems.size) {
                                                0 -> Toast.makeText(applicationContext, getString(R.string.noProfileSelected), Toast.LENGTH_LONG).show()
                                                1 -> editDialogTrigger = editDialogTrigger.not()
                                                else -> Toast.makeText(applicationContext, getString(R.string.moreThanOneProfileSelected), Toast.LENGTH_LONG).show()
                                            } }) { Text(getString(R.string.edit)) }

                                    // Delete button
                                    TextButton(onClick = { deleteDialogTrigger = true }, enabled = activateControl.value) { Text(getString(R.string.delete)) }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (addDialogTrigger) { AddDialog() }
        if (editDialogTrigger) EditDialog(selectedItems[0])
        if (deleteDialogTrigger) DeleteProfile()
    }


    @Composable
    fun ShowHeader() {
        val checkedState = remember { mutableStateOf(false) }
        Checkbox(checked = checkedState.value, colors = CheckboxDefaults.colors(), onCheckedChange = {
            checkedState.value = false
        })

        // Name
        Text(getString(R.string.profileName),
            Modifier
                .width(160.dp)
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(nameFieldWidth),
            textAlign = TextAlign.Left, color = MaterialTheme.colorScheme.onSurface, maxLines = 1, overflow = TextOverflow.Ellipsis,
            style = TextStyle(fontFamily = FontFamily.Default, fontSize = fontSize.sp, fontWeight = FontWeight.Bold))
        HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Comment
        Spacer(modifier = Modifier.width(cellPadding.dp))
        Text(getString(R.string.description), color = MaterialTheme.colorScheme.onSurface, fontSize = fontSize.sp,
            style = TextStyle(fontFamily = FontFamily.Default, fontSize = fontSize.sp, fontWeight = FontWeight.Bold))
    }

    @Composable
    fun ShowRow(item: Profiles) {
        // Edit/Delete check box
        val checkedState = remember { mutableStateOf(false) }
        if (resetCheckBoxes.value) checkedState.value = false
        Checkbox(checked = checkedState.value,  colors = CheckboxDefaults.colors(), onCheckedChange = {
            resetCheckBoxes.value = false
            checkedState.value =  it
            if (it) selectedItems.add(item._id)
            else {
                val pos = selectedItems.indexOf(item._id)
                if (pos >= 0) selectedItems.removeAt(pos)
            }
            editMode.value = (selectedItems.size != 0)
        })

        // Name
        val themeColor = MaterialTheme.colorScheme.primary
        Text(item.name,
            Modifier
                .width(160.dp)
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(nameFieldWidth),textAlign = TextAlign.Left, color = themeColor, fontSize = fontSize.sp, maxLines = 1, overflow = TextOverflow.Ellipsis)
        HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Comment
        Spacer(modifier = Modifier.width(cellPadding.dp))
        Text(item.description, color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)
    }

    @Composable
    fun AddDialog() {
        //val profile = profilesViewModel.createNewProfile() //Profiles(_id = 0, name = name.value, comment = comment.value, height = 0, dob = 0L,sex = "", weight_thresholds = "", fluid_thresholds = "", active_tabs = "")
        val name = remember { mutableStateOf("") }
        val description = remember { mutableStateOf("") }

        Dialog(onDismissRequest = { showAddDialog.value = false }) {
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(getString(R.string.addProfileDialog), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))
                    Text("")
                    OutlinedTextField(value = name.value, onValueChange = { name.value = it }, label = { Text(text = getString(R.string.profileName)) }, singleLine = true,
                        modifier = Modifier.padding(vertical = 8.dp).fillMaxWidth())
                    OutlinedTextField(value = description.value, onValueChange = { description.value = it }, label = { Text(text = getString(R.string.description)) },
                        modifier = Modifier.padding(vertical = 8.dp).fillMaxWidth())
                    Text("")
                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showAddDialog.value = false }) { Text(getString(R.string.cancel)) }
                        TextButton(onClick = {
                            if (name.value.isNotEmpty()) {
                                //val newItem = Profiles(0, name.value, comment.value)
                                profilesViewModel.createNewProfile(name = name.value, description = description.value)
                                //profilesViewModel.upsert(newItem)
                                showAddDialog.value = false
                            }
                            else Toast.makeText(applicationContext, getString(R.string.noProfileName), Toast.LENGTH_LONG).show()
                        }) { Text(getString(R.string.save)) }
                    }
                }
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun EditDialog(id: Int) {
        if (selectedItems.size == 0) {
            Toast.makeText(this, "No profiles selected!", Toast.LENGTH_LONG).show()
            showEditDialog.value = false
            return
        }

        val editItem = profilesViewModel.get(selectedItems[0])
        if (editItem == null) {
            Toast.makeText(this, "Entry with id " + selectedItems[0] + " not found!", Toast.LENGTH_LONG).show()
            showEditDialog.value = false
            return
        }

        val name = remember { mutableStateOf(editItem.name) }
        val description = remember { mutableStateOf(editItem.description) }

        BasicAlertDialog(onDismissRequest = { showEditDialog.value = false }) {
            // modifier = Modifier.wrapContentWidth().wrapContentHeight(),
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(getString(R.string.editProfileDialog), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))

                    Text("")
                    OutlinedTextField(value = name.value, onValueChange = { name.value = it }, label = { Text(text = getString(R.string.profileName)) }, singleLine = true,
                        modifier = Modifier.padding(vertical = 8.dp).fillMaxWidth()
                    )
                    OutlinedTextField(value = description.value, onValueChange = { description.value = it }, label = { Text(text = "Comment") }, modifier = Modifier
                        .padding(vertical = 8.dp)
                        .fillMaxWidth()
                    )
                    Text("")

                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showEditDialog.value = false }) { Text(getString(R.string.cancel)) }
                        TextButton(onClick = {
                            // Validate input
                            if (name.value.isNotEmpty()) {
                                editItem.description = description.value
                                editItem.name = name.value

                                profilesViewModel.upsert(editItem)
                                showEditDialog.value = false
                                selectedItems.clear()
                                resetCheckBoxes.value = true
                                editMode.value = false

                                // Did we update the name of the active profile?
                                if (id == selectedProfileId) { //profilesViewModel.getActiveProfile()._id) {
                                    selectedProfileName = editItem.name
                                    val editor = preferences.edit()
                                    editor.putString("PROFILE_NAME", editItem.name).apply()
                                }
                            } else Toast.makeText(applicationContext, getString(R.string.noProfileName), Toast.LENGTH_LONG).show()
                        }) { Text(getString(R.string.save)) }
                    }
                }
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    private fun DeleteProfile() {
        if (selectedItems.size == 0) {
            Toast.makeText(this, "No profile selected!", Toast.LENGTH_LONG).show()
            showDeleteDialog.value = false
            return
        }

        if (profilesViewModel.count() == 1L) {
            Toast.makeText(this, getString(R.string.defaultProfileNeedsToRemain), Toast.LENGTH_LONG).show()
            showDeleteDialog.value = false
            return
        }

        // Determine how many records are associated with this profile
        var sql = "SELECT COUNT(*) FROM data WHERE "
        for (id in selectedItems) if (id > 0) sql += "profile_id=$id OR "
        sql = sql.substring(0, sql.length - 4)
        val records = MainActivity.viewModel.getInt(sql)

        BasicAlertDialog(onDismissRequest = { showDeleteDialog.value = false }) {
            // modifier = Modifier.wrapContentWidth().wrapContentHeight(),
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(getString(R.string.warning), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))
                    Text("")
                    Text(getString(R.string.deleteProfiles  , selectedItems.count(), records) + "\n\n" + getString(R.string.doYouReallyWantToContinue))
                    Text("")
                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showDeleteDialog.value = false }) { Text(getString(R.string.cancel)) }
                        TextButton(onClick = {
                            for (id in selectedItems) {
                                if (id > 0) {
                                    // Delete item in the db
                                    profilesViewModel.delete(id)
                                    // Delete associated settings
                                    settingsViewModel.deleteProfile(id)
                                    // If the currently active profile is getting deleted set the first profile as new active
                                    if (id == selectedProfileId) { //profilesViewModel.activeProfile._id) {
                                        val np = profilesViewModel.get(profilesViewModel.getFirst())
                                        if (np != null) {
                                            settingsViewModel.setActiveProfile(np._id)
                                            selectedProfileName = np.name //profilesViewModel.get(newActive)?.name ?: ""
                                        }
                                    }
                                    showDeleteDialog.value = false
                                }
                            }
                            // All selected items were deleted
                            editMode.value = false
                            selectedItems.clear()
                        }) { Text(getString(R.string.yes)) }
                    }
                }
            }
        }
    }
}
