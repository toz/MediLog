/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.settings

import android.app.Application
import android.content.SharedPreferences
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import androidx.sqlite.db.SimpleSQLiteQuery
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.settingsFields
import com.zell_mbc.medilog.data.MediLogDB
import com.zell_mbc.medilog.data.Settings
import com.zell_mbc.medilog.preferences.SettingsActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class SettingsViewModel(application: Application): AndroidViewModel(application) {
    val app = application
    //var id  = 0

    @JvmField
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)
    //val preferences = Preferences.getSharedPreferences(app)

    private val dao = MediLogDB.getDatabase(app).SettingsDao()

    val separator = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, ",")
    private val lineSeparator = System.lineSeparator()

    // viewModelScope is ok because dao.upsert is a suspend function, w/o suspend -> crash
    fun upsert(setting: Settings) = viewModelScope.launch(Dispatchers.IO) { dao.upsert(setting) }
    fun delete(_id: Int) = viewModelScope.launch(Dispatchers.IO) { dao.delete(_id) }

    // Wait for the new id to be returned
    fun upsertBlocking(s: Settings): Long {
        var id = -1L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                id = dao.upsert(s)
                if (id == -1L) id = s._id.toLong() // If this is an update return current id
            }
            j.join()
        }
        return id
    }

    fun backup(): List<Settings>? {
        var l: List<Settings>? = null
        runBlocking {
            val j = launch(Dispatchers.IO) {
                l = dao.backup()
            }
            j.join()
        }
        return l
    }

    fun getValue(profile_id: Int = activeProfileId, key: String, default: String = ""): String {
        var value: String? = null
        runBlocking {
            val j = launch(Dispatchers.IO) {
                value = dao.getValue(profile_id, key)
            }
            j.join()
        }
        return value ?: default
    }

    private fun getId(profile_id: Int = activeProfileId, key: String): Long {
        var id: Long = -1L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                id = dao.getId(profile_id, key)
            }
            j.join()
        }
        return id
    }

    fun setValue(profile_id: Int = activeProfileId, key: String, value: String): Long {
        val id = getId(profile_id = profile_id, key = key) // Does this setting exist already?
        val setting = Settings(_id = id, profile_id = profile_id, _key = key, value = value)

        runBlocking {
            val j = launch(Dispatchers.IO) {
              if (id > 0L) dao.update(setting)
              else dao.insert(setting)
            }
            j.join()
        }
        return id
    }

    fun setActiveProfile(newId: Int) {
        val editor = preferences.edit()
        editor.putInt("activeProfile", newId).apply()
        setValue(profile_id = 0,key = "activeProfile",value = newId.toString())
    }

    fun deleteProfile(profile_id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            dao.deleteProfile(profile_id)
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            dao.deleteAll()
        }
    }

    fun query(sql: String): Long {
        var ret = -1L
        runBlocking {
            val j = launch(Dispatchers.IO) { ret = dao.query(SimpleSQLiteQuery(sql)) }
            j.join()
        }
        return ret
    }


    // Run query which returns an integer result
    fun getInt(_key: String): Int {
        var s:String? = null
        runBlocking {
            val j = launch(Dispatchers.IO) {
                s = dao.getValue(activeProfileId, _key)
            }
            j.join()
        }
        var ret = -1
        if (s != null)
            ret = try { s!!.toInt() } catch (_: Exception) { -1 }

        return ret
    }


    fun dataToCSV(items: List<Settings>): String {
        if (items.isEmpty()) {
            // This will always be called from a coroutine, hence the looper is required
            //Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show() }
            return ""
        }

        val sb = StringBuilder()

        val tableFields = settingsFields

        // Compile header line
        var line = ""
        for (field in tableFields) {
            line += field.name + separator
        }
        line += lineSeparator

        sb.append(line)
        //Log.d("Backup", line)

        for (item in items) {
            line = ""
            // Not elegant but this makes sure the field order always matches the header line
            for (field in tableFields) { //MainActivity.settingsFields) {
                when (field.name) {
                    "profile_id"           -> line += item.profile_id.toString() + separator
                    "_key"                 -> line += item._key + separator
                    "value"                -> {
                        if (item.value.contains("\n")) item.value = item.value.replace("\n","\\n")  // Look for line breaks
                        if (item.value.contains(Delimiter.STRING)) item.value = item.value.replace(Delimiter.STRING.toString(),"&quot;")  // Look for "
                        line += Delimiter.STRING + item.value + Delimiter.STRING + separator
                    }
                    // Should never happen! If this shows we are dealing with a bug
                    else -> {
                        Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Field $field not handled?!", Toast.LENGTH_LONG).show() }
                        return sb.toString()
                    }
                }
            }
            line += lineSeparator
            sb.append(line)
        }
        return sb.toString()
    }
}
