/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.settings

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import com.zell_mbc.medilog.data.Settings

@Dao

abstract class SettingsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(obj: Settings): Long

    @Update
    abstract fun update(obj: Settings)

    @Upsert
    abstract suspend fun upsert(setting: Settings): Long

    @Transaction
    open fun insertOrUpdate(setting: Settings): Long {
        val id = insert(setting)
        if (id == -1L) update(setting)
        return id
    }

    @Query("SELECT * from settings WHERE profile_id=:profileId AND _key=:key")
    abstract fun getItem(profileId: Int, key: String): Settings?

    @Query("SELECT value from settings WHERE profile_id=:profileId AND _key=:key")
    abstract fun getValue(profileId: Int, key: String): String?

    @Query("SELECT _id from settings WHERE profile_id=:profileId AND _key=:key")
    abstract fun getId(profileId: Int, key: String): Long

    @Query("UPDATE settings SET value=:value where profile_id=:profileId and _key=:key")
    abstract fun set(profileId: Int, key: String, value: String): Int

    @Query("SELECT * from settings ORDER BY profile_id ASC") // Used by Backup routine
    abstract fun backup(): List<Settings>

    @Query("DELETE FROM settings WHERE _id=:id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE FROM settings WHERE profile_id=:profile_id")
    abstract suspend fun deleteProfile(profile_id: Int)

    // Empty Table
    @Query("DELETE FROM settings")
    abstract suspend fun deleteAll()

    @RawQuery
    abstract fun query(query: SimpleSQLiteQuery): Long

    @Query("DELETE FROM sqlite_sequence WHERE name = 'settings'")
    abstract fun resetPrimaryKey()

}
