/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.tags

import android.app.Application
import android.content.SharedPreferences
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.tagFields
import com.zell_mbc.medilog.data.MediLogDB
import com.zell_mbc.medilog.data.Tags
import com.zell_mbc.medilog.preferences.SettingsActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.flow.Flow


class TagsViewModel(application: Application): AndroidViewModel(application) {
    val app = application
    var id  = 0

    @JvmField
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    private val dao = MediLogDB.getDatabase(app).TagsDao()

    val separator = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, ",")
    private val lineSeparator = System.lineSeparator()


    fun getAllRecords(dataType: Int = -1): Flow<List<Tags>> {
        return if (dataType > 0) dao.getAllRecords(dataType) else dao.getAllRecords()
    }

    // viewModelScope is ok because dao.upsert is a suspend function, w/o suspend -> crash
    fun upsert(tag: Tags) = viewModelScope.launch(Dispatchers.IO) { dao.upsert(tag) }
    fun delete(_id: Int) = viewModelScope.launch(Dispatchers.IO) { dao.delete(_id) }

    fun count(dataType: Int = -1): Long {
        var count = 0L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                count = if (dataType > 0) dao.count(dataType) else dao.count()
            }
            j.join()
        }
        return count
    }

    // Wait for the new id to be returned
    fun upsertBlocking(c: Tags): Long {
        var id = -1L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                id = dao.upsert(c)
                if (id == -1L) id = c._id.toLong() // If this is an update return current id
            }
            j.join()
        }
        return id
    }

    // Takes the _id and returns the associated tag id
    fun searchTag(search: String): String {
        for (tag in backup()) if (tag.tag.contains(search)) return tag._id.toString()
        return ""
    }


    // Takes the _id and returns the associated tag item
    fun getItem(_id: Int): Tags? {
        var item: Tags? = null
        runBlocking {
            val j = launch(Dispatchers.IO) {
                item = dao.getItem(_id)
            }
            j.join()
        }
        //val x = backup()
        return item
    }

    // Takes the _id and returns the associated tag item
    fun getItem(idAsString: String): Tags? {
        // Check id
        val _id = try { idAsString.replace(" ","").toInt() }  catch (_: Exception) { -1 }
        return if (_id == -1)  null else getItem(_id)
    }

    // Run query which returns an integer result
    fun getColor(tag: String): String {
        val item = getItem(tag)
        return item?.color ?: ""
    }

    fun backup():List<Tags> {
        var items = emptyList<Tags>()
        runBlocking {
            val j = launch(Dispatchers.IO) {
                items = dao.backup()
            }
            j.join()
        }
        return items
    }

    fun dataToCSV(items: List<Tags>): String {
        if (items.isEmpty()) {
            // This will always be called from a coroutine, hence the looper is required
            //Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show() }
            return ""
        }

        val sb = StringBuilder()

        val tableFields = tagFields

        // Compile header line
        var line = ""
        for (field in tableFields) {
            line += field.name + separator
        }
        line += lineSeparator

        sb.append(line)
        //Log.d("Backup", line)

        for (item in items) {
            line = ""
            // Not elegant but this makes sure the field order always matches the header line
            for (field in tableFields) { //MainActivity.templateFields) {
                line += when (field.name) {
                    "_id"                    -> item._id.toString() + separator
                    "type"                   -> item.type.toString() + separator
                    "tag"               -> item.tag + separator
                    "color"                  -> item.color + separator
                    // Should never happen! If this shows we are dealing with a bug
                    else -> {
                        Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Field $field not handled?!", Toast.LENGTH_LONG).show() }
                        return sb.toString()
                    }
                }
            }
            line += lineSeparator
            sb.append(line)
        }
        return sb.toString()
    }
}
