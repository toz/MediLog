/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.tags

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import com.zell_mbc.medilog.data.Tags
import kotlinx.coroutines.flow.Flow

@Dao
abstract class TagsDao {
    // ############################################################
    // Tags table
    // ############################################################
    @Upsert
    abstract suspend fun upsert(tags: Tags): Long

    @RawQuery(observedEntities = [Tags::class])
    abstract fun query(query: SupportSQLiteQuery): Flow<List<Tags>>

    @Query("SELECT * FROM tags")
    abstract fun getAllRecords(): Flow<List<Tags>>

    @Query("SELECT * FROM tags WHERE type=:type")
    abstract fun getAllRecords(type: Int): Flow<List<Tags>>

    @Query("SELECT count(*) FROM tags WHERE type=:type")
    abstract fun count(type: Int): Long

    @Query("SELECT count(*) FROM tags")
    abstract fun countAll(): Long

    @Query("DELETE from tags where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("SELECT * from tags where _id = :id")
    abstract fun getItem(id: Int): Tags

    @Query("DELETE from tags")
    abstract fun deleteAll()

    @Query("DELETE FROM sqlite_sequence WHERE name = 'tags'")
    abstract fun resetPrimaryKey()

    // -----------------
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertCategory(obj: Tags): Long

    @Query("UPDATE tags SET type=:type, tag=:tag where _id=:id")
    abstract suspend fun updateCategory(id: Int, type: Int, tag: String)

    @Query("SELECT count(*) FROM tags")
    abstract fun count(): Long

    @Query("SELECT * from tags where _id = :id")
    abstract fun getCategoryList(id: Int): MutableList<Tags>

    @Query("SELECT * from tags")
    abstract fun getCategoryList(): MutableList<Tags>

    @Query("SELECT * from tags") // Used by Backup routine
    abstract fun backup(): List<Tags>

    @Query("SELECT * from tags LIMIT 1") // Get first tag
    abstract fun getFirst(): Tags
}
