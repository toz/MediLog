/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.tags

import android.content.Context
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Category
import androidx.compose.material3.AssistChip
import androidx.compose.material3.AssistChipDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.diary.DiaryEditActivity
import com.zell_mbc.medilog.diary.DiaryViewModel

// Show all tags in one or more rows
@OptIn(ExperimentalLayoutApi::class)
@Composable
fun TagRows(context: Context, viewModel: TagsViewModel, tagIds: String, setTagIds: (String) -> Unit, setShowTagsDialog: (Boolean) -> Unit ) {
    // Default values
    var tagName: String
    var backgroundColor = Color.Transparent
    var textColor = MaterialTheme.colorScheme.primary
    var tagsShown = 0 // Track if at least one tag is shown

    val tagStrings = tagIds.split(Delimiter.TAG)

    FlowRow {
        Text(context.getString(R.string.tags) + ": ")
        for (tagString in tagStrings) {
            val tagItem = viewModel.getItem(tagString)
            if (tagItem != null) {
                tagsShown += 1
                tagName = tagItem.tag
                if (tagItem.color.isNotEmpty()) backgroundColor = try {
                    Color(android.graphics.Color.parseColor(tagItem.color))
                } catch (e: Exception) {
                    Color.Transparent
                }
                if (MainActivity.isDarkThemeOn(context) && backgroundColor != Color.Transparent) textColor = MaterialTheme.colorScheme.onPrimary
                AssistChip(
                    modifier = Modifier.height(height = 30.dp),                         // M3 default ch height = 40.dp
                    onClick = { setShowTagsDialog(true) },
                    colors = AssistChipDefaults.assistChipColors( containerColor = backgroundColor, labelColor = textColor),
                    border = if (backgroundColor == Color.Transparent) BorderStroke(1.dp, MaterialTheme.colorScheme.primary) else null,
                    label = { Text(text = tagName, style = MaterialTheme.typography.bodySmall) }
                )
            }
        }
        if (tagsShown == 0) IconButton(onClick = { setShowTagsDialog(true) }) { Icon(imageVector = Icons.Default.Category, contentDescription = context.getString(R.string.tags)) }
        // Delete-all-tags chip
        else AssistChip(
            label = { Text("") },
            modifier = Modifier.height(height = 30.dp),                         // M3 default ch height = 40.dp
            colors = AssistChipDefaults.assistChipColors(containerColor = Color.Transparent),
            onClick = { setTagIds("") },
            border = null,
            leadingIcon = { Icon(modifier = Modifier.height(height = 30.dp), imageVector = Icons.Default.Cancel, contentDescription = context.getString(R.string.tags)) } )
    }
}
