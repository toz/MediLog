/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.tags

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.core.graphics.toColorInt
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.MainActivity.Companion.isDarkThemeOn
import com.zell_mbc.medilog.MainActivity.Companion.tagsViewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Tags
import com.zell_mbc.medilog.dialogs.ColorDialog
import com.zell_mbc.medilog.dialogs.colourSaver
import com.zell_mbc.medilog.dialogs.toHexString

// Dialog which allows to select a text template
class TagsDialog(val context: Context) {
        val colors = listOf(
            Color(0xFFEF9A9A),
            Color(0xFFF48FB1),
            Color(0xFF80CBC4),
            Color(0xFFA5D6A7),
            Color(0xFFFFCC80),
            Color(0xFFFFAB91),
            Color(0xFF81D4FA),
            Color(0xFFCE93D8),
            Color(0xFFB39DDB)
        )

    private val showDeleteDialog = mutableStateOf(false)
    private val showEditDialog = mutableStateOf(false)
    private val showAddDialog = mutableStateOf(false)
    private val editMode  = mutableStateOf(false)
    private var selectedItems  = mutableListOf<Int>()

    private var header = ""
    private var dataType = 0

    @Composable
    fun ShowDialog(selectedTags: String, setShowDialog: (Boolean) -> Unit, setValue: (String) -> Unit) {
        //val controller = LocalSoftwareKeyboardController.current
        dataType = MainActivity.viewModel.dataType
        val dataList = tagsViewModel.getAllRecords(dataType).collectAsState(initial = emptyList())
        selectedItems.clear()

        // Preload selectedItems Array
        if (selectedTags.isNotEmpty()) {
            val tagStrings = selectedTags.split(Delimiter.TAG)
            for (tagId in tagStrings) selectedItems.add(tagId.replace(" ", "").toInt())
            editMode.value = (selectedItems.size != 0)
        }

        val dialogHeader = if (tagsViewModel.count(dataType) > 0) context.getString(R.string.pickTag) else context.getString(R.string.addTag)
        Dialog(onDismissRequest = { setShowDialog(false) }) {
            Surface(shape = RoundedCornerShape(16.dp)) {
                Box( // Bottom box first
                    modifier = Modifier.fillMaxSize().padding(start = 16.dp, end = 16.dp),
                    contentAlignment = Alignment.BottomEnd
                ) {
                    Column(modifier = Modifier.fillMaxWidth()) {
                        HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.secondary) // Lines starting from the top                                    Spacer(modifier = Modifier.height(20.dp))
                        ShowBottomRow()
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.End, verticalAlignment = Alignment.CenterVertically
                        ) {
                            TextButton(onClick = { setShowDialog(false) }) { Text(context.getString(R.string.cancel), color = MaterialTheme.colorScheme.primary) }
                            TextButton(onClick = {
                                setShowDialog(false)
                                setValue(if (selectedItems.size > 0) selectedItems.joinToString(Delimiter.TAG) else "")
                            }) { Text(context.getString(R.string.done), color = MaterialTheme.colorScheme.primary) }
                        }
                    }
                }
                // Content box
                Box(modifier = Modifier.padding(top = 16.dp, start = 16.dp, end = 16.dp, bottom = 35.dp), contentAlignment = Alignment.Center) {
                    Column {
                        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                            Text(text = context.getString(R.string.tags), style = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold))
                        }
                        Spacer(modifier = Modifier.height(8.dp))
                        Row(modifier = Modifier.padding(bottom = 60.dp)) {
                            Column {
                                Text(dialogHeader, color = MaterialTheme.colorScheme.primary)
                                Spacer(modifier = Modifier.height(8.dp))
                                HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.primary) // Lines starting from the top
                                LazyColumn {
                                    items(dataList.value) { item ->
                                        var selected by remember { mutableStateOf(isSelected(item)) }

                                        //ShowItemRow(item)
                                        Row(verticalAlignment = Alignment.CenterVertically) {
//                                            val buttonText = remember { mutableStateOf(isSelected(item)) }
                                            val backgroundColor = textToColor(item.color)
                                            val textColor = if (isDarkThemeOn(context) && backgroundColor != Color.Transparent) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.primary
                                            FilterChip(
                                                onClick = {
                                                    selected = !selected
                                                    val pos = selectedItems.indexOf(item._id)
                                                    if (pos >= 0) selectedItems.removeAt(pos) else selectedItems.add(item._id)
                                                    editMode.value = (selectedItems.size != 0)
                                                },
                                                selected = selected,
                                                colors = FilterChipDefaults.filterChipColors(
                                                    containerColor = backgroundColor, labelColor = textColor,
                                                    selectedContainerColor = backgroundColor, selectedLabelColor = textColor
                                                ),
                                                label = { Text(item.tag) },
                                                leadingIcon = if (selected) { { Icon(imageVector = Icons.Filled.Done, contentDescription = "Done icon") } } else null,
                                                border = if (backgroundColor == Color.Transparent) BorderStroke(1.dp, MaterialTheme.colorScheme.primary) else null
                                            )
                                            Spacer(Modifier.weight(1f))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun ShowBottomRow() {
        var deleteDialogTrigger by remember { showDeleteDialog }
        var editDialogTrigger by remember { showEditDialog }
        var addDialogTrigger by remember { showAddDialog }
        val activateControl = remember { editMode }

        Row {
            IconButton(onClick = { addDialogTrigger = true }) { Icon(imageVector = Icons.Default.Add, contentDescription = "Add") }
            IconButton(onClick = { editDialogTrigger = true }, enabled = activateControl.value ) { Icon(imageVector = Icons.Default.Edit, contentDescription = "Edit") }
            IconButton(onClick = { deleteDialogTrigger = true }, enabled = activateControl.value ) { Icon(imageVector = Icons.Default.Delete, contentDescription = "Delete") }
        }

        if (addDialogTrigger) AddButton()
        if (editDialogTrigger) EditButton()
        if (deleteDialogTrigger) DeleteButton()
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun AddButton() {
        val defaultBackground = Color.Transparent
        var textValue by remember { mutableStateOf("") }
        var backgroundColor by remember { mutableStateOf(defaultBackground) }
        var showColorOptionsDialog by remember { mutableStateOf(false) }
        val textColor = if (isDarkThemeOn(context) && backgroundColor != defaultBackground) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.primary

        BasicAlertDialog(onDismissRequest = { showEditDialog.value = false }) {
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(context.getString(R.string.add) + " " + context.getString(R.string.tag), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))

                    Text("")
                    OutlinedTextField(value = textValue, onValueChange = { textValue = it }, label = { Text(text = context.getString(R.string.tag)) }, modifier = Modifier
                        .padding(vertical = 8.dp)
                        .fillMaxWidth()
                    )
                    Row {
                        TextButton(onClick = { showColorOptionsDialog = true }, colors = ButtonDefaults.buttonColors(contentColor = textColor, containerColor = backgroundColor)) { Text(context.getString(R.string.color)) }
                        IconButton(onClick = { backgroundColor = defaultBackground }) { Icon(Icons.Default.Cancel, contentDescription = null) }
                    }
                    Text("")

                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showAddDialog.value = false }) { Text(context.getString(R.string.cancel)) }
                        TextButton(onClick = {
                            if (textValue.isNotEmpty()) {
                                val newItem = Tags(0, dataType, textValue, backgroundColor.toHexString())
                                tagsViewModel.upsert(newItem)
                                showAddDialog.value = false
                            } else Toast.makeText(context, context.getString(R.string.emptyTag), Toast.LENGTH_LONG).show()
                        }) { Text(context.getString(R.string.save)) }
                    }
                }
            }
            if (showColorOptionsDialog) {
                var currentlySelected by rememberSaveable(saver = colourSaver()) { mutableStateOf(colors[0]) }

                ColorDialog(
                    colorList = colors,
                    onDismiss = { showColorOptionsDialog = false },
                    currentlySelected = currentlySelected,
                    onColorSelected = {
                        currentlySelected = it
                        backgroundColor = it
                    }
                )
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun EditButton() {
        if (selectedItems.size == 0) {
            Toast.makeText(context, "No " + context.getString(R.string.tag) + " selected!", Toast.LENGTH_LONG).show()
            showEditDialog.value = false
            return
        }

        val editItem = tagsViewModel.getItem(selectedItems[0])
        if (editItem == null) {
            Toast.makeText(context, "Entry with id " + selectedItems[0] + " not found!", Toast.LENGTH_LONG).show()
            return
        }

        val defaultBackground = if (editItem.color.isNotEmpty() && editItem.color != Color.Transparent.toHexString())
            textToColor(editItem.color)
            else MaterialTheme.colorScheme.onPrimary
        var textValue by remember { mutableStateOf(editItem.tag) }
        var backgroundColor by remember { mutableStateOf(defaultBackground) }
        val textColor = if (isDarkThemeOn(context) && backgroundColor != MaterialTheme.colorScheme.onPrimary) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.primary
        var showColorOptionsDialog by remember { mutableStateOf(false) }

        BasicAlertDialog(onDismissRequest = { showEditDialog.value = false }) {
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    //val keyboardController = LocalSoftwareKeyboardController.current

                    Text(context.getString(R.string.edit) + " " + context.getString(R.string.tag), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))

                    Text("")
                    OutlinedTextField(value = textValue, onValueChange = { textValue = it }, label = { Text(text = context.getString(R.string.tag)) }, modifier = Modifier
                        .padding(vertical = 8.dp)
                        .fillMaxWidth())
                    Row {
                        TextButton(onClick = { showColorOptionsDialog = true }, colors = ButtonDefaults.buttonColors(contentColor = textColor, containerColor = backgroundColor)) { Text(context.getString(R.string.color)) }
                        IconButton(onClick = { backgroundColor = Color.Transparent }) { Icon(Icons.Default.Cancel, contentDescription = null) }
                    }
                    Text("")

                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showEditDialog.value = false }) { Text(context.getString(R.string.cancel)) }
                        TextButton(onClick = {
                            // Validate input
                            if (textValue.isNotEmpty()) {
                                editItem.tag = textValue
                                editItem.color = backgroundColor.toHexString()
                                tagsViewModel.upsert(editItem)
                                showEditDialog.value = false
                            } else Toast.makeText(context, context.getString(R.string.emptyTag), Toast.LENGTH_LONG).show()
                        }) { Text(context.getString(R.string.save)) }
                    }
                }
            }
             if (showColorOptionsDialog) {
                 var currentlySelected by rememberSaveable(saver = colourSaver()) { mutableStateOf(colors[0]) }

                 ColorDialog(
                    colorList = colors,
                    onDismiss = { showColorOptionsDialog = false },
                    currentlySelected = currentlySelected,
                    onColorSelected = {
                       currentlySelected = it
                       backgroundColor = it
                       }
                )
             }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun DeleteButton() {
        if (selectedItems.size == 0) {
            Toast.makeText(context, "No " + context.getString(R.string.tag) + " selected!", Toast.LENGTH_LONG).show()
            showDeleteDialog.value = false
            return
        }

        val warningText = if (selectedItems.size > 1) context.getString(R.string.thisIsGoing, selectedItems.size) + "\n\n" + context.getString(R.string.doYouReallyWantToContinue)
        else context.getString(R.string.doYouReallyWantToContinue)

        BasicAlertDialog(onDismissRequest = { showDeleteDialog.value = false }) {
            // modifier = Modifier.wrapContentWidth().wrapContentHeight(),
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(context.getString(R.string.delete) + " $header", style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))
                    Text("")
                    Text(warningText)
                    Text("")
                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showDeleteDialog.value = false }) { Text(context.getString(R.string.cancel)) }
                        TextButton(onClick = {
                            for (id in selectedItems) {
                                if (id > 0) {
                                    // Delete item in the db
                                    tagsViewModel.delete(id)
                                }
                            }
                            // All selected items were deleted
                            selectedItems.clear()

                            showDeleteDialog.value = false
                        }) { Text(context.getString(R.string.yes)) }
                    }
                }
            }
        }
    }

    @Composable
    private fun textToColor(s: String): Color {
        return  try { Color(s.toColorInt())
        } catch (_: Exception ) { Color.Transparent } //MaterialTheme.colorScheme.onPrimary }
    }

    fun isSelected(item: Tags): Boolean = (selectedItems.indexOf(item._id) > 0)

}