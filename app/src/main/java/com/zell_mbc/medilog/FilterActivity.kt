/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Clear
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme.colorScheme
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.MenuDefaults
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.Filter
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.glucose.GlucoseViewModel
import com.zell_mbc.medilog.oximetry.OximetryViewModel
import com.zell_mbc.medilog.support.MedilogTheme
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.tags.TagRows
import com.zell_mbc.medilog.tags.TagsDialog
import com.zell_mbc.medilog.temperature.TemperatureViewModel
import com.zell_mbc.medilog.fluid.FluidViewModel
import com.zell_mbc.medilog.tags.TagsViewModel
import com.zell_mbc.medilog.weight.WeightViewModel
import java.text.DateFormat
import java.util.Calendar

class FilterActivity: AppCompatActivity() {

    var activity = this
    lateinit var viewModel: DataViewModel
    lateinit var snackbarDelegate: SnackbarDelegate
    var activeTab = -1

    private var minStart = 0L

    // Hold filter start and end date in milliseconds
    private var filterStart = 0L
    private var filterEnd = 0L
    private var calendar: Calendar = Calendar.getInstance() // Calendar instance for various calculations
    var tagFilter by mutableStateOf("")

    private var timeFilterType = Filter.OFF
    private var rollingTimeframe = 0
    private var rollingValue = ""

    val showDatePickerDialog = mutableStateOf(false)

    val START = true
    val END = false
    var setStartEndDate = true

    lateinit var tagsDialog: TagsDialog
    lateinit var tagsViewModel: TagsViewModel

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("rollingTimeframe", rollingTimeframe)
        outState.putString("rollingValue", rollingValue)
        outState.putString("tagFilter", tagFilter)
        outState.putInt("timeFilterType", timeFilterType)
        outState.putLong("filterStart", filterStart)
        outState.putLong("filterEnd", filterEnd)
        outState.putInt("activeTab", activeTab)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title = getString(R.string.setFilter)

        // Filters a tab specific, hence we need to retrieve the active ViewModel
        activeTab = if (savedInstanceState != null) savedInstanceState.getInt("activeTab",-1)
        else // Initial launch of activity
            intent.getIntExtra("activeTab", -1)

        viewModel = // MainActivity.viewModel
        when(activeTab) {
            Tabs.WEIGHT -> ViewModelProvider(this)[WeightViewModel::class.java]
            Tabs.BLOODPRESSURE -> ViewModelProvider(this)[BloodPressureViewModel::class.java]
            Tabs.DIARY -> ViewModelProvider(this)[DiaryViewModel::class.java]
            Tabs.FLUID -> ViewModelProvider(this)[FluidViewModel::class.java]
            Tabs.GLUCOSE -> ViewModelProvider(this)[GlucoseViewModel::class.java]
            Tabs.TEMPERATURE -> ViewModelProvider(this)[TemperatureViewModel::class.java]
            else -> ViewModelProvider(this)[OximetryViewModel::class.java]
        }
        viewModel.loadFilter()
        tagsViewModel = ViewModelProvider(this)[TagsViewModel::class.java]

        // Retrieve filter settings for this tab
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        // Initialize filter variables
        if (savedInstanceState != null) { // Restored activity, pull values from bundle
            rollingTimeframe = savedInstanceState.getInt("rollingTimeframe", getString(R.string.ROLLING_FILTER_TIMEFRAME_DEFAULT).toInt())
            rollingValue = savedInstanceState.getString("rollingValue", getString(R.string.ROLLING_FILTER_VALUE_DEFAULT))
            tagFilter = savedInstanceState.getString("tagFilter", "")
            timeFilterType = savedInstanceState.getInt("timeFilterType", getString(R.string.FILTER_MODE_DEFAULT).toInt())
            filterStart  = savedInstanceState.getLong("filterStart",0L)
            filterEnd  = savedInstanceState.getLong("filterEnd",0L)
        }
        else {
            rollingTimeframe = preferences.getInt(viewModel.rollingFilterTimeframePref,getString(R.string.ROLLING_FILTER_TIMEFRAME_DEFAULT).toInt())
            rollingValue = preferences.getInt(viewModel.rollingFilterValuePref, getString(R.string.ROLLING_FILTER_VALUE_DEFAULT).toInt()).toString()
            tagFilter = preferences.getString(viewModel.tagFilterPref, "").toString()
            timeFilterType = preferences.getInt(viewModel.timeFilterModePref, (this).getString(R.string.FILTER_MODE_DEFAULT).toInt())
            filterStart = viewModel.filterStart
            filterEnd   = viewModel.filterEnd
        }

        tagsDialog = TagsDialog(this)

        enableEdgeToEdge() //This will include/color the top Android info bar
        //safeDrawingPadding().
        setContent {
            val snackbarHostState = remember { SnackbarHostState() }
            snackbarDelegate = SnackbarDelegate(snackbarHostState, rememberCoroutineScope())
            //Modifier.padding(WindowInsets.systemBars.only(WindowInsetsSides.Top))
            MedilogTheme {
                Scaffold(Modifier.safeDrawingPadding(),
                    snackbarHost = { SnackbarHost(snackbarHostState) },
                    content = { padding ->
                        Column(modifier = Modifier.padding(padding)) { Filters()
                        }
                    })
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun Filters() {
        val scrollState = rememberScrollState()
        val filterOptions = listOf(getString(R.string.filter_off), getString(R.string.filter_static), getString(R.string.filter_rolling))
        var selectedFilter by remember { mutableStateOf(filterOptions[timeFilterType]) }

        // Static filter
        // Set button text to date if a filter is set
        var staticStartDate by rememberSaveable { mutableStateOf(if (viewModel.filterStart > 0L) DateFormat.getDateInstance(DateFormat.SHORT).format(viewModel.filterStart) else getString(R.string.startDate)) }
        var staticEndDate by rememberSaveable { mutableStateOf(if (viewModel.filterEnd != 0L) DateFormat.getDateInstance(DateFormat.SHORT).format(viewModel.filterEnd) else getString(R.string.endDate)) }
        var showTagsDialog by rememberSaveable { mutableStateOf(false) }

        // Define Min/Max date values
        val items = viewModel.getItems("ASC", filtered = false)
        if (items.isNotEmpty()) minStart = items[0].timestamp
        else {
            minStart = 0
            snackbarDelegate.showSnackbar(getString(R.string.emptyTable))
            return
        }

        // Rolling filter
        val rollingTimeframes = listOf(getString(R.string.days), getString(R.string.weeks), getString(R.string.months), getString(R.string.years))

        var expanded by remember { mutableStateOf(false) }
        var rollingTimeframeCompose by remember { mutableStateOf(rollingTimeframes[rollingTimeframe]) }
        var rollingValueCompose by rememberSaveable { mutableStateOf(rollingValue) }

        var datePickerDialogTrigger by remember { showDatePickerDialog }

        Surface(modifier = Modifier.padding(top = 16.dp), shape = RoundedCornerShape(16.dp)) {
            Box( // Bottom box first
                modifier = Modifier.fillMaxSize().padding(start = 16.dp, end = 16.dp),
                contentAlignment = Alignment.BottomEnd
            ) {
                Row(verticalAlignment = Alignment.Bottom, horizontalArrangement = Arrangement.End, modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 16.dp)) {
                    TextButton(onClick = { activity.finish() }, modifier = Modifier.padding(end = 16.dp)) { Text(getString(R.string.cancel)) }
                    TextButton(onClick = { saveButton() }, modifier = Modifier.padding(end = 8.dp)) { Text(getString(R.string.save)) }
                }
            }
            // Content box
            Box(modifier = Modifier.padding(bottom = 56.dp), contentAlignment = Alignment.Center) {
                Column(modifier = Modifier
                    .fillMaxHeight()
                    .verticalScroll(state = scrollState)
                    .padding(top = 0.dp, start = 48.dp)) {
                    Text(getString(R.string.time) + ":") //, color = colorScheme.secondary)
                    Spacer(modifier = Modifier.height(8.dp))
                    filterOptions.forEach { label ->
                        Row(
                            modifier = Modifier.fillMaxWidth().height(32.dp)
                                .selectable(
                                    selected = (selectedFilter == label),
                                    onClick = { selectedFilter = label },
                                    role = Role.RadioButton
                                )
                                .padding(horizontal = 16.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                modifier = Modifier.padding(end = 16.dp),
                                selected = (selectedFilter == label),
                                onClick = null, // null recommended for accessibility with screen readers
                                //colors = RadioButtonDefaults.colors(selectedColor = colorScheme.secondary)
                            )
                            Text(text = label) //, color = colorScheme.secondary)
                        }
                    }

                    // Support Diary category filter
                    if (viewModel.dataType == Tabs.DIARY) {
                        Text("")
                        TagRows(activity, tagsViewModel, tagFilter, setTagIds = { tagFilter = it }, setShowTagsDialog = { showTagsDialog = it })
                    }

                    timeFilterType = filterOptions.indexOf(selectedFilter)
                    if (timeFilterType == Filter.STATIC) {
                        Spacer(modifier = Modifier.height(8.dp))
                        HorizontalDivider(color = colorScheme.secondaryContainer)
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(getString(R.string.details)) //, color = colorScheme.secondary)
                        Spacer(modifier = Modifier.height(8.dp))
                        Row {
                            // Start date button
                            OutlinedButton(onClick = {
                                datePickerDialogTrigger = datePickerDialogTrigger.not()
                                setStartEndDate = START
                            }, modifier = Modifier.width(150.dp)) { Text(staticStartDate) }

                            // Reset Start Date Button
                            IconButton(onClick = {
                                staticStartDate = getString(R.string.startDate)
                                filterStart = 0
                            }, modifier = Modifier.padding(end = 24.dp))
                            { Icon(imageVector = Icons.Outlined.Clear, contentDescription = "") } //, tint = colorScheme.secondary) }
                        }
                        // End Date
                        Row {
                            OutlinedButton(onClick = {
                                datePickerDialogTrigger = datePickerDialogTrigger.not()
                                setStartEndDate = END
                            }, modifier = Modifier.width(150.dp)) { Text(staticEndDate) }
                            IconButton(onClick = {
                                staticEndDate = getString(R.string.endDate)
                                filterEnd = 0
                            }) { Icon(imageVector = Icons.Outlined.Clear, contentDescription = "") }//, tint = colorScheme.secondary) }
                        }
                    }

                    if (timeFilterType == Filter.ROLLING) {
                        Spacer(modifier = Modifier.height(8.dp))
                        HorizontalDivider(color = colorScheme.secondary)
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(getString(R.string.details)) //, color = colorScheme.secondary)
                        Spacer(modifier = Modifier.height(8.dp))
                        Row {
                            Spacer(modifier = Modifier.height(8.dp))
                            val keyboardController = LocalSoftwareKeyboardController.current

                            TextField(
                                value = rollingValueCompose, onValueChange = {
                                    rollingValueCompose = it
                                    rollingValue = rollingValueCompose
                                },
    //                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword, imeAction = ImeAction.Next),
                                modifier = Modifier.width(80.dp)
                            )
                            Spacer(modifier = Modifier.width(5.dp))
                            ExposedDropdownMenuBox(expanded = expanded, onExpandedChange = { expanded = !expanded }) {
                                TextField(readOnly = true,
                                    modifier = Modifier
                                        .width(150.dp)
                                        .menuAnchor( MenuAnchorType.PrimaryEditable, true)
                                        .onFocusChanged { if (it.isFocused) keyboardController?.hide() },
                                    value = rollingTimeframeCompose, onValueChange = { },
                                    trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) }
                                )
                                ExposedDropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
                                    rollingTimeframes.forEach { rollingSelectedOption ->
                                        DropdownMenuItem(
                                            text = { Text(text = rollingSelectedOption, color = colorScheme.primary) },
                                            colors = MenuDefaults.itemColors(),
                                            onClick = {
                                                rollingTimeframeCompose = rollingSelectedOption
                                                rollingTimeframe = rollingTimeframes.indexOf(rollingSelectedOption)
                                                expanded = false
                                            })
                                    }
                                }
                            }
                            Spacer(modifier = Modifier.height(8.dp))
                        }
                    }
                }
            }
        }
        if (datePickerDialogTrigger) DatePickerDialog(
                onDateSelected = { if (setStartEndDate == START) staticStartDate = it else staticEndDate = it },
                onDismiss = { datePickerDialogTrigger = false }
        )

        // Dialogs
        if (showTagsDialog)
                tagsDialog.ShowDialog(selectedTags = tagFilter, setShowDialog = { showTagsDialog = it }) {
                    tagFilter =  it //tagsDialog.selectedItems // Category ids
                }

    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun DatePickerDialog(
        onDateSelected: (String) -> Unit,
        onDismiss: () -> Unit
    ) {
        val curC: Calendar = Calendar.getInstance()
        val datePickerState = rememberDatePickerState(curC.timeInMillis)
        //Log.d("Debug ", "CurC: " + curC.toString())

        val selectedDate = datePickerState.selectedDateMillis?.let { DateFormat.getDateInstance(DateFormat.SHORT).format(it) } ?: ""

        DatePickerDialog(
            onDismissRequest = { onDismiss() },
            confirmButton = {
                OutlinedButton(onClick = {
                    onDateSelected(selectedDate)
                    onDismiss()
                    val calendar: Calendar = Calendar.getInstance()
                    val date = DateFormat.getDateInstance(DateFormat.SHORT).parse(selectedDate)
                    if (date != null) {
                        calendar.time = date
                        if (setStartEndDate == START) filterStart = calendar.timeInMillis else filterEnd = calendar.timeInMillis
                    }
                    //Log.d("Debug ", "Date: $filterStart $filterEnd")
                }) { Text(text = getString(R.string.save)) }
            },
            dismissButton = { OutlinedButton(onClick = { onDismiss() }) { Text(text = getString(R.string.cancel)) } }
        ) { DatePicker(state = datePickerState) }
    }


    private fun saveButton() {
        if (timeFilterType == Filter.STATIC) {
            // If both are set to 0 the filter is effectively switched off
            if ((filterStart + filterEnd == 0L) && tagFilter.isEmpty()) timeFilterType = Filter.OFF

            // If both are set filterEnd has to be > filterStart
            if (filterStart > 0 && filterEnd >0 )
                if (filterStart >= filterEnd) {
                    snackbarDelegate.showSnackbar(activity.getString(R.string.invalidInput))
                    return
                }

            // Set time to one millisecond past midnight so the filter starts with all data for the starting day
            if (filterStart > 0) {
                calendar.timeInMillis = filterStart
                calendar.set(Calendar.MILLISECOND,1)
                calendar.set(Calendar.SECOND,0)
                calendar.set(Calendar.MINUTE,0)
                calendar.set(Calendar.HOUR_OF_DAY,0)
                filterStart = calendar.timeInMillis
            }
        }

        if (timeFilterType == Filter.ROLLING) {
            viewModel.rollingTimeframe = rollingTimeframe
            try {
                val tmp = rollingValue.toInt()
                if (tmp <= 0) {
                    snackbarDelegate.showSnackbar(activity.getString(R.string.invalidInput))
                    return
                }
                viewModel.rollingValue = tmp
            } catch (_: Exception ) {
                snackbarDelegate.showSnackbar(activity.getString(R.string.invalidInput))
                return
            }
        }

        // Save filter parameters
        viewModel.timeFilterMode = timeFilterType
        viewModel.setFilter(filterStart, filterEnd, tagFilter) // Make sure Values are updated
       // this.invalidateOptionsMenu()  // Make sure the filter icon is up to date

        //viewModel.buildFilterString() // Rebuild active filter
         // Rebuild active filter
        //viewModel.filterChanged = true

        activity.finish()
        startActivity(Intent(this, MainActivity::class.java)) // Relaunch MainActivity so new filter settings get picked up
    }

}