/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.texttemplates

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.textTemplatesViewModel
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.TextTemplates

// Dialog which allows to select a text template
class TextTemplateDialog(val context: Context) {
    private val showDeleteDialog = mutableStateOf(false)
    private val showEditDialog = mutableStateOf(false)
    private val showAddDialog = mutableStateOf(false)
    private val editMode  = mutableStateOf(false)
    private val resetCheckBoxes  = mutableStateOf(false)
    private var selectedItems  = mutableListOf<Int>()

    private var header = ""
    private var dataType = 0

    @Composable
    fun ShowDialog(setShowDialog: (Boolean) -> Unit, setValue: (String) -> Unit) {
        dataType = viewModel.dataType

        val dataList = textTemplatesViewModel.getAllRecords(dataType).collectAsState(initial = emptyList())
        Dialog(onDismissRequest = { setShowDialog(false) }) {
            Surface(shape = RoundedCornerShape(16.dp)) {
                Box(contentAlignment = Alignment.Center) {
                    Column(modifier = Modifier.padding(16.dp)) {
                        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically) {
                            ShowHeaderRow(context.getString(R.string.textTemplates))
                        }
                        Spacer(modifier = Modifier.height(20.dp))
                        Row(modifier = Modifier.padding(start = 0.dp)) {
                            Column() {
                                Text(context.getString(R.string.textTemplatesDialogDescription), color = MaterialTheme.colorScheme.primary)
                                Spacer(modifier = Modifier.height(20.dp))
                                HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.primary) // Lines starting from the top
                                Box{
                                    LazyColumn {
                                        items(dataList.value) { item ->
                                            Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Start) {
                                                if (editMode.value == true) {
                                                    val checkedState = remember { mutableStateOf(false) }
                                                    if (resetCheckBoxes.value) checkedState.value = false
                                                    Checkbox(checked = checkedState.value, colors = CheckboxDefaults.colors(), onCheckedChange = {
                                                        resetCheckBoxes.value = false
                                                        checkedState.value = it
                                                        if (it) selectedItems.add(item._id)
                                                        else {
                                                            val pos = selectedItems.indexOf(item._id)
                                                            if (pos >= 0) selectedItems.removeAt(pos)
                                                        }
                                                        editMode.value = (selectedItems.size != 0)
                                                    })
                                                }
                                                val s = item.template
                                                TextButton(onClick = {
                                                    setValue(s)
                                                    setShowDialog(false)
                                                }) { Text(text = s, style = TextStyle(color = MaterialTheme.colorScheme.primary))}
                                                //Spacer(Modifier.weight(1f))
                                            }
                                            HorizontalDivider(thickness = Dp.Hairline, color = MaterialTheme.colorScheme.secondary)
                                        }
                                        item() {
                                            HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.secondary) // Lines starting from the top                                    Spacer(modifier = Modifier.height(20.dp))
                                            ShowBottomRow()
                                            Row(
                                                modifier = Modifier.fillMaxWidth(),
                                                horizontalArrangement = Arrangement.End,
                                                verticalAlignment = Alignment.CenterVertically
                                            ) { if (editMode.value) TextButton(onClick = {
                                                setShowDialog(false)
                                                editMode.value = false
                                            }) { Text(context.getString(R.string.done), color = MaterialTheme.colorScheme.primary) }
                                            else  TextButton(onClick = { editMode.value = true }) { Text(context.getString(R.string.edit), color = MaterialTheme.colorScheme.primary) }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun ShowHeaderRow(t: String) {
        header = t
        Text(text = t, style = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold))
    }

    @Composable
    fun ShowBottomRow() {
        var deleteDialogTrigger by remember { showDeleteDialog }
        var editDialogTrigger by remember { showEditDialog }
        var addDialogTrigger by remember { showAddDialog }
        val activateControl = remember { editMode }

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(onClick = { addDialogTrigger = true }, enabled = activateControl.value ) { Icon(imageVector = Icons.Default.Add, contentDescription = "Add") }
            IconButton(onClick = { editDialogTrigger = true }, enabled = activateControl.value ) { Icon(imageVector = Icons.Default.Edit, contentDescription = "Edit") }
            IconButton(onClick = { deleteDialogTrigger = true }, enabled = activateControl.value ) { Icon(imageVector = Icons.Default.Delete, contentDescription = "Delete") }
        }

        if (addDialogTrigger) AddButton()
        if (editDialogTrigger) EditButton()
        if (deleteDialogTrigger) DeleteButton()
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun AddButton() {
        var textValue by remember { mutableStateOf("") }

        BasicAlertDialog(onDismissRequest = { showEditDialog.value = false }) {
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(context.getString(R.string.add) + " " + context.getString(R.string.textTemplate), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))

                    Text("")
                    OutlinedTextField(value = textValue, onValueChange = { textValue = it }, label = { Text(text = context.getString(R.string.textTemplate)) }, modifier = Modifier
                        .padding(vertical = 8.dp)
                        .fillMaxWidth()
                    )
                    Text("")

                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showAddDialog.value = false }) { Text(context.getString(R.string.cancel)) }
                        TextButton(onClick = {
                            if (textValue.isNotEmpty()) {
                                val newItem = TextTemplates(0, dataType, textValue)
                                textTemplatesViewModel.upsert(newItem)
                                showAddDialog.value = false
                            } else Toast.makeText(context, context.getString(R.string.emptyTemplate), Toast.LENGTH_LONG).show()
                        }) { Text(context.getString(R.string.save)) }
                    }
                }
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun EditButton() {
        if (selectedItems.size == 0) {
            Toast.makeText(context, "No " + context.getString(R.string.textTemplate) + " selected!", Toast.LENGTH_LONG).show()
            showEditDialog.value = false
            return
        }

        val editItem = textTemplatesViewModel.getItem(selectedItems[0])
        if (editItem == null) {
            Toast.makeText(context, "Entry with id " + selectedItems[0] + " not found!", Toast.LENGTH_LONG).show()
            return
        }

        var textValue by remember { mutableStateOf(editItem.template) }

        BasicAlertDialog(onDismissRequest = { showEditDialog.value = false }) {
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(context.getString(R.string.edit) + " " + context.getString(R.string.textTemplate), style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))

                    Text("")
                    OutlinedTextField(value = textValue, onValueChange = { textValue = it }, label = { Text(text = context.getString(R.string.textTemplate)) }, modifier = Modifier
                        .padding(vertical = 8.dp)
                        .fillMaxWidth()
                    )
                    Text("")

                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showEditDialog.value = false }) { Text(context.getString(R.string.cancel)) }
                        TextButton(onClick = {
                            // Validate input
                            if (textValue.isNotEmpty()) {
                                editItem.template = textValue
                                MainActivity.textTemplatesViewModel.upsert(editItem)
                                showEditDialog.value = false
                                selectedItems.clear()
                                resetCheckBoxes.value = true
                            } else Toast.makeText(context, context.getString(R.string.noTextTemplate), Toast.LENGTH_LONG).show()
                        }) { Text(context.getString(R.string.save)) }
                    }
                }
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun DeleteButton() {
        if (selectedItems.size == 0) {
            Toast.makeText(context, "No " + context.getString(R.string.textTemplate) + " selected!", Toast.LENGTH_LONG).show()
            showDeleteDialog.value = false
            return
        }

        val warningText = if (selectedItems.size > 1) context.getString(R.string.thisIsGoing, selectedItems.size) + "\n\n" + context.getString(R.string.doYouReallyWantToContinue)
        else context.getString(R.string.doYouReallyWantToContinue)

        BasicAlertDialog(onDismissRequest = { showDeleteDialog.value = false }) {
            // modifier = Modifier.wrapContentWidth().wrapContentHeight(),
            Surface(shape = MaterialTheme.shapes.large, modifier = Modifier.fillMaxWidth(.9f)) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(context.getString(R.string.delete) + " $header", style = TextStyle(fontSize = 24.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold))
                    Text("")
                    Text(warningText)
                    Text("")
                    Row(modifier = Modifier.align(Alignment.End)) {
                        TextButton(onClick = { showDeleteDialog.value = false }) { Text(context.getString(R.string.cancel)) }
                        TextButton(onClick = {
                            for (id in selectedItems) {
                                if (id > 0) {
                                    // Delete item in the db
                                    textTemplatesViewModel.delete(id)
                                }
                            }
                            // All selected items were deleted
                            selectedItems.clear()

                            showDeleteDialog.value = false
                        }) { Text(context.getString(R.string.yes)) }
                    }
                }
            }
        }
    }
}