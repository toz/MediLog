/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.texttemplates

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import com.zell_mbc.medilog.data.TextTemplates
import kotlinx.coroutines.flow.Flow

@Dao
abstract class TextTemplatesDao {
    // ############################################################
    // TextTemplates table
    // ############################################################
    @Upsert
    abstract suspend fun upsert(textTemplates: TextTemplates): Long

    @RawQuery(observedEntities = [TextTemplates::class])
    abstract fun query(query: SupportSQLiteQuery): Flow<List<TextTemplates>>

    @Query("SELECT * FROM texttemplates")
    abstract fun getAllRecords(): Flow<List<TextTemplates>>

    @Query("SELECT * FROM texttemplates WHERE type=:type")
    abstract fun getAllRecords(type: Int): Flow<List<TextTemplates>>

    @Query("SELECT count(*) FROM texttemplates WHERE type=:type")
    abstract fun count(type: Int): Long

    @Query("SELECT count(*) FROM texttemplates")
    abstract fun countAll(): Long

    @Query("DELETE from texttemplates where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("SELECT * from texttemplates where _id = :id")
    abstract fun getItem(id: Int): TextTemplates

    @Query("DELETE from texttemplates")
    abstract fun deleteAll()

    @Query("DELETE FROM sqlite_sequence WHERE name = 'texttemplates'")
    abstract fun resetPrimaryKey()

    // -----------------
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTemplate(obj: TextTemplates): Long

    @Query("UPDATE texttemplates SET type=:type, template=:template where _id=:id")
    abstract suspend fun updateTemplate(id: Int, type: Int, template: String)



    @Query("SELECT count(*) FROM texttemplates")
    abstract fun count(): Long

    @Query("SELECT * from texttemplates where _id = :id")
    abstract fun getTemplateList(id: Int): MutableList<TextTemplates>

    @Query("SELECT * from texttemplates")
    abstract fun getTemplateList(): MutableList<TextTemplates>

    @Query("SELECT * from texttemplates") // Used by Backup routine
    abstract fun backup(): List<TextTemplates>

    @Query("SELECT * from texttemplates LIMIT 1") // Get first profile
    abstract fun getFirst(): TextTemplates
}
