/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.texttemplates

import android.app.Application
import android.content.SharedPreferences
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.MainActivity.Companion.templateFields
import com.zell_mbc.medilog.data.MediLogDB
import com.zell_mbc.medilog.data.TextTemplates
import com.zell_mbc.medilog.preferences.SettingsActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.flow.Flow


class TextTemplatesViewModel(application: Application): AndroidViewModel(application) {
    val app = application
    var id  = 0

    @JvmField
    //val preferences: SharedPreferences = Preferences.getSharedPreferences(app)
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    val dao = MediLogDB.getDatabase(app).TextTemplatesDao()

    val separator = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, ",")
    private val lineSeparator = System.lineSeparator()


    fun getAllRecords(dataType: Int = -1): Flow<List<TextTemplates>> {
        return if (dataType > 0) dao.getAllRecords(dataType) else dao.getAllRecords()
    }

    fun upsert(template: TextTemplates) = viewModelScope.launch(Dispatchers.IO) { dao.upsert(template) }
    fun delete(_id: Int) = viewModelScope.launch(Dispatchers.IO) { dao.delete(_id) }

    // Wait for the new id to be returned
    fun upsertBlocking(t: TextTemplates): Long {
        var id = -1L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                id = dao.upsert(t)
                if (id == -1L) id = t._id.toLong() // If this is an update return current id
            }
            j.join()
        }
        return id
    }

    fun count(dataType: Int = -1): Long {
        var count = 0L
        runBlocking {
            val j = launch(Dispatchers.IO) {
                count = if (dataType > 0) dao.count(dataType) else dao.count()
            }
            j.join()
        }
        return count
    }

    // Run query which returns an integer result
    fun getItem(_id: Int): TextTemplates? {
        var item: TextTemplates? = null
        runBlocking {
            val j = launch(Dispatchers.IO) {
                item = dao.getItem(_id)
            }
            j.join()
        }
        return item
    }

    fun backup():List<TextTemplates> {
        var items = emptyList<TextTemplates>()
        runBlocking {
            val j = launch(Dispatchers.IO) {
                items = dao.backup()
            }
            j.join()
        }
        return items
    }

    fun dataToCSV(items: List<TextTemplates>): String {
        if (items.isEmpty()) {
            // This will always be called from a coroutine, hence the looper is required
            //Handler(Looper.getMainLooper()).post { Toast.makeText(app, app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show() }
            return ""
        }

        val sb = StringBuilder()

        val tableFields = templateFields //getTableColumnNames("texttemplates", app)

        // Compile header line
        var line = ""
        for (field in tableFields) {
            line += field.name + separator
        }
        line += lineSeparator

        sb.append(line)
        //Log.d("Backup", line)

        for (item in items) {
            line = ""
            // Not elegant but this makes sure the field order always matches the header line
            for (field in tableFields) { //MainActivity.templateFields) {
                when (field.name) {
                    "_id"                    -> line += item._id.toString() + separator
                    "type"                   -> line += item.type.toString() + separator
                    "template"                -> {
                        if (item.template.contains("\n")) item.template = item.template.replace("\n","\\n")  // Look for line breaks
                        if (item.template.contains(Delimiter.STRING)) item.template = item.template.replace(Delimiter.STRING.toString(),"&quot;")  // Look for "
                        line += Delimiter.STRING + item.template + Delimiter.STRING + separator
                    }
                    // Should never happen! If this shows we are dealing with a bug
                    else -> {
                        Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Field $field not handled?!", Toast.LENGTH_LONG).show() }
                        return sb.toString()
                    }
                }
            }
            line += lineSeparator
            sb.append(line)
        }
        return sb.toString()
    }
}
