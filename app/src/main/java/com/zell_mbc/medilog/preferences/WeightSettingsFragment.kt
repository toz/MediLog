/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.preferences
import android.os.Bundle
import android.text.InputType
import android.widget.Toast
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreference
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_TEMPERATURE_THRESHOLDS
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_WEIGHT_TARE
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_WEIGHT_THRESHOLD

class WeightSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.weight_preferences, rootKey)

        // Fix an Android bug: https://developer.android.com/develop/ui/views/components/settings/customize-your-settings#customize_an_edittextpreference_dialog
        val weightUnit = preferenceManager.findPreference<ListPreference>(SettingsActivity.KEY_PREF_WEIGHT_UNIT)
        weightUnit?.summary = requireContext().getString(R.string.weightUnitSummary) + ", " + weightUnit.value
        weightUnit?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = weightUnit.summary.toString()
            weightUnit.summary = tmpString.replace(weightUnit.value.toString(), newValue.toString())
            true
        }

        val tare = preferenceManager.findPreference<EditTextPreference>(KEY_PREF_WEIGHT_TARE)
        tare?.setOnBindEditTextListener { editText -> editText.inputType = InputType.TYPE_CLASS_NUMBER }
        tare?.summary = requireContext().getString(R.string.tareSummary) + ", " + tare.text + " " + weightUnit?.value
        tare?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            var value = 0
            try { value = newValue.toString().toInt()
                val tmpString = tare.summary.toString()
                tare.summary = tmpString.replace(tare.text.toString(), newValue.toString())
                true
            }
            catch (_: NumberFormatException) {
                Toast.makeText(requireContext(), requireContext().getString(R.string.invalidInput), Toast.LENGTH_LONG).show()
                false
            }
        }

        val thresholdObject = preferenceManager.findPreference<EditTextPreference>(KEY_PREF_WEIGHT_THRESHOLD)
        thresholdObject?.summary = requireContext().getString(R.string.thresholdSummaryRange) + ", " + thresholdObject.text
        thresholdObject?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = thresholdObject.summary.toString()
            thresholdObject.summary = tmpString.replace(thresholdObject.text.toString(), newValue.toString())
            true
        }

        val paperSize = preferenceManager.findPreference<ListPreference>(SettingsActivity.KEY_PREF_WEIGHT_PAPER_SIZE)
        paperSize?.summary = requireContext().getString(R.string.paperSizeSummary) + ", " + paperSize.value
        paperSize?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = paperSize.summary.toString()
            paperSize.summary = tmpString.replace(paperSize.value.toString(), newValue.toString())
            true
        }


        val bodyHeight = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_BODY_HEIGHT)
        bodyHeight?.setOnBindEditTextListener { editText -> editText.inputType = InputType.TYPE_CLASS_NUMBER }
        bodyHeight?.summary = requireContext().getString(R.string.heightSummary) + ", " + bodyHeight.text
        bodyHeight?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = requireContext().getString(R.string.invalidBodyHeight)
            try { newValue.toString().toInt()
                val tmpString = bodyHeight.summary.toString()
                bodyHeight.summary = tmpString.replace(bodyHeight.text.toString(), newValue.toString())
                true
            }
            catch (_: NumberFormatException) {
                Toast.makeText(requireContext(), tmpString, Toast.LENGTH_LONG).show()
                false
            }
        }

        val preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE)?.isEnabled = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_SHOW_MOVING_AVERAGE, false)

        val movingAverage = preferenceManager.findPreference<SwitchPreference>(SettingsActivity.KEY_PREF_WEIGHT_SHOW_MOVING_AVERAGE)
        movingAverage?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE)?.isEnabled = newValue.toString().toBoolean()
            true
        }

    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }
}