/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.preferences

//import com.takisoft.preferencex.PreferenceFragmentCompat
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.widget.Toast
import androidx.documentfile.provider.DocumentFile
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import java.text.DateFormat


class BackupSettingsFragment : PreferenceFragmentCompat() {
    //override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
     override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.backup_preferences, rootKey)

        class DigitsOnlyFilter: InputFilter {
            private val filterString = "123456789"

            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                try {
                    if (filterString.indexOf(source.toString()) >= 0) return null
                    else Toast.makeText(requireContext(), requireContext().getString(R.string.allowedInput) + " $filterString", Toast.LENGTH_LONG).show()
                } catch (nfe: NumberFormatException) { return "" }  // Define abstract function
                return ""
            }
        }

        // Make sure only digits get entered
        var editTextPreference = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_BACKUP_WARNING)
        if (editTextPreference != null) {
            editTextPreference.setOnBindEditTextListener { editText ->
                val filterArray = arrayOfNulls<InputFilter>(2)
                filterArray[0] = InputFilter.LengthFilter(2)
                filterArray[1] = DigitsOnlyFilter()
                editText.filters = filterArray
            }
        }
        editTextPreference = preferenceManager.findPreference(SettingsActivity.KEY_PREF_AUTO_BACKUP)
        if (editTextPreference != null) {
            editTextPreference.setOnBindEditTextListener { editText ->
                val filterArray = arrayOfNulls<InputFilter>(2)
                filterArray[0] = InputFilter.LengthFilter(2)
                filterArray[1] = DigitsOnlyFilter()
                editText.filters = filterArray
            }
        }

        /*val switchPreference = preferenceManager.findPreference<SwitchPreference>(SettingsActivity.KEY_PREF_ADD_TIMESTAMP)
        if (switchPreference != null) {
            if (switchPreference.isChecked)
                switchPreference.summary = "MediLog-Backup-2024-09-11 08-38-47.zip"
        }*/

        val lastBackup = preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_LAST_BACKUP)
        val preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())

        if (lastBackup != null) {
            var status = getString(R.string.unknown)
            val timestamp = preferences.getLong("LAST_BACKUP", 0L)
            if (timestamp > 0) {
                val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
                val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
                status = dateFormat.format(timestamp) + " " + timeFormat.format(timestamp)
            }
            status = getString(R.string.lastBackup) + " " + status
            lastBackup.title = status

            // Start adhoc backup
            lastBackup.setOnPreferenceClickListener {
                // Check Uri
                //Log.d("Preference", "1")
                val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
                if (uriString.isNullOrEmpty()) {
                    Toast.makeText(requireContext(), getString(R.string.missingBackupLocation), Toast.LENGTH_LONG).show()
                    false //Log.d("Preference", "i")
                } else {
                    // Password
                    var zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                    if (zipPassword.isNullOrEmpty()) zipPassword = ""
                    //Log.d("Preference", "i")

                    val uri = Uri.parse(uriString)
                    val dFolder = DocumentFile.fromTreeUri(requireContext(), uri)
                    if (dFolder == null) Toast.makeText(requireContext(), getString(R.string.missingBackupLocation), Toast.LENGTH_LONG).show()
                    else MainActivity.viewModel.writeBackup(uri, zipPassword, false)
                    true
                }
            }
        }

        val backupUri = preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_BACKUP_URI)
        if (backupUri != null) {
            val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
            backupUri.summary = uriString

            // Set backup location
            backupUri.setOnPreferenceClickListener {
                val i = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
                i.addCategory(Intent.CATEGORY_DEFAULT)
                startActivityForResult(Intent.createChooser(i, "Choose directory"), 9999)
                //Log.d("Preference", "i")
                true
            }
        }
     }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }
}

