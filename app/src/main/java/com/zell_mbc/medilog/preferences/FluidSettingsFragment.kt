/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.preferences

import android.os.Bundle
import android.text.InputType
import android.widget.Toast
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R

class FluidSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.fluid_preferences, rootKey)

        val fluidUnit = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_FLUID_UNIT)
        fluidUnit?.summary = requireContext().getString(R.string.fluidUnitSummary) + ", " + fluidUnit.text
        fluidUnit?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = fluidUnit.summary.toString()
            fluidUnit.summary = tmpString.replace(fluidUnit.text.toString(), newValue.toString())
            true
        }

        val fluidThreshold = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_FLUID_THRESHOLD)
        fluidThreshold?.setOnBindEditTextListener { editText -> editText.inputType = InputType.TYPE_CLASS_NUMBER }
        var thresholds = ""
        if (fluidThreshold != null) {
            if (fluidThreshold.text.isNullOrEmpty()) {
                thresholds = requireContext().getString(R.string.FLUID_THRESHOLD_DEFAULT)
                fluidThreshold.text = thresholds
            }
            else thresholds = "" + fluidThreshold.text
            fluidThreshold.summary = requireContext().getString(R.string.thresholdSummaryRange) + ", $thresholds"
        }

        fluidThreshold?.summary = requireContext().getString(R.string.preferencesFluidThresholdSummary) + ", " + fluidThreshold.text + " " + fluidUnit?.text
        fluidThreshold?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = requireContext().getString(R.string.invalidTreshold)
            try {
                val value = newValue.toString().toInt()
                val tmpString = fluidThreshold?.summary.toString()
                fluidThreshold?.summary = tmpString.replace(fluidThreshold.text.toString(), value.toString())
                true
            } catch (_: NumberFormatException) {
                Toast.makeText(requireContext(), tmpString, Toast.LENGTH_LONG).show()
                false
            }
        }

        val paperSize = preferenceManager.findPreference<ListPreference>(SettingsActivity.KEY_PREF_FLUID_PAPER_SIZE)
            paperSize?.summary = requireContext().getString(R.string.paperSizeSummary) + ", " + paperSize.value
            paperSize?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                val tmpString = paperSize.summary.toString()
                paperSize.summary = tmpString.replace(paperSize.value.toString(), newValue.toString())
                true
            }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}