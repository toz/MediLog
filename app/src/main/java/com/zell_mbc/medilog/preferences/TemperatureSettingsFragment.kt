/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.preferences

import android.os.Bundle
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
//import com.takisoft.preferencex.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_TEMPERATURE_THRESHOLDS

class TemperatureSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.temperature_preferences, rootKey)

        val paperSize = preferenceManager.findPreference<ListPreference>(SettingsActivity.KEY_PREF_TEMPERATURE_PAPER_SIZE)
        paperSize?.summary = requireContext().getString(R.string.paperSizeSummary) + ", " + paperSize.value
        paperSize?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = paperSize.summary.toString()
            paperSize.summary = tmpString.replace(paperSize.value.toString(), newValue.toString())
            true
        }

        val unit = preferenceManager.findPreference<ListPreference>(SettingsActivity.KEY_PREF_TEMPERATURE_UNIT)
        unit?.summary = requireContext().getString(R.string.temperatureUnitSummary) + ", " + unit.value
        unit?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = unit.summary.toString()
            unit.summary = tmpString.replace(unit.value.toString(), newValue.toString())
            true
        }

        val thresholdObject = preferenceManager.findPreference<EditTextPreference>(KEY_PREF_TEMPERATURE_THRESHOLDS)
        thresholdObject?.summary = requireContext().getString(R.string.thresholdSummaryRange) + ", " + thresholdObject.text
        thresholdObject?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = thresholdObject.summary.toString()
            thresholdObject.summary = tmpString.replace(thresholdObject.text.toString(), newValue.toString())
            true
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}
