/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.preferences

import android.app.Activity
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import com.zell_mbc.medilog.MainActivity.Companion.feedbackString
import com.zell_mbc.medilog.MainActivity.Companion.userFeedback
import com.zell_mbc.medilog.MainActivity.Companion.userFeedbackLevel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.support.MedilogTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.withLink
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.LINK_COLOR
import com.zell_mbc.medilog.databinding.UsersettingsBinding

import java.text.DateFormat
import java.util.*

class UserFeedbackFragment : Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: UsersettingsBinding? = null
    private val binding get() = _binding!!

    var feedbackStringVal1 = ""
    var feedbackStringVal2 = ""
    var lastRunString = ""
    var userFeedbackString = ""
    var lastRun = 0L
    lateinit var preferences: SharedPreferences
    lateinit var activity: Activity

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = UsersettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())

        feedbackStringVal2 = ""
        if (feedbackStringVal1.contains(":")) {
            feedbackStringVal2 = feedbackStringVal1.substring(feedbackStringVal1.indexOf(":") + 1)
            feedbackStringVal1 =
                feedbackStringVal1.substring(0, (feedbackStringVal1.indexOf(":") - 1))
        }

        userFeedbackString = preferences.getString("USER_FEEDBACK_STRING", getString(R.string.nothing)) + ""

        lastRun = preferences.getLong("LAST_FEEDBACK_RUN",0L)
        lastRunString = if (lastRun > 0L) DateFormat.getDateInstance(DateFormat.SHORT).format(lastRun) + " - " + DateFormat.getTimeInstance().format(lastRun)
        else getString(R.string.never)

        binding.composeView.setContent { MedilogTheme { ShowContent() } }
    }

    @Composable
    fun ShowContent() {
        val scrollState = rememberScrollState()

        val feedbackEnabled = remember { mutableStateOf(false) }
        feedbackEnabled.value = userFeedbackLevel > 0 //preferences.getInt("USER_FEEDBACK_SETTING", 0) > 0
        feedbackString = remember { mutableStateOf(userFeedbackString) }
        feedbackStringVal1 = feedbackString.value

        val lastFeedbackTime =  remember { mutableStateOf(lastRunString) }
        if (lastFeedbackTime.value.isEmpty()) lastFeedbackTime.value = getString(R.string.never)

        Column(
                modifier = Modifier.verticalScroll(state = scrollState)
                    .padding(start = 16.dp, top = 8.dp)
            ) {
                Row {
                    Surface(
                        modifier = Modifier.size(50.dp),
                        color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0f)
                    ) {
                        Image(
                            painter = painterResource(id = R.mipmap.ic_launcher_inverted),
                            contentDescription = null
                        )
                    }
                    Column(modifier = Modifier.padding(start = 8.dp, top = 6.dp)) {
                        Text(
                            stringResource(id = R.string.appName),
                            style = MaterialTheme.typography.bodyLarge,
                            color = MaterialTheme.colorScheme.primary,
                            textAlign = TextAlign.End
                        )
                        Text(
                            stringResource(id = R.string.appDescription),
                            style = MaterialTheme.typography.bodyMedium,
                            color = MaterialTheme.colorScheme.primary
                        )
                    }
                }

                val uriHandler = LocalUriHandler.current

                // Rational
                Text(
                    stringResource(id =R.string.whyUserFeedback),
                    modifier = Modifier.padding(top = 16.dp),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.primary
                )
                Text(buildAnnotatedString {
                    append("")
                    withLink(LinkAnnotation.Url(url = stringResource(id = R.string.UserFeedbackUrl), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(stringResource(id = R.string.UserFeedbackUrl)) }
                }, style = MaterialTheme.typography.bodyMedium, textDecoration = TextDecoration.Underline)
                Row(modifier = Modifier.padding(top = 6.dp)) {
                    Text(
                        getString(R.string.feedbackEnabled),
                        modifier = Modifier.padding(top = 16.dp),
                        style = MaterialTheme.typography.bodyMedium,
                        color = MaterialTheme.colorScheme.primary
                    )
                    Switch(
                        modifier = Modifier.padding(start = 8.dp),
                        checked = feedbackEnabled.value,
                        onCheckedChange = { feedbackEnabled.value = it
                            userFeedbackLevel = if(it) 1 else 0
                            val editor = preferences.edit()
                            editor.putInt("USER_FEEDBACK_SETTING", userFeedbackLevel)
                            editor.apply()
                        }
                    )
                }
                Text(getString(R.string.lastFeedback) + " " + lastFeedbackTime.value,
                    modifier = Modifier.padding(top = 4.dp),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.primary
                )
                Text(
                    getString(R.string.dataSent),
                    modifier = Modifier.padding(top = 4.dp),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.primary
                )
                Text(
                    feedbackStringVal1,
                    modifier = Modifier.padding(start = 8.dp, top = 0.dp),
                    style = MaterialTheme.typography.bodySmall,
                    color = MaterialTheme.colorScheme.primary
                )
                Text(
                    feedbackStringVal2,
                    modifier = Modifier.padding(start = 8.dp, top = 0.dp),
                    style = MaterialTheme.typography.bodySmall,
                    color = MaterialTheme.colorScheme.primary
                )
                Text(
                    getString(R.string.otherFeedbackMeans),
                    modifier = Modifier.padding(top = 16.dp),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.primary
                )

                // Email
                Text(buildAnnotatedString {
                    append("")
                    withLink(LinkAnnotation.Url(url = "mailto:" + stringResource(id = R.string.email), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(stringResource(id = R.string.email)) }
                }, style = MaterialTheme.typography.bodyMedium, textDecoration = TextDecoration.Underline)

                // AppUrl
                Text(buildAnnotatedString {
                    append("")
                    withLink(LinkAnnotation.Url(url = stringResource(id = R.string.AppURL), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(stringResource(id = R.string.AppURL)) }
                }, style = MaterialTheme.typography.bodyMedium, textDecoration = TextDecoration.Underline)

                Button(modifier = Modifier.padding(start = 8.dp, top = 16.dp),
                    onClick = {
                        feedbackString.value   = userFeedback(requireContext())
                        lastFeedbackTime.value = DateFormat.getDateInstance(DateFormat.SHORT).format(Date()) + " - " + DateFormat.getTimeInstance().format(Date())
                    }) {
                    Text(getString(R.string.instantFeedback))
                }
            }
    }

}