/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.preferences

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.addCallback
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R


class SettingsActivity : AppCompatActivity(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

    override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
            // Instantiate the new Fragment
            val args = pref.extras
            val fragment = supportFragmentManager.fragmentFactory.instantiate(classLoader, pref.fragment.toString())
            fragment.arguments = args
            //fragment.setTargetFragment(caller, 0)
            // Replace the existing Fragment with the new Fragment
            supportFragmentManager.beginTransaction().replace(android.R.id.content, fragment).addToBackStack(null).commit()
            return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge() //This will include/color the top Android info bar
        WindowCompat.setDecorFitsSystemWindows(window, true)

        // Set preference descriptions before they get build
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val threshold = "" + preferences.getString(KEY_PREF_TEMPERATURE_THRESHOLDS,"")
        if (threshold.isEmpty()) {
            val itemUnit = "" + preferences.getString(KEY_PREF_TEMPERATURE_UNIT,getString(R.string.TEMPERATURE_CELSIUS))
            val default = if (itemUnit == getString(R.string.TEMPERATURE_CELSIUS)) getString(R.string.TEMPERATURE_THRESHOLDS_CELSIUS_DEFAULT) else getString(R.string.TEMPERATURE_THRESHOLDS_FAHRENHEIT_DEFAULT)
            val editor = preferences.edit()
            editor.putString(KEY_PREF_TEMPERATURE_THRESHOLDS, default)
            editor.apply()
            }
        supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()

       onBackPressedDispatcher.addCallback(this) { handleBackPress() }  // Handle the 'hardware' back button
    }

    // Handle top bar back arrow
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            handleBackPress()
            return true
        }
        return false
    }

    // This is required to catch the back buttons which otherwise won't reload the activity to reflect settings changes
    private fun handleBackPress() {
        if (supportFragmentManager.backStackEntryCount == 0) { // Reload main activity if on top level
            val intent = Intent(application, MainActivity::class.java)
            startActivity(intent)
        }
        else supportFragmentManager.popBackStack()
    }

    // This is called when the backup folder setting is changed
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        data ?: return

        if (resultCode == RESULT_OK) {
            val preferences = PreferenceManager.getDefaultSharedPreferences(this)
//            Log.i("Test", "Result URI " + data.data)
            val editor = preferences.edit()
            editor.putString(KEY_PREF_BACKUP_URI, data.data.toString())
            editor.apply()
            // Refresh screen
            this.recreate()
        }
    }
    
    companion object {
        const val KEY_PREF_DELIMITER = "delimiter"
        const val KEY_PREF_TEMPERATURE_UNIT              = "etTemperatureUnit"
        const val KEY_PREF_TEMPERATURE_THRESHOLDS        = "etTemperatureThresholds"
        const val KEY_PREF_TEMPERATURE_SHOW_THRESHOLDS   = "cbShowTemperatureThresholds"
        const val KEY_PREF_TEMPERATURE_SHOW_GRID         = "cbShowTemperatureGrid"
        const val KEY_PREF_TEMPERATURE_SHOW_LEGEND       = "cbShowTemperatureLegend"
        const val KEY_PREF_TEMPERATURE_LANDSCAPE         = "cbTemperatureLandscape"
        const val KEY_PREF_TEMPERATURE_PAPER_SIZE        = "liTemperaturePaperSize"
        const val KEY_PREF_TEMPERATURE_HIGHLIGHT_VALUES  = "cbTemperatureHighlightValues"
        const val KEY_PREF_TEMPERATURE_SHOWTIME          = "swTemperatureShowtime"
        const val KEY_PREF_TEMPERATURE_BLENDINITEMS      = "swTemperatureBlendInDiary"
        const val KEY_PREF_TEMPERATURE_PDF_DAYSEPARATOR  = "swTemperaturePdfDaySeparatorLines"

        const val KEY_PREF_GLUCOSE_SHOW_THRESHOLDS    = "cbShowGlucoseThresholds"
        const val KEY_PREF_GLUCOSE_UNIT               = "lpGlucoseUnit"
        const val KEY_PREF_GLUCOSE_THRESHOLDS         = "etGlucoseThresholds"
        const val KEY_PREF_GLUCOSE_SHOW_GRID          = "cbShowGlucoseGrid"
        const val KEY_PREF_GLUCOSE_SHOW_LEGEND        = "cbShowGlucoseLegend"
        const val KEY_PREF_GLUCOSE_LANDSCAPE          = "cbGlucoseLandscape"
        const val KEY_PREF_GLUCOSE_PAPER_SIZE         = "liGlucosePaperSize"
        const val KEY_PREF_LOG_KETONE                 = "cbLogKetone"
        const val KEY_PREF_GLUCOSE_HIGHLIGHT_VALUES   = "cbGlucoseHighlightValues"
        const val KEY_PREF_GLUCOSE_SHOWTIME           = "swGlucoseShowtime"
        const val KEY_PREF_GLUCOSE_BLENDINITEMS       = "swGlucoseBlendInDiary"
        const val KEY_PREF_GLUCOSE_PDF_DAYSEPARATOR    = "swGlucosePdfDaySeparatorLines"

        const val KEY_PREF_OXIMETRY_SHOW_THRESHOLDS   = "cbShowOximetryThresholds"
        const val KEY_PREF_OXIMETRY_UNIT              = "etOximetryUnit"
        const val KEY_PREF_OXIMETRY_THRESHOLDS        = "etOximetryThresholds"
        const val KEY_PREF_OXIMETRY_SHOW_GRID         = "cbShowOximetryGrid"
        const val KEY_PREF_OXIMETRY_SHOW_LEGEND       = "cbShowOximetryLegend"
        const val KEY_PREF_OXIMETRY_LANDSCAPE         = "cbOximetryLandscape"
        const val KEY_PREF_OXIMETRY_PAPER_SIZE        = "liOximetryPaperSize"
        const val KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES  = "cbOximetryHighlightValues"
        const val KEY_PREF_OXIMETRY_SHOWTIME          = "swOximetryShowtime"
        const val KEY_PREF_OXIMETRY_BLENDINITEMS      = "swOximetryBlendInDiary"
        const val KEY_PREF_OXIMETRY_PDF_DAYSEPARATOR  = "swOximetryPdfDaySeparatorLines"

        const val KEY_PREF_WEIGHT_HIGHLIGHT_VALUES    = "cbWeightHighlightValues"
        const val KEY_PREF_WEIGHT_UNIT                = "liWeightUnit"
        const val KEY_PREF_BODY_HEIGHT                = "etHeight"
        const val KEY_PREF_WEIGHT_THRESHOLD           = "etWeightThreshold"
        const val KEY_PREF_LOG_FAT                    = "cbLogBodyFat"
        const val KEY_PREF_FAT_MIN_MAX                = "etFatMinMax"
        const val KEY_PREF_SHOW_WEIGHT_THRESHOLD      = "cbShowWeightThreshold"
        const val KEY_PREF_SHOW_WEIGHT_GRID           = "cbShowWeightGrid"
        const val KEY_PREF_SHOW_WEIGHT_LEGEND         = "cbShowWeightLegend"
        const val KEY_PREF_WEIGHT_SHOW_MOVING_AVERAGE = "cbWeightShowMovingAverage"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE = "etWeightMovingAverageSize"
        const val KEY_PREF_WEIGHT_LANDSCAPE           = "cbWeightLandscape"
        const val KEY_PREF_WEIGHT_PAPER_SIZE          = "liWeightPaperSize"
        const val KEY_PREF_WEIGHT_TARE                = "etTare"
        const val KEY_PREF_WEIGHT_SHOWTIME            = "swWeightShowtime"
        const val KEY_PREF_WEIGHT_BLENDINITEMS        = "swWeightBlendInDiary"
        const val KEY_PREF_WEIGHT_PDF_DAYSEPARATOR    = "swWeightPdfDaySeparatorLines"
        const val KEY_PREF_SHOW_WEIGHT_LINEAR_TRENDLINE = "cbWeightLinearTrendline"

        const val KEY_PREF_FLUID_THRESHOLD            = "evWaterThreshold"
        const val KEY_PREF_FLUID_UNIT                 = "evWaterUnit"
        const val KEY_PREF_FLUID_SUMMARYPDF           = "swShowSummaryInPDF"
        const val KEY_PREF_FLUID_SUMMARY_CHART        = "swShowSummaryInChart"
        const val KEY_PREF_SHOW_FLUID_THRESHOLD       = "cbShowWaterThreshold"
        const val KEY_PREF_SHOW_FLUID_GRID            = "cbShowWaterGrid"
        const val KEY_PREF_SHOW_FLUID_LEGEND          = "cbShowWaterLegend"
        const val KEY_PREF_FLUID_LANDSCAPE            = "cbWaterLandscape"
        const val KEY_PREF_FLUID_PAPER_SIZE           = "liWaterPaperSize"
        const val KEY_PREF_FLUID_SHOWTIME             = "swWaterShowtime"
        const val KEY_PREF_FLUID_BLENDINITEMS         = "swWaterBlendInDiary"
        const val KEY_PREF_FLUID_PDF_DAYSEPARATOR    = "swWaterPdfDaySeparatorLines"

        const val KEY_PREF_BLOODPRESSURE_HIGHLIGHT_VALUES = "cbBloodPressureHighlightValues"
        const val KEY_PREF_BLOODPRESSURE_UNIT         = "etBloodPressureUnit"
        const val KEY_PREF_LOG_HEART_RHYTHM           = "cbLogHeartRhythm"
        const val KEY_PREF_showBloodPressureThreshold = "cbShowBloodPressureThreshold"
        const val KEY_PREF_showBloodPressureGrid      = "cbShowBloodPressureGrid"
        const val KEY_PREF_showBloodPressureLegend    = "cbShowBloodPressureLegend"
        const val KEY_PREF_SHOWPULSE                               = "cbShowPulse"
        const val KEY_PREF_hyperGrade3                             = "grade3"
        const val KEY_PREF_hyperGrade2                             = "grade2"
        const val KEY_PREF_hyperGrade1                             = "grade1"
        const val KEY_PREF_HYPOTENSION                             = "etHypotension"
        const val KEY_PREF_BLOODPRESSURE_SHOW_MOVING_AVERAGE       = "cbBloodPressureShowMovingAverage"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE       = "etBloodPressureMovingAverageSize"
        const val KEY_PREF_BLOODPRESSURE_PAPER_SIZE                = "liBloodPressurePaperSize"
        const val KEY_PREF_BLOODPRESSURE_LANDSCAPE                 = "cbBloodPressureLandscape"
        const val KEY_PREF_BLOODPRESSURE_TIMEZONES                 = "etTimezones"
        const val KEY_PREF_BLOODPRESSURE_SHOWTIME                  = "swBloodPressureShowtime"
        const val KEY_PREF_BLOODPRESSURE_BLENDINITEMS              = "swBloodPressureBlendInDiary"
        const val KEY_PREF_BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW = "etMultiMeasurementsWindow"
        const val KEY_PREF_BLOODPRESSURE_PDF_DAYSEPARATOR          = "swBloodPressurePdfDaySeparatorLines"
        const val KEY_PREF_BLOODPRESSURE_RIBBON_CHART              =  "swBloodPressureRibbonChart"
        const val KEY_PREF_SHOW_BLOODPRESSURE_LINEAR_TRENDLINE     = "cbBloodPressureLinearTrendline"

        const val KEY_PREF_DIARY_PAPER_SIZE        = "liDiaryPaperSize"
        const val KEY_PREF_DIARY_LANDSCAPE         = "cbDiaryLandscape"
        const val KEY_PREF_DIARY_SHOWTIME          = "swDiaryShowtime"
        const val KEY_PREF_DIARY_SHOWGOOD          = "swShowGoodEmoji"
        const val KEY_PREF_DIARY_SHOWNOTGOOD       = "swShowNotGoodEmoji"
        const val KEY_PREF_DIARY_SHOWBAD           = "swShowBadEmoji"
        const val KEY_PREF_DIARY_BLENDINITEMS      = "swDiaryBlendInItems"
        const val KEY_PREF_DIARY_PDF_DAYSEPARATOR  = "swDiaryPdfDaySeparatorLines"

        const val KEY_PREF_BACKUP_WARNING  = "etBackupWarning"
        const val KEY_PREF_AUTO_BACKUP     = "etAutoBackup"
        const val KEY_PREF_LAST_BACKUP     = "tvLastBackup"
        const val KEY_PREF_BACKUP_URI      = "tvBackupUri"
        const val KEY_PREF_ADD_TIMESTAMP   = "cbAddTimestamp"

        const val KEY_PREF_ACTIVE_TABS_SET      = "liActiveTabs"

        const val KEY_PREF_TEXT_SIZE = "listTextSize"
        const val KEY_PREF_QUICKENTRY = "quickEntry"
        const val KEY_PREF_APP_THEME = "lpAppTheme"
        const val KEY_PREF_PDF_TEXT_SIZE = "etPdfTextSize"
        const val KEY_PREF_PDF_ORDER = "liPdfDataOrder"

        const val KEY_PREF_PASSWORD = "zipPassword"
        const val KEY_PREF_BIOMETRIC = "spEnableBiometric"
        const val KEY_PREF_ENABLECRASHLOGS = "spTurnOnCrashLogs"
        const val KEY_PREF_BLOCK_SCREENSHOTS = "spBlockScreenshots"
        const val KEY_PREF_FORCE_REAUTHENTICATION = "etAuthenticationTimeout"
        const val KEY_PREF_DYNAMIC_COLOR = "swDynamicColor"

        const val KEY_PREF_ENABLEATTACHMENTS = "cbEnableAttachments"

    }
}