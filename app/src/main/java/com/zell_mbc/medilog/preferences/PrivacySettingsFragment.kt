/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.preferences
/*
    This file is part of MediLog.

    MediLog is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MediLog is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MediLog.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2018-2022 by Thomas Zell
*/

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.zell_mbc.medilog.support.BiometricHelper
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R

class PrivacySettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.privacy_preferences, rootKey)

        // Check if biometric device  exists, if not remove biometric setting in settings activity and set authenticated to always true
        val biometricHelper = BiometricHelper(requireContext())
        val canAuthenticate = biometricHelper.canAuthenticate(false)
        if (canAuthenticate != 0) { // Disable controls if biometric is not supported
            val p1: Preference? = findPreference(SettingsActivity.KEY_PREF_BIOMETRIC)
            if (p1 != null) {p1.isEnabled = false }
            val p2: Preference? = findPreference(SettingsActivity.KEY_PREF_FORCE_REAUTHENTICATION)
            if (p2 != null) {p2.isEnabled = false }
        }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}