/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.preferences

import android.os.Bundle
import android.text.InputType
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R

class
OximetrySettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.oximetry_preferences, rootKey)

        val paperSize = preferenceManager.findPreference<ListPreference>(SettingsActivity.KEY_PREF_OXIMETRY_PAPER_SIZE)
        paperSize?.summary = requireContext().getString(R.string.paperSizeSummary) + ", " + paperSize.value
        paperSize?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = paperSize.summary.toString()
            paperSize.summary = tmpString.replace(paperSize.value.toString(), newValue.toString())
            true
        }

        val thresholdObject = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_OXIMETRY_THRESHOLDS)
        var thresholds = ""
        if (thresholdObject?.text.isNullOrEmpty()) {
            thresholds = requireContext().getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT)
            thresholdObject?.text = thresholds
        }
        else thresholds = "" + thresholdObject.text
        thresholdObject?.summary = requireContext().getString(R.string.thresholdSummaryLower) + ", $thresholds%"
        thresholdObject?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = thresholdObject.summary.toString()
            thresholdObject.summary = tmpString.replace(thresholdObject.text.toString(), newValue.toString())
            true
        }

        // Fix an Android bug: https://developer.android.com/develop/ui/views/components/settings/customize-your-settings#customize_an_edittextpreference_dialog
        thresholdObject?.setOnBindEditTextListener { editText -> editText.inputType = InputType.TYPE_CLASS_NUMBER }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}
