/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.preferences
/*
    This file is part of MediLog.

    MediLog is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MediLog is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MediLog.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2018-2022 by Thomas Zell
*/

import android.app.AlertDialog
import android.os.Build
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.widget.Toast
import androidx.preference.EditTextPreference
import androidx.preference.MultiSelectListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreference
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.TextSize
import com.zell_mbc.medilog.R

class GeneralSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.general_preferences, rootKey)

    class DelimiterInputFilter(validDelimiters: String): InputFilter {
            private var filterString = ""
            private var filterHint = ""

            init{
                this.filterString = validDelimiters
                for (c in validDelimiters) this.filterHint = this.filterHint + c + " " // Add blanks for better visibility
            }

            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                try
                {
                    val input = (dest.subSequence(0, dstart).toString() + source + dest.subSequence(dend, dest.length))
                    if (filterString.indexOf(input) >= 0) return null
                    else
                        if (input.isNotEmpty()) Toast.makeText(context, requireContext().getString(R.string.allowedDelimiters) + " $filterHint", Toast.LENGTH_LONG).show()
                }
                catch (_: NumberFormatException) { return "" }  // Define abstract function
                return ""
            }
        }

        class NumericFilter: InputFilter {
            private val filterString = "0123456789"

            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                try {
                    if (filterString.indexOf(source.toString()) >= 0) return null
                    else Toast.makeText(requireContext(), requireContext().getString(R.string.allowedInput) + " $filterString", Toast.LENGTH_LONG).show()
                } catch (_: NumberFormatException) { return "" }  // Define abstract function
                return ""
            }
        }

        // Hide DynamicColor toggle if no device support
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            val switch = preferenceManager.findPreference<SwitchPreference>("swDynamicColor")
            switch?.isVisible = false
        }

    val editTextPreference = preferenceManager.findPreference<EditTextPreference>("delimiter")
    if (editTextPreference != null) {
        editTextPreference.setOnBindEditTextListener { editText ->
            val filterArray = arrayOfNulls<InputFilter>(2)
            filterArray[0] = InputFilter.LengthFilter(1)
            filterArray[1] = DelimiterInputFilter(this.getString(R.string.csvSeparators))
            editText.filters = filterArray
        }

        Preference.OnPreferenceChangeListener { _, newValue ->
            var rtnval = true
            if (newValue.toString().isEmpty()) {
                val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
                builder.setTitle(getString(R.string.invalidInput))
                builder.setMessage(getString(R.string.emptySeparator))
                builder.setPositiveButton(android.R.string.ok, null)
                builder.show()
                rtnval = false
            }
            rtnval
        }.also { editTextPreference.onPreferenceChangeListener = it }
    }

    val textSize = preferenceManager.findPreference<EditTextPreference>(SettingsActivity.KEY_PREF_TEXT_SIZE)
    if (textSize != null)
        {
            textSize.setOnBindEditTextListener { editText ->
                val filterArray = arrayOfNulls<InputFilter>(2)
                filterArray[0] = InputFilter.LengthFilter(2)
                filterArray[1] = NumericFilter()
                editText.filters = filterArray
            }

            textSize.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                // Proper integer?
                var error = false
                var value = -1
                try {
                    value = newValue.toString().toInt()
                } catch (_: Exception) {
                    error = true
                }
                if (error || (value < TextSize.MIN.toInt() || value > TextSize.MAX.toInt())) {
                    val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
                    builder.setTitle(getString(R.string.invalid) + " " + getString(R.string.value))
                    builder.setMessage(getString(R.string.invalidTextSize) + " " + requireContext().getString(R.string.choseValue, TextSize.MIN, TextSize.MAX))
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.show()
                }
                !error
            }
        }

    // Populate tab selection array
    findPreference<MultiSelectListPreference>(SettingsActivity.KEY_PREF_ACTIVE_TABS_SET)?.apply {
        val tabIDs = ArrayList<String>()
        val tabLabels = ArrayList<String>()
        this.values.clear()
        for (tab in MainActivity.availableTabs) {
            tabIDs.add(tab.id.toString())
            tabLabels.add(tab.label)
            if (tab.active) values.add(tab.id.toString())
        }
        val entryDisplay = tabLabels.toTypedArray()
        val entryIDs     = tabIDs.toTypedArray()

        entries     = entryDisplay
        entryValues = entryIDs
    }

    val activeTabsPref = preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_ACTIVE_TABS_SET)
    if (activeTabsPref != null) {
        val activeTabsPrefListener: Preference.OnPreferenceChangeListener =
            object: Preference.OnPreferenceChangeListener {
                override fun onPreferenceChange(preference: Preference, newValue: Any?): Boolean {
                    var activeTabs = ""
                    val hashSet = newValue as HashSet<*>
                    if (hashSet.size  == 0) {
                        Toast.makeText(requireContext(), getString(R.string.noActiveTab), Toast.LENGTH_LONG).show()
                        return false
                    }
                    for (item in hashSet) activeTabs += "$item,"
                    activeTabs = activeTabs.substring(0, activeTabs.length -1) // Remove trailing ,

                    val preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
                    val editor = preferences.edit()
                    editor.putString("activeTabs", activeTabs)
                    editor.apply()

                    //settingsViewModel.setActiveTabs(value = activeTabs)
                    return true
                }
            }
        activeTabsPref.onPreferenceChangeListener = activeTabsPrefListener
    }

}

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}