/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.preferences

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.widget.Toast
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreference
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.bloodpressure.BloodPressureHelper

class BloodPressureSettingsFragment : PreferenceFragmentCompat() {
    val gradeSettingsDivider = "/"

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.bloodpressure_preferences, rootKey)

        val preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE)?.isEnabled = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_SHOW_MOVING_AVERAGE, false)

        val movingAverage = preferenceManager.findPreference<SwitchPreference>(SettingsActivity.KEY_PREF_BLOODPRESSURE_SHOW_MOVING_AVERAGE)
        movingAverage?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            preferenceManager.findPreference<Preference>(SettingsActivity.KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE)?.isEnabled = newValue.toString().toBoolean()
            true
        }

        val paperSize = preferenceManager.findPreference<ListPreference>(SettingsActivity.KEY_PREF_BLOODPRESSURE_PAPER_SIZE)
        paperSize?.summary = requireContext().getString(R.string.paperSizeSummary) + ", " + paperSize.value
        paperSize?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val tmpString = paperSize.summary.toString()
            paperSize.summary = tmpString.replace(paperSize.value.toString(), newValue.toString())
            true
        }

        class GradeInputFilter: InputFilter {
            private val filterString = "0123456789/"

            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                try {
                    if (filterString.indexOf(source.toString()) >= 0) return null
                    else Toast.makeText(requireContext(), requireContext().getString(R.string.allowedInput) + " $filterString", Toast.LENGTH_LONG).show()
                } catch (_: NumberFormatException) { return "" }  // Define abstract function
                return ""
            }
        }

        // Check grade entries
        val editPrefs = ArrayList<EditTextPreference?>()
        editPrefs.add(preferenceManager.findPreference("grade1"))
        editPrefs.add(preferenceManager.findPreference("grade2"))
        editPrefs.add(preferenceManager.findPreference("grade3"))
        editPrefs.add(preferenceManager.findPreference("etHypotension"))

        for (editTextPreference in editPrefs)
            if (editTextPreference != null) {
                editTextPreference.setOnBindEditTextListener { editText ->
                    val filterArray = arrayOfNulls<InputFilter>(2)
                    filterArray[0] = InputFilter.LengthFilter(7)
                    filterArray[1] = GradeInputFilter()
                    editText.filters = filterArray
                }

                editTextPreference.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                    var rtnval = true
                    var error: Boolean

                    // Divider present?
                    error = (!newValue.toString().contains(gradeSettingsDivider))

                    // Proper integers?
                    try {
                        val grade = newValue.toString().split(gradeSettingsDivider).toTypedArray()
                        grade[0].toInt()
                        grade[1].toInt()
                    } catch (_: Exception) { error = true }
                    if (error) {
                        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
                        builder.setTitle(getString(R.string.invalidThresholds))
                        builder.setMessage(getString(R.string.invalidGrade))
                        builder.setPositiveButton(android.R.string.ok, null)
                        builder.show()
                        rtnval = false
                    }
                    rtnval
                }
            }
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        BloodPressureHelper(requireContext())
    }
}