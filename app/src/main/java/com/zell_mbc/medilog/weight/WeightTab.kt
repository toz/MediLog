/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.weight

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.MainActivity.Companion.Threshold.MAX_BODYFAT
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.decimalSeparator
import com.zell_mbc.medilog.MainActivity.Companion.decimalSeparators
import com.zell_mbc.medilog.MainActivity.Companion.modifyDecimalSeparator
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.support.checkThresholds
import java.util.Date

class   WeightTab(context: Context, snackbarDelegate: SnackbarDelegate): BaseTab(context, snackbarDelegate) {

    override var editActivityClass: Class<*> = WeightEditActivity::class.java
    override var infoActivityClass: Class<*> = WeightInfoActivity::class.java
    override var chartActivityClass: Class<*> = WeightChartActivity::class.java

    private var logFat = false

    // String representation of input values
    private var fatLowerThreshold = 0f
    private var fatUpperThreshold = 0f

    private val fatColumnWidth = 45

    init {
        // -------------------------------
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_HIGHLIGHT_VALUES, false)
        showTime        = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_SHOWTIME, context.getString(R.string.SHOWTIME_DEFAULT).toBoolean())
        logFat = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_FAT, context.getString(R.string.LOG_FAT_DEFAULT).toBoolean())
        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, context.getString(R.string.WEIGHT_UNIT_DEFAULT))

        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD, context.getString(R.string.WEIGHT_THRESHOLD_DEFAULT))
        var th = checkThresholds(context, s, context.getString(R.string.WEIGHT_THRESHOLD_DEFAULT), R.string.weight)
        upperThreshold = try { th[1].toFloat() } catch  (_: NumberFormatException) { 0f }
        lowerThreshold = try { th[0].toFloat() } catch  (_: NumberFormatException) { 0f }

        if (logFat && highlightValues) {
            th = checkThresholds(
                context,
                "" + preferences.getString(SettingsActivity.KEY_PREF_FAT_MIN_MAX,context.getString(R.string.FAT_MIN_MAX_DEFAULT)),
                context.getString(R.string.FAT_MIN_MAX_DEFAULT),
                R.string.bodyFat
            )
            fatLowerThreshold = try { th[0].toFloat() } catch  (_: NumberFormatException) { 0f }
            fatUpperThreshold = try { th[1].toFloat() } catch  (_: NumberFormatException) { 0f }
        }
    }

    @Composable
    override fun ShowContent(padding: PaddingValues) {
        super.ShowContent(padding)
        showComment.value = comment.value.isNotEmpty() // For some reason the app will crash if this is in super?

        if (value1Width == 0.dp) MeasureValue1String("100,00")

        //val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent, focusedContainerColor = Color.Transparent)
        // Spacer(modifier = Modifier.height(5.dp))
        LazyColumn(state = listState, horizontalAlignment = Alignment.Start, modifier = Modifier.fillMaxWidth().padding(start = 8.dp, end = 8.dp)) {
            item {
                if (quickEntry) {
                    //val focusManager = LocalFocusManager.current
                    val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }
                    val weightHint = if (modifyDecimalSeparator) context.getString(R.string.weightHint).replace('.', decimalSeparator) else context.getString(R.string.weightHint)

                    Row(Modifier.fillMaxWidth()) {
                        //CompositionCounter(DEBUGTAG)
                        // Weight
                        TextField(
                            value = value1.value,
                            colors = textFieldColors,
                            onValueChange = {
                                if (!logFat) showComment.value = true // only activate comment field if logFat is not visible
                                updateValue1(it)
                                if (((it.length == 4) && (!decimalSeparators.contains(it[it.length-1]))) || (it.length >= 5)) // Allow both y and .
                                    focusManager?.moveFocus(FocusDirection.Next)
                            },
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                            singleLine = true,
                            textStyle = TextStyle(),
                            label = { Text(text = stringResource(id = R.string.weight) + "*", maxLines = 1, overflow = TextOverflow.Ellipsis) },
                            placeholder = { Text(text = "$weightHint $itemUnit") },
                            modifier = Modifier.weight(1f).padding(end = cellPadding.dp).focusRequester(textField1),
                            keyboardActions = KeyboardActions(onDone = {
                                keyboardController?.hide()
                                addItem()
                            }),
                        )
                        if (logFat) {
                            val bodyFatHint = if (modifyDecimalSeparator) context.getString(R.string.bodyFatEntryHint).replace('.', decimalSeparator) else context.getString(R.string.bodyFatEntryHint)
                            TextField(
                                value = value2.value,
                                onValueChange = {
                                    updateValue2(it)
                                    if (showComment.value && ( (it.length == 3) && (!decimalSeparators.contains(it[it.length-1])) ) || (it.length >= 4)) focusManager?.moveFocus(FocusDirection.Next) //textField3.requestFocus()
                                    showComment.value = true
                                },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                                textStyle = TextStyle(),
                                label = { Text(text = stringResource(id = R.string.bodyFat), maxLines = 1, overflow = TextOverflow.Ellipsis) },
                                placeholder = { Text(text = "$bodyFatHint %") },
                                modifier = Modifier.weight(1f).focusRequester(textField2).padding(end = cellPadding.dp),
                                keyboardActions = KeyboardActions(onDone = {
                                    keyboardController?.hide()
                                    addItem() }),
                                colors = textFieldColors
                            )
                        }
                    } // Row

                    if (showComment.value)
                    // Comment
                        TextField(
                            value = comment.value,
                            colors = textFieldColors,
                            onValueChange = { updateComment(it) },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            singleLine = true,
                            textStyle = TextStyle(),
                            modifier = Modifier.fillMaxWidth().focusRequester(textField3),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            trailingIcon = {
                                IconButton(onClick = {
                                    openTextTemplatesDialog.value = true
                                })
                                { Icon(Icons.Outlined.Abc, contentDescription = null) }
                            },
                            keyboardActions = KeyboardActions(onDone = {
                                keyboardController?.hide()
                                addItem() })
                        )

                    SideEffect {
                        // Set cursor to first field after addItem completed
                        if (activateFirstField) {
                            textField1.requestFocus()
                            activateFirstField = false }
                    }

                    // Dialog section
                    if (openTextTemplatesDialog.value) {
                        textTemplateDialog.ShowDialog(setShowDialog = { openTextTemplatesDialog.value = it }) {
                            comment.value += it
                            textField3.requestFocus()
                        }
                    }
                } // End quick entry
            }
//            items(items = dataList.value, key = { item -> item._id }) { item ->
            items(dataList.value) { item ->
                HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, thickness = 1.dp) // Lines starting from the top
                Row(modifier = Modifier.fillMaxWidth().height(IntrinsicSize.Min).clickable { selection.value = item }, verticalAlignment = Alignment.CenterVertically) {
                    ShowRow(item)
                }
                if (selection.value != null) {
                    ItemClicked(selection.value!!._id)
                    selection.value = null
                }
            }
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        // UTC
        //val timeInUtc = item.timestamp - TimeZone.getDefault().rawOffset
        //Text(formatDateString(timeInUtc), Modifier.padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(dateColumnWidth), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)

        Text(formatDateString(item.timestamp), Modifier.padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(dateColumnWidth), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)

        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        var fontStyle = FontStyle.Normal
        var textColor = MaterialTheme.colorScheme.primary
        var weightString: String

        // A diary item blended in
        if (item.type != viewModel.dataType) {
            fontStyle = FontStyle.Italic
            val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
            Text(s, Modifier.padding(start = cellPadding.dp), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp, fontStyle = fontStyle)
            return
        }

        // Making sure the value can be converted
        val weightFloat = try { item.value1.toFloat()} catch (e: NumberFormatException) {0f}
        weightString = "%.1f".format(weightFloat) // Limit to one decimal
        if (highlightValues && ((weightFloat < lowerThreshold) || (weightFloat > upperThreshold))) textColor = MaterialTheme.colorScheme.error

        if (modifyDecimalSeparator) weightString = weightString.replace('.', decimalSeparator)

        Text(weightString, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
            .width(value1Width), textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp,
            fontStyle = fontStyle)

        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Body fat
        if (logFat) {
            var s = item.value2.ifEmpty { "" }
            if (modifyDecimalSeparator) { s = s.replace('.', decimalSeparator) }

            textColor = MaterialTheme.colorScheme.primary
            if (highlightValues) {
                val bodyFat = try { item.value2.toFloat() } catch (e: NumberFormatException) { 0f }
                if ((bodyFat < fatLowerThreshold) || (bodyFat > fatUpperThreshold))
                    textColor = MaterialTheme.colorScheme.error
            }

            //if (MainActivity.modifyDecimalSeparator) s = s.replace('.', MainActivity.decimalSeparator)
            Text(s , Modifier.padding(start = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
                .width(fatColumnWidth.dp), color = textColor, fontSize = fontSize.sp, overflow = TextOverflow.Ellipsis, fontStyle = fontStyle)
            VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator
        }

        // Comment
        val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""

        // style is for removing the padding between multiline text
        Text(s, Modifier.padding(start = cellPadding.dp), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp,
            fontStyle = fontStyle, style = TextStyle(lineHeight = TextUnit.Unspecified, platformStyle = PlatformTextStyle(includeFontPadding = false)))
    }


    override fun addItem() {
        if (!quickEntry) {
            startEditing(-1) // Indicate that this is about a new item
            return
        }

        // Close keyboard after entry is done
        hideKeyboard()

        // Check empty variables
        if (value1.value.isEmpty()) {
            snackbarDelegate.showSnackbar(context.getString(R.string.weightMissing))
//            Toast.makeText(requireContext(), getString(R.string.weightMissing), Toast.LENGTH_LONG).show()
            return
        }

        value1.value = value1.value.replace(decimalSeparator, '.')
        var weightValue = viewModel.validValueF(value1.value, Threshold.MAX_WEIGHT)
        if (weightValue <= 0) {
            snackbarDelegate.showSnackbar(context.getString(R.string.invalid) + " " + context.getString(R.string.value) + " " + value1.value)
            return
        }

        // Grab tare
        val tareString = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_TARE, "0") // Default is no tare
        // Only check tare if an actual value is set
        if (!tareString.isNullOrBlank() && tareString != "0") {
            val tareValue = viewModel.validValueF(tareString)
            if (tareValue <= 0f) {
                snackbarDelegate.showSnackbar(context.getString(R.string.invalid) + " " + context.getString(R.string.tare) + " " + context.getString(R.string.value) + " $tareString")
                return
            }
            else snackbarDelegate.showSnackbar(context.getString(R.string.tareApplied) + ": $tareString$itemUnit")

            // Subtract tare from weight
            weightValue -= tareValue
            if (weightValue <= 0) { // Check result again
                snackbarDelegate.showSnackbar(context.getString(R.string.invalid) + " " + context.getString(R.string.weight) + " " + context.getString(R.string.value) + " $weightValue ( " + context.getString(R.string.tareApplied) + ")")
                return
            }
            value1.value = weightValue.toString()
        }

        // Valid bodyFat?
        if (value2.value.isNotBlank()) {
            //value2Value.value = value2Value.value.replace(MainActivity.decimalSeparator, '.')
            //val bodyFatValue = viewModel.validValueF(value2Value.value, MainActivity.MAX_BODYFAT)
            val bodyFatValue = viewModel.validValueF(value2.value, MAX_BODYFAT)
            if (bodyFatValue <= 0) {
                snackbarDelegate.showSnackbar(context.getString(R.string.invalid) + " " + context.getString(R.string.bodyFat) + " " + context.getString(R.string.value) + " " + value2.value)
                return
            }
        }

        //val timeInUtc = Date().time + TimeZone.getDefault().rawOffset
        //val item = Data(_id = 0, timestamp = timeInUtc, comment = comment.value, type = viewModel.dataType, value1 = value1.value, value2 = value2.value, value3 = "", value4 = "", attachment = "", tags = NO_TAGS, category_id = -1, profile_id = activeProfileId)
        val item = Data(_id = 0, timestamp = Date().time, comment = comment.value, type = viewModel.dataType, value1 = value1.value, value2 = value2.value, value3 = "", value4 = "", attachment = "", tags = NO_TAGS, category_id = -1, profile_id = activeProfileId)
        viewModel.upsert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp))
            snackbarDelegate.showSnackbar(context.getString(R.string.filteredOut))

        cleanUpAfterAddItem()
    }

}