/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.weight
import android.os.Bundle
import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.lifecycle.ViewModelProvider
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottom
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStart
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLine
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.cartesian.rememberVicoZoomState
import com.patrykandpatrick.vico.compose.common.ProvideVicoTheme
import com.patrykandpatrick.vico.compose.m3.common.rememberM3VicoTheme
import com.patrykandpatrick.vico.core.cartesian.Zoom
import com.patrykandpatrick.vico.core.cartesian.axis.HorizontalAxis
import com.patrykandpatrick.vico.core.cartesian.axis.VerticalAxis
import com.patrykandpatrick.vico.core.cartesian.data.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.cartesian.data.CartesianLayerRangeProvider
import com.patrykandpatrick.vico.core.cartesian.data.lineSeries
import com.patrykandpatrick.vico.core.cartesian.layer.LineCartesianLayer
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.ChartActivity
import com.zell_mbc.medilog.base.rememberMarker
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.checkThresholds
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeightChartActivity : ChartActivity() {
    override val filename = "WeightChart.jpg"

    var showBodyFat = false
    //var thresholdLine = 0
    private var minY = 999.9
    private var maxY = 0.1
    private var upperThresholdLine = 0f
    private var lowerThresholdLine = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Not yet implemented
        showBodyFat = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_FAT, getString(R.string.LOG_FAT_DEFAULT).toBoolean())

        showLinearTrendline = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_LINEAR_TRENDLINE, false)
        showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_GRID, getString(R.string.SHOW_WEIGHT_GRID_DEFAULT).toBoolean())
        showLegend = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_LEGEND, getString(R.string.SHOW_WEIGHT_LEGEND_DEFAULT).toBoolean())
        showMovingAverage = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_SHOW_MOVING_AVERAGE, false)
        showThreshold = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_THRESHOLD, getString(R.string.SHOW_WEIGHT_THRESHOLD_DEFAULT).toBoolean())
        if (showThreshold) {
            val s = "" + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD, "")
            val th = checkThresholds(this, s, getString(R.string.WEIGHT_THRESHOLD_DEFAULT), R.string.weight)
            lowerThresholdLine = try { th[0].toFloat() } catch  (e: NumberFormatException) { 0f }
            upperThresholdLine = try { th[1].toFloat() } catch  (e: NumberFormatException) { 0f }
        }

        legendText = mutableListOf(getString(R.string.weight))
        chartColors = mutableListOf(lineColor1)

        if (showBodyFat) {
            legendText.add(getString(R.string.bodyFat))
            chartColors.add(lineColor2)
        }
        if (showLinearTrendline) {
            legendText.add(getString(R.string.trendline))
            chartColors.add(linearTrendlineColor)
        }

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[WeightViewModel::class.java]

        // For the charts only native entries can be considered, no matter what showAllTabs is set to
        val tmp = viewModel.blendInItems
        viewModel.blendInItems = false
        items = viewModel.getItems("ASC", filtered = true)
        viewModel.blendInItems = tmp

        if (items.size < 2) {
            Toast.makeText(this, getString(R.string.notEnoughDataForChart), Toast.LENGTH_LONG).show()
            finish()
            return // Needs return to avoid the rest to be executed because finish() may not kill the activity fast enough
        }

        // Fill data maps
        if (showMovingAverage) {
            val p = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE, getString(R.string.WEIGHT_MOVING_AVERAGE_SIZE_DEFAULT))
            if (p != null) {
                val period = try { p.toInt() } catch (e: NumberFormatException) { 0 }
                val ma = getMovingAverageFloat(items, period)
                var i = 0
                for (item in items) {item.value1 = ma[i++].toString() }
            }
        }

        // Collect min max y values
        for (item in items) {
            val i1 = try { item.value1.toFloat() } catch (e: NumberFormatException) { 0F }
            if (showLinearTrendline) linearTrendline.add(i1)
            if (i1 < minY) minY = i1.toDouble()
            if (i1 > maxY) maxY = i1.toDouble()

            if (showBodyFat) {
                val i2 = try { item.value2.toFloat() } catch (e: NumberFormatException) { 0F }
                if (i2 < minY) minY = i2.toDouble()
            }
        }
        minY = minY.toInt().toDouble() - 1.0
        maxY = maxY.toInt().toDouble() + 1.0

        // Compile linear trendline
        if (showLinearTrendline) linearTrendline = getLinearTrendlineFloat(linearTrendline)

        //val x = items.toString()
        xAxisOffset = items[0].timestamp
        value1Data = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value1.toFloat() } catch (e: NumberFormatException) { 0F } })
        if (showBodyFat) value2Data = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value2.toFloat() } catch (e: NumberFormatException) { 0F } })
        if (showLinearTrendline) {
            var i = 0
            linearTrendlineData = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { linearTrendline[i++] } catch (e: NumberFormatException) { 0F } })
        }
        showContent()
    }

    @Composable
    override fun ShowContent() {
        Box(Modifier.safeDrawingPadding()) {
            val modelProducer = remember { CartesianChartModelProducer() }
            LaunchedEffect(Unit) {
                withContext(Dispatchers.Default) {
                    modelProducer.runTransaction {
                        lineSeries {
                            series(value1Data.keys, value1Data.values)
                            if (showBodyFat) series(value2Data.keys, value2Data.values)
                            if (showLinearTrendline) series(linearTrendlineData.keys, linearTrendlineData.values)
                        }
                    }
                }
            }
            ProvideVicoTheme(rememberM3VicoTheme()) { ComposeChart(modelProducer, Modifier.fillMaxHeight()) }
        }
    }

    @Composable
    private fun ComposeChart(modelProducer: CartesianChartModelProducer, modifier: Modifier) {
        val lineProvider = LineCartesianLayer.LineProvider.series(
            buildList {
                add(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(chartColors[0]))))
                if (showBodyFat) add(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(chartColors[1])), areaFill = null))
                if (showLinearTrendline) add(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(linearTrendlineColor)), areaFill = null))
            }
        )
        val span = maxY - minY
        CartesianChartHost(
            chart = rememberCartesianChart(
                rememberLineCartesianLayer(lineProvider = lineProvider, rangeProvider = CartesianLayerRangeProvider.fixed(minY = minY, maxY = maxY)),
                startAxis = VerticalAxis.rememberStart(
                    guideline = guideline(), // horizontal lines
                    itemPlacer = if (span > 5) remember { VerticalAxis.ItemPlacer.step(step =  { (span / 5).toInt().toDouble() }, shiftTopLines = false) }
                    else remember { VerticalAxis.ItemPlacer.step(step = { (0.5).toDouble() }, shiftTopLines = false) }
                ),
                bottomAxis = HorizontalAxis.rememberBottom(
                    guideline = guideline(), // vertical lines
                    valueFormatter = bottomAxisValueFormatter, // Convert values back to proper dates
                ),
                legend = if (showLegend) rememberLegend() else null,
                decorations = if (showThreshold) listOf(helperLine("", upperThresholdLine.toDouble(), chartColors[0]), helperLine("", lowerThresholdLine.toDouble(), chartColors[0])) else listOf(),
                marker = rememberMarker(),
            ),
            modelProducer = modelProducer,
            modifier = modifier,
            zoomState = rememberVicoZoomState(zoomEnabled = true, maxZoom = Zoom.max(Zoom.static(100f), Zoom.Content), initialZoom = Zoom.min(Zoom.static(), Zoom.Content)),
        )
    }
}
