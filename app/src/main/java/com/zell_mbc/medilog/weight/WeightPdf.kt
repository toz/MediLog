/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.weight

import android.app.Activity
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.widget.Toast
import androidx.compose.material3.MaterialTheme
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.decimalSeparator
import com.zell_mbc.medilog.MainActivity.Companion.modifyDecimalSeparator
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.checkThresholds
import kotlin.math.roundToLong

class WeightPdf(activity: Activity): BasePdf(activity) {
    // Base class overrides
    override var itemName = activity.getString(R.string.weight)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_LANDSCAPE, activity.getString(R.string.WEIGHT_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_PAPER_SIZE, activity.getString(R.string.WEIGHT_PAPER_SIZE_DEFAULT))
    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_BLENDINITEMS, activity.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())
    override var printDaySeparatorLines = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_PDF_DAYSEPARATOR, activity.getString(R.string.DAY_SEPARATOR_LINE_DEFAULT).toBoolean())
    override var highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_HIGHLIGHT_VALUES, false)

    // Weight specific
    private val unit = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, activity.getString(R.string.WEIGHT_UNIT_DEFAULT))
    private val logBodyFat = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_FAT, activity.getString(R.string.LOG_FAT_DEFAULT).toBoolean())

    private var fatTab = 0f

    private var weightTabWidth = 0f
    private var fatTabWidth = 0f

    private var fatLowerThreshold = 0
    private var fatUpperThreshold = 100


    override fun createPdfDocument(): PdfDocument? {
        // Check here to be able to return right away in case of error
        if (viewModel.getSize(true) == 0) {
            Toast.makeText(activity, activity.getString(R.string.bloodPressure) + ": " + activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        super.createPdfDocument()

        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD, activity.getString(R.string.WEIGHT_THRESHOLD_DEFAULT))
        val th = checkThresholds(activity, s, activity.getString(R.string.WEIGHT_THRESHOLD_DEFAULT), R.string.weight)
        val lowerThreshold = try { th[0].toFloat() } catch  (_: NumberFormatException) { 0f }
        val upperThreshold = try { th[1].toFloat() } catch  (_: NumberFormatException) { 0f }

        //var weightThreshold = 0
        //if (!s.isNullOrEmpty()) weightThreshold = s.toInt()

        // Only check thresholds if bodyFat is logged
        if (logBodyFat) {
            val minMax = checkBodyFatThresholds(activity)
            fatLowerThreshold = minMax[0]
            fatUpperThreshold = minMax[1]
        }

        // -----------
        setColumns()

        initDataPage()

        var f = pdfDataTop + pdfLineSpacing
        commentWidth = measureColumn(pdfRightBorder - commentTab)

        var currentDay = toStringDate(pdfItems[0].timestamp)

        for (item in pdfItems) {
            if (printDaySeparatorLines) {
                val itemDay = toStringDate(item.timestamp)
                if (currentDay != itemDay) {
                    f -= (pdfLineSpacing - pdfDaySeparatorLineSpacer) // Go back one line and add half a daySeparatorLineHeight
                    canvas.drawLine(pdfLeftBorder, f, pdfRightBorder, f, pdfPaintDaySeparator)
                    f += (pdfLineSpacing - pdfDaySeparatorLineSpacer)
                    currentDay = itemDay
                }
            }

            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)
            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)

            // If this is a blended item, draw comment and be done
            if (item.type != Tabs.WEIGHT) {
                f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
                continue
            }

            val weightValue = try {
                item.value1.toFloat()
            } catch (_: NumberFormatException) {
                0f
            }

            val roundedString = weightValue.toString() + unit
            if (highlightValues && ((weightValue < lowerThreshold) || (weightValue > upperThreshold))) canvas.drawText(roundedString, dataTab + space, f, pdfPaintHighlight)
            else canvas.drawText(roundedString, dataTab + space, f, pdfPaint)

            if (logBodyFat) {
                val fatValue = try {
                    item.value2.toInt()
                } catch (_: NumberFormatException) {
                    0
                }

                val bodyFat = if (item.value2.isNotBlank()) item.value2 + "%" else ""
                if (highlightValues && (fatValue < fatLowerThreshold || fatValue > fatUpperThreshold))
                    canvas.drawText(bodyFat, fatTab + space, f, pdfPaintHighlight)
                else {
                    canvas.drawText(bodyFat, fatTab + space, f, pdfPaint)
                }
            }
            f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
        }
        // finish the page
        document.finishPage(page)

        // Add statistics page
        createPage()
        drawStatsPage(pdfPaint, pdfPaintHighlight)
        document.finishPage(page)

        /* Add chart page
        createPage()
        addChartPage()
        document.finishPage(page) */

        return document
    }

    /*fun addChartPage() {
        // Draw individual chart
        val chart = WeightChart(activity = activity, chartWidth = pageWidth)
        //…and let super do the bitmap handling
        super.addChartPage(chart)
    }*/

    override fun setColumns() {
        weightTabWidth = pdfPaintHighlight.measureText(activity.getString(R.string.weight))

        dataTab = pdfLeftBorder + dateTabWidth + padding // Need a bit more space to cater for month name length
        if (logBodyFat) {
            fatTabWidth = pdfPaintHighlight.measureText(activity.getString(R.string.bodyFat))
            fatTab = dataTab + weightTabWidth + padding
            commentTab = fatTab + fatTabWidth + padding
        }
        else commentTab = dataTab + weightTabWidth + padding
    }

    // Draw header and columns
    override fun initDataPage() {
        drawHeader()

        // Data section
        canvas.drawLine(dataTab, pdfHeaderBottom, dataTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint) // Line before comment

        canvas.drawText(activity.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.weight), dataTab + space, pdfDataTop, pdfPaintHighlight)
        if (logBodyFat) {
            canvas.drawText(activity.getString(R.string.bodyFat), fatTab + space, pdfDataTop, pdfPaintHighlight)
            canvas.drawText(activity.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
            canvas.drawLine(fatTab, pdfHeaderBottom, fatTab, pdfDataBottom, pdfPaint) // Line before body fat
        }
        else canvas.drawText(activity.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
    }

    private fun drawStatsPage(pdfPaint: Paint, pdfPaintHighlight: Paint) {
        super.drawStatsPage(R.drawable.ic_weight_filled)

        var y = pdfDataTop + pdfLineSpacing

        // BMI stuff
        // Get most recent weight
        val weight = try {
            viewModel.getLast()?.value1?.toFloat() ?: 0f
        } catch (_: NumberFormatException) {
            0f
        }

        val sBodyHeight = "" + preferences.getString(SettingsActivity.KEY_PREF_BODY_HEIGHT, "")
        val itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, activity.getString(R.string.WEIGHT_UNIT_DEFAULT))
        var bodyHeight = 0f
        var bmiString = activity.getString(R.string.notAvailableShort)
        var sTargetWeightLow = ""
        var sTargetWeightHigh = ""
        var sWeightDifferenceLow = ""
        var sWeightDifferenceHigh = ""
        var fBMI = 0f
        var fTargetWeightHigh = 0.0
        var fTargetWeightLow = 0.0
        var weightDifferenceLow = 0.0
        var weightDifferenceHigh = 0.0

        y += pdfLineSpacing
        y += pdfLineSpacing
        if (sBodyHeight.isNotEmpty() && weight > 0f) {
            try {
                bodyHeight = sBodyHeight.toFloat()
            } catch (_: NumberFormatException) {
                bodyHeight = 0f
                Toast.makeText(activity, activity.getString(R.string.invalidBodyHeight), Toast.LENGTH_LONG).show()
            }
            fBMI = if (itemUnit.contains("lb")) // Imperial measures
                weight / bodyHeight / bodyHeight * 703 // Requires Height in inches and weight in lbs
            else // Metric
                weight / bodyHeight / bodyHeight * 10000 // Height is entered in cm, formula expects m -> * 10k

            fTargetWeightLow = if (itemUnit.contains("lb")) // Imperial measures
                18.5 * bodyHeight * bodyHeight / 703 // Requires Height in inches and weight in lbs
            else // Metric
                18.5 * bodyHeight * bodyHeight / 10000 // Height is entered in cm, formula expects m -> * 10k

            weightDifferenceLow = weight - fTargetWeightLow
            weightDifferenceHigh = weight - fTargetWeightHigh

            sTargetWeightLow = ((fTargetWeightLow * 100).roundToLong() / 100f).toString() // Round to 2 digits
            sWeightDifferenceLow = ((weightDifferenceLow * 100).roundToLong() / 100f).toString() // Round to 2 digits
            sTargetWeightHigh = ((fTargetWeightHigh * 100).roundToLong() / 100f).toString() // Round to 2 digits
            sWeightDifferenceHigh = ((weightDifferenceHigh * 100).roundToLong() / 100f).toString() // Round to 2 digits

            bmiString = ((fBMI * 100).roundToLong() / 100f).toString() // Round to 2 digits

            if (modifyDecimalSeparator) bmiString = bmiString.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sTargetWeightLow = sTargetWeightLow.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sWeightDifferenceLow = sWeightDifferenceLow.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sTargetWeightHigh = sTargetWeightHigh.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sWeightDifferenceHigh = sWeightDifferenceHigh.replace('.', decimalSeparator)

            canvas.drawText(activity.getString(R.string.bmiActual) + " $bmiString", pdfLeftBorder, y, pdfPaint)
            y += pdfLineSpacing
        }

        if (bmiString != activity.getString(R.string.notAvailableShort)) { // Don't bother showing if not calculated
            canvas.drawText(activity.getString(R.string.bmiNormal) + " $sTargetWeightLow - $sTargetWeightHigh $itemUnit", pdfLeftBorder, y, pdfPaint)
            y += pdfLineSpacing
            canvas.drawText(activity.getString(R.string.bmiDifference) + " $sWeightDifferenceLow - $sWeightDifferenceHigh $itemUnit", pdfLeftBorder, y, pdfPaint)
        }

        y += pdfLineSpacing
        canvas.drawLine(pdfLeftBorder, y, pdfRightBorder, y, pdfPaintDaySeparator)
        y += pdfLineSpacing

        if ((viewModel.filterStart + viewModel.filterStart) > 0L) {
            gatherData(filtered = true)
            y = ShowBlock(filterData, y)

            canvas.drawLine(pdfLeftBorder, y, pdfRightBorder, y, pdfPaintDaySeparator)
            y += pdfLineSpacing
        }

        gatherData(filtered = false)
        y = ShowBlock(totalData, y)
    }

    fun gatherData(filtered: Boolean) {
        val arr = if (filtered) filterData else totalData

        count = viewModel.getSize(filtered)
        var item: Data? = viewModel.getFirst(filtered)
        if (item == null) {
            Toast.makeText(activity, activity.getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }

        val startDate = dateFormat.format(item.timestamp)

        item = viewModel.getLast(filtered)
        if (item == null) {
            Toast.makeText(activity, activity.getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val endDate = dateFormat.format(item.timestamp)

        arr.add(if (!filtered) activity.getString(R.string.measurementsInDB) + " $count"
        else activity.getString(R.string.measurementsInFilter) + " $count")

        val avg = viewModel.getAvgFloat("value1", filtered)
        var tmp = avg.toString()
        if (tmp.length > 5) tmp = tmp.substring(0, 5)
        if (modifyDecimalSeparator) tmp = tmp.replace('.', decimalSeparator)
        arr.add(activity.getString(R.string.average) + ": $tmp $unit")

        val min = viewModel.getMinValue1(filtered)
        val max = viewModel.getMaxValue1(filtered)
        var minMaxString = activity.getString(R.string.minMaxValues) + " $min - $max $unit"
        if (modifyDecimalSeparator) minMaxString = minMaxString.replace('.', decimalSeparator)
        arr.add(minMaxString)
        arr.add(activity.getString(R.string.timePeriod) + " $startDate - $endDate")
    }

    fun ShowBlock(arr: ArrayList<String>, y: Float): Float {
        var yPos = y
        arr.forEach {
            canvas.drawText(it, pdfLeftBorder, yPos, pdfPaint)
            yPos += pdfLineSpacing
        }
        return yPos
    }
}