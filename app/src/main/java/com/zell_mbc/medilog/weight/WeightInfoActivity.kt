/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.weight

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.HorizontalDivider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.base.InfoActivity
import com.zell_mbc.medilog.preferences.SettingsActivity
import kotlin.math.roundToLong
import androidx.compose.material3.Text
import androidx.compose.material3.MaterialTheme
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.withLink
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity.Companion.LINK_COLOR

class WeightInfoActivity: InfoActivity() {
    private var sBMI = ""
    private var sTargetWeightLow = ""
    private var sTargetWeightHigh = ""
    private var sWeightDifferenceLow = ""
    private var sWeightDifferenceHigh = ""
    private var bodyHeight = 0f
    private var fBMI = 0f
    private var fTargetWeightHigh = 0.0
    private var fTargetWeightLow = 0.0
    private var weightDifferenceLow = 0.0
    private var weightDifferenceHigh = 0.0
    var weight = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[WeightViewModel::class.java]

        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, getString(R.string.WEIGHT_UNIT_DEFAULT))
        gatherBMI()
        gatherData(false)

        setContent { StartCompose() }
    }

    @Composable
    fun ShowBlock(arr: ArrayList<String>) {
        arr.forEach { Text(it, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary, style = MaterialTheme.typography.bodyMedium) }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.verticalScroll(rememberScrollState()).padding(8.dp)) {
            HeaderBlock(R.drawable.ic_weight_filled)
            // BMI stuff
            Text(getString(R.string.bmiActual) + " $sBMI", modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
            if (sBMI != getString(R.string.notAvailableShort)) { // Don't bother showing if not calculated
                Text(getString(R.string.bmiNormal) + " $sTargetWeightLow - $sTargetWeightHigh $itemUnit", lineHeight = 15.sp, fontSize = 12.sp, modifier = Modifier.padding(start = leftPadding.dp, top = 0.dp), color = MaterialTheme.colorScheme.primary)
                Text(getString(R.string.bmiDifference) + " $sWeightDifferenceLow - $sWeightDifferenceHigh $itemUnit", lineHeight = 15.sp, fontSize = 12.sp, modifier = Modifier.padding(start = leftPadding.dp, top = 0.dp), color = MaterialTheme.colorScheme.primary)
            }

            Text(buildAnnotatedString {
                append("")
                withLink(LinkAnnotation.Url(url = stringResource(id = R.string.bmiChart), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(stringResource(id = R.string.bmiChart)) }
            }, modifier = Modifier.padding(start = leftPadding.dp), style = MaterialTheme.typography.bodySmall, textDecoration = TextDecoration.Underline)

            Text("")
            HorizontalDivider(modifier = Modifier.fillMaxWidth().padding(start = leftPadding.dp), thickness = 1.dp, color = MaterialTheme.colorScheme.primaryContainer)
            Text("")
            if ((viewModel.filterStart + viewModel.filterStart) > 0L) {
                gatherData(true)
                ShowBlock(filterData)
                Text("")
                HorizontalDivider(modifier = Modifier.fillMaxWidth().padding(start = leftPadding.dp), thickness = 1.dp, color = MaterialTheme.colorScheme.primaryContainer)
                Text("")
            }
            measurementsIn = measurementsInDB
            ShowBlock(totalData)
            ShowAttachments()
        }
    }

    private fun gatherBMI() {
        try {
            val tmp = viewModel.getLast()?.value1?.toFloat()
            if (tmp != null) weight = tmp }
        catch (e: NumberFormatException) { weight = 0f }

        sBMI = getString(R.string.notAvailableShort)

        val sBodyHeight = "" + preferences.getString(SettingsActivity.KEY_PREF_BODY_HEIGHT, "")
        //val bodyHeight = activeProfile?.height ?: 0
        if (sBodyHeight.isNotEmpty() && weight > 0f) {
            try {
                bodyHeight = sBodyHeight.toFloat()
            } catch (e: NumberFormatException) {
                bodyHeight = 0f
                Toast.makeText(application, getString(R.string.invalidBodyHeight), Toast.LENGTH_LONG).show()
            }
            fBMI = if (itemUnit.contains("lb")) // Imperial measures
                weight / bodyHeight / bodyHeight * 703 // Requires Height in inches and weight in lbs
            else // Metric
                weight / bodyHeight / bodyHeight * 10000 // Height is entered in cm, formula expects m -> * 10k

            // Adult normal BMI 18.5 - 25
            fTargetWeightHigh = if (itemUnit.contains("lb")) // Imperial measures
                24.9 * bodyHeight * bodyHeight / 703 // Requires Height in inches and weight in lbs
            else // Metric
                24.9 * bodyHeight * bodyHeight / 10000 // Height is entered in cm, formula expects m -> * 10k

            fTargetWeightLow = if (itemUnit.contains("lb")) // Imperial measures
                18.5 * bodyHeight * bodyHeight / 703 // Requires Height in inches and weight in lbs
            else // Metric
                18.5 * bodyHeight * bodyHeight / 10000 // Height is entered in cm, formula expects m -> * 10k
            weightDifferenceLow = weight - fTargetWeightLow
            weightDifferenceHigh = weight - fTargetWeightHigh

            sBMI = ((fBMI * 100).roundToLong() / 100f).toString() // Round to 2 digits
            sTargetWeightLow = ((fTargetWeightLow * 100).roundToLong() / 100f).toString() // Round to 2 digits
            sWeightDifferenceLow = ((weightDifferenceLow * 100).roundToLong() / 100f).toString() // Round to 2 digits
            sTargetWeightHigh = ((fTargetWeightHigh * 100).roundToLong() / 100f).toString() // Round to 2 digits
            sWeightDifferenceHigh = ((weightDifferenceHigh * 100).roundToLong() / 100f).toString() // Round to 2 digits

            if (modifyDecimalSeparator) sBMI = sBMI.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sTargetWeightLow = sTargetWeightLow.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sWeightDifferenceLow = sWeightDifferenceLow.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sTargetWeightHigh = sTargetWeightHigh.replace('.', decimalSeparator)
            if (modifyDecimalSeparator) sWeightDifferenceHigh = sWeightDifferenceHigh.replace('.', decimalSeparator)
        }
    }

    private fun gatherData(filtered: Boolean) {
        val arr = if (filtered) filterData else totalData

        count = viewModel.getSize(filtered)
        var item: Data? = viewModel.getFirst(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val startDate = dateFormat.format(item.timestamp)

        item = viewModel.getLast(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val endDate = dateFormat.format(item.timestamp)

        arr.add(if (!filtered) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count")

        val avg = viewModel.getAvgFloat("value1", filtered)
        var tmp = avg.toString()
        if (tmp.length > 5) tmp = tmp.substring(0, 5)
        if (modifyDecimalSeparator) tmp = tmp.replace('.', decimalSeparator)
        arr.add(getString(R.string.average) + ": $tmp $itemUnit")

        val min = viewModel.getMinValue1(filtered)
        val max = viewModel.getMaxValue1(filtered)
        minMaxString = getString(R.string.minMaxValues) + " $min - $max $itemUnit"
        if (modifyDecimalSeparator) minMaxString = minMaxString.replace('.', decimalSeparator)
        arr.add(minMaxString)
        arr.add(getString(R.string.timePeriod) + " $startDate - $endDate")
    }
}