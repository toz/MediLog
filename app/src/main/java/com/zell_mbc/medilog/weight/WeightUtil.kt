/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.weight

import android.content.Context
import android.widget.Toast
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.preferences.SettingsActivity

fun checkBodyFatThresholds(context: Context): Array<Int> {
    //val userOutputService: UserOutputService = UserOutputServiceImpl(context,null)
    var fatLowerThreshold = 0
    var fatUpperThreshold = 100
    val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    val delimiter = "-"
    val defaultThresholds = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
    var checkValue = preferences.getString(SettingsActivity.KEY_PREF_FAT_MIN_MAX, defaultThresholds)
    if (!checkValue.isNullOrEmpty()) {
        val minMax = checkValue.split(delimiter)

        try {
            fatLowerThreshold = minMax[0].toInt()
        } catch (e: NumberFormatException) {
            Toast.makeText(context, "Invalid Body Fat thresholds: $checkValue", Toast.LENGTH_LONG).show()
            //userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }
        try {
            fatUpperThreshold = minMax[1].toInt()
        } catch (e: NumberFormatException) {
            Toast.makeText(context, "Invalid Body Fat thresholds: $checkValue", Toast.LENGTH_LONG).show()
            //userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }

        if (fatUpperThreshold <= fatLowerThreshold) {
            Toast.makeText(context, "Invalid Body Fat thresholds: $checkValue", Toast.LENGTH_LONG).show()
            //userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }

        if (fatUpperThreshold > 100) {
            Toast.makeText(context, "Invalid Body Fat thresholds: $checkValue", Toast.LENGTH_LONG).show()
            //userOutputService.showAndHideMessageForLong("Invalid Body Fat thresholds: $checkValue")
            checkValue = context.resources.getString(R.string.FAT_MIN_MAX_DEFAULT)
        }

    }
    else checkValue = defaultThresholds

    var minMax = checkValue.split(delimiter)
    val ret = try { arrayOf(minMax[0].toInt(), minMax[1].toInt())}
    catch (e: NumberFormatException) {
        minMax = defaultThresholds.split(delimiter)
        arrayOf(minMax[0].toInt(), minMax[1].toInt())
    }

    return ret
}
