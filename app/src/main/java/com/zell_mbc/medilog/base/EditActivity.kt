/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.addCallback
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Attachment
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material.icons.filled.Save
import androidx.compose.material3.BottomAppBarDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.core.content.FileProvider
import androidx.preference.PreferenceManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.dialogs.MediLogTimePickerDialog
import com.zell_mbc.medilog.texttemplates.TextTemplateDialog
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.MedilogTheme
import kotlinx.coroutines.launch
import java.io.File
import java.io.InputStream
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.support.deleteApplicationCache
import com.zell_mbc.medilog.support.getAttachmentFolder
import com.zell_mbc.medilog.support.getCorrectedDateFormat
import com.zell_mbc.medilog.tags.TagsDialog
import com.zell_mbc.medilog.tags.TagsViewModel
import java.text.DecimalFormatSymbols
import java.util.Calendar
import java.util.TimeZone
import kotlin.text.contains

abstract class EditActivity: AppCompatActivity() {
    abstract val dataType: Int

    lateinit var viewModel: DataViewModel

    lateinit var preferences: SharedPreferences
    lateinit var editItem: Data
    lateinit var dateFormat: DateFormat
    private lateinit var timeFormat: DateFormat
    lateinit var snackbarDelegate: SnackbarDelegate
    lateinit var activity: Activity

    //private val PERMISSION_CODE = 1000
    private val IMAGE_CAPTURE_CODE = 1001
    private val FILE_CAPTURE_CODE = 1002

    var fileName = ""
    //var fileUri: Uri? = null
    var attachmentFolder = ""
    var attchementsEnabled = false

    var value1Unit = ""
    var value2Unit = ""
    var value1Hint = ""
    var value2Hint = ""
    var highlightValues = false

    // Dialog control variables
    var showTextTemplatesDialog by mutableStateOf(false)
    var showDatePickerDialog    by mutableStateOf(false)
    var showTimePickerDialog    by mutableStateOf(false)
    var showTagsDialog    by mutableStateOf(false)

    // Data values
    var timestamp = 0L
    var commentString by mutableStateOf("") // MutableState makes Compose monitor the value
    var value1String  by mutableStateOf("")
    var value2String  by mutableStateOf("")
    var value3String  by mutableStateOf("")
    var value4String  by mutableStateOf("")
    var tagIds  by mutableStateOf("")
    var attachmentString by mutableStateOf("")
    var attachmentDrawable by mutableIntStateOf(0)

    var unsavedAttachment = false
    var unsavedAttachmentUri: Uri? = null // Unlink images, Local "files" may not be available as files, hence use this to save the Uri

    // Date/Time picker button labels
    var itemTimeString by mutableStateOf("")
    var itemDateString by mutableStateOf("")

    lateinit var textTemplateDialog: TextTemplateDialog
    lateinit var tagsDialog: TagsDialog

    // Retrieve decimal separator for current language
    val decimalSeparator = DecimalFormatSymbols.getInstance().decimalSeparator
    val modifyDecimalSeparator = (decimalSeparator.compareTo('.') != 0)

    // Vars to be stored in bundle
    var editItemIndex = -1

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //Log.d(DEBUGTAG, "onSaveInstance:" + outState )
        outState.putInt("editItemIndex",editItemIndex)
        outState.putLong("timestamp",timestamp)
        outState.putString("value1String",value1String)
        outState.putString("value2String",value2String)
        outState.putString("value3String",value3String)
        outState.putString("value4String",value4String)
        outState.putString("commentString",commentString)
        outState.putString("commentString",commentString)
        outState.putString("commentString",commentString)
        outState.putString("tagIds",tagIds)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         //   Log.d(DEBUGTAG, "onCreate:" + savedInstanceState )
        activity = this

        //setOnBackPressListener { handleBackPress() }
       // viewModel = MainActivity.viewModel

        editItemIndex = if (savedInstanceState != null) savedInstanceState.getInt("editItemIndex",0)
        else // Initial launch of activity
            intent.getIntExtra("editItemIndex", -1)

        editItem = if (editItemIndex < 1) { // We are dealing with a new item, create it first
            Data(0, timestamp = Date().time, type = viewModel.dataType, profile_id = activeProfileId, tags = NO_TAGS, category_id = -1, attachment = "") }
        else {
            val tmpItem = viewModel.getItem(editItemIndex)
            if (tmpItem == null) {
                //(getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0) // Close keyboard after entry is done
                Toast.makeText(this, getString(R.string.waitForUIUpdate), Toast.LENGTH_LONG).show()
                //snackbarDelegate.showSnackbar(getString(R.string.waitForUIUpdate))
                finish()
                return
            }
            tmpItem
        }

        if (savedInstanceState != null) { // Restored activity, pull values from bundle
            value1String  = savedInstanceState.getString("value1String","")
            value2String  = savedInstanceState.getString("value2String","")
            value3String  = savedInstanceState.getString("value3String","")
            value4String  = savedInstanceState.getString("value4String","")
            commentString = savedInstanceState.getString("commentString","")
            tagIds        = savedInstanceState.getString("tagIds","")
            timestamp     = savedInstanceState.getLong("timestamp",0L)
        }
        else { // Initial launch, pull values from database
            value1String  = editItem.value1
            value2String  = editItem.value2
            value3String  = editItem.value3
            value4String  = editItem.value4
            commentString = editItem.comment
            tagIds        = editItem.tags
            timestamp     = editItem.timestamp
        }

        dateFormat = getCorrectedDateFormat(this) //Handle EN_DE bug?
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())

        // Collect values from editItem and convert to strings for Compose fields
        itemTimeString = timeFormat.format(timestamp)
        itemDateString = dateFormat.format(timestamp)

        attachmentString = editItem.attachment
        if (attchementsEnabled) if (attachmentString.isNotEmpty()) attachmentDrawable = R.drawable.ic_baseline_attach_file_24

        textTemplateDialog = TextTemplateDialog(this)
        tagsDialog = TagsDialog(this)

        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        if (preferences.getBoolean(SettingsActivity.KEY_PREF_BLOCK_SCREENSHOTS, false))
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        highlightValues    = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_HIGHLIGHT_VALUES, false)
        //attchementsEnabled = preferences.getBoolean(SettingsActivity.KEY_PREF_ENABLEATTACHMENTS, false)
        attachmentFolder = getAttachmentFolder(this)

        enableEdgeToEdge() //This will include/color the top Android info bar
        onBackPressedDispatcher.addCallback(this) { handleBackPress() } // Handle the 'hardware' back button
    }

    // This handles the toolbar back arrow
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            if (isDirty()) {
                MaterialAlertDialogBuilder(this)
                    .setTitle(resources.getString(R.string.attention))
                    .setMessage(getString(R.string.unsavedData))
                    .setNegativeButton(resources.getString(R.string.no)) { _, _ ->  // Respond to negative button press
                    }
                    .setPositiveButton(resources.getString(R.string.yes)) { _, _ ->  // Respond to positive button press
                        finish()
                    }
                    .show()
            }
            else finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun handleBackPress(): Boolean {
        val dirty = isDirty()
        // perform action
        if (dirty) {
            MaterialAlertDialogBuilder(activity)
                .setTitle(resources.getString(R.string.attention))
                .setMessage(getString(R.string.unsavedData))
                .setNegativeButton(resources.getString(R.string.no)) { _, _ ->  // Respond to negative button press
                    //return@setNegativeButton
                }
                .setPositiveButton(resources.getString(R.string.yes)) { _, _ ->  // Respond to positive button press
                    finish()
                }
                .show()
        }
        return !dirty
    }

    override fun onDestroy() {
        super.onDestroy()
        deleteApplicationCache(this) // delete orphaned images
    }

    // Check if any field has changed
    fun isDirty(): Boolean { return ((editItem.timestamp  != timestamp) ||
                                     (editItem.comment    != commentString) ||
                                     (editItem.value1     != value1String) ||
                                     (editItem.value2     != value2String) ||
                                     (editItem.value3     != value3String) ||
                                     (editItem.value4     != value4String) ||
                                     (editItem.tags       != tagIds) ||
                                     (editItem.attachment != attachmentString))
    }


    //Kick off compose framework
    @Composable
    abstract fun ShowContent()

    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    @Composable
    fun StartCompose() {
        val snackbarHostState = remember { SnackbarHostState() }
        snackbarDelegate = SnackbarDelegate(snackbarHostState, rememberCoroutineScope())

        // Scroll UI when keyboard opens
        // https://medium.com/@mark.frelih_9464/how-to-handle-automatic-content-resizing-when-keyboard-is-visible-in-jetpack-compose-1c76e0e17c57
        val scrollState = rememberScrollState()
        val coroutineScope = rememberCoroutineScope()
        val keyboardHeight = WindowInsets.ime.getBottom(LocalDensity.current)

        LaunchedEffect(key1 = keyboardHeight) {
            coroutineScope.launch {
                scrollState.scrollBy(keyboardHeight.toFloat())
            }
        }

        KeyboardAware {

            MedilogTheme {
                Scaffold( //contentWindowInsets = WindowInsets(0.dp),
                    snackbarHost = { SnackbarHost(snackbarHostState) },
                    /* topBar = {
                    TopAppBar(modifier = Modifier.height(40.dp),
                        title = { "" },
                        actions = {
                            IconButton(onClick = { saveItem() }) { Icon(imageVector = Icons.Default.Save, contentDescription = "Search") }
                            IconButton(onClick = { getAttachment() }) { Icon(imageVector = Icons.Default.Attachment, contentDescription = "Attachment") }
                        }
                    )
                },*/
                    floatingActionButton = {
                        FloatingActionButton(
                            onClick = { deleteItem() },
                            containerColor = BottomAppBarDefaults.bottomAppBarFabColor,
                            elevation = FloatingActionButtonDefaults.elevation()
                        ) {
                            Icon(Icons.Filled.DeleteForever, "Delete item")
                        }
                    },
                    content = { paddingValues ->
                        Column(modifier = Modifier.padding(paddingValues).verticalScroll(scrollState)) { // This is necessary so the topbar is recognised
                            LaunchedEffect(key1 = keyboardHeight) {
                                coroutineScope.launch {
                                    scrollState.scrollBy(keyboardHeight.toFloat())
                                }
                            }
                            ShowContent()
                        }
                    }
                )
            }
        }
    }

    @Composable
    fun KeyboardAware(
        content: @Composable () -> Unit
    ) {
        Box(modifier = Modifier.imePadding()) {
            content()
        }
    }

    @Composable
    fun DateTimeBlock() {
        Surface(
            color = MaterialTheme.colorScheme.secondaryContainer,
            //border = BorderStroke(1.dp, MaterialTheme.colorScheme.secondary),
            shape = RoundedCornerShape(8.dp),
            tonalElevation = 8.dp,
            modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.fillMaxWidth().padding(start = 16.dp, top = 16.dp, end = 16.dp, bottom = 16.dp)
            ) {
                Row(verticalAlignment = Alignment.CenterVertically)
                {
                    Column(Modifier.width(80.dp)) { Text(getString(R.string.date), color = MaterialTheme.colorScheme.onTertiaryContainer) }
                    Column { OutlinedButton(onClick = { showDatePickerDialog = true }) { Text(itemDateString, color = MaterialTheme.colorScheme.onTertiaryContainer) } }
                    Column(horizontalAlignment = Alignment.End, modifier = Modifier.fillMaxWidth()) {
                        IconButton(modifier = Modifier.scale(1.5f), onClick = { saveItem() }) { Icon(imageVector = Icons.Default.Save, contentDescription = "Save") }
                        if (attchementsEnabled) IconButton(onClick = { getAttachment() }) { Icon(imageVector = Icons.Default.Attachment, contentDescription = "Attachment") }
                    }
                }
                Row(verticalAlignment = Alignment.CenterVertically)
                {
                    Column(Modifier.width(80.dp)) { Text(getString(R.string.time), color = MaterialTheme.colorScheme.onTertiaryContainer) }
                    Column { OutlinedButton(onClick = { showTimePickerDialog = true }) { Text(itemTimeString, color = MaterialTheme.colorScheme.onTertiaryContainer) } }
                    // modifier = Modifier.width(buttonWidth),
                }
            }
        }
        Text("")
    }


    @Composable
    fun AttachmentBlock() {
        if (!attchementsEnabled) return
        //var showAttachmentString = ""

        // If there's a temporary string show this one instead of what's stored
        //showAttachmentString = if (temporaryAttachmentUri != null) temporaryAttachmentUri.toString() else attachmentString

        if (attachmentString.isEmpty()) return

        val colorFilter = if (isSystemInDarkTheme()) ColorFilter.tint(Color.White) else null
        with (attachmentString) {
            when {
                contains(".jpg") ||
                contains(".png") -> {
                    val u = getAttachmentUri(attachmentString)
                    if (u == null) return

                    val input = contentResolver.openInputStream(u)

                    if (input == null) return
                    val bitmap by remember { mutableStateOf(BitmapFactory.decodeStream(input, null, null)!!.asImageBitmap()) }

                    //bitmap.value = BitmapFactory.decodeStream(input, null,null)
                    input.close()
                    // if (bitmap != null) {
                    //   val bm = bitmap!!.asImageBitmap()
                    Image(bitmap = bitmap, contentDescription = "Image", modifier = Modifier.fillMaxWidth().clickable { onAttachmentClick() })
                }
                contains(".pdf") -> Image(modifier = Modifier.requiredSize(50.dp).clickable { onAttachmentClick() }, painter = painterResource(id = R.drawable.baseline_picture_as_pdf_24), colorFilter = colorFilter, contentDescription = "PDF Attachment")
                else ->             Image(modifier = Modifier.requiredSize(50.dp).clickable { onAttachmentClick() }, painter = painterResource(id = R.drawable.ic_baseline_attach_file_24), colorFilter = colorFilter, contentDescription = "Attachment")
            }
        }
    }

    @Composable
    fun OpenDatePickerDialog() {
        MediLogDatePickerDialog(
            onAccept = {
                showDatePickerDialog = false // close dialog
                if (it != null) { // Set the date
                    timestamp = it
                    itemDateString = dateFormat.format(it) // Update text button
                }
            },
            onCancel = { showDatePickerDialog = false }//close dialog
        )
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun MediLogDatePickerDialog(
        onAccept: (Long?) -> Unit,
        onCancel: () -> Unit,
    ) {
        val state = rememberDatePickerState(initialSelectedDateMillis = timestamp + TimeZone.getDefault().rawOffset)

        // Preserve time value
        val oldDateTime = Calendar.getInstance()
        oldDateTime.timeInMillis = timestamp

        DatePickerDialog(
            onDismissRequest = { },
            confirmButton = { Button(onClick = {
                if (state.selectedDateMillis != null) {
                    // Convert from UTC and apply preserved time
                    val selectedLocal = Calendar.getInstance()
                    selectedLocal.timeInMillis = state.selectedDateMillis!! - TimeZone.getDefault().rawOffset
                    selectedLocal.set(Calendar.HOUR_OF_DAY, oldDateTime.get(Calendar.HOUR_OF_DAY))
                    selectedLocal.set(Calendar.MINUTE, oldDateTime.get(Calendar.MINUTE))
                    selectedLocal.set(Calendar.SECOND, 0)
                    onAccept(selectedLocal.timeInMillis)
                }
            }) { Text(getString(R.string.ok)) } },
            dismissButton = { Button(onClick = onCancel) { Text(getString(R.string.cancel)) } }) {
            DatePicker(state = state)
        }
    }

    @Composable
    fun OpenTimePickerDialog() {
        MediLogTimePickerDialog(
            activity = activity,
            initValue = timestamp,
            onCancel = { showTimePickerDialog = false },
            onConfirm = {
                val cal = Calendar.getInstance()
                cal.timeInMillis = timestamp
                cal.set(Calendar.HOUR_OF_DAY, it.get(Calendar.HOUR_OF_DAY))
                cal.set(Calendar.MINUTE, it.get(Calendar.MINUTE))
                //cal.isLenient = false
                timestamp = cal.timeInMillis // Update timestamp with new time settings
                itemTimeString = timeFormat.format(cal.timeInMillis)
                showTimePickerDialog = false
            },
        )
    }

    // Supporting functions
    fun deleteItem() {
        // Do your really?
        val title = getString(R.string.deleteItem)
        MaterialAlertDialogBuilder(this)
            .setTitle(title)
            .setMessage(resources.getString(R.string.doYouReallyWantToContinue))
            .setNeutralButton(resources.getString(R.string.cancel)) { _, _ ->
                // Respond to neutral button press
            }
            .setPositiveButton(resources.getString(R.string.yes)) { _, _ ->
                viewModel.delete(editItem._id)

                // Does this entry have an associated photo?
                if (attachmentString.isNotEmpty()) {
                    val file = File(attachmentFolder, attachmentString)
                    //var ret = false
                    if (file.exists()) file.delete()
                    //Log.d("File delete:", ret.toString())
                }
                snackbarDelegate.showSnackbar(getString(R.string.word_item).replaceFirstChar { it.titlecase(Locale.ROOT) } + " " + getString(R.string.word_deleted))
                finish()
            }
            .show()
    }

    @SuppressLint("SimpleDateFormat")
    open fun saveItem() {
        // Any temp attachments which need to be confirmed?
        if (unsavedAttachment)
            attachmentString = createAttachment()

        //Transfer strings back to editItem before it get's saved to the DB
        editItem.value1    = value1String
        editItem.value2    = value2String
        editItem.value3    = value3String
        editItem.value4    = value4String
        editItem.tags      = tagIds

        editItem.comment   = commentString
        editItem.timestamp = timestamp
        editItem.attachment = attachmentString

        viewModel.upsert(editItem)
        //if (finish) finish() // Close current window / activity
    }

    // +++++++++++++++++++++++++++++++++
    // Attachment support functions
    // +++++++++++++++++++++++++++++++++

    fun attachmentPresent(): Boolean {
        return (attachmentString.isNotEmpty())
    }


    // Move and rename selected file/photo to local encrypted MediLog storage
    // This function gets called upon saving this editItem
    // FileUri is the new file, attachment is either empty or points to an old attachment
    @SuppressLint("SimpleDateFormat")
    fun createAttachment(): String {
        var newAttachment = ""

        // Any attachments to look after?
        if (attachmentString.isNotEmpty()) {
            val fileName: String
            if (!attachmentString.contains("files")) { // If uri points to local files it has been stored already
                //Log.d("Debug:", "External file")
                val resolver = contentResolver
                val prefix = com.zell_mbc.medilog.support.ATTACHMENT_LABEL + viewModel.itemName + "_" + SimpleDateFormat("yyMMddHHmmss").format(Date().time) + "_"
                //if (!fileName.contains(prefix))
                val fileUri = getAttachmentUri(attachmentString) ?: return ""
                fileName = prefix + getFileName(resolver, fileUri) // Images get their prefix on photo creation time, no need to add twice
                val inputStream: InputStream? = resolver.openInputStream(fileUri)
                val newFile = File(attachmentFolder, fileName)

                //val newFile = File(attachmentFolder, fileName)
                //val masterKey = MasterKey.Builder(this, MasterKey.DEFAULT_MASTER_KEY_ALIAS).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
                //val newEncryptedFile = EncryptedFile.Builder(this, newFile, masterKey, EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB).build()
                //inputStream.use { input -> newEncryptedFile.openFileOutput().use { output -> input!!.copyTo(output) } }

                inputStream.use { input -> newFile.outputStream().use { output -> input!!.copyTo(output) } }

                // Do we hold a different file as attachment = Is this an update? Delete old file
                if (editItem.attachment != attachmentString) {
                    val oldFile = File(attachmentFolder, editItem.attachment)
                    if (oldFile.exists()) oldFile.delete()
                    //Log.d("Photo-delete old:", ret2.toString())
                }
                newAttachment = fileName // Store file name only, the actual location is app dependend
                unsavedAttachment = false
            }
        }
        return newAttachment
    }

    // Turn Uri into String
    fun getFileName(resolver: ContentResolver, fileUri: Uri): String? {
        var uri = fileUri
        var result: String?

        //if uri is content
        if (uri.scheme != null && uri.scheme == "content") {
            val cursor: Cursor? = resolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    //local filesystem
                    var index = cursor.getColumnIndex("_data")
                    if (index == -1) //google drive
                        index = cursor.getColumnIndex("_display_name")
                    result = cursor.getString(index)
                    uri = if (result != null) Uri.parse(result) else return null
                }
            } finally {
                cursor!!.close()
            }
        }
        result = uri.path

        //get filename + ext of path
        val cut = result!!.lastIndexOf('/')
        if (cut != -1) result = result.substring(cut + 1)
        return result
    }


    // Creates Uri from string
    fun getAttachmentUri(attachment: String): Uri? {
        var fileUri: Uri?

        // Are we dealing with an unsaved file and file != image?
        // If yes return the Uri we kept upon selecting
        if (unsavedAttachmentUri != null) return unsavedAttachmentUri

        //From here on in it can be only unsaved images or saved attachments

        // If a path is present this is an unsaved and unencrypted(!) image
        val tmpAttachment = attachment.contains("/")
        val file = if (tmpAttachment) File(attachment) else File(attachmentFolder, attachment)

        if (file.exists()) {
            fileUri = FileProvider.getUriForFile(this, applicationContext.packageName + ".provider", file)
        }
        else { // Should not happen…
            snackbarDelegate.showSnackbar("Error: Attachment not found!")
            return null
        }

        // Proper attachments are encrypted
        //if (!tmpAttachment) fileUri = decryptAttachment(this, fileUri)

        return fileUri
    }


    fun onAttachmentClick() {
        val uri = getAttachmentUri(attachmentString)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = uri
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        try {
            startActivity(intent)
            //Toast.makeText(this, "Attachment clicked", Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            snackbarDelegate.showSnackbar(getString(R.string.eShareError) + ": " + e.localizedMessage)
        }
    }

    fun hideKeyboard() {
        val v = currentFocus
        if (v != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }


    // Add Attachment functions
    private fun getAttachment() {
        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.attachmentDialogTitle))
            .setMessage(resources.getString(R.string.attachmentDialogText))
            .setNegativeButton(resources.getString(R.string.photo)) { _, _ -> openCamera() }
            .setPositiveButton(resources.getString(R.string.file)) { _, _ -> getFile() }
            .show()
    }


    private fun getFile() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "application/*" }
        startActivityForResult(intent, FILE_CAPTURE_CODE)
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "MediLogPicture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "Ad-hoc snapshot")

        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        // set filename
        //val timeStamp = SimpleDateFormat(MainActivity.DATE_TIME_PATTERN).format(Date().time)
        fileName = "photo.jpg" //ATTACHMENT_LABEL +  viewModel.itemName + "_" + timeStamp + ".jpg"
        //Log.d("New file:", fileName)

        // set direcory folder
        val file = File(cacheDir, fileName)
        val fileUri = FileProvider.getUriForFile(this, applicationContext.packageName + ".provider", file)

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            //Log.d("Request code:",requestCode.toString())
            when (requestCode) {
                FILE_CAPTURE_CODE -> {
                    try {
                        val fileUri = data?.data
                        //attachmentDrawable = R.drawable.ic_baseline_attach_file_24
                        unsavedAttachment = true
                        unsavedAttachmentUri = fileUri
                        attachmentString = fileUri.toString()
                    } catch (_: Exception) {
                        //Toast userOutputService.showMessageAndWaitForLong(this.getString(R.string.eSelectDirectory) + " " + data)
                    }
                }
                // Camera image will be stored in cache folder by Android
                IMAGE_CAPTURE_CODE -> {
                    //File object of camera image
                    val file = File(cacheDir, fileName)
                    //val fileSize = file.length()
                    //Log.d("Photo size:", fileSize.toString())
                    // Todo: if (fileSize > MAX_IMAGE) resize image
                    attachmentString = file.toString() //fileUri.toString()
                    unsavedAttachment = true
                }
            }
        }
    }

    fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch(_: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) snackbarDelegate.showSnackbar(errorMessage)
        return valueInt
    }

}