/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.base

import android.content.SharedPreferences
import android.os.Bundle
import android.text.format.Formatter
import android.view.WindowManager
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.MedilogTheme
import com.zell_mbc.medilog.support.getCorrectedDateFormat
import java.text.DateFormat
import java.text.DecimalFormatSymbols

abstract class InfoActivity : AppCompatActivity() {
    lateinit var preferences: SharedPreferences
    lateinit var viewModel: DataViewModel

    var filterData = ArrayList<String>(0)
    var totalData = ArrayList<String>(0)

    var measurementsIn = ""
    var measurementsInDB = ""
    var measurementsInFilter = ""
    var timeperiodInDB = ""
    var timeperiodInFilter = ""
    var timeperiodIn = ""

    var dateFormat: DateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
    var count = 0
    var timePeriod = ""
    val leftPadding = 16
    val bottomPadding = 16
    val cellPadding = 2
    var attachmentCount = 0
    var attachmentSize = 0L
    var itemUnit = ""
    var avgString = ""
    var minMaxString = ""

    // Retrieve decimal separator for current language
    val decimalSeparator = DecimalFormatSymbols.getInstance().decimalSeparator
    val modifyDecimalSeparator = (decimalSeparator.compareTo('.') != 0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge() //This will include/color the top Android info bar

        dateFormat = getCorrectedDateFormat(this)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        if (preferences.getBoolean(SettingsActivity.KEY_PREF_BLOCK_SCREENSHOTS, false))
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        attachmentCount = 0 //countAttachments(this, ATTACHMENT_LABEL + viewModel.itemName)
        attachmentSize = 0 //attachmentSize(this, ATTACHMENT_LABEL + viewModel.itemName)
    }

    @Composable
    fun ShowAttachments() {
        if (attachmentCount > 0) {
            Text("")
            Text(getString(R.string.attachments) + " : ", modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary, style = MaterialTheme.typography.bodyMedium)
            Text(getString(R.string.count) + " : " + attachmentCount.toString(), modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary, style = MaterialTheme.typography.bodySmall)
            Text(getString(R.string.size) + " : " + Formatter.formatFileSize(application, attachmentSize), modifier = Modifier.padding(start = leftPadding.dp, bottom = bottomPadding.dp), color = MaterialTheme.colorScheme.primary, style = MaterialTheme.typography.bodySmall)
        }
    }

    // Kick off compose framework
    @Composable
    abstract fun ShowContent()

    @Composable
    fun StartCompose() {
        MedilogTheme { ShowContent() }
    }

    @Composable
    fun HeaderBlock(drawable: Int) {
        val background = MaterialTheme.colorScheme.secondaryContainer
        Surface(
            color = background,
            shape = RoundedCornerShape(8.dp),
            tonalElevation = 8.dp,
            modifier = Modifier.safeDrawingPadding().fillMaxWidth()) {
         Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp, bottom = 16.dp)
            ) {
                Row(horizontalArrangement = Arrangement.End) {
                    Text(getString(R.string.statistics), modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.onSecondaryContainer, fontWeight = FontWeight.Bold)
                    Spacer(modifier = Modifier.weight(1f)) // Somehow needed to move the pic to the right
                    Surface(modifier = Modifier.size(50.dp).padding(end = 8.dp),color = background) {
                        Image(painter = painterResource(id = drawable), contentDescription = null)
                    }
                }
            }
        }
    }
}
