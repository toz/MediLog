/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */
package com.zell_mbc.medilog.base

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.SoftwareKeyboardController
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.withLink
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.LINK_COLOR
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.texttemplates.TextTemplateDialog
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.support.getCorrectedDateFormat
import com.zell_mbc.medilog.tags.TagsDialog
import java.text.DateFormat
import java.util.Calendar
import java.util.Locale
import java.util.regex.Pattern

abstract class BaseTab(val context: Context, val snackbarDelegate: SnackbarDelegate) {
    abstract var editActivityClass: Class<*>
    abstract var infoActivityClass: Class<*>
    abstract var chartActivityClass: Class<*>

    lateinit var dataList: State<List<Data>>
    lateinit var listState: LazyListState

    //New
    lateinit var value1:  MutableState<String>
    lateinit var value2:  MutableState<String>
    lateinit var value3:  MutableState<String>
    lateinit var value4:  MutableState<String>
    lateinit var comment: MutableState<String>
    lateinit var tags:    MutableState<String>

    lateinit var showComment: MutableState<Boolean>
    lateinit var selection: MutableState<Data?>
    lateinit var textFieldColors: TextFieldColors

    var keyboardController: SoftwareKeyboardController? = null
    var focusManager: FocusManager? = null

    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val dateFormat: DateFormat
    private val timeFormat: DateFormat

    var highlightValues: Boolean = false
    var showTime: Boolean = true
    var quickEntry: Boolean = true
    var selectedField = ""

    var fontSize = 0
    var dateColumnWidth = 0.dp
    var value1Width = 0.dp
    var value2Width = 0.dp
    var value3Width = 0.dp

    val PAPERCLIP =  "\uD83D\uDCCE"

    var itemUnit = ""
    var lowerThreshold = 0f
    var upperThreshold = 0f

    val cellPadding = 5 // Padding before and after a text in the grid
    var rowPadding = 1 // Padding tab list rows (top & bottom)
    var activateFirstField = false

    // Dialog control variables
    var showOpenDialog = mutableStateOf(false)
    var showTagsDialog = mutableStateOf(false)
    var openTextTemplatesDialog = mutableStateOf(false)
    private var showDialog = mutableStateOf(false)

    var textTemplateDialog: TextTemplateDialog
    var tagsDialog: TagsDialog

    val urlPattern: Pattern = Pattern.compile(
        "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
        Pattern.CASE_INSENSITIVE or Pattern.MULTILINE or Pattern.DOTALL
    )


    init {
        dateFormat  = getCorrectedDateFormat(context)
        timeFormat  = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())

        fontSize = context.getString(R.string.TEXT_SIZE_DEFAULT).toInt() // Set defined value in case the below fails
        val checkValue = preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, context.getString(R.string.TEXT_SIZE_DEFAULT))
        if (!checkValue.isNullOrEmpty()) {
            var tmp: Int
            try { tmp = checkValue.toInt()
            } catch (e: NumberFormatException) {
                tmp = context.getString(R.string.TEXT_SIZE_DEFAULT).toInt()
                snackbarDelegate.showSnackbar("Invalid Font Size value: $checkValue")
            }
            if (tmp > 0) fontSize = tmp
        }

        quickEntry = preferences.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)
        textTemplateDialog = TextTemplateDialog(context)
        tagsDialog = TagsDialog(context)
    }

    // Helper functions to make sure UI and data are in sync
    fun updateValue1(value: String) {
        value1.value = value
        viewModel.value1 = value
    }
    fun updateValue2(value: String) {
        value2.value = value
        viewModel.value2 = value
    }
    fun updateValue3(value: String) {
        value3.value = value
        viewModel.value3 = value
    }
    fun updateValue4(value: String) {
        value4.value = value
        viewModel.value4 = value
    }
    fun updateComment(value: String) {
        comment.value = value
        viewModel.comment = value
    }

    @Composable
    open fun ShowContent(padding: PaddingValues) {
        // Initialize lateinits
        value1  = remember { mutableStateOf( viewModel.value1 )}
        value2  = remember { mutableStateOf( viewModel.value2 )}
        value3  = remember { mutableStateOf( viewModel.value3 )}
        value4  = remember { mutableStateOf( viewModel.value4 )}
        comment = remember { mutableStateOf( viewModel.comment )}
        tags    = remember { mutableStateOf( viewModel.tags )}

        showComment = remember { mutableStateOf(false) }
       // showComment.value = comment.value.isNotEmpty()

        keyboardController = LocalSoftwareKeyboardController.current
        focusManager = LocalFocusManager.current
        textFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent, focusedContainerColor = Color.Transparent)

        dataList = viewModel.query().collectAsState(initial = emptyList())
        selection = remember { mutableStateOf(null) }
        listState = rememberLazyListState()

        if (dateColumnWidth == 0.dp) MeasureDateString(showTime)
    }

    abstract fun addItem()

    fun showInfoScreen(context: Context) {
        val items = viewModel.getItems("ASC", filtered = true)
        if (items.isNotEmpty()) {
            val intent = Intent(context, infoActivityClass)
            context.startActivity(intent)
        }
        else Toast.makeText(context, context.getString(R.string.warning) + " " + context.getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
    }

    fun showChartScreen(context: Context) {
        val items = viewModel.getItems("ASC", filtered = true)
        if (items.size < 2) {
            Toast.makeText(context, context.getString(R.string.notEnoughDataForChart), Toast.LENGTH_LONG).show()
            return
        }

        val intent = Intent(context, chartActivityClass)
        context.startActivity(intent)
    }

    // Helper functions
    // Measure width of date string
    @Composable
    fun MeasureDateString(showTime: Boolean = true) {
        val textStyle = TextStyle(fontSize = fontSize.sp)
        val sampleCal = Calendar.getInstance()
        sampleCal.set(2015,11,17,23,5)
        val millis = sampleCal.timeInMillis
        val sampleString = if (showTime) dateFormat.format(millis) + " - " + timeFormat.format(millis) else dateFormat.format(millis)
        MeasureTextWidth(sampleString) { textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            dateColumnWidth = textWidth //+ (textWidth / 10) // Add a 10% buffer
        }
    }

    // Helper functions
    @Composable
    fun MeasureValue1String(s: String) {
        MeasureTextWidth(s) { textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            value1Width = textWidth
        }
    }

    @Composable
    fun MeasureValue2String(s: String) {
        MeasureTextWidth(s) { textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            value2Width = textWidth
        }
    }

    @Composable
    fun MeasureValue3String(s: String) {
        MeasureTextWidth(s) { textWidth ->
            Text(text = "", fontSize = fontSize.sp)
            value3Width = textWidth
        }
    }

    @Composable
    fun MeasureTextWidth(sampleText: String, content: @Composable (width: Dp) -> Unit) {
        SubcomposeLayout { constraints ->
            val textWidth = subcompose(sampleText) {
                Text(sampleText, fontSize = fontSize.sp)
            }[0].measure(Constraints()).width.toDp()

            val contentPlaceable = subcompose("content") {
                content(textWidth)
            }[0].measure(constraints)
            layout(contentPlaceable.width, contentPlaceable.height) {
                contentPlaceable.place(0, 0)
            }
        }
    }

    @Composable
    fun ItemClicked(selectedId: Int) {
        val editItem = viewModel.getItem(selectedId)

        // Are we dealing with a genuine item or one which is blended in?
        if (editItem != null) {
            if (editItem.type != viewModel.dataType) {
                snackbarDelegate.showSnackbar(context.getString(R.string.blendedItem))
                return
            }
        }
        else {
            snackbarDelegate.showSnackbar("Error: Item with id $selectedId not found!")
            return
        }

        val openDialog = remember { mutableStateOf(true) }

        if (selectedField.equals("comment")) {
            selectedField = ""
            val urlMatcher = urlPattern.matcher(editItem.comment)
            var matchStart: Int
            var matchEnd: Int
            var url = ""
            if (urlMatcher.find()) {
                matchStart = urlMatcher.start(1)
                matchEnd = urlMatcher.end()
                url = editItem.comment.substring(matchStart, matchEnd)

                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                context.startActivity(intent)
            }
            else if (openDialog.value) startEditing(selectedId)
        }
        else if (openDialog.value) startEditing(selectedId)
                //showDialog.value = false

        openDialog.value = false
    }

    fun startEditing(index: Int) {
        //viewModel.editItemIndex = index
        val intent = Intent(context, editActivityClass)
        intent.putExtra("editItemIndex", index)
        context.startActivity(intent)
    }

    fun hideKeyboard() {
        /*        val v = MainActivity.mediLog.currentFocus
                if (v != null) {
                    val imm = MainActivity.mediLog.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }*/
    }

    @Composable
    fun HideKeyboard() {
        val controller = LocalSoftwareKeyboardController.current
        controller?.hide()
        /*val v = context.currentFocus
        if (v != null) {
            val imm = activity?.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(v.windowToken, 0)
        }*/
    }

    fun showKeyboard() {
        /*val v = context.currentFocus
        if (v != null) {
            val imm = activity?.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
            v.requestFocus()
            imm.showSoftInput(v, 0) // Todo: Doesn't work reliably
        }*/
    }

    @Composable
    fun ShowKeyboard() {
        val controller = LocalSoftwareKeyboardController.current
        controller?.show()
    }

    fun formatDateString(m: Long): String = if (showTime) dateFormat.format(m) + " - " + timeFormat.format(m) else dateFormat.format(m)

    // Post addItem actions
    fun cleanUpAfterAddItem() {
        // New
        // Clean up UI
        value1.value  = ""
        value2.value  = ""
        value3.value  = ""
        value4.value  = ""
        comment.value = ""
        tags.value    = ""

        // Clean up data container
        viewModel.value1  = ""
        viewModel.value2  = ""
        viewModel.value3 = ""
        viewModel.value4 = ""
        viewModel.comment = ""
        viewModel.tags = ""

        hideKeyboard()
        activateFirstField = true //<- Potentially make this a setting? Annoying if you add only one item at a time
    }

    @Composable
    fun compileAnnotatedString(source: String): AnnotatedString {
        val urlMatcher = urlPattern.matcher(source)
        var matchStart: Int
        var matchEnd: Int
        var url = ""
        var retString: AnnotatedString = AnnotatedString("")
        if (urlMatcher.find()) {
            matchStart = urlMatcher.start(1)
            matchEnd = urlMatcher.end()
            url = source.substring(matchStart, matchEnd)

            retString = androidx.compose.ui.text.buildAnnotatedString {
                append(source.substring(0, matchStart))
                withLink(LinkAnnotation.Url(url = url, TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(url) }
                append(source.substring(matchEnd))
            }
        }
        else retString = androidx.compose.ui.text.buildAnnotatedString { append(source) }
        return retString
    }
}
