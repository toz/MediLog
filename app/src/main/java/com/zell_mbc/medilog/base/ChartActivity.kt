/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.base

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.text.Layout
import android.view.WindowManager
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.preference.PreferenceManager
import com.patrykandpatrick.vico.compose.common.component.rememberLineComponent
import com.patrykandpatrick.vico.compose.common.component.rememberShapeComponent
import com.patrykandpatrick.vico.compose.common.component.rememberTextComponent
import com.patrykandpatrick.vico.compose.common.component.shapeComponent
import com.patrykandpatrick.vico.compose.common.data.rememberExtraLambda
import com.patrykandpatrick.vico.compose.common.dimensions
import com.patrykandpatrick.vico.compose.common.rememberHorizontalLegend
import com.patrykandpatrick.vico.compose.common.shape.dashedShape
import com.patrykandpatrick.vico.compose.common.vicoTheme
import com.patrykandpatrick.vico.core.cartesian.CartesianDrawingContext
import com.patrykandpatrick.vico.core.cartesian.CartesianMeasuringContext
import com.patrykandpatrick.vico.core.cartesian.data.CartesianValueFormatter
import com.patrykandpatrick.vico.core.cartesian.decoration.HorizontalLine
import com.patrykandpatrick.vico.core.common.Legend
import com.patrykandpatrick.vico.core.common.LegendItem
import com.patrykandpatrick.vico.core.common.shape.CorneredShape
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.MedilogTheme
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.Locale
import java.util.concurrent.TimeUnit

abstract class ChartActivity: AppCompatActivity() {
    lateinit var preferences: SharedPreferences
    lateinit var viewModel: DataViewModel

    var showLegend = false
    var showGrid = false
    var showMovingAverage = false
    var showThreshold = false
    var showLinearTrendline = false

    lateinit var chartColors: MutableList<Color>
    lateinit var legendText: MutableList<String>

    lateinit var items: MutableList<Data>
    abstract val filename: String

    // Data lists
    open val timestamps = mutableListOf<Int>()

    lateinit var value1Data: Map<Long, Float>
    lateinit var value2Data: Map<Long, Float>

    lateinit var value1DataI: Map<Long, Int>
    lateinit var value2DataI: Map<Long, Int>
    lateinit var value3DataI: Map<Long, Int>

    lateinit var linearTrendlineData: Map<Long, Float>
    var linearTrendline = mutableListOf<Float>()
    val lineColor1 = Color(0xffb983ff)
    val lineColor2 = Color(0xff91b1fd)
    val lineColor3 = Color(0xff8fdaff)

    val linearTrendlineColor = Color.Gray

    var xAxisOffset = 0L
    private val xAxisPrecision = 60000 // 1 Minute
    var xAxisFormat = SimpleDateFormat("MMM dd")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        if (preferences.getBoolean(SettingsActivity.KEY_PREF_BLOCK_SCREENSHOTS, false))
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        enableEdgeToEdge() //This will include/color the top Android info bar
    }

    val bottomAxisValueFormatter = CartesianValueFormatter { contextRanges, x, _ -> xAxisFormat.format((x * xAxisPrecision) + xAxisOffset)}
    fun scaledTimeStamp(timestamp: Long) = ((Instant.ofEpochMilli(timestamp).toEpochMilli()- xAxisOffset) / xAxisPrecision)


    fun showContent() {
        checkTimeSpan()

        setContent {
            MedilogTheme {
                Modifier.safeDrawingPadding()
                ShowContent()
            }
        }
    }

    @Composable
    abstract fun ShowContent()

    @Composable
    fun guideline() = if (showGrid) rememberLineComponent(
        color = MaterialTheme.colorScheme.outlineVariant,
        shape = remember { dashedShape(shape = CorneredShape.Pill, dashLength = 4.dp, gapLength = 8.dp) },
    ) else null

    @Composable
    fun helperLine(label: String, value: Double, color: Color): HorizontalLine {
        return HorizontalLine(
            y = { value },
            label = { label }, //, " + value.toInt() },
            line = rememberLineComponent(color, HORIZONTAL_LINE_THICKNESS_DP.dp),
            labelComponent =
            rememberTextComponent(
                background = rememberShapeComponent(shape = CorneredShape.Pill, color = color),
                padding = dimensions(HORIZONTAL_LINE_LABEL_HORIZONTAL_PADDING_DP.dp, HORIZONTAL_LINE_LABEL_VERTICAL_PADDING_DP.dp),
                margins = dimensions(HORIZONTAL_LINE_LABEL_MARGIN_DP.dp),
                typeface = Typeface.MONOSPACE,
                textAlignment = Layout.Alignment.ALIGN_OPPOSITE,
            ),
        )
    }

    @Composable
    fun rememberLegend(): Legend<CartesianMeasuringContext, CartesianDrawingContext> {
        val labelComponent = rememberTextComponent(vicoTheme.textColor)
        return rememberHorizontalLegend(
            items = rememberExtraLambda {
                chartColors.forEachIndexed { index, color ->
                    add(
                        LegendItem(
                            icon = shapeComponent(color, CorneredShape.Pill),
                            labelComponent = labelComponent,
                            label = legendText[index],
                        )
                    )
                }
            },
            iconSize = 8.dp,
            //iconPadding = 8.dp,
            iconLabelSpacing = 4.dp,
            padding = dimensions(top = 8.dp),
        )
    }

    private val HORIZONTAL_LINE_THICKNESS_DP = 1f
    private val HORIZONTAL_LINE_LABEL_HORIZONTAL_PADDING_DP = 8f
    private val HORIZONTAL_LINE_LABEL_VERTICAL_PADDING_DP = 2f
    private val HORIZONTAL_LINE_LABEL_MARGIN_DP = 4f

    // Generic functions

    // Int
    fun getMovingAverageInt(source: MutableList<Int>, period: Int): MutableList<Int> {
        val sample = Array(period) { 0 } // Create array of float, with all values set to 0
        val n = source.size
        var ma: Int
        val target = mutableListOf<Int>()

        // the first n values in the sma will be off -> set them to the first weight value
        for (i in 0 until period) sample[i] = source[0]

        for (i in 0 until n) {
            for (ii in 0..period-2) {
                sample[ii] = sample[ii + 1]
            }
            sample[period - 1] = source[i]

            ma = 0
            for (ii in 0 until period) {
                ma += sample[ii]
            }
            ma /= period
            target.add(ma)
        }
        return target
    }

    // Float
    fun getMovingAverageFloat(source: MutableList<Data>, period: Int): MutableList<Float> {
        val sample = Array(period) { 0f} // Create array of float, with all values set to 0
        val n = source.size
        var ma: Float
        val target = mutableListOf<Float>()

        // the first n values in the sma will be off -> set them to the first weight value
        for (i in 0 until period) {
            sample[i] = try { source[0].value1.toFloat() } catch (e: NumberFormatException) { 0F }
        }

        for (i in 0 until n) {
            for (ii in 0..period-2) {
                sample[ii] = sample[ii + 1]
            }
            sample[period - 1] = try { source[i].value1.toFloat() } catch (e: NumberFormatException) { 0F }

            ma = 0f
            for (ii in 0 until period) {
                ma += sample[ii]
            }
            ma /= period
            target.add(ma)
        }
        return target
    }

    fun getLinearTrendlineFloat(source: MutableList<Float>): MutableList<Float> {
        // https://classroom.synonym.com/calculate-trendline-2709.html
        val target = mutableListOf<Float>()

        var a = 0f
        val b: Float
        var b1 = 0
        var b2 = 0f
        var c = 0
        val f: Float
        val g: Float
        val m: Float
        val n = source.size
        for (i in 1..n) {
            a += i * source[i - 1]
            b1 += i
            b2 += source[i - 1]
            c += i * i
        }
        a *= n
        b = b1 * b2
        c *= n
        val d: Float = b1 * try { b1.toFloat()  } catch (e: NumberFormatException) { 0F }
        m = (a - b) / (c - d)
        val e: Float = b2
        f = m * b1
        g = (e - f) / n
        var value: Float
        for (i in 1..n) {
            value = m * i + g
            target.add(value)
        }
        return target
    }

    fun getLinearTrendlineInt(source: MutableList<Int>): MutableList<Float> {
        // https://classroom.synonym.com/calculate-trendline-2709.html
        val target = mutableListOf<Float>()

        var a = 0f
        val b: Float
        var b1 = 0
        var b2 = 0f
        var c = 0
        val f: Float
        val g: Float
        val m: Float
        val n = source.size
        for (i in 1..n) {
            a += i * source[i - 1]
            b1 += i
            b2 += source[i - 1]
            c += i * i
        }
        a *= n
        b = b1 * b2
        c *= n
        val d: Float = b1 * try { b1.toFloat()  } catch (e: NumberFormatException) { 0F }
        m = (a - b) / (c - d)
        val e: Float = b2
        f = m * b1
        g = (e - f) / n
        var value: Float
        for (i in 1..n) {
            value = m * i + g
            target.add(value)
        }
        return target
    }

    // Decide if yy-mm or mm-dd is shown on X-Axis
    @SuppressLint("SimpleDateFormat")
    fun checkTimeSpan() {
        val first = items.first().timestamp
        val last = items.last().timestamp
        val dayInMs = TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)
        val diff = last - first
        val days = diff / dayInMs

        val formatString = when {
            days < 10 -> "EEE " + (if (Locale.getDefault().toString() == "en_US") "hh" else "HH") + ":mm"
            days > 365 -> "yyyy MMM"
            else -> "MMM dd"
        }
        xAxisFormat = SimpleDateFormat(formatString)
    }
}
