/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.SharedPreferences
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.pdf.PdfDocument
import android.graphics.text.LineBreaker
import android.graphics.text.MeasuredText
import android.net.Uri
import android.os.Build
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.DatePattern
import com.zell_mbc.medilog.MainActivity.Companion.Tabs.DIARY
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.io.File
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import kotlin.math.min


abstract class BasePdf(val activity: Activity) {
    abstract val itemName: String

    private var headerText = ""
    private var formatedDate = ""

    // PDF Report variables
    var document = PdfDocument()
    val pdfPaint = Paint()
    val pdfPaintHighlight = Paint()
    val pdfPaintUnderline = Paint(Paint.UNDERLINE_TEXT_FLAG)
    val pdfPaintSmall = Paint()
    val pdfPaintBackground = Paint()
    val pdfPaintDaySeparator = Paint()
    val pdfDaySeparatorLineSpacer = 2 // Line which separates days

    var totalDays = 0L
    var lastDay   = 0L
    var firstDay  = 0L

    var dateTabWidth = 0f
    var timeWidth = 0f
    var dateWidth = 0f
    var padding = 0f
    val space = 5f
    var commentWidth = 0

    lateinit var pdfItems: List<Data>
    private var pdfOrder = ""

    abstract val landscape: Boolean
    abstract val highlightValues: Boolean
    abstract val blendInItems: Boolean
    abstract val pageSize: String?
    abstract val printDaySeparatorLines: Boolean

    lateinit var canvas: Canvas
    lateinit var page: PdfDocument.Page
    private var pageNumber = 1
    private val a4 = arrayOf(595, 842)
    private val letter = arrayOf(612, 792)

    var pdfRightBorder = 0f // Set in fragment based on canvas size - pdfLeftBorder
    var pdfDataBottom = 0f // Set in fragment based on canvas size
    private var pdfFooterTop = 0f
    private val pdfHeaderTop = 30f // Distance to physical top of page
    private val pdfFooterHeight = 20f // Height of the footer, distance to physical bottom of page
    val pdfHeaderBottom = 50f // Height of the header
    val pdfDataTop = 70f
    var pdfLineSpacing = 15f
    val pdfLeftBorder = 10f
    var dataTab = 0f
    var commentTab = 0f
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)

    var pageWidth = 0
    private var pageHeight = 0

    // Data variables
    var filterData = ArrayList<String>(0)
    var totalData = ArrayList<String>(0)
    var dateFormat: DateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
    var count = 0
    var timePeriod = ""

    fun createPage() {
        pageWidth = getPageWidth(pageSize)
        pageHeight = getPageHeight(pageSize)

        val pageInfo: PdfDocument.PageInfo? = if (landscape) PdfDocument.PageInfo.Builder(pageHeight, pageWidth, pageNumber++).create()
        else PdfDocument.PageInfo.Builder(pageWidth, pageHeight, pageNumber++).create()

        page = document.startPage(pageInfo)
        canvas = page.canvas

        pdfRightBorder = canvas.width.toFloat() - pdfLeftBorder
        pdfFooterTop = canvas.height.toFloat() - pdfFooterHeight
        pdfDataBottom = pdfFooterTop - 15
        pageWidth = pdfRightBorder.toInt() // Todo: Not accurate

    }

    @SuppressLint("SimpleDateFormat")
    fun savePdfDocument(): Uri? {
        val authority = activity.applicationContext.packageName + ".provider"

        val filePath = File(activity.cacheDir, "")
        val fn = itemName + "-" + SimpleDateFormat(DatePattern.DATE).format(Date().time) + ".pdf"
        val newFile = File(filePath, fn)
        val uri = FileProvider.getUriForFile(activity, authority, newFile)
        try {
            val out = activity.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.run { writeTo(out) }
                out.run {
                    flush()
                    close()
                }
            }
        } catch (e: IOException) {
            Toast.makeText(activity, activity.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }
        return uri
    }

    private fun getPageWidth(pageSize: String?): Int {
        return when (pageSize) {
            "Letter" -> letter[0]
            else -> a4[0]
        }
    }

    private fun getPageHeight(pageSize: String?): Int {
        return when (pageSize) {
            "Letter" -> letter[1]
            else -> a4[1]
        }
    }


    fun multipleLines(comment: String, y: Float): Float {
        // No space left for comments
        if (commentWidth <= 0) y

        // ------ Break text into multiple lines
        var f = y
        var lines = listOf(comment)
        // any line breaks?
        if (comment.indexOf('\n') > 0) {
            lines = comment.split('\n')
        }
        for (line in lines) {
            // Next check if text fits…
            if (line.length <= commentWidth) {
                canvas.drawText(line, commentTab + space, f, pdfPaint)
                f = checkForNewPage(f + pdfLineSpacing)
            } else {
                if (Build.VERSION.SDK_INT > 28) { // Below 2 functions are not available on SDK prior to 29
                    val mt: MeasuredText = MeasuredText.Builder(line.toCharArray())
                        .appendStyleRun(pdfPaint, line.length, false) // Use paint for "Hello, "
                        .build()

                    val lb = LineBreaker.Builder() // Use simple line breaker
                        .setBreakStrategy(LineBreaker.BREAK_STRATEGY_SIMPLE) // Do not add hyphenation.
                        .setHyphenationFrequency(LineBreaker.HYPHENATION_FREQUENCY_NONE) // Build the LineBreaker
                        .build()

                    val c = LineBreaker.ParagraphConstraints()
                    c.width = pdfRightBorder - commentTab

                    val r = lb.computeLineBreaks(mt, c, 0)

                    var prevOffset = 0
                    //Log.d("Line: ", line)
                    for (ii in 1..r.lineCount) {  // iterate over the lines
                        val nextOffset = r.getLineBreakOffset(ii - 1)
                        val t = line.substring(prevOffset, nextOffset)
//                            Log.d("t: ", t)
                        canvas.drawText(t, commentTab + space, f, pdfPaint)
                        f = checkForNewPage(f + pdfLineSpacing)
                        prevOffset = nextOffset
                    }
                } else {
                    // Old Android versions
                    var cutPosition = commentWidth
                    var cutLine = line.substring(0, cutPosition)
                    var remaining = line.substring(cutPosition)
                    while (cutLine.isNotEmpty()) {
                        canvas.drawText(cutLine, commentTab + space, f, pdfPaint)
                        cutPosition = min(commentWidth, remaining.length)
                        cutLine = remaining.substring(0, cutPosition)
                        remaining = remaining.substring(cutPosition)
                        f = checkForNewPage(f + pdfLineSpacing)
                    }
                }
            }
        }
        return f
        // --- End multiple lines
    }

    abstract fun setColumns()
    abstract fun initDataPage()

    open fun drawHeader() {
        canvas.drawRect(0f,0f, canvas.width.toFloat(), pdfHeaderBottom, pdfPaintBackground) // Draw to edges

        val logoSize = 30
        val logoPosition = (pdfHeaderBottom-logoSize)/2
        val bitmap = AppCompatResources.getDrawable(activity, R.mipmap.ic_launcher)?.toBitmap(logoSize, logoSize)
        if (bitmap != null) canvas.drawBitmap(bitmap, logoPosition, logoPosition, pdfPaint)

        val dtString = activity.getString(R.string.date) + " " + formatedDate
        val dateWidth = pdfPaint.measureText(dtString)
        val pdfHeaderDateColumn = pdfRightBorder - dateWidth

        // Draw header
        canvas.drawText(headerText, pdfLeftBorder + logoSize + logoPosition, pdfHeaderTop, pdfPaint)
        canvas.drawText(
            dtString,
            pdfHeaderDateColumn,
            pdfHeaderTop,
            pdfPaint
        )
        pdfDrawFooter()
    }

    private fun pdfDrawFooter() {
        // Draw Footer
        val pageNumberString = "- " + (pageNumber -1).toString() + " -"
        val textWidth = pdfPaint.measureText(pageNumberString,0, pageNumberString.length)
        canvas.drawText(pageNumberString, (canvas.width.toFloat() - textWidth) /2  , pdfFooterTop, pdfPaint)
        canvas.drawLine(pdfLeftBorder, pdfDataBottom, pdfRightBorder, pdfDataBottom, pdfPaint)
    }


    fun checkForNewPage(line: Float): Float {
        val ret = if (line > pdfDataBottom) {
            document.finishPage(page)
            createPage()
            initDataPage()
            pdfDataTop + pdfLineSpacing
        } else line
        return ret
    }

    // How many chars can we fit in our column
    fun measureColumn(commentWidth: Float): Int {
        if (commentWidth <= 0.0) return 0

        var width = 1
        val sampleText = "x".repeat(commentWidth.toInt()+1)
        var x = pdfPaint.measureText(sampleText,0, width)
        var measure: String
        while (x < commentWidth) {
            measure = sampleText.substring(0,width++)
            x = pdfPaint.measureText(measure)
        }
        return width
    }

    // This function assumes we will always print filtered content, if
    open fun createPdfDocument(): PdfDocument? {
        pageNumber = 1
        document = PdfDocument()  // Reset document

        val s = preferences.getString(SettingsActivity.KEY_PREF_PDF_TEXT_SIZE, activity.getString(R.string.PDF_TEXT_SIZE_DEFAULT))
        var textSize = 0f
        if (!s.isNullOrEmpty()) textSize = s.toFloat()

        pdfPaint.isFakeBoldText = false
        pdfPaint.color = Color.BLACK
        pdfPaint.textSize = textSize

        pdfPaintHighlight.isFakeBoldText = true
        pdfPaintHighlight.textSize = textSize

        pdfPaintUnderline.isFakeBoldText = false
        pdfPaintUnderline.color = Color.BLACK
        pdfPaintUnderline.textSize = textSize

        pdfPaintSmall.textSize = textSize -2

        pdfPaintDaySeparator.color = Color.parseColor("#EDF4F9") // Very light gray
        pdfPaintDaySeparator.strokeWidth = 2f
        pdfPaintDaySeparator.style = Paint.Style.FILL_AND_STROKE
        pdfPaintBackground.color = Color.parseColor("#E4ECF2") // Light gray

        formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        pdfOrder= preferences.getString(SettingsActivity.KEY_PREF_PDF_ORDER, activity.getString(R.string.PDF_DATA_ORDER_DEFAULT)).toString()
        pdfItems = viewModel.getItems(pdfOrder, true)
        if (pdfItems.isEmpty()) {
            Toast.makeText(activity, activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        // Measure width of date tab
        padding = 3 * space
        var dtString = toStringDate(pdfItems[0].timestamp) + "  " + toStringTime(pdfItems[0].timestamp)
        dateTabWidth = pdfPaintHighlight.measureText(dtString)

        dtString = toStringDate(pdfItems[0].timestamp)
        dateWidth = pdfPaintHighlight.measureText(dtString)

        dtString = toStringTime(pdfItems[0].timestamp)
        timeWidth = pdfPaintHighlight.measureText(dtString)

        val fm: Paint.FontMetrics = pdfPaintHighlight.fontMetrics
        pdfLineSpacing = fm.bottom - fm.top + fm.leading

        headerText = activity.getString(R.string.reportTitle, itemName, MainActivity.profilesViewModel.get(activeProfileId)?.name ?: "" ) // get(MainActivity.profilesViewModel.activeProfileId)?.name ?: "")
        createPage()

        return null
    }

    open fun drawStatsPage(drawable: Int) {

        // Data section
        val msToDay: Long = 1000 * 60 * 60 * 24
        val firstDayDate  = viewModel.getFirst()
        val lastDayDate   = viewModel.getLast()
        firstDay                = firstDayDate?.timestamp ?: 0
        lastDay                 = lastDayDate?.timestamp ?: 0
        totalDays               = (lastDay - firstDay) / msToDay + 1 // From ms to days

        drawHeader()

        canvas.drawText(activity.getString(R.string.statistics) + ":", pdfLeftBorder, pdfDataTop + pdfLineSpacing, pdfPaintHighlight)
        if (drawable > 0) {
            val bitmap = ContextCompat.getDrawable(activity, drawable)?.toBitmap()
            if (bitmap != null) {
                val bitmapSize = 40
                val rect = RectF(pdfRightBorder - bitmapSize, pdfDataTop, pdfRightBorder, pdfDataTop + bitmapSize)
                canvas.drawBitmap(bitmap, null, rect, pdfPaint)
            }
        }
    }

    // Add bitmap to PDF document
/*    fun addChartPage(chart: BaseChart?){
        chart ?: return

        val bm = chart.createChartBitmap()
        if (bm != null) {
            drawHeader()
            canvas.drawBitmap(bm, pdfLeftBorder, pdfDataTop + pdfLineSpacing, pdfPaint)
        }
    }
*/

    // Helper stuff
    fun toStringDate(l: Long): String { return DateFormat.getDateInstance().format(l) }

    @SuppressLint("SimpleDateFormat")
    fun toStringTime(l: Long): String { return SimpleDateFormat("HH:mm").format(l) }

    fun drawFilter(y: Float): Float {
        var filterString = ""
        var yy = y
        if (viewModel.filterActive()) {
            if (viewModel.filterStart > 0L && + viewModel.filterEnd > 0L) filterString = toStringDate(viewModel.filterStart) + " - " + toStringDate(viewModel.filterEnd) else
            if (viewModel.filterStart > 0L) filterString = toStringDate(viewModel.filterStart) + " - " + activity.getString(R.string.today) else
            if (viewModel.filterEnd > 0L)   filterString = " - " + toStringDate(viewModel.filterEnd)

            yy = y + pdfLineSpacing + pdfLineSpacing
            canvas.drawText(activity.getString(R.string.action_filter) + " " + activity.getString(R.string.timeframe) + ": " + filterString, pdfLeftBorder, yy, pdfPaint)
            if (viewModel.dataType == DIARY && viewModel.tagFilter.isNotEmpty() ) {
                yy = y + pdfLineSpacing + pdfLineSpacing
                canvas.drawText(activity.getString(R.string.tags) + ": " + viewModel.tagFilter, pdfLeftBorder, yy, pdfPaint)
            }
        }
        return yy
    }

}