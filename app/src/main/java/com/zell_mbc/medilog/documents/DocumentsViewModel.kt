/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.documents

import android.app.Application
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity

class DocumentsViewModel(application: Application): DataViewModel(application, Tabs.DOCUMENTS) {
    override val filterStartPref = "DOCUMENTS_FILTER_START"
    override val filterEndPref = "DOCUMENTS_FILTER_END"
    override val timeFilterModePref =  "DOCUMENTS_FILTER_MODE"
    override val rollingFilterValuePref = "DOCUMENTS_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "DOCUMENTS_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.documents)
    override var blendInItems = false
}
