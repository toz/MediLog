/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.diary

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zell_mbc.medilog.documents.DocumentsEditActivity
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.documents.DocumentsChartActivity
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.support.onAttachmentClick
import java.util.Date

class DocumentsTab(context: Context, snackbarDelegate: SnackbarDelegate): BaseTab(context, snackbarDelegate) {
    override var editActivityClass: Class<*> = DocumentsEditActivity::class.java
    override var infoActivityClass: Class<*> = DocumentsInfoActivity::class.java
    override var chartActivityClass: Class<*> = DocumentsChartActivity::class.java

    init {
        showTime = false
        quickEntry = false
    }

    @Composable
    override fun ShowContent(padding: PaddingValues) {
        super.ShowContent(padding)

        LazyColumn(state = listState, horizontalAlignment = Alignment.Start, modifier = Modifier.fillMaxWidth().padding(start = 8.dp, end = 8.dp)) {
            item {
                if (quickEntry) {
                    val (textField1) = remember { FocusRequester.createRefs() }
                    Row(modifier = Modifier.fillMaxWidth()) {
                        TextField(
                            value = comment.value,
                            onValueChange = { updateComment(it) },
                            singleLine = false,
                            maxLines = 2,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text, capitalization = KeyboardCapitalization.Sentences),
                            label = { Text(text = stringResource(id = R.string.description) ) },
                            colors = textFieldColors,
                            modifier = Modifier.weight(1f).focusRequester(textField1),
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )
                    } // Row
                    SideEffect {
                        // Set cursor to first field after addItem completed
                        if (activateFirstField) {
                            textField1.requestFocus()
                            activateFirstField = false }
                    }

                    // Dialog section
                   /* if (openTextTemplatesDialog.value)
                        textTemplateDialog.ShowDialog(setShowDialog = { openTextTemplatesDialog.value = it }) {
                            comment.value += it
                            showKeyboard()
                        }*/
                }
            }
            items(dataList.value) { item ->
                HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.secondaryContainer) // Lines starting from the top
                Row(
                    modifier = Modifier.height(IntrinsicSize.Min).fillMaxWidth().width(IntrinsicSize.Max).clickable { selection.value = item },
                    verticalAlignment = Alignment.CenterVertically
                )
                {
                    ShowRow(item)
                }
                if (selection.value != null) {
                    ItemClicked(selection.value!!._id)
                    selection.value = null
                }
            }
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(formatDateString(item.timestamp), Modifier
            .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
            .width(dateColumnWidth), fontSize = fontSize.sp)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator
        Text(item.comment, Modifier.padding(start = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).clickable {
            selection.value = item
            selectedField = "comment"},
            fontSize = fontSize.sp,
            style = TextStyle(lineHeight = TextUnit.Unspecified, platformStyle = PlatformTextStyle(includeFontPadding = false)) // style is for removing the padding between multiline text
        )
        if (item.attachment.isNotEmpty()) {
            val colorFilter = if (isSystemInDarkTheme()) ColorFilter.tint(Color.White) else null
            Image(
                modifier = Modifier.padding(start = cellPadding.dp).clickable { onAttachmentClick(context, item.attachment) },
                painter = painterResource(id = getDrawable(item.attachment)), contentDescription = "Attachment icon",
                colorFilter = colorFilter)
        }
    }

    fun getDrawable(fileName: String): Int {
        return when {
            fileName.contains(".jpg") ||
            fileName.contains(".png") -> R.drawable.baseline_image_24
            fileName.contains(".pdf") -> R.drawable.baseline_picture_as_pdf_24
            else -> R.drawable.ic_baseline_attach_file_24
        }
    }

    override fun addItem() {
        if (!quickEntry) {
            startEditing(-1) // Indicate that this is about a new item
            return
        }

        // Close keyboard after entry is done
        hideKeyboard()

        // If value is empty, replace with default text based on state
        if (comment.value.isEmpty()) {
            snackbarDelegate.showSnackbar(context.getString(R.string.documentMissing))
            return
        }

        if (tags.value.isEmpty()) {
            tags.value = NO_TAGS
        }

        val item = Data(0, Date().time, comment.value, viewModel.dataType, "", value2.value,"","", "", tags = tags.value, category_id = -1, profile_id = activeProfileId)
        viewModel.upsert(item)
        if ((viewModel.filterEnd > 0 ) && (viewModel.filterEnd < item.timestamp)) snackbarDelegate.showSnackbar(context.getString(R.string.filteredOut))
        cleanUpAfterAddItem()
    }
}