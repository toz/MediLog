/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.documents

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.EditActivity

class DocumentsEditActivity : EditActivity() {
    override val dataType = Tabs.DOCUMENTS

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this)[DocumentsViewModel::class.java]
        attchementsEnabled = true

        super.onCreate(savedInstanceState)

        // All preparation done, start Compose
        setContent {
            StartCompose() }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier
            .padding(start = 8.dp, end = 8.dp)
            .imePadding()) {
            DateTimeBlock()
            val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent, focusedContainerColor = Color.Transparent)

            Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Row(Modifier.fillMaxWidth()) {
                    // Comment
                    TextField(
                        modifier = Modifier.fillMaxWidth(),
                        value = commentString,
                        colors = textFieldColors,
                        onValueChange = { commentString = it },
                        label = { Text(text = stringResource(id = R.string.description), style = MaterialTheme.typography.bodyMedium) },
                    )
                }
            }
            Text("")
            if (attachmentPresent()) {
                Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    AttachmentBlock()
                }
            }
        }
  //      }

        // Dialogs
        if (showTagsDialog)
            tagsDialog.ShowDialog(selectedTags = tagIds, setShowDialog = { showTagsDialog = it }) {
                tagIds = it
            }

        if (showTextTemplatesDialog)
            textTemplateDialog.ShowDialog(setShowDialog = { showTextTemplatesDialog = it }) { commentString += it }

        if (showDatePickerDialog) OpenDatePickerDialog()
        if (showTimePickerDialog) OpenTimePickerDialog()    }


    override fun saveItem() {
        // Check empty variables
        if (commentString.isEmpty()) {
            hideKeyboard()
            snackbarDelegate.showSnackbar(getString(R.string.documentMissing))
            return
        }
        super.saveItem()
        finish() // Close current window / activity
    }
}