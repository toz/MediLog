/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.diary

import android.app.Activity
import android.graphics.Paint
import android.graphics.Path
import android.graphics.pdf.PdfDocument
import android.util.Log
import android.widget.Toast
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.time.DayOfWeek
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.math.max
import android.graphics.Typeface
import java.time.ZoneId


class DocumentsPdf(activity: Activity): BasePdf(activity) {
    // Base class overrides
    override var itemName = activity.getString(R.string.diary)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_LANDSCAPE, activity.getString(R.string.DIARY_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_DIARY_PAPER_SIZE, activity.getString(R.string.DIARY_PAPER_SIZE_DEFAULT))
    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_BLENDINITEMS, activity.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())
    override var printDaySeparatorLines = preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_PDF_DAYSEPARATOR, activity.getString(R.string.DAY_SEPARATOR_LINE_DEFAULT).toBoolean())
    override var highlightValues = false // Not applicable

    // Diary specific
    private var warningTab = 0f

    private var notGoodSmiley = activity.getString(R.string.notGoodSmileyEmojy)
    private var badSmiley =  activity.getString(R.string.badSmileyEmojy)
    private var goodSmiley =  "" // Don't show - activity.getString(R.string.goodSmileyEmojy)

    //---------------------------------------
    private val FILLER = -1f
    private val EMPTY = -2f
    private val GOOD = 0f
    private val NOT_GOOD = 1f

    var boxSize = 0f
    var boxPadding = 0f
    var dayOfWeekWidth = 20f
    var monthRow = 0f
    var yearRow = 0f
    var totalColumns = 0
    var startColumn = 0
    var firstColumn = true // Trigger actions on first column

    var paintFiller = Paint()
    var paintGood = Paint()
    var paintNotGood = Paint()
    var paintBad = Paint()
    var paintGray = Paint()

    data class Day(
        var date: LocalDate,
        var state: Float,
        var count: Int
    )
    val days = ArrayList<Day>()

    override fun createPdfDocument(): PdfDocument? {
        // Check here to be able to return right away in case of error
        if (MainActivity.viewModel.getSize(true) == 0) {
            Toast.makeText(activity, activity.getString(R.string.bloodPressure) + ": " + activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        super.createPdfDocument()

        val notGood = "1"
        val bad = "2"
    
        notGoodSmiley = if (preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_SHOWNOTGOOD, true)) activity.getString(R.string.notGoodSmileyEmojy) else " "
        badSmiley =     if (preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_SHOWBAD, true)) activity.getString(R.string.badSmileyEmojy) else " "
        goodSmiley =    if (preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_SHOWGOOD, false)) activity.getString(R.string.goodSmileyEmojy) else " "
    
        val pdfPaintItalics = Paint()
        pdfPaintItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        val pdfPaintBoldItalics = Paint()
        pdfPaintBoldItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        pdfPaintBoldItalics.isFakeBoldText = true
    
        setColumns()
        initDataPage()
        var f = pdfDataTop + pdfLineSpacing
        commentWidth = measureColumn(pdfRightBorder - commentTab)
    
        var currentDay = toStringDate(pdfItems[0].timestamp)
    
        for (item in pdfItems) {
                if (printDaySeparatorLines) {
                    val itemDay = toStringDate(item.timestamp)
                    if (currentDay != itemDay) {
                        f -= (pdfLineSpacing - pdfDaySeparatorLineSpacer) // Go back one line and add half a daySeparatorLineHeight
                        canvas.drawLine(pdfLeftBorder, f, pdfRightBorder, f, pdfPaintDaySeparator)
                        f += (pdfLineSpacing - pdfDaySeparatorLineSpacer)
                        currentDay = itemDay
                    }
                }
    
                f = checkForNewPage(f)
    
                canvas.drawText(toStringDate(item.timestamp) + " " + toStringTime(item.timestamp) , pdfLeftBorder, f, pdfPaint)
                val state = when (item.value2) {
                    notGood -> notGoodSmiley
                    bad      -> badSmiley
                    else     -> goodSmiley
                }
    
                canvas.drawText(state, warningTab, f, pdfPaint)
                f = multipleLines(item.comment, f)
            }
            document.finishPage(page)
    
            // Add statistics page, landscape
            compileData()
            // Rows don't fit in portrait
            if (calculateDimensions() > 1) landscape = true
            createPage()
    
            // Re-caclulate dimensions with landscape set
            if (landscape) {
                calculateDimensions()
                landscape = false
            }
            drawHeader()
            drawCalendar()
            document.finishPage(page)
    
            return document
        } 

    private fun compileData() {
        days.clear() // Make sure array is empty

        // For the charts only native diary entries can be considered, no matter what showAllTabs is set to
        val tmp = blendInItems
        blendInItems = false
        val items = MainActivity.viewModel.getItems("ASC", filtered = true)
        blendInItems = tmp

        // Add empty items at the beginning so the chart always starts on first day of week
        //--------------------
        val firstDate = LocalDate.from(LocalDateTime.ofInstant(Instant.ofEpochMilli(items[0].timestamp), ZoneId.systemDefault())
        )

        val dayInWeek = firstDate.dayOfWeek.value -1
        for (i in dayInWeek downTo 1) {
            val emptyDay = Day(firstDate.minusDays(i.toLong()), EMPTY, 1)
            days.add(emptyDay)
        }
        //--------------------

        // Fill the matrix
        var previousDay = Day(LocalDate.now(), FILLER, 1) // Today +1 as a temporary setting

        for (item in items) {
            val itemDay = Day(
                LocalDate.from(LocalDateTime.ofInstant(Instant.ofEpochMilli(item.timestamp), ZoneId.systemDefault())),
                if (item.value2.isEmpty()) GOOD else item.value2.toFloat(), 1
            )

            // Another value on the same day
            if (itemDay.date == previousDay.date) {
                /* Average up state
                previousDay.state = ((previousDay.state * previousDay.count) + itemDay.state) / (previousDay.count + 1)
                previousDay.count++ */
                previousDay.state = max(previousDay.state, itemDay.state) // Settle on maximum value
            }
            // New day
            else {
                // Fill the day gap if necessary
                var gapDay = Day(previousDay.date.plusDays(1), FILLER, 1)
                while (gapDay.date < itemDay.date) {
                    days.add(gapDay)
                    gapDay = Day(gapDay.date.plusDays(1), FILLER, 1)
                }
                days.add(itemDay)
                previousDay = itemDay
            }
        }
    }

    private fun calculateDimensions():Int {
        totalColumns = (days.size / 7) // items / 7 days a week
        startColumn = 1
        var y = pdfDataTop + pdfLineSpacing
        yearRow = y
        y += pdfLineSpacing         // Row 2, month headers
        monthRow = y

        y += pdfLineSpacing / 2

        // Calculate available space
        val calendarWidth = pdfRightBorder - pdfLeftBorder - dayOfWeekWidth - 10f
        val maxBoxSize = 30f
        val minBoxSize = 15f
        boxPadding = 7f
        boxSize = calendarWidth / (totalColumns + boxPadding)
        // Get rid of fractions because we can only handle complete boxes
        boxSize = boxSize.toInt().toFloat()
        if (boxSize > maxBoxSize) boxSize = maxBoxSize
        if (boxSize < minBoxSize) {
            boxSize = minBoxSize
            // How many boxsize columns can we show?
        }
        startColumn = totalColumns - (calendarWidth / (boxSize + boxPadding)).toInt()  // Skip the initial columns so the last column fits

        pdfLineSpacing = boxSize + boxPadding

        return startColumn
    }

    private fun drawCalendar() {
        var y = monthRow + (pdfLineSpacing / 2)

        // Top corner calendar
        val firstCalendarColumn = pdfLeftBorder + dayOfWeekWidth
        val firstCalendarRow = y + (pdfLineSpacing/2) // 1 1/2 rows spacing between header and calendar

        // Row header column - Names of days
        y = firstCalendarRow + (boxSize/4*3)
        canvas.drawText(DayOfWeek.of(1).toString().substring(0, 1),pdfLeftBorder,y, pdfPaint)
        y += 2 * (boxSize + boxPadding)
        canvas.drawText(DayOfWeek.of(3).toString().substring(0, 1),pdfLeftBorder,y, pdfPaint)
        y += 2 * (boxSize + boxPadding)
        canvas.drawText(DayOfWeek.of(5).toString().substring(0, 1),pdfLeftBorder,y, pdfPaint)
        y += 2 * (boxSize + boxPadding)
        canvas.drawText(DayOfWeek.of(7).toString().substring(0, 1),pdfLeftBorder,y, pdfPaint)
        y += 2 * (boxSize + boxPadding)
        val calendarBottom = y

        //for (dataRow in rows) {
        var dayOfWeek = 0

        var x = firstCalendarColumn
        y = firstCalendarRow
        var currentColumn = 1
        firstColumn = true
        paintFiller.style = Paint.Style.STROKE
        paintFiller.color = Color.LightGray.toArgb() //COLOR_GOOD.toArgb()
        paintGood.style = Paint.Style.FILL
        paintGood.color = android.graphics.Color.GREEN
        paintNotGood.style = Paint.Style.STROKE
        paintNotGood.color = MainActivity.AMBER.toArgb()
        paintBad.style = Paint.Style.FILL
        paintBad.color = android.graphics.Color.RED
        paintGray.color = android.graphics.Color.GRAY

        days.forEach {
            // In case there's not enough space for all columns, skip the ones at the beginning
            if (currentColumn > startColumn) {
                if (firstColumn) x = firstCalendarColumn // Reset x position
                Log.d("x,y: ", " $x, $y")
                tableCell(it.state, it.date.toString(), x, y)
                firstColumn = false
            }

            y += pdfLineSpacing
            if (dayOfWeek++ == 6) {
                dayOfWeek = 0
                y = firstCalendarRow
                x += (boxSize + boxPadding)
                currentColumn++
            }
        }

        // Legend
        y = calendarBottom
        x = pdfLeftBorder + 100
        canvas.drawText(activity.getString(R.string.stateNoValue), pdfLeftBorder, y + ((boxSize + boxPadding)/2), pdfPaint)
        canvas.drawRect(x,y, x + boxSize, y + boxSize, paintFiller)
        y += (boxSize + boxPadding)

        canvas.drawText(activity.getString(R.string.stateGood), pdfLeftBorder, y + ((boxSize + boxPadding)/2), pdfPaint)
        canvas.drawRect(x,y, x + boxSize, y + boxSize, paintGood)
        y += (boxSize + boxPadding)

        canvas.drawText(activity.getString(R.string.stateNotGood), pdfLeftBorder, y + ((boxSize + boxPadding)/2), pdfPaint)
        canvas.drawRect(x,y, x + boxSize, y + boxSize, paintNotGood)
        drawTriangle(x,y, paintNotGood)
        y += (boxSize + boxPadding)

        canvas.drawText(activity.getString(R.string.stateBad), pdfLeftBorder, y + ((boxSize + boxPadding)/2), pdfPaint)
        canvas.drawRect(x,y, x + boxSize, y + boxSize, paintBad)

        drawFilter(y)
    }

    fun tableCell(
        state: Float = 0f,
        date: String = "",
        x: Float,
        y: Float) {

        // Month header and separators
        if (date.isNotEmpty()) {
            val m = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).dayOfMonth
            if (m == 1 || firstColumn) {
                val month = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).month
                canvas.drawText(month.toString().substring(0,3), x, monthRow, pdfPaint)
            }
            if (LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).dayOfYear == 1 || firstColumn) {
                canvas.drawText(LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).year.toString(), x, yearRow, pdfPaint)
                // Log.d("Year: ", LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).year.toString() + " $x, $yearRow")
            }
            // Draw month separator lines
            val leftBorder = (m < 8)
            val topBorder = (m == 1)
            if (leftBorder) canvas.drawLine(x - boxPadding/2, y - (boxPadding/2), x - (boxPadding/2), y + boxSize + (boxPadding/2), paintFiller)
            if (topBorder) canvas.drawLine(x - (boxPadding/2), y  - boxPadding/2, x + boxSize + (boxPadding/2), y - boxPadding/2, paintFiller)
        }

        if (state == EMPTY) return // nothing to do

        val paint = when (state) {
            FILLER -> paintFiller
            GOOD -> paintGood
            NOT_GOOD -> paintNotGood
            else -> paintBad
        }
        //   paintBad.color = Color(ColorUtils.blendARGB(COLOR_GOOD.toArgb(), COLOR_BAD.toArgb(), state / 2)).toArgb()
        canvas.drawRect(x,y, x + boxSize, y + boxSize, paint)
        if (state == NOT_GOOD) drawTriangle(x,y, paint) //drawPattern(x,y, paint)
    }


    private fun drawTriangle(x: Float, y: Float, paint: Paint) {
        val path = Path()
        //path.fillType = Path.FillType.WINDING
        path.moveTo(x, y)
        path.lineTo(x, y+boxSize)
        path.lineTo(x+boxSize, y)
        path.close()
        paint.style = Paint.Style.FILL
        canvas.drawPath(path, paint)
        paint.style = Paint.Style.STROKE // Reset to stroke so subsequent boxes don't get filled
    }

    /*
    private fun drawPattern(x: Float, y: Float, paint: Paint) {
        // Define the rectangle dimensions
        val left = x.toInt()
        val top = y.toInt()
        val right = left + boxSize.toInt()
        val bottom = top + boxSize.toInt()

        val interval = 2

        // Triangle
        for (i in 0 .. boxSize.toInt() step interval) {
            canvas.drawLine(left.toFloat(), bottom-boxSize+i, (left+i).toFloat(), bottom-boxSize , paint)
        }

        // Vertical
                for (i in left .. right step interval) {
        // Draw a diagonal line from top left to bottom right
        canvas.drawLine(i.toFloat(), top.toFloat(), i.toFloat(), bottom.toFloat(), paint)
        }
        // Horizontal
        for (i in top .. bottom step interval) {
            // Draw a diagonal line from top left to bottom right
            canvas.drawLine(left.toFloat(), i.toFloat(), right.toFloat(), i.toFloat(), paint)
        }
    }
*/
    
override fun setColumns() {
    val warningSignWidth = pdfPaintHighlight.measureText(activity.getString(R.string.warningSign))
    warningTab = pdfLeftBorder + dateTabWidth + padding
    commentTab = warningTab + warningSignWidth + space + space
}

override fun initDataPage() {
    drawHeader()

    // Data section
    canvas.drawText(activity.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
    canvas.drawText(activity.getString(R.string.diary), warningTab, pdfDataTop, pdfPaintHighlight)
    canvas.drawLine(warningTab - space, pdfHeaderBottom, warningTab - space, pdfDataBottom, pdfPaint)
}

}