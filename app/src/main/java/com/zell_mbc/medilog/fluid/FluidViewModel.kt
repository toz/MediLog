/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.fluid

import android.app.Application
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity

class FluidViewModel(application: Application): DataViewModel(application, Tabs.FLUID) {
    override val filterStartPref = "FLUIDFILTERSTART"
    override val filterEndPref = "FLUIDFILTEREND"
    override val timeFilterModePref =  "FLUID_FILTER_MODE"
    override val rollingFilterValuePref = "FLUID_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "FLUID_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.fluid)

    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_FLUID_BLENDINITEMS, app.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())

    fun getDays(filtered: Boolean, order: String): MutableList<Data> {
        // Spell out all fields as otherwise the result can't be matched to item = activity crash
        // Make sure to call out all fields explicitly as otherwise the sql statement will crash!!
        var string = "SELECT _id, timestamp, comment, type, SUM(CAST(value1 as int)) AS value1, value2, value3, value4, attachment, profile_id, category_id, tags FROM data WHERE "
        string += if (blendInItems) " (type = $dataType OR type = ${Tabs.DIARY})"
        else "type=$dataType"

        string += " AND profile_id = $activeProfileId"

        if (filtered) {
            getFilter()
            if ((filterStart != 0L) && (filterEnd != 0L)) string += " AND timestamp >= $filterStart AND timestamp <= $filterEnd"
            if ((filterStart == 0L) && (filterEnd != 0L)) string += " AND timestamp <= $filterEnd"
            if ((filterStart != 0L) && (filterEnd == 0L)) string += " AND timestamp >= $filterStart"
        }
        // GROUP BY day
        string += " GROUP BY value2 ORDER BY value2 $order"
        return getDataList(string)
    }
}