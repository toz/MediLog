/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.fluid

import android.os.Bundle
import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottom
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStart
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLine
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.cartesian.rememberVicoZoomState
import com.patrykandpatrick.vico.compose.common.ProvideVicoTheme
import com.patrykandpatrick.vico.compose.common.fill
import com.patrykandpatrick.vico.compose.m3.common.rememberM3VicoTheme
import com.patrykandpatrick.vico.core.cartesian.Zoom
import com.patrykandpatrick.vico.core.cartesian.axis.HorizontalAxis
import com.patrykandpatrick.vico.core.cartesian.axis.VerticalAxis
import com.patrykandpatrick.vico.core.cartesian.data.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.cartesian.data.CartesianLayerRangeProvider
import com.patrykandpatrick.vico.core.cartesian.data.lineSeries
import com.patrykandpatrick.vico.core.cartesian.layer.LineCartesianLayer
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.ChartActivity
import com.zell_mbc.medilog.base.rememberMarker
import com.zell_mbc.medilog.preferences.SettingsActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FluidChartActivity: ChartActivity() {
    override val filename = "WaterChart.jpg"

    var fluid = mutableListOf<Float>()
    var thresholdLine = 0
    private var minY = 999
    private var maxY = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        legendText = mutableListOf(getString(R.string.fluid))

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_FLUID_GRID, true)
        showLegend = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_FLUID_LEGEND, false)
        showThreshold = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_FLUID_THRESHOLD, getString(R.string.SHOW_FLUID_THRESHOLD_DEFAULT).toBoolean())
        if (showThreshold) {
            val temp = "" + preferences.getString(SettingsActivity.KEY_PREF_FLUID_THRESHOLD, "")
            thresholdLine = try { temp.toInt() } catch (_: NumberFormatException) { getString(R.string.FLUID_THRESHOLD_DEFAULT).toInt() }
        }

        // Activities need to be self sufficient because MainActivity might get killed by OS
        // Need local viewModel to be able to set it to FluidViewModel in order to access getDays()
        val viewModel: FluidViewModel = ViewModelProvider(this)[FluidViewModel::class.java]

        // For the charts only native diary entries can be considered, no matter what showAllTabs is set to
        val tmp = viewModel.blendInItems
        val dailyView  = preferences.getBoolean(SettingsActivity.KEY_PREF_FLUID_SUMMARY_CHART, false)
        viewModel.blendInItems = false
        items = if (dailyView) viewModel.getDays(true, "ASC")
        else viewModel.getItems("ASC", filtered = true)
        viewModel.blendInItems = tmp

        if (items.size < 2) {
            Toast.makeText(this, getString(R.string.notEnoughDataForChart), Toast.LENGTH_LONG).show()
            finish()
            return // Needs return to avoid the rest to be executed because finish() may not kill the activity fast enough
        }

        // Collect minimal y value
        for (item in items) {
            val y = try { item.value1.toInt() } catch (_: NumberFormatException) { 0 }
            if (y < minY) minY = y
            if (y > maxY) maxY = y
        }

        minY -= 10
        maxY += 10

        // Fill data maps
        xAxisOffset = items[0].timestamp
        value1Data = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value1.toFloat() } catch (_: NumberFormatException) { 0F } })

        showContent()
    }

    @Composable
    override fun ShowContent() {
        Box(Modifier.safeDrawingPadding()) {
            chartColors = mutableListOf(Color(0xffb983ff))

            val modelProducer = remember { CartesianChartModelProducer() }
            LaunchedEffect(Unit) {
                withContext(Dispatchers.Default) {
                    modelProducer.runTransaction {
                        lineSeries { series(value1Data.keys, value1Data.values) }
                    }
                }
            }
            ProvideVicoTheme(rememberM3VicoTheme()) {
                ComposeChart(modelProducer, Modifier.fillMaxHeight())
            }
        }
    }

    @Composable
    private fun ComposeChart(modelProducer: CartesianChartModelProducer, modifier: Modifier) {
        val lineProvider = LineCartesianLayer.LineProvider.series(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(fill(chartColors[0]))))

        CartesianChartHost(
            chart = rememberCartesianChart(
                rememberLineCartesianLayer(lineProvider = lineProvider, rangeProvider = CartesianLayerRangeProvider.fixed(minY = minY.toDouble(),maxY = maxY.toDouble())),
                startAxis = VerticalAxis.rememberStart(
                    guideline = guideline(),
                ),
                bottomAxis = HorizontalAxis.rememberBottom(
                    valueFormatter = bottomAxisValueFormatter,
                    guideline = guideline(),
                ),
                legend = if (showLegend) rememberLegend() else null,
                decorations = if (showThreshold) listOf(helperLine("", thresholdLine.toDouble(), chartColors[0])) else listOf(),
                marker = rememberMarker()
            ),
            modelProducer = modelProducer,
            modifier = modifier,
            zoomState = rememberVicoZoomState(zoomEnabled = true, maxZoom = Zoom.max(Zoom.static(100f), Zoom.Content), initialZoom = Zoom.min(Zoom.static(), Zoom.Content)),
        )
    }
}