/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.fluid

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.EditActivity
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.text.SimpleDateFormat

class FluidEditActivity: EditActivity() {
    override val dataType = Tabs.FLUID

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this)[FluidViewModel::class.java]
        super.onCreate(savedInstanceState)

        value1Unit = " " + preferences.getString(SettingsActivity.KEY_PREF_FLUID_UNIT, getString(R.string.FLUID_UNIT_DEFAULT))
        value1Hint = if (MainActivity.modifyDecimalSeparator) getString(R.string.fluidHint).replace('.', MainActivity.decimalSeparator) else getString(R.string.fluidHint)

        // All preparation done, start Compose
        setContent { StartCompose() }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(start = 8.dp, end = 8.dp)) {
            DateTimeBlock()

            val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent, focusedContainerColor = Color.Transparent)
            val focusManager = LocalFocusManager.current
            val (textField1, textField2) = remember { FocusRequester.createRefs() }

            Row(Modifier.fillMaxWidth()) {
                // Weight
                TextField(
                    value = value1String,
                    colors = textFieldColors,
                    onValueChange = {
                        value1String = it
                        if (((it.length == 4) && (!it.endsWith(MainActivity.decimalSeparator))) || (it.length >= 5))
                            focusManager.moveFocus(FocusDirection.Next)
                    },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                    singleLine = true,
                    textStyle = TextStyle(),
                    label = { Text(text = stringResource(id = R.string.fluid) + "*", maxLines = 1) },
                    placeholder = { Text(text = "$value1Hint $value1Unit") },
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 20.dp)
                        .focusRequester(textField1),
                )
            }
            Row {
                // Comment
                TextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .focusRequester(textField2),
                    value = commentString,
                    colors = textFieldColors,
                    onValueChange = { commentString = it
                        //  commentString = it
                    },
                    singleLine = false,
                    label = { Text(text = stringResource(id = R.string.comment)) },
                    trailingIcon = {
                        IconButton(onClick = {
                            showTextTemplatesDialog = true
                        })
                        { Icon(Icons.Outlined.Abc, contentDescription = null) }
                    },
                )
            }
            Text("")

            if (attachmentPresent()) {
                Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    AttachmentBlock()
                }
            }
        }

        // Dialogs
        if (showTextTemplatesDialog)
            textTemplateDialog.ShowDialog(setShowDialog = { showTextTemplatesDialog = it }) { commentString += it }

        if (showDatePickerDialog) OpenDatePickerDialog()
        if (showTimePickerDialog) OpenTimePickerDialog()    }


    //Tab specific pre-save checks
    @SuppressLint("SimpleDateFormat")
    override fun saveItem() {
        hideKeyboard()

        // Check empty variables
        if (value1String.isEmpty()) {
            snackbarDelegate.showSnackbar(this.getString(R.string.fluidMissing))
            return
        }

        // Valid fluid?
        value1String = value1String.replace(MainActivity.decimalSeparator, '.')
        val value1Value = viewModel.validValueI(value1String, Threshold.MAX_FLUID)
        if (value1Value <= 0) {
            snackbarDelegate.showSnackbar(this.getString(R.string.invalid) + " " + this.getString(R.string.fluid) + " "+ this.getString(R.string.value) + " $value1String")
            return
        }

        // Update Day field in case date got changed
        value2String = SimpleDateFormat("yyyyMMdd").format(timestamp)

        super.saveItem()
        finish() // Close current window / activity
    }

}