/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.fluid

import android.app.Activity
import android.graphics.pdf.PdfDocument
import android.widget.Toast
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.preferences.SettingsActivity

class FluidPdf(activity: Activity): BasePdf(activity) {
    // Base class overrides
    override var itemName = activity.getString(R.string.fluid)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_FLUID_LANDSCAPE, activity.getString(R.string.FLUID_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_FLUID_PAPER_SIZE, activity.getString(R.string.FLUID_PAPER_SIZE_DEFAULT))
    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_FLUID_BLENDINITEMS, activity.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())
    override var printDaySeparatorLines = preferences.getBoolean(SettingsActivity.KEY_PREF_FLUID_PDF_DAYSEPARATOR, activity.getString(R.string.DAY_SEPARATOR_LINE_DEFAULT).toBoolean())
    override var highlightValues = false // Not activitylicable 

    // Water specific
    private val fluidUnit = preferences.getString(SettingsActivity.KEY_PREF_FLUID_UNIT, activity.getString(R.string.FLUID_UNIT_DEFAULT))
    private val quantity = activity.getString(R.string.quantity) + " (" + fluidUnit + ")"

    private val warningSign = activity.getString(R.string.warningSign)
    private var warningTab = 0f

    var dataType = 0

    override fun createPdfDocument(): PdfDocument? {
        // Check here to be able to return right away in case of error
        if (MainActivity.fluidViewModel.getSize(true) == 0) {
            Toast.makeText(activity, activity.getString(R.string.fluid) + ": " + activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        dataType = MainActivity.fluidViewModel.dataType
        super.createPdfDocument()

        val fluidThreshold = preferences.getString(SettingsActivity.KEY_PREF_FLUID_THRESHOLD, activity.getString(R.string.FLUID_THRESHOLD_DEFAULT))!!.toInt()

        // -----------
        val summarizedPDF = preferences.getBoolean(SettingsActivity.KEY_PREF_FLUID_SUMMARYPDF, activity.getString(R.string.FLUID_SHOW_PDF_SUMMARY_DEFAULT).toBoolean())
/*        val pdfItems: List<Data> = if (summarizedPDF) MainActivity.fluidViewModel.getDays(true)
        else MainActivity.fluidViewModel.getItems("ASC", true)
*/
        // Default pdfItems gets set in super, ch
        if (summarizedPDF) {
            val pdfOrder = "" + preferences.getString(SettingsActivity.KEY_PREF_PDF_ORDER, activity.getString(R.string.PDF_DATA_ORDER_DEFAULT))
            pdfItems = MainActivity.fluidViewModel.getDays(true, pdfOrder)
        }

        setColumns()
        initDataPage()
        var f = pdfDataTop + pdfLineSpacing
        commentWidth = measureColumn(pdfRightBorder - commentTab)

        var currentDay = toStringDate(pdfItems[0].timestamp)

        for (item in pdfItems) {
            if (printDaySeparatorLines) {
                val itemDay = toStringDate(item.timestamp)
                if (currentDay != itemDay) {
                    f -= (pdfLineSpacing - pdfDaySeparatorLineSpacer) // Go back one line and add half a daySeparatorLineHeight
                    canvas.drawLine(pdfLeftBorder, f, pdfRightBorder, f, pdfPaintDaySeparator)
                    f += (pdfLineSpacing - pdfDaySeparatorLineSpacer)
                    currentDay = itemDay
                }
            }
            f = checkForNewPage(f)
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder, f, pdfPaint)
            if (!summarizedPDF) canvas.drawText(toStringTime(item.timestamp), pdfLeftBorder, f, pdfPaint)

            // If this is a blended item, draw comment and be done
            if (item.type != dataType) {
                f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
                continue
            }

            val value = try {
                item.value1.toInt()
            } catch  (_: NumberFormatException) {
                0
            }

            if (value < fluidThreshold) canvas.drawText(warningSign, warningTab, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
        }
        // finish the page
        document.finishPage(page)

        /* Add chart page
        createPage()
        addChartPage()
        document.finishPage(page) */

        return document
    }

    override fun setColumns() {
        val warningSignWidth = pdfPaintHighlight.measureText(activity.getString(R.string.warningSign))
        val fluidTabWidth = pdfPaintHighlight.measureText(quantity)

        warningTab = pdfLeftBorder + dateTabWidth + padding
        dataTab    = warningTab + warningSignWidth
        commentTab = dataTab + fluidTabWidth
    }

    override fun initDataPage() {
        drawHeader() // VieModel function doesn't need to highlight

        // Data section
        canvas.drawText(activity.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(quantity, warningTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(warningTab - space, pdfHeaderBottom, warningTab - space, pdfDataBottom, pdfPaint)  // Line before data
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }

    }