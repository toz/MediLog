/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.fluid

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.base.InfoActivity
import com.zell_mbc.medilog.preferences.SettingsActivity

class FluidInfoActivity: InfoActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) // View Model needs to be initilaizid before super is called

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[FluidViewModel::class.java]

        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_FLUID_UNIT, getString(R.string.FLUID_UNIT_DEFAULT))

        setContent { StartCompose() }
    }

    @Composable
    fun ShowBlock() {
        Text(measurementsIn, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
        Text(avgString, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
        Text(minMaxString, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
        Text(timePeriod, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(8.dp)) {
            HeaderBlock(R.drawable.ic_fluid)
            if ((viewModel.filterStart + viewModel.filterStart) > 0L) {
                gatherData(true)
                ShowBlock()
                Text("")
                HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.secondary)
                Text("")
            }
            gatherData(false)
            ShowBlock()
            ShowAttachments()
        }
    }

    private fun gatherData(filtered: Boolean) {
        count = viewModel.getSize(filtered)
        var item: Data? = viewModel.getFirst(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val startDate = dateFormat.format(item.timestamp)

        item = viewModel.getLast(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val endDate = dateFormat.format(item.timestamp)

        measurementsIn = if (!filtered) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"

        val avg = viewModel.getAvgInt("value1", filtered)
        val tmp = avg.toString()
        //       if (tmp.length > 5) tmp = tmp.substring(0, 5)
        //       if (MainActivity.modifyDecimalSeparator) tmp = tmp.replace('.', MainActivity.decimalSeparator)
        avgString = getString(R.string.average) + ": $tmp$itemUnit"

        val min = viewModel.getMinValue1(filtered).toInt()
        val max = viewModel.getMaxValue1(filtered).toInt()
        minMaxString = getString(R.string.minMaxValues) + " $min - $max$itemUnit"
        if (modifyDecimalSeparator) minMaxString =
            minMaxString.replace('.', decimalSeparator)

        timePeriod = getString(R.string.timePeriod) + " $startDate - $endDate"
    }

}
