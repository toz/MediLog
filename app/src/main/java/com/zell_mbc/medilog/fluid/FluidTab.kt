/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.fluid

import android.annotation.SuppressLint
import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Filter
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.support.checkThresholds
import java.text.SimpleDateFormat
import java.util.Date

class FluidTab(context: Context, snackbarDelegate: SnackbarDelegate): BaseTab(context, snackbarDelegate) {

    override var editActivityClass: Class<*> = FluidEditActivity::class.java
    override var infoActivityClass: Class<*> = FluidInfoActivity::class.java
    override var chartActivityClass: Class<*> = FluidChartActivity::class.java

    private var fluidToday = 0

    init {
        // -------------------------------
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_HIGHLIGHT_VALUES, false)
        showTime        = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_SHOWTIME, context.getString(R.string.SHOWTIME_DEFAULT).toBoolean())
        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_FLUID_UNIT, context.getString(R.string.FLUID_UNIT_DEFAULT))

        val s = preferences.getString(SettingsActivity.KEY_PREF_FLUID_THRESHOLD, context.getString(R.string.FLUID_THRESHOLD_DEFAULT)) + "-"
        val th = checkThresholds(context, s, context.getString(R.string.FLUID_THRESHOLD_DEFAULT), R.string.fluid) // Add - to string to reflect that it's a lower threshold
        lowerThreshold = try { th[0].toFloat() } catch  (_: NumberFormatException) { 0f }
        showTime        = preferences.getBoolean(SettingsActivity.KEY_PREF_FLUID_SHOWTIME, context.getString(R.string.SHOWTIME_DEFAULT).toBoolean())
    }

    @Composable
    override fun ShowContent(padding: PaddingValues) {
        super.ShowContent(padding)
        showComment.value = comment.value.isNotEmpty() // For some reason the app will crash if this is in super?

        if (value1Width == 0.dp) MeasureValue1String("9999")

        LazyColumn(state = listState, horizontalAlignment = Alignment.Start, modifier = Modifier.fillMaxWidth().padding(start = 8.dp, end = 8.dp)) {
            item {
                if (quickEntry) {
                    fluidToday = viewModel.getToday()

                    val focusManager = LocalFocusManager.current
                    val (textField1, textField2) = remember { FocusRequester.createRefs() }

                    Row(Modifier.fillMaxWidth()) {
                        // Water
                        TextField(
                            value = value1.value,
                            colors = textFieldColors,
                            onValueChange = {
                                val tmp = it.filter  { it.isDigit() }
                                updateValue1(tmp)
                                showComment.value = true
                                if (it.length >= 4) focusManager.moveFocus(FocusDirection.Next)
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                            textStyle = TextStyle(),
                            label = { Text(text = stringResource(id = R.string.fluid) +"*", maxLines = 1, overflow = TextOverflow.Ellipsis) },
                            placeholder  = { Text(text = stringResource(id = R.string.fluidHint) + " $itemUnit") },

                            modifier = Modifier
                                .weight(1f)
                                .padding(end = cellPadding.dp)
                                .focusRequester(textField1),
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )
                        SideEffect {
                            // Set cursor to first field after addItem completed
                            if (activateFirstField) {
                                textField1.requestFocus()
                                activateFirstField = false }
                        }
                    } // Row

                    if (showComment.value) {
                        // Comment
                        TextField(
                            value = comment.value,
                            colors = textFieldColors,
                            onValueChange = { updateComment(it) },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            singleLine = true,
                            //maxLines = 2,
                            textStyle = TextStyle(),
                            modifier = Modifier.fillMaxWidth().focusRequester(textField2),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            trailingIcon = { IconButton(onClick = { openTextTemplatesDialog.value = true })
                            { Icon(Icons.Outlined.Abc,contentDescription = null) } },
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )
                    }

                    val lt = try { lowerThreshold.toInt() } catch  (_: NumberFormatException) { 0 }
                    val s = context.getString(R.string.today) + ": " + fluidToday.toString() + " / $lt" + itemUnit
                    if (fluidToday >= lt) Text(s, color = Color.Green)
                    else Text(s, color = MaterialTheme.colorScheme.error)

                    // Dialog section
                    if (openTextTemplatesDialog.value) {
                        textTemplateDialog.ShowDialog(setShowDialog = { openTextTemplatesDialog.value = it }) {
                            comment.value += it
                            textField2.requestFocus()
                        }
                    }
                    //if (viewModel.openSearchDialog.value) SearchDialog(setShowDialog = { viewModel.openSearchDialog.value = it })
                } // End quick entry
            }
            items(dataList.value) { item ->
                // Lines starting from the top
                HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, thickness = 1.dp)
                Row(modifier = Modifier.fillMaxWidth().height(IntrinsicSize.Min).clickable { selection.value = item }, verticalAlignment = Alignment.CenterVertically) {
                    ShowRow(item)
                }
                if (selection.value != null) {
                    ItemClicked(selection.value!!._id)
                    selection.value = null
                }
            }
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(formatDateString(item.timestamp), Modifier.padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(dateColumnWidth),color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        var fontStyle = FontStyle.Normal
        val textColor = MaterialTheme.colorScheme.primary

        // A diary item blended in
        if (item.type != viewModel.dataType) {
            fontStyle = FontStyle.Italic
            val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
            Text(s, Modifier.padding(start = cellPadding.dp), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp, fontStyle = fontStyle)
            return
        }

        // Value
        Text(item.value1, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp)
            .width(value1Width),textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Comment
        Spacer(modifier = Modifier.width(cellPadding.dp))
        val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
        Text(s, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle, style = TextStyle(lineHeight = TextUnit.Unspecified, platformStyle = PlatformTextStyle(includeFontPadding = false)))
    }


    @SuppressLint("SimpleDateFormat")
    override fun addItem() {
        if (!quickEntry) {
            startEditing(-1) // Indicate that this is about a new item
            return
        }

        // Close keyboard after entry is done
        hideKeyboard()

        // Check empty variables
        if (value1.value.isEmpty()) {
            snackbarDelegate.showSnackbar(context.getString(R.string.fluidMissing))
            return
        }

        value1.value = value1.value.replace(MainActivity.decimalSeparator, '.')
        val value1Int = viewModel.validValueI(value1.value, Threshold.MAX_FLUID)
        if (value1Int <= 0) {
            snackbarDelegate.showSnackbar(context.getString(R.string.invalid) + " " + context.getString(R.string.fluid) + " " + context.getString(R.string.value) + " $value1Int")
            return
        }

        val item = Data(_id = 0, timestamp = Date().time, comment = comment.value, type = viewModel.dataType, value1 = value1.value, value2 = SimpleDateFormat("yyyyMMdd").format(Date().time), value3 = "", value4 = "", attachment = "", tags = Filter.NO_TAGS, category_id = -1, profile_id = activeProfileId)
        viewModel.upsert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp))
            snackbarDelegate.showSnackbar(context.getString(R.string.filteredOut))

        cleanUpAfterAddItem()
    }
}