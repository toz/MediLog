/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.dialogs

import android.content.Context
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.getCorrectedDateFormat
import java.text.DateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

// Dialog which allows to select a text template
class MultipleMeasurementsDialog(val context: Context, val vm: BloodPressureViewModel) {
    var dataList: MutableList<Data>
    var dateFormat: DateFormat
    var timeFormat: DateFormat
    private val multipleMeasurementsTimeWindow: String
    private var datalistSize: Int

    init {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        multipleMeasurementsTimeWindow = "" + preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW, context.getString(R.string.BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW_DEFAULT))

        dataList  = getData()

        datalistSize = dataList.size
        dateFormat  = getCorrectedDateFormat(context) //DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
        timeFormat  = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())
    }

    fun getData(): MutableList<Data> {
        val today = Calendar.getInstance()
        today.add(Calendar.MINUTE, -multipleMeasurementsTimeWindow.toInt())
        val backMilliseconds = today.timeInMillis

        val profile = activeProfileId.toString()
        val dataType = vm.dataType.toString()
        //val filter = vm.compileFilter()
        var query = "SELECT * FROM data WHERE"
        query += " profile_id = $profile AND type = $dataType"
        query += " AND timestamp > $backMilliseconds"
        query += " ORDER BY timestamp DESC"
        return vm.getDataList(query)
    }

    @Composable
    fun ShowDialog(setShowDialog: (Boolean) -> Unit) {
        // If only one record is found there is nothing to do
        if (datalistSize <= 1) {
            setShowDialog(false)
            return
        }
        Dialog(onDismissRequest = { setShowDialog(false) }) {
            Surface(shape = RoundedCornerShape(16.dp)) {
                Box(contentAlignment = Alignment.Center) {
                    Column(modifier = Modifier.padding(20.dp)) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(text = context.getString(R.string.multipleMeasurements), style = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold))
                        }
                        Spacer(modifier = Modifier.height(20.dp))
                        Row {
                            Column(modifier = Modifier.fillMaxWidth()) {
                                val activateButtons = remember { mutableStateOf(true) }

                                Text(context.getString(R.string.multipleMeasurementsFound, datalistSize, multipleMeasurementsTimeWindow), color = MaterialTheme.colorScheme.primary)
                                Text(text = AnnotatedString(text = " - ")
                                    .plus(AnnotatedString(context.getString(R.string.ignore), spanStyle = SpanStyle(fontWeight = FontWeight.Bold)))
                                    .plus(AnnotatedString(" " + context.getString(R.string.keepAllRecords))))
                                Text(text = AnnotatedString(text = " - " + context.getString(R.string.replaceWith) + " ")
                                    .plus(AnnotatedString(context.getString(R.string.average), spanStyle = SpanStyle(fontWeight = FontWeight.Bold))))
                                Text(text = AnnotatedString(text = " - ")
                                    .plus(AnnotatedString(context.getString(R.string.delete), spanStyle = SpanStyle(fontWeight = FontWeight.Bold)))
                                    .plus(AnnotatedString(" " + context.getString(R.string.previousMeasurements))))
                                Text("")
                                HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.primary) // Lines starting from the top
                                Spacer(modifier = Modifier.size(5.dp))                                //Box(modifier = Modifier.fillMaxWidth()){
                                LazyColumn(modifier = Modifier.padding(start = 8.dp)) {
                                    items(dataList) { item ->
                                        //val checkedState = remember { mutableStateOf(true) }
                                        Row(verticalAlignment = Alignment.CenterVertically) {
                                            val s = dateFormat.format(item.timestamp) + " - " + timeFormat.format(item.timestamp) + ", "+ item.value1 + " " + item.value2 + " " + item.value3 + " " + item.comment
                                            Text(s, color = MaterialTheme.colorScheme.primary, maxLines = 1, overflow = TextOverflow.Ellipsis)
                                        }
                                    }
                                }
                                Column { //} modifier = Modifier.align(Alignment.BottomCenter)) {
                                    Spacer(modifier = Modifier.size(5.dp))                                //Box(modifier = Modifier.fillMaxWidth()){
                                    HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.secondary) // Lines starting from the top                                    Spacer(modifier = Modifier.height(20.dp))
//                                        ShowBottomRow()
                                    Row(
                                        modifier = Modifier.fillMaxWidth(),
                                        horizontalArrangement = Arrangement.End,
                                        verticalAlignment = Alignment.CenterVertically) {
                                        TextButton(onClick = { setShowDialog(false) }) { Text(context.getString(R.string.ignore), color = MaterialTheme.colorScheme.primary) }
                                        TextButton(onClick = {  compileAverage()
                                                                setShowDialog(false)
                                                             }, enabled = activateButtons.value) { Text(context.getString(R.string.average)) }
                                        TextButton(onClick = {  replacePreviousMeasurements()
                                                                setShowDialog(false)
                                                             }, enabled = activateButtons.value) { Text(context.getString(R.string.delete)) }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun compileAverage() {
        var sysAvg = 0
        var diaAvg = 0
        var pulseAvg = 0
        var avgComment = ""
        var avgProfile = 0
        var itemCount = 0

        // Refresh to catch the current addition
        dataList.forEach {
            try {
                sysAvg += it.value1.toInt()
                diaAvg += it.value2.toInt()
                if (it.value3.isNotEmpty()) pulseAvg += it.value3.toInt()
            } catch (_: NumberFormatException)  {}
            avgComment += it.comment
            avgProfile = it.profile_id
            itemCount++
            vm.delete(it._id)
        }
        sysAvg /= itemCount
        diaAvg /= itemCount
        if (pulseAvg > 0) pulseAvg /= itemCount

        vm.upsert(Data(_id = 0, timestamp = Date().time, profile_id = avgProfile , comment = avgComment, value1 = sysAvg.toString(), value2 = diaAvg.toString(), value3 = pulseAvg.toString(), type = Tabs.BLOODPRESSURE, tags = NO_TAGS, category_id = -1, attachment = "" ))
    }

    private fun replacePreviousMeasurements() {
        var count = 0
        dataList.forEach {
            // Skip most recent entry
            if (count++ > 0) vm.delete(it._id)
        }
    }
}