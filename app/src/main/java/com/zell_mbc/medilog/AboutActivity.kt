/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.format.Formatter
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withLink
import androidx.compose.ui.unit.dp
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.LINK_COLOR
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.profiles.ProfilesViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.ATTACHMENT_LABEL
import com.zell_mbc.medilog.support.MedilogTheme
import com.zell_mbc.medilog.support.attachmentSize
import com.zell_mbc.medilog.support.countAttachments
import com.zell_mbc.medilog.support.getMemoryConsumption
import com.zell_mbc.medilog.support.getVersionCode
import com.zell_mbc.medilog.support.getVersionName
import com.zell_mbc.medilog.weight.WeightViewModel
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

class AboutActivity : AppCompatActivity() {
    lateinit var preferences: SharedPreferences
    lateinit var viewModel: DataViewModel
    private var recordCount = ""
    private var databaseSize = ""
    private var attachmentSize = ""
    var attachments = 0
    private var profileCount = 0L
    var memoryConsumption = ""
    private var versionCode = ""
    private var versionName = ""
    private var blockScreenShots = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        blockScreenShots = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOCK_SCREENSHOTS, false)
        if (blockScreenShots) window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        // Activities need to be self sufficient because MainActivity might get killed by OS
        val viewModelProvider = ViewModelProvider(this)
        viewModel = viewModelProvider[WeightViewModel::class.java]

        // Retrieve data
        recordCount = viewModel.count().toString()
        val f = getDatabasePath("MediLogDatabase")
        databaseSize = Formatter.formatFileSize(this, f?.length() ?: 0)

        memoryConsumption = Formatter.formatFileSize(this, getMemoryConsumption())

        val profileViewModel = ViewModelProvider(this)[ProfilesViewModel::class.java]
        profileCount = profileViewModel.count()

        val l = attachmentSize(this, ATTACHMENT_LABEL)
        attachmentSize = Formatter.formatFileSize(this, l)
        attachments = countAttachments(this, ATTACHMENT_LABEL)

        versionCode = getVersionCode(this)
        versionName = getVersionName(this)

        enableEdgeToEdge() //This will include/color the top Android info bar
        setContent { MedilogTheme { ShowContent() } }
    }


    @SuppressLint("SimpleDateFormat", "PackageManagerGetSignatures")
    @Composable
    fun ShowContent() {
        val scrollState = rememberScrollState()
        MedilogTheme {
            var s: String
            Column(modifier = Modifier.safeDrawingPadding().verticalScroll(state = scrollState).padding(start = 16.dp, top = 8.dp)) {
                Row {
                    Surface(modifier = Modifier.size(50.dp), color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0f)) {
                        Image(painter = painterResource(id = R.mipmap.ic_launcher_inverted), contentDescription = null)
                    }
                    Column(modifier = Modifier.padding(start = 8.dp, top = 6.dp)) {
                        Text(stringResource(id = R.string.appName), style = MaterialTheme.typography.bodyLarge, color = MaterialTheme.colorScheme.primary, textAlign = TextAlign.End)
                        Text(stringResource(id = R.string.appDescription), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                    }
                }
                Text("Version $versionName, build $versionCode", modifier = Modifier.padding(top = 8.dp), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                Text(stringResource(id = R.string.zellMBC), modifier = Modifier.padding(top = 8.dp), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                // Email
                Text(buildAnnotatedString {
                    append("")
                    withLink(LinkAnnotation.Url(url = "mailto:" + stringResource(id = R.string.email), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(stringResource(id = R.string.email)) }
                }, style = MaterialTheme.typography.bodyMedium, textDecoration = TextDecoration.Underline)

                // AppUrl
                Text(buildAnnotatedString {
                    append("")
                    withLink(LinkAnnotation.Url(url = stringResource(id = R.string.AppURL), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(stringResource(id = R.string.AppURL)) }
                }, style = MaterialTheme.typography.bodyMedium, textDecoration = TextDecoration.Underline)

                // ------------ Backup -----------
                val timestamp = remember { mutableLongStateOf(preferences.getLong("LAST_BACKUP", 0L)) }

                val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
                val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)

                val str = if (timestamp.longValue > 0) " " + dateFormat.format(timestamp.longValue) + " " + timeFormat.format(timestamp.longValue)
                else  " " + stringResource(id = R.string.never)
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(text = stringResource(R.string.lastBackup), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                    TextButton(onClick = { timestamp.longValue = runBackup(timestamp.longValue) }, modifier = Modifier.padding(end = 16.dp)) { Text(str, style = MaterialTheme.typography.bodyMedium, color = Color(LINK_COLOR), textDecoration = TextDecoration.Underline) }
                }
                // ---------------- End backup ----------------

                Text(stringResource(id = R.string.statistics), style = MaterialTheme.typography.bodyLarge, color = MaterialTheme.colorScheme.primary)

                s = getString(R.string.memoryConsumption) + ": $memoryConsumption"
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                s = getString(R.string.profiles) + ": $profileCount"
                Text(s, modifier = Modifier.padding(top = 8.dp), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                s = getString(R.string.database) + " " + getString(R.string.entries) + ": $recordCount"
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                s = getString(R.string.database) + " " + getString(R.string.size) + ": $databaseSize"
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                if (attachments > 0) {
                    s = getString(R.string.attachments) + ": $attachments"
                    Text(s, modifier = Modifier.padding(top = 8.dp), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                    s = getString(R.string.size) + ": $attachmentSize"
                    Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                }

                // Security
                Text(stringResource(id = R.string.security), modifier = Modifier.padding(top = 8.dp), style = MaterialTheme.typography.bodyLarge, color = MaterialTheme.colorScheme.primary)

                s = getString(R.string.databaseEncryption) + ": " + if (preferences.getBoolean("encryptedDB", false)) getString(R.string.onSwitch)
                else getString(R.string.offSwitch)
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                /*s = getString(R.string.attachmentEncryption)  + ": " + if (preferences.getBoolean("encryptedAttachments", false)) getString(R.string.onSwitch)
                else getString(R.string.offSwitch)
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)*/

                s = getString(R.string.biometricProtection) + ": " + if (preferences.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) getString(R.string.onSwitch)
                else getString(R.string.offSwitch)
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                s = getString(R.string.protectedBackup) + ": " + if (("" + preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")).isNotEmpty()) getString(R.string.onSwitch)
                else getString(R.string.offSwitch)
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                s = getString(R.string.blockScreenshots) + ": " + if (blockScreenShots) getString(R.string.onSwitch)
                else getString(R.string.offSwitch)
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                @Suppress("DEPRECATION") val sig =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) application.packageManager.getPackageInfo(application.packageName, PackageManager.GET_SIGNING_CERTIFICATES).signingInfo.apkContentsSigners[0]
                    else application.packageManager.getPackageInfo(application.packageName, PackageManager.GET_SIGNATURES).signatures[0]

                val certStream: InputStream = ByteArrayInputStream(sig.toByteArray())
                val subject = try {
                    val certFactory = CertificateFactory.getInstance("X509")
                    val x509Cert: X509Certificate =
                        certFactory.generateCertificate(certStream) as X509Certificate
                    x509Cert.subjectDN.toString()
                } catch (e: CertificateException) {
                    "Unknown"
                }
                s = "Application Certificate: $subject"
                Text(s, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)

                // ThirdParty
                Text("")
                Text(stringResource(id = R.string.Copyright2), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                Text(buildAnnotatedString {
                    append("")
                    withLink(LinkAnnotation.Url(url = stringResource(id = R.string.thirdparty), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))
                    ) { append(stringResource(id = R.string.thirdparty)) }
                }, style = MaterialTheme.typography.bodyMedium, textDecoration = TextDecoration.Underline)

                // Copyright
                val year = SimpleDateFormat("yyyy")
                Text(
                    stringResource(id = R.string.Copyright1, year.format(Date()).toString()),
                    modifier = Modifier.padding(top = 8.dp),
                    style = MaterialTheme.typography.bodySmall, color = MaterialTheme.colorScheme.primary
                )
                Text(buildAnnotatedString {
                    append("")
                    withLink(LinkAnnotation.Url(url = stringResource(id = R.string.mediLogLicense), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))
                    ) { append(stringResource(id = R.string.mediLogLicense)) }
                }, style = MaterialTheme.typography.bodySmall, textDecoration = TextDecoration.Underline)
            }
        }
    }


    private fun runBackup(timestamp: Long): Long {
        // Check if everything is in place for auto backups
        val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
        if (uriString.isNullOrEmpty()) {
            createIntent()
            return Date().time
        }

        // Valid location?
        val uri = Uri.parse(uriString)
        val dFolder = DocumentFile.fromTreeUri(application, uri)
        if (dFolder == null) {
            createIntent()
            return Date().time
        }

        // Password
        var zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
        if (zipPassword.isNullOrEmpty()) zipPassword = ""

        viewModel.writeBackup(uri, zipPassword, false)

        // If recordCount = 0, writeBackup won't do a backup
        if (recordCount != "0") {
            val t = Date().time
            val editor = preferences.edit()
            editor.putLong("LAST_BACKUP_CHECK", t)
            editor.apply()
            return t
        }
        else return timestamp
    }

    private fun createIntent() {
        Toast.makeText(application, getString(R.string.missingBackupLocation), Toast.LENGTH_LONG).show()

        // Pick backup folder
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        backupLauncher.launch(Intent.createChooser(intent, getString(R.string.selectDirectory)))
    }

    private val backupLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            data ?: return@registerForActivityResult // No data = nothing to do, shouldn't happen

            var backupFolderUri: Uri? = null

            // Check Uri is sound
            try {
                backupFolderUri = data.data
            } catch (e: Exception) {
                Toast.makeText(this, this.getString(R.string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
            }
            if (backupFolderUri == null) {
                Toast.makeText(this, this.getString(R.string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                return@registerForActivityResult
            }

            val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
            if (zipPassword.isNullOrEmpty()) {
                val customDialog = Dialog(this)
                customDialog.setContentView(R.layout.input_dialog)
                customDialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                val title: TextView = customDialog.findViewById(R.id.tvTitle)
                title.text = getString(R.string.enterPassword)
                val txtInput: TextView = customDialog.findViewById(R.id.etInput)
                val positiveButton: Button = customDialog.findViewById(R.id.btPositive)
                val negativeButton: Button = customDialog.findViewById(R.id.btNegative)
                positiveButton.text = getString(R.string.submit)
                negativeButton.text = getString(R.string.cancel)
                txtInput.hint = getString(R.string.password)
                txtInput.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                positiveButton.setOnClickListener {
                    viewModel.writeBackup(backupFolderUri, txtInput.text.toString())
                    customDialog.dismiss()
                }
                negativeButton.setOnClickListener {
                    customDialog.dismiss()
                }
                customDialog.show()
            } else viewModel.writeBackup(backupFolderUri, zipPassword)
        }
    }

}