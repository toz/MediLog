/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.BiometricHelper
import org.acra.config.dialog
import org.acra.config.mailSender
import org.acra.data.StringFormat
import org.acra.dialog.BuildConfig
import org.acra.ktx.initAcra

class MediLog : Application() {
    var authenticated = false
    private var hasBiometric = true
    private lateinit var preferences: SharedPreferences

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    override fun onCreate() {
        super.onCreate()

        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        // If Authenticated is false lets check if we want to and if this device can authenticate
      if (!authenticated) {
          if (!preferences.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) {
              authenticated = true         // If user set biometric login off, we are always authenticated
          } else { // Biometric setting is on -> Let's check if authentication is possible
              val biometricHelper = BiometricHelper(this)
              // Check if biometric device  exists, if not remove biometric setting in settings activity and set authenticated to always true
              if (biometricHelper.hasHardware(false)) {
                  authenticated = false    // Make sure we launch the Biometric logon process in onStart
              } else {
                  authenticated = true
                  hasBiometric = false
              }
          }
      }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)

        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        if (!preferences.getBoolean(SettingsActivity.KEY_PREF_ENABLECRASHLOGS, true)) return

        initAcra {
            //core configuration:
            buildConfigClass = BuildConfig::class.java
            reportFormat = StringFormat.KEY_VALUE_LIST

            mailSender {
                //required
                mailTo = "medilog@zell-mbc.com"
                reportAsFile = true
                reportFileName = "crashLog.txt"
                subject = getString(R.string.crashTitle)
                body = getString(R.string.crashMessageEmail)
            }
            dialog {
                text = getString(R.string.crashMessageDialog)
                title = getString(R.string.crashTitle)
                positiveButtonText = getString(R.string.ok)
                negativeButtonText = getString(R.string.cancel)
                commentPrompt = getString(R.string.crashContext)
                resIcon = android.R.drawable.ic_dialog_alert
                emailPrompt = ""
            }
        }
    }
}

