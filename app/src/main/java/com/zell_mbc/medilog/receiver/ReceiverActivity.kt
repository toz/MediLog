/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.receiver

import android.content.Intent
import android.content.Intent.EXTRA_STREAM
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.toColorInt
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.activeTabRoute
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.scaffold.Screens
import com.zell_mbc.medilog.settings.SettingsViewModel
import com.zell_mbc.medilog.support.getAttachmentFolder
import com.zell_mbc.medilog.tags.TagsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import java.io.InputStream
import java.net.URL
import java.util.Date
import java.util.regex.Pattern
import javax.net.ssl.HttpsURLConnection

class ReceiverActivity: AppCompatActivity() {

    // https://anant-raman.medium.com/extract-hyperlinks-and-emails-from-a-text-and-perform-actions-in-android-kotlin-5483fe74da5e
    private val urlPattern: Pattern = Pattern.compile(
        "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
        Pattern.CASE_INSENSITIVE or Pattern.MULTILINE or Pattern.DOTALL
    )

    private val emailPattern: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    //Function to extract hyperlinks from a given stringprivate
    fun getHyperLinks(s: String): List<Pair<Int, Int>> {
        val urlList = mutableListOf<Pair<Int, Int>>()
        val urlMatcher = urlPattern.matcher(s)
        var matchStart: Int
        var matchEnd: Int
        while (urlMatcher.find()) {
            matchStart = urlMatcher.start(1)
            matchEnd = urlMatcher.end()
            urlList.add(Pair(matchStart, matchEnd))
            val url = s.substring(matchStart, matchEnd)
        }
        return urlList
    }

    //Function to extract emails from a given string
    private fun getEmailLists(s: String): List<Pair<Int, Int>> {
        val emailList = mutableListOf<Pair<Int, Int>>()
        val emailMatcher = emailPattern.matcher(s)
        while (emailMatcher.find()) {
            val email = emailMatcher.group()
            emailList.add(Pair(emailMatcher.start(), emailMatcher.start() + email.length))
        }
        return emailList
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        if (intent?.action == Intent.ACTION_SEND)
            when(intent.type) {
               "text/plain" -> handleSendText(intent) // Handle text being sent
               "application/pdf" -> handlePdf(intent) // Handle text being sent
        }
    }

    //Function to customise texts which are identifed as a hyperlink or an email
    private fun customiseText(spanStr: SpannableString, start: Int, end: Int): SpannableString {
        val clickSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                // Write the actions you want to be performed on click of the particular hyperlink or email
            }
        }
        spanStr.setSpan(clickSpan, start, end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)//Change the colour of the hyperlink or the email
        spanStr.setSpan(ForegroundColorSpan("#FF39ACEE".toColorInt()), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spanStr
    }

    private fun extractUrl(source: String): String {
        val urlMatcher = urlPattern.matcher(source)
        var matchStart: Int
        var matchEnd: Int
        return if (urlMatcher.find()) {
            matchStart = urlMatcher.start(1)
            matchEnd = urlMatcher.end()
            source.substring(matchStart, matchEnd)
        }
        else ""
    }

    fun extractSigmaData(urlString: String): String {
        var retValue = ""

        runBlocking {
            val j = launch(Dispatchers.IO) {
                var url = URL(urlString)
                val urlConnection = url.openConnection() as HttpsURLConnection
                try {
                    val website = urlConnection.inputStream.bufferedReader().readText()
                    var searchText = "<meta name='description' content='Name: "
                    var index = website.indexOf(searchText)
                    if (index >= 0) {
                        val newStart = website.substring(index + searchText.length)
                        var index = newStart.indexOf("\n")
                        retValue = newStart.substring(0, index)
                    }
                } finally {
                    urlConnection.disconnect()
                }
            }
            j.join()
        }
        return retValue
    }

    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            /*val listOfUrls = getHyperLinks(it)
            val listOfEmails = getEmailLists(it)
            val spanPoint = SpannableString(it)
            var point: SpannableString? = null
            for (url in listOfUrls) {
                point = customiseText(spanPoint, url.first, url.second)
            }
            for (email in listOfEmails) {
                point = customiseText(spanPoint, email.first,   email.second)
            }*/

            val diaryViewModel    = ViewModelProvider(this)[DiaryViewModel::class.java]

            var sportLabel = "Sport"
            var tagId = ""
            var text = it.toString()

            val url = extractUrl(text)
            if (url.isNotEmpty()) {
                if (url.contains("sigma-sharing.com")) {
                    val extract = extractSigmaData(url)
                    if (extract.isNotEmpty()) {
                        text = "$extract, $url"
                        tagId = ViewModelProvider(this)[TagsViewModel::class.java].searchTag(sportLabel)
                    }
                }
            }
            val item = Data(0, Date().time, text, Tabs.DIARY, value1 = "", value2 = "0", value3 = "", value4 = "", attachment = "", tags = tagId, category_id = -1, profile_id = activeProfileId)
            diaryViewModel.upsertBlocking(item) // Blocking to make sure data is written before Activity is killed

            val message = getString(R.string.receivedDiaryEntry) + ": $text"
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()

            finish()

            /* Todo
             Receiver Settings screen:
             - Open MediLog yes/no
             - Assign tag to sigma-sharing
             */
        }
    }

    private fun handlePdf(intent: Intent) {
        val pdfUri = intent.extras?.get(EXTRA_STREAM) as Uri

        val filenameString = pdfUri.toString()
        if (filenameString.lowercase().contains(".pdf")) {
            val fileName = filenameString.substring(filenameString.lastIndexOf("/") +1)
            val newFile = File(getAttachmentFolder(this), fileName)

            val resolver = contentResolver
            val inputStream: InputStream? = resolver.openInputStream(pdfUri)
            inputStream.use { input -> newFile.outputStream().use { output -> input!!.copyTo(output) } }

            val viewModelProvider = ViewModelProvider(this)
            val documentsViewModel    = viewModelProvider[DiaryViewModel::class.java]
            val settingsViewModel = viewModelProvider[SettingsViewModel::class.java]

            val item = Data(0, timestamp = Date().time, comment = fileName, type = Tabs.DOCUMENTS, value1 = "", value2 = "", value3 = "", value4 = "", attachment = fileName, tags ="", category_id = -1, profile_id = activeProfileId)
            documentsViewModel.upsert(item)

            settingsViewModel.setValue(key = "activeTab", value = Screens.Documents.route)
            activeTabRoute = Screens.Documents.route

            val message = getString(R.string.receivedDocument) + ": $fileName"
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
        /*val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)*/
    }


}