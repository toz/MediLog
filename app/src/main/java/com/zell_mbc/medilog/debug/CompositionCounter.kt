/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

@file:Suppress("NOTHING_TO_INLINE")
package com.zell_mbc.medilog.debug

import android.util.Log
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember
import com.zell_mbc.medilog.MainActivity.Companion.DEBUGTAG

class Ref(var value: Int)

const val EnableDebugCompositionLogs = true //false

/* Usage
Row(Modifier.fillMaxWidth()) {
    CompositionCounter(DEBUGTAG)
    // Weight
    TextField(
…
*/

@Composable
inline fun CompositionCounter(tag: String) {
    if (EnableDebugCompositionLogs) {
        val ref = remember { Ref(0) }
        SideEffect { ref.value++ }
        Log.d(tag, "Compositions: ${ref.value}")
    }
}
