/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Color.RED
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.text.format.DateUtils
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate.*
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.PromptInfo
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowRight
import androidx.compose.material.icons.automirrored.outlined.Send
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.AreaChart
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.outlined.FilterList
import androidx.compose.material.icons.outlined.FilterListOff
import androidx.compose.material.icons.outlined.FolderZip
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.OpenInBrowser
import androidx.compose.material.icons.outlined.Person
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material.icons.rounded.MoreVert
import androidx.compose.material.icons.rounded.Search
import androidx.compose.material3.BottomAppBarDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MediumTopAppBar
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.core.view.WindowCompat
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.preference.PreferenceManager
import com.google.android.material.color.DynamicColors
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.R.*
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.bloodpressure.BloodPressurePdf
import com.zell_mbc.medilog.bloodpressure.BloodPressureTab
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.data.ImportActivity
import com.zell_mbc.medilog.data.TextTemplates
import com.zell_mbc.medilog.diary.DiaryPdf
import com.zell_mbc.medilog.diary.DiaryTab
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.diary.DocumentsTab
import com.zell_mbc.medilog.documents.DocumentsViewModel
import com.zell_mbc.medilog.glucose.GlucosePdf
import com.zell_mbc.medilog.glucose.GlucoseTab
import com.zell_mbc.medilog.glucose.GlucoseViewModel
import com.zell_mbc.medilog.oximetry.OximetryPdf
import com.zell_mbc.medilog.oximetry.OximetryTab
import com.zell_mbc.medilog.oximetry.OximetryViewModel
import com.zell_mbc.medilog.profiles.ProfilesActivity
import com.zell_mbc.medilog.profiles.ProfilesViewModel
import com.zell_mbc.medilog.profiles.UNSET
import com.zell_mbc.medilog.scaffold.BottomNavigationItem
import com.zell_mbc.medilog.scaffold.DataTab
import com.zell_mbc.medilog.scaffold.MediLogSearchBar
import com.zell_mbc.medilog.scaffold.NotYetAuthorized
import com.zell_mbc.medilog.scaffold.Screens
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_APP_THEME
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_AUTO_BACKUP
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_BACKUP_WARNING
import com.zell_mbc.medilog.preferences.SettingsActivity.Companion.KEY_PREF_FORCE_REAUTHENTICATION
import com.zell_mbc.medilog.settings.SettingsViewModel
import com.zell_mbc.medilog.temperature.TemperaturePdf
import com.zell_mbc.medilog.temperature.TemperatureTab
import com.zell_mbc.medilog.temperature.TemperatureViewModel
import com.zell_mbc.medilog.texttemplates.TextTemplatesViewModel
import com.zell_mbc.medilog.support.AppStart
import com.zell_mbc.medilog.support.BiometricHelper
import com.zell_mbc.medilog.support.ChangelogFragment
import com.zell_mbc.medilog.support.MedilogTheme
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.support.checkAppStart
import com.zell_mbc.medilog.support.determineAvailableTabs
import com.zell_mbc.medilog.support.getVersionCode
import com.zell_mbc.medilog.tags.TagsViewModel
import com.zell_mbc.medilog.fluid.FluidPdf
import com.zell_mbc.medilog.fluid.FluidTab
import com.zell_mbc.medilog.fluid.FluidViewModel
import com.zell_mbc.medilog.weight.WeightPdf
import com.zell_mbc.medilog.weight.WeightTab
import com.zell_mbc.medilog.weight.WeightViewModel
import java.io.*
import java.net.HttpURLConnection
import java.net.URL 
import java.net.URLEncoder
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.UUID.randomUUID
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {
    private lateinit var preferences: SharedPreferences
    //private var activeTabRoute = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge() //This will include/color the top Android info bar
        super.onCreate(savedInstanceState)

        medilogApplicationContext = applicationContext as MediLog

        //preferences = Preferences.getSharedPreferences(this)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        val window = this@MainActivity.window

        // Initialise Navigation tabs array
        availableTabs.clear()
        availableTabs.add(DataTab(id = Tabs.WEIGHT, label = getString(string.weight), iconActive = drawable.ic_weight_filled, iconInactive = drawable.ic_weight_outlined, route = "weight_route"))
        availableTabs.add(DataTab(id = Tabs.BLOODPRESSURE, label = getString(string.bloodPressure), iconActive = drawable.ic_blood_pressure_filled, iconInactive = drawable.ic_blood_pressure_outlined, route = "bloodpressure_route"))
        availableTabs.add(DataTab(id = Tabs.DIARY, label = getString(string.diary), iconActive = drawable.ic_diary_filled, iconInactive = drawable.ic_diary_outlined, route = "diary_route"))
        availableTabs.add(DataTab(id = Tabs.FLUID, label = getString(string.fluid), iconActive = drawable.ic_fluid, iconInactive = drawable.ic_fluid, route = "fluid_route"))
        availableTabs.add(DataTab(id = Tabs.GLUCOSE, label = getString(string.glucose), iconActive = drawable.ic_glucose_filled, iconInactive = drawable.ic_glucose_outlined, route = "glucose_route"))
        availableTabs.add(DataTab(id = Tabs.TEMPERATURE, label = getString(string.temperature), iconActive = drawable.ic_outline_device_thermostat_24, iconInactive = drawable.ic_outline_device_thermostat_24, route = "temperature_route"))
        availableTabs.add(DataTab(id = Tabs.OXIMETRY, label = getString(string.oximetry), iconActive = drawable.ic_spo2_filled, iconInactive = drawable.ic_spo2_outlined, route = "oximetry_route"))
        availableTabs.add(DataTab(id = Tabs.DOCUMENTS, label = getString(string.documents), iconActive = drawable.baseline_attach_file_24, iconInactive = drawable.baseline_attach_file_24, route = "documents_route"))

        val viewModelProvider = ViewModelProvider(this)
        weightViewModel = viewModelProvider[WeightViewModel::class.java]
        bloodPressureViewModel = viewModelProvider[BloodPressureViewModel::class.java]
        diaryViewModel = viewModelProvider[DiaryViewModel::class.java]
        fluidViewModel = viewModelProvider[FluidViewModel::class.java]
        glucoseViewModel = viewModelProvider[GlucoseViewModel::class.java]
        temperatureViewModel = viewModelProvider[TemperatureViewModel::class.java]
        oximetryViewModel = viewModelProvider[OximetryViewModel::class.java]
        documentsViewModel = viewModelProvider[DocumentsViewModel::class.java]

        profilesViewModel = viewModelProvider[ProfilesViewModel::class.java]
        textTemplatesViewModel = viewModelProvider[TextTemplatesViewModel::class.java]
        tagsViewModel = viewModelProvider[TagsViewModel::class.java]
        settingsViewModel = viewModelProvider[SettingsViewModel::class.java]

        //blockScreenShots = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOCK_SCREENSHOTS, false)
        if (preferences.getBoolean(SettingsActivity.KEY_PREF_BLOCK_SCREENSHOTS, false)) window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        userFeedbackLevel = preferences.getInt("USER_FEEDBACK_SETTING", 0) // Drives level of feedback (nothing / standard / debug)

        // Check DynamicColor
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            dynamicColorOn = preferences.getBoolean(SettingsActivity.KEY_PREF_DYNAMIC_COLOR, false)
            if (dynamicColorOn) DynamicColors.applyToActivitiesIfAvailable(application) // Required for view based screens
        }

        DynamicColors.applyToActivitiesIfAvailable(application)

        val theme = preferences.getString(KEY_PREF_APP_THEME, getString(string.DARK_MODE_DEFAULT))
        val themes = resources.getStringArray(array.themeValues)

        when (theme) {
            themes[1] -> setDefaultNightMode(MODE_NIGHT_NO)
            themes[2] -> setDefaultNightMode(MODE_NIGHT_YES)
            themes[3] -> setDefaultNightMode(MODE_NIGHT_AUTO_BATTERY)
            else -> setDefaultNightMode(MODE_NIGHT_FOLLOW_SYSTEM)
        }

        // Force crash for debug purposes
        //throw RuntimeException("This is a forced crash for debug purposes");
        val editor = preferences.edit()

        // Profile section
        // Get profile id which was active on last run
        if (activeProfileId < 1) { // Nothing set? Should only happen on first launch
            val temp = settingsViewModel.getValue(profile_id = 0, key = "activeProfile")
            activeProfileId = try { temp.toInt() } catch (_: NumberFormatException) { profilesViewModel.getFirst() }
        }

        if (activeProfileId < 1) // Still nothing set? Should only happen on first launch
        {
            // Capture old style user name if it exists, else use default
            val userName = preferences.getString("userName", getString(string.userNameDefault)).toString()
            val newProfile = profilesViewModel.createNewProfile(name = userName)
            settingsViewModel.setActiveProfile(newProfile._id)
            activeProfileId = newProfile._id
        }

        // Assign appropriate viewModel for the active tab
        activeTabRoute = settingsViewModel.getValue(key = "activeTab", default = "weight_route")
        saveActiveTab(activeTabRoute)

        var enabledTabs = preferences.getString("activeTabs", "").toString()
        if (enabledTabs.isEmpty()) {
            if (weightViewModel.count() > 0) // We got data -> set tabs based on what's in DB
                enabledTabs = determineAvailableTabs(activeProfileId)
            else enabledTabs = Tabs.ALL_TABS
            editor.putString("activeTabs", enabledTabs).apply()
        }

        setActiveTabs(enabledTabs) // Apply tab settings to availableTabs array

        // Check AppStart status
        when (checkAppStart(this)) {
            AppStart.FIRST_TIME -> {
                // App runs for the first time -> set defaults
                MaterialAlertDialogBuilder(this)
                    .setTitle(resources.getString(string.disclaimer))
                    .setMessage(resources.getString(string.disclaimerText))
                    .setPositiveButton(resources.getString(string.agree)) { _, _ ->
                        // Respond to positive button press
                    }
                    .show()
                checkUSDefaults()
            }

            AppStart.FIRST_TIME_VERSION -> {
                val dialogFragment = ChangelogFragment()
                val ft = supportFragmentManager.beginTransaction()
                val prev: Fragment? = supportFragmentManager.findFragmentByTag("ChangelogFragment")
                if (prev != null) ft.remove(prev)
                ft.addToBackStack(null)
                dialogFragment.show(ft, "ChangelogFragment")
            }

            else -> { /* Do nothing */
            }
        }

        // Initialize delimiter on first run based on locale
        var delimiter = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, "")
        if (delimiter.equals("")) {
            val currentLanguage = Locale.getDefault().displayLanguage
            delimiter = if (currentLanguage.contains("Deutsch")) ";" else ","

            editor.putString(SettingsActivity.KEY_PREF_DELIMITER, delimiter)
            editor.apply()
        }

        editor.putBoolean("encryptedAttachments", true)
        editor.apply()

/*        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        WindowCompat.getInsetsController(window, window.decorView).apply {
            isAppearanceLightStatusBars = !isDarkThemeOn(applicationContext)
        }
*/
        // Todo check Google Backup
        checkBackup()

        // deprecated after API 30
        //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE) // Required so Navbar and FAB move up when keyboard is visible

//        if (tagsViewModel.count())

        // Migrate old templates, added with v3.0, delete after a few versions
        val oldTemplates = preferences.getString("etTextTemplates", "") + ""
        if (oldTemplates.isNotEmpty()) {
            val t = oldTemplates.split(",")
            val dt = viewModel.dataType // Pick first active tab, not ideal but hey
            t.forEach {
                textTemplatesViewModel.upsert(TextTemplates(0, dt, it))
            }
            val e = preferences.edit()
            e.putString("etTextTemplates", "").apply()
        }

        // Check user feedback
       if (userFeedbackLevel > 0) {
            val lastRun = preferences.getLong("LAST_FEEDBACK_RUN", 0L)
            if (!DateUtils.isToday(lastRun)) userFeedback(this) // Check if it ran already today
        }

        WindowCompat.setDecorFitsSystemWindows(window, true)
        setContent {
            StartCompose()
        }
    }


    private fun checkUSDefaults() {
        // At this time, only three countries - Burma, Liberia, and the US - have not adopted the International System of Units (SI, or metric system) as their official system of weights and measures
        // Retrieve system language
        val country = Locale.getDefault().country
        when (country) {
            "US" -> {
                // Todo: Switch to settings
                val editor = preferences.edit()
                editor.putString(SettingsActivity.KEY_PREF_WEIGHT_PAPER_SIZE, "Letter")
                editor.putString(SettingsActivity.KEY_PREF_BLOODPRESSURE_PAPER_SIZE, "Letter")
                editor.putString(SettingsActivity.KEY_PREF_DIARY_PAPER_SIZE, "Letter")
                editor.putString(SettingsActivity.KEY_PREF_TEMPERATURE_PAPER_SIZE, "Letter")
                editor.putString(SettingsActivity.KEY_PREF_GLUCOSE_PAPER_SIZE, "Letter")
                editor.putString(SettingsActivity.KEY_PREF_OXIMETRY_PAPER_SIZE, "Letter")
                editor.putString(SettingsActivity.KEY_PREF_FLUID_PAPER_SIZE, "Letter")
                editor.putString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, "lbs")
                editor.putString(SettingsActivity.KEY_PREF_FLUID_UNIT, "oz")
                editor.putString(SettingsActivity.KEY_PREF_FLUID_THRESHOLD, "125")
                editor.apply()
            }
        }
    }

    private fun checkBackup() {
        // Todo: Check Google backup
        // https://developer.android.com/guide/topics/data/autobackup
        // Is it on?
        // How are we doing re. the 25MB limit
        // Reflect Google backup status (size and last run) in About screen

        // Is the DB empty? If yes, don't bother with backups
        // We consider everything below 5 entries to be simple tests
        val records = viewModel.count()
        if (records < 5) return

        // Did we check backup status already today? If yes, don't bother again
        if (DateUtils.isToday(preferences.getLong("LAST_BACKUP_CHECK", 0L))) return // Check if it ran already today

        var editor = preferences.edit()
        editor.putLong("LAST_BACKUP_CHECK", Date().time).apply()

        val lastBackup = preferences.getLong("LAST_BACKUP", 0L)
        val backupAgeMS = Date().time - lastBackup
        val backupAgeDays = TimeUnit.DAYS.convert(backupAgeMS, TimeUnit.MILLISECONDS)

        // Backup warning
        val backupWarningDays: Int
        // Check if backupWarning is enabled
        val backupWarning = preferences.getString(KEY_PREF_BACKUP_WARNING, getString(string.BACKUP_WARNING_DEFAULT))
        if (!backupWarning.isNullOrEmpty()) {
            backupWarningDays = try { backupWarning.toInt() } catch (_: NumberFormatException) { getString(string.BACKUP_WARNING_DEFAULT).toInt() }
            if (backupAgeDays >= backupWarningDays) {
                val message = getString(string.backupOverdue) + ".  " + when (lastBackup) {
                    0L   -> getString(string.backupOverdueMsg1, records)
                    else -> getString(string.backupOverdueMsg2, records, backupAgeDays)
                }
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            }
        }

        // Auto Backup
        // Check if autoBackup is enabled
        val autoBackup = preferences.getString(KEY_PREF_AUTO_BACKUP, getString(string.AUTO_BACKUP_DEFAULT))
        if (autoBackup.isNullOrEmpty()) return

        val autoBackupDays = try { autoBackup.toInt() } catch (_: NumberFormatException) { getString(string.AUTO_BACKUP_DEFAULT).toInt() }

        if (backupAgeDays >= autoBackupDays) {
            // Check if everything is in place for auto backups
            val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
            if (uriString.isNullOrEmpty()) {
                Toast.makeText(this, getString(string.missingBackupLocation), Toast.LENGTH_LONG).show()
                return
            }

            // Valid location?
            val uri = Uri.parse(uriString)
            val dFolder = DocumentFile.fromTreeUri(this, uri)
            if (dFolder == null) {
                Toast.makeText(this, getString(string.invalidBackupLocation), Toast.LENGTH_LONG).show()
                return
            }

            // Password
            var zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
            if (zipPassword.isNullOrEmpty()) zipPassword = ""

            viewModel.writeBackup(uri, zipPassword, true)

            editor = preferences.edit()
            editor.putLong("LAST_BACKUP_CHECK", Date().time)
            editor.apply()
        }
    }

    override fun onStart() {
        super.onStart()
        checkTimeout()
    }

    private fun checkTimeout() {
        if (!preferences.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) {
            medilogApplicationContext.authenticated = true
            authenticated.value = true
            return
        }

        val tmp = "" + preferences.getString(KEY_PREF_FORCE_REAUTHENTICATION, getString(string.FORCE_REAUTHENTICATION_DEFAULT))
        val threshold = 60000 * try {
            tmp.toLong()
        } catch (_: NumberFormatException) {
            getString(string.FORCE_REAUTHENTICATION_DEFAULT).toLong()
        }

        // Check timer
        val now = Calendar.getInstance().timeInMillis
        val then = preferences.getLong("STOPWATCH", 0L)
        val diff = now - then

        // First check if authentication is enabled
        // If yes, check if re-authentication needs to be forced = if more than threshold milliseconds passed after last onStop event was executed
        if (diff > threshold) {
            medilogApplicationContext.authenticated = false
            authenticated.value = false
        }

        val biometricHelper = BiometricHelper(this)
        val canAuthenticate = biometricHelper.canAuthenticate(true)
        if (!medilogApplicationContext.authenticated && canAuthenticate == 0) {
            // No idea why I need this
            val newExecutor: Executor = Executors.newSingleThreadExecutor()
            val activity: FragmentActivity = this
            val myBiometricPrompt = BiometricPrompt(activity, newExecutor, object : BiometricPrompt.AuthenticationCallback() {

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Snackbar
                        .make(findViewById(android.R.id.content), getString(string.retryActionText), 30000)
                        .setAction(getString(string.retryAction)) { onStart() }
                        .setActionTextColor(RED)
                        .show()
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    medilogApplicationContext.authenticated = true
                    authenticated.value = true
                    //runOnUiThread { binding.viewPager.visibility = View.VISIBLE }
                }
            })
            // Todo: Fix deprecation
            if (Build.VERSION.SDK_INT > 29) {
                val promptInfo = PromptInfo.Builder()
                    .setTitle(getString(string.appName))
                    .setDeviceCredentialAllowed(true)  // Allow to use pin as well
                    .setSubtitle(getString(string.biometricLogin))
                    .setConfirmationRequired(false)
                    .build()
                myBiometricPrompt.authenticate(promptInfo)
            } else {
                // Pin fallback Disabled for now until bug https://issuetracker.google.com/issues/142740104 is fixed)
                val promptInfo = PromptInfo.Builder()
                    .setTitle(getString(string.appName))
                    .setNegativeButtonText(getString(string.cancel))
                    .setSubtitle(getString(string.biometricLogin))
                    .setConfirmationRequired(false)
                    .build()
                myBiometricPrompt.authenticate(promptInfo)
            }
        }
    }

    private val backupLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            data ?: return@registerForActivityResult // No data = nothing to do, shouldn't happen

            var backupFolderUri: Uri? = null

            // Check Uri is sound
            try {
                backupFolderUri = data.data
            } catch (_: Exception) {
                Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
            }
            if (backupFolderUri == null) {
                Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                return@registerForActivityResult
            }

            val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
            if (zipPassword.isNullOrEmpty()) {
                val customDialog = Dialog(this)
                customDialog.setContentView(layout.input_dialog)
                customDialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                val title: TextView = customDialog.findViewById(id.tvTitle)
                title.text = getString(string.enterPassword)
                val txtInput: TextView = customDialog.findViewById(id.etInput)
                val positiveButton: Button = customDialog.findViewById(id.btPositive)
                val negativeButton: Button = customDialog.findViewById(id.btNegative)
                positiveButton.text = getString(string.submit)
                negativeButton.text = getString(string.cancel)
                txtInput.hint = getString(string.password)
                txtInput.inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD
                positiveButton.setOnClickListener {
                    viewModel.writeBackup(backupFolderUri, txtInput.text.toString())
                    customDialog.dismiss()
                }
                negativeButton.setOnClickListener {
                    customDialog.dismiss()
                }
                customDialog.show()
            } else viewModel.writeBackup(backupFolderUri, zipPassword)
        }
    }


    private val restoreLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            data ?: return@registerForActivityResult // No data = nothing to do, shouldn't happen

            var backupFileUri: Uri? = null
            try {
                backupFileUri = data.data
            } catch (_: Exception) {
                Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
            }

            backupFileUri ?: return@registerForActivityResult

            // Check if user needs to be warned
            val existingRecords = viewModel.count()
            if (existingRecords > 0) {
                MaterialAlertDialogBuilder(this)
                    .setTitle(resources.getString(string.warning))
                    .setMessage(resources.getString(string.thisIsGoing, existingRecords) + "\n" + getString(string.doYouReallyWantToContinue))
                    .setPositiveButton(resources.getString(string.yes)) { _, _ ->
                        launchRestoreIntent(backupFileUri)
                    }
                    .setNegativeButton(resources.getString(string.cancel)) { _, _ ->
                        // Respond to negative button press
                    }
                    .show()
            } else launchRestoreIntent(backupFileUri)
        }
    }


    private val importLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            data ?: return@registerForActivityResult // No data = nothing to do, shouldn't happen

            var importFileUri: Uri? = null
            try {
                importFileUri = data.data
            } catch (_: Exception) {
                Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
            }

            importFileUri ?: return@registerForActivityResult
            launchRestoreIntent(importFileUri)
        }
    }

    private fun getPdfClass(): BasePdf? {
        val pdfClass = when (viewModel.dataType) {
            Tabs.BLOODPRESSURE -> BloodPressurePdf(this@MainActivity)
            Tabs.WEIGHT -> WeightPdf(this@MainActivity)
            Tabs.DIARY -> DiaryPdf(this@MainActivity)
            Tabs.GLUCOSE -> GlucosePdf(this@MainActivity)
            Tabs.FLUID -> FluidPdf(this@MainActivity)
            Tabs.TEMPERATURE -> TemperaturePdf(this@MainActivity)
            Tabs.OXIMETRY -> OximetryPdf(this@MainActivity)
            else -> null
        }
        return pdfClass
    }

    private fun openFile(type: String) {
        if (viewModel.getSize(true) == 0) {
            Toast.makeText(this, getString(string.emptyTable) + " " + getString(string.pdfCantCreate), Toast.LENGTH_LONG).show()
            return
        }

        var uri: Uri? = null
        when (type) {
            IntentTypes.PDF -> {
                val pdfHandler = getPdfClass()
                pdfHandler?.createPdfDocument()
                uri = pdfHandler?.savePdfDocument()
            }

            IntentTypes.CSV -> {
                uri = viewModel.writeCsvFile(true)
            }
        }

        if (uri != null) {
            val intent = Intent(ACTION_VIEW)
            //intent.type = type
            intent.setDataAndType(uri, type)
            intent.flags = FLAG_GRANT_READ_URI_PERMISSION
            val chooser = createChooser(intent, null)
            try {
                startActivity(chooser)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this, getString(string.eShareError) + ": " + e.localizedMessage, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun sendPdfFile(type: String, zipPassword: String = "") {
        if (viewModel.getSize(true) == 0) {
            Toast.makeText(this, getString(string.emptyTable) + " " + getString(string.pdfCantCreate), Toast.LENGTH_LONG).show()
            return
        }

        val intent = Intent(ACTION_SEND)
        intent.type = type
        intent.flags = FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(EXTRA_TEXT, getString(string.sendText))

        intent.putExtra(EXTRA_SUBJECT, "MediLog " + getString(string.data))
        var uri: Uri? = null
        val pdfHandler = getPdfClass()
        val pdfDoc = pdfHandler?.createPdfDocument()
        when (type) {
            IntentTypes.ZIP -> uri = viewModel.getZIP(pdfDoc, zipPassword)
            IntentTypes.CSV -> uri = viewModel.writeCsvFile(true)
            IntentTypes.PDF -> uri = viewModel.savePdfDocument(pdfDoc)
        }

        if ((uri != null) && (packageManager.resolveActivity(intent, 0) != null)) {
            intent.putExtra(EXTRA_STREAM, uri)
            try {
                startActivity(createChooser(intent, getString(string.sendText)))
            } catch (e: Exception) {
                Toast.makeText(this, getString(string.eShareError) + ": " + e.localizedMessage, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun sendCsvFile() {
        val type = IntentTypes.CSV
        val intent = Intent(ACTION_SEND)
        intent.type = type
                    intent.flags = FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(EXTRA_TEXT, getString(string.sendText))

        intent.putExtra(EXTRA_SUBJECT, "MediLog " + getString(string.data))
        var uri: Uri? = null
        when (type) {
            IntentTypes.CSV -> uri = viewModel.writeCsvFile(true)
        }

        if ((uri != null) && (packageManager.resolveActivity(intent, 0) != null)) {
            intent.putExtra(EXTRA_STREAM, uri)
            try {
                startActivity(createChooser(intent, getString(string.sendText)))
            } catch (e: Exception) {
                Toast.makeText(this, getString(string.eShareError) + ": " + e.localizedMessage, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getSendZipPassword() {
        val customDialog = Dialog(this)
        customDialog.setContentView(layout.input_dialog)
        customDialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val title: TextView = customDialog.findViewById(id.tvTitle)
        title.text = getString(string.enterPassword)
        val txtInput: TextView = customDialog.findViewById(id.etInput)
        val positiveButton: Button = customDialog.findViewById(id.btPositive)
        val negativeButton: Button = customDialog.findViewById(id.btNegative)
        positiveButton.text = getString(string.submit)
        negativeButton.text = getString(string.cancel)
        txtInput.hint = getString(string.password)
        txtInput.inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD
        positiveButton.setOnClickListener {
            sendPdfFile(IntentTypes.ZIP, txtInput.text.toString())
            customDialog.dismiss()
        }
        negativeButton.setOnClickListener {
            customDialog.dismiss()
        }
        customDialog.show()
    }


    private fun launchRestoreIntent(backupFileUri: Uri) {
        // Analyse file
        val cr = contentResolver

        when (val mimeType = cr.getType(backupFileUri)) {
            IntentTypes.ZIP -> {
                val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                if (zipPassword.isNullOrEmpty()) getOpenZipPassword(backupFileUri)
                else {
                    val intent = Intent(application, ImportActivity::class.java)
                    intent.putExtra("URI", backupFileUri.toString())
                    intent.putExtra("PWD", zipPassword)
                    startActivity(intent)
                }
            }

            "application/csv",
            "text/csv",
            "text/comma-separated-values" -> {
                val intent = Intent(application, ImportActivity::class.java)
                intent.putExtra("URI", backupFileUri.toString())
                intent.putExtra("PWD", "SIDELOAD")
                startActivity(intent)
            }

            else -> Toast.makeText(this, "Don't know how to handle file type: $mimeType", Toast.LENGTH_LONG).show()
        }
    }


    private fun getOpenZipPassword(zipUri: Uri) {
        val customDialog = Dialog(this)
        customDialog.setContentView(layout.input_dialog)
        customDialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val title: TextView = customDialog.findViewById(id.tvTitle)
        title.text = getString(string.enterPassword)
        val txtInput: TextView = customDialog.findViewById(id.etInput)
        val positiveButton: Button = customDialog.findViewById(id.btPositive)
        val negativeButton: Button = customDialog.findViewById(id.btNegative)
        positiveButton.text = getString(string.submit)
        negativeButton.text = getString(string.cancel)
        txtInput.hint = getString(string.password)
        txtInput.inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD
        positiveButton.setOnClickListener {
            val intent = Intent(application, ImportActivity::class.java)
            intent.putExtra("URI", zipUri.toString())
            intent.putExtra("PWD", txtInput.text.toString())
            //Toast.makeText(this, "0: Before startActivity", Toast.LENGTH_LONG).show()
            TimeUnit.SECONDS.sleep(2L)
            startActivity(intent)
            customDialog.dismiss()
        }
        negativeButton.setOnClickListener {
            customDialog.dismiss()
        }
        customDialog.show()
    }

// #####################################################
// Application independent code
// #####################################################
// https://dev.to/ahmmedrejowan/hide-the-soft-keyboard-and-remove-focus-from-edittext-in-android-ehp
override fun dispatchTouchEvent(event: MotionEvent): Boolean {
    if (event.action == MotionEvent.ACTION_DOWN) {
        val v: View? = currentFocus
        if (v is EditText) {
            val outRect = android.graphics.Rect()
            v.getGlobalVisibleRect(outRect)
            if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                v.clearFocus()
                val imm = this.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
                imm!!.hideSoftInputFromWindow(v.getWindowToken(), 0)
            }
        }
    }
    return super.dispatchTouchEvent(event)
}

    public override fun onStop() {
        super.onStop()
        // Save active tab
        val editor = preferences.edit()
        editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
        editor.apply()
    }

    // Reads current tab setting from preferences and stores it in a dedicated string preference
    private fun setActiveTabs(enabledTabs: String) {
        availableTabs.forEach() { if (enabledTabs.contains(it.id.toString())) it.active = true }

        // Make sure at least one tab is active
        var check = false
        availableTabs.forEach() { if (it.active == true) check = true }

        // Should never happen
        if (!check) {
            // Set all tabs to active as emergency measure
            availableTabs.forEach() { it.active = true }
            Log.d(DEBUGTAG, "Something is wrong with enabledTabs: $enabledTabs")
        }
    }


    // Saves the active tab so the same tab is active when app gets reopened
    private fun saveActiveTab(tab: String) {
        settingsViewModel.setValue(key = "activeTab", value = tab)
        activeTabRoute = tab

        viewModel = when (tab) { // Known data types
            Screens.BloodPressure.route -> ViewModelProvider(this)[BloodPressureViewModel::class.java]
            Screens.Diary.route -> ViewModelProvider(this)[DiaryViewModel::class.java]
            Screens.Fluid.route -> ViewModelProvider(this)[FluidViewModel::class.java]
            Screens.Glucose.route -> ViewModelProvider(this)[GlucoseViewModel::class.java]
            Screens.Temperature.route -> ViewModelProvider(this)[TemperatureViewModel::class.java]
            Screens.Oximetry.route -> ViewModelProvider(this)[OximetryViewModel::class.java]
            Screens.Documents.route -> ViewModelProvider(this)[DocumentsViewModel::class.java]
            else -> ViewModelProvider(this)[WeightViewModel::class.java]
        }
        viewModel.loadFilter() // Make sure filter settings are applied
    }

    // Scaffold and other UI stuff
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun StartCompose() {
        var currentTab: BaseTab? = null // Keep a handle on the current tab so we can store current values in case the Tab gets destroyed
        var showOverflowMenu by remember { mutableStateOf(false) }
        val snackbarHostState = remember { SnackbarHostState() }
        val snackbarDelegate = SnackbarDelegate(snackbarHostState, rememberCoroutineScope())

        val weightTab = WeightTab(this@MainActivity, snackbarDelegate)
        val bloodPressureTab = BloodPressureTab(this@MainActivity, snackbarDelegate)
        val diaryTab = DiaryTab(this@MainActivity, snackbarDelegate)
        val fluidTab = FluidTab(this@MainActivity, snackbarDelegate)
        val temperatureTab = TemperatureTab(this@MainActivity, snackbarDelegate)
        val oximetryTab = OximetryTab(this@MainActivity, snackbarDelegate)
        val glucoseTab = GlucoseTab(this@MainActivity, snackbarDelegate)
        val documentsTab = DocumentsTab(this@MainActivity, snackbarDelegate)

        // Check authentication status before showing stuff
        authenticated.value = medilogApplicationContext.authenticated
        val go = remember { authenticated }
        if (!go.value) {
            NotYetAuthorized()
            return
        }

        var searchBarOpen by remember { mutableStateOf(false) }
        //var activeTabRoute by remember { mutableStateOf( settingsViewModel.getValue(key = "activeTab", default = "weight_route") ) }
        var enableActivities by remember { mutableStateOf( activeTabRoute != Screens.Documents.route) }

        val navController = rememberNavController()

        var activeTabIndex = getTabIndex(activeTabRoute)
        if (activeTabIndex < 0) { // Should never happen
            Log.d(DEBUGTAG, "activeTabIndex = $activeTabIndex, activeTabRoute = $activeTabRoute")
            MaterialAlertDialogBuilder(this)
                .setTitle("Severe Error!")
                .setMessage("activeTabIndex = $activeTabIndex, activeTabRoute = $activeTabRoute. Please restart the app!")
                .setPositiveButton(resources.getString(string.ok)) { _, _ ->  }
                .show()
            activeTabIndex = 0 // Set to defined emergency value
        }
        val activeTabItem = availableTabs[activeTabIndex]

        // Check if activeTabRoute is visible, if not pick the first active one
        if (!activeTabItem.active) {
            // Activate first active tab
            for (i in 0..< availableTabs.size) {
                if (availableTabs[i].active) {
                    activeTabRoute = availableTabs[i].route
                    break
                }
            }
        }
        else activeTabRoute = activeTabItem.route

        var navigationSelectedItem by remember { mutableStateOf(activeTabRoute) }
        var filterActive by remember { mutableStateOf( viewModel.filterActive() ) }

        MedilogTheme {
//            Box(modifier = Modifier.safeDrawingPadding()) {
            val controller = LocalSoftwareKeyboardController.current
            val header = medilogApplicationContext.getString(string.appName) + if (profilesViewModel.count() > 1L) " - " + (profilesViewModel.get(activeProfileId)?.name ?: "") else ""
            // TopAppBarDefaults.pinnedScrollBehavior()
            val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior() // pinnedScrollBehavior() //TopAppBarDefaults.exitUntilCollapsedScrollBehavior(rememberTopAppBarState())

            Scaffold(
                //safeDrawingPadding().
                modifier = Modifier.nestedScroll((scrollBehavior.nestedScrollConnection)),
                snackbarHost = { SnackbarHost(snackbarHostState) },
                topBar = {
                    if (searchBarOpen)
                        MediLogSearchBar(setShowDialog = { searchBarOpen = it }, application) {
                            if (it > 0) { currentTab?.startEditing(it) }
                        }
                    else MediumTopAppBar(
                        title = { Text(header) },
                        actions = {
                                 /* Don't think I should show this here
                            IconButton(onClick = {
                                    val intent = Intent(this@MainActivity, ProfilesActivity::class.java)
                                    this@MainActivity.startActivity(intent)
                            }) { Icon(imageVector = Icons.Rounded.Person, contentDescription = "Profile") }*/
                            IconButton(enabled = enableActivities, onClick = {
                                val intent = Intent(this@MainActivity, FilterActivity::class.java)
                                intent.putExtra("activeTab", viewModel.dataType)
                                startActivity(intent)
                            }) { Icon(imageVector = if (filterActive) Icons.Outlined.FilterList else Icons.Outlined.FilterListOff, contentDescription = "Filter" )}
                            IconButton(onClick = { searchBarOpen = true }) { Icon(imageVector = Icons.Rounded.Search, contentDescription = "Search") }
                            //Spacer(modifier = Modifier.width(10.dp))
                            IconButton(enabled = enableActivities, onClick = { currentTab?.showInfoScreen(this@MainActivity) }) { Icon(imageVector = Icons.Default.Info, contentDescription = "Info Screen") }
                            IconButton(enabled = enableActivities, onClick = { currentTab?.showChartScreen(this@MainActivity) }) { Icon(imageVector = Icons.Default.AreaChart, contentDescription = "Chart Screen") }
                            IconButton(onClick = { showOverflowMenu = !showOverflowMenu }) { Icon(imageVector = Icons.Rounded.MoreVert, contentDescription = "Overflow") }
                        },
                        scrollBehavior = scrollBehavior,
                        modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection)
                    )
                },
                bottomBar = {
                    if (BottomNavigationItem().bottomNavigationItems().size > 1) NavigationBar {// Only show navbar if there's something to navigate
                        //getting the list of bottom navigation items for our data class
                        BottomNavigationItem().bottomNavigationItems().forEachIndexed { index, navigationItem ->
                            NavigationBarItem(
                                selected = navigationItem.route == navigationSelectedItem,
                                label = { Text(navigationItem.label) },
                                icon = { Icon(ImageVector.vectorResource(if (navigationItem.route == navigationSelectedItem) navigationItem.iconActive else navigationItem.iconInactive), contentDescription = navigationItem.label) },
                                onClick = {
                                    navigationSelectedItem = navigationItem.route
                                    navController.navigate(navigationItem.route) {
                                        // Pop up to the start destination of the graph to avoid building up a large stack of destinations on the back stack as users select items
                                        popUpTo(navController.graph.findStartDestination().id) { saveState = true }
                                        launchSingleTop = true // Avoid multiple copies of the same destination when re-selecting the same item
                                        restoreState = true
                                    }
                                }
                            )
                        }
                    }
                },
                floatingActionButton = {
                    FloatingActionButton(
                        onClick = {
                            controller?.hide()
                            currentTab?.addItem() },
                        containerColor = BottomAppBarDefaults.bottomAppBarFabColor,
                        elevation = FloatingActionButtonDefaults.elevation()
                    ) {
                        Icon(Icons.Filled.Add, "Add item")
                    }
                },
            )
            { paddingValues ->
                NavHost(
                    navController = navController,
                    startDestination = activeTabRoute,
                    modifier = Modifier.padding(paddingValues = paddingValues)
                ) {
                    // https://medium.com/@nitheeshag/navigation-in-jetpack-compose-with-animations-724037d7b119
                    composable(Screens.Weight.route) {
                        saveActiveTab(Screens.Weight.route)
                        weightTab.ShowContent(padding = paddingValues)
                        currentTab = weightTab
                        filterActive = viewModel.filterActive()
                        enableActivities = true
                    }
                    composable(Screens.BloodPressure.route) {
                        saveActiveTab(Screens.BloodPressure.route)
                        bloodPressureTab.ShowContent(padding = paddingValues)
                        currentTab = bloodPressureTab
                        enableActivities = true
                    }
                    composable(Screens.Diary.route) {
                        saveActiveTab(Screens.Diary.route)
                        diaryTab.ShowContent(padding = paddingValues)
                        currentTab = diaryTab
                        filterActive = viewModel.filterActive()
                        enableActivities = true
                    }
                    composable(Screens.Fluid.route) {
                        saveActiveTab(Screens.Fluid.route)
                        fluidTab.ShowContent(padding = paddingValues)
                        currentTab = fluidTab
                        enableActivities = true
                    }
                    composable(Screens.Temperature.route) {
                        saveActiveTab(Screens.Temperature.route)
                        temperatureTab.ShowContent(padding = paddingValues)
                        currentTab = temperatureTab
                        filterActive = viewModel.filterActive()
                        enableActivities = true
                    }
                    composable(Screens.Oximetry.route) {
                        saveActiveTab(Screens.Oximetry.route)
                        oximetryTab.ShowContent(padding = paddingValues)
                        currentTab = oximetryTab
                        filterActive = viewModel.filterActive()
                        enableActivities = true
                    }
                    composable(Screens.Glucose.route) {
                        saveActiveTab(Screens.Glucose.route)
                        glucoseTab.ShowContent(padding = paddingValues)
                        currentTab = glucoseTab
                        filterActive = viewModel.filterActive()
                        enableActivities = true
                    }
                    composable(Screens.Documents.route) {
                        saveActiveTab(Screens.Documents.route)
                        documentsTab.ShowContent(padding = paddingValues)
                        currentTab = documentsTab
                        filterActive = viewModel.filterActive()
                        enableActivities = false
                    }
                }
            }
            if (showOverflowMenu) OverflowMenu(onDismissRequest = { showOverflowMenu = false }, true)
        }
    }

    // Returns index of item with the respective route
    private fun getTabIndex(route: String): Int {
        for (i in 0..< availableTabs.size) {
            if (availableTabs[i].route == route)  {
                return i
            }
        }
        return -1
/*
        var ret = -1
        BottomNavigationItem().bottomNavigationItems().forEachIndexed { index, navigationItem ->
            if (navigationItem.route == route) return index
        }
        return ret*/
    }

    @Composable
    fun OverflowMenu(
        onDismissRequest: (Boolean) -> Unit,
        expanded: Boolean
    ) {
        var showCsvMenu by remember { mutableStateOf(false) }
        var showPdfMenu by remember { mutableStateOf(false) }
        var showDataMenu by remember { mutableStateOf(false) }
        var selectedMenu = ""
        val defaultBackground = MaterialTheme.colorScheme.surfaceContainer
        val selectedBackground = MaterialTheme.colorScheme.surfaceContainerHighest

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.TopEnd)
        ) {
            DropdownMenu(
                expanded = expanded,
                { onDismissRequest(false) }
            ) {
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.action_settings), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.Settings, contentDescription = null) },
                    onClick = {
                        val intent = Intent(this@MainActivity, SettingsActivity::class.java)
                        this@MainActivity.startActivity(intent)
                        onDismissRequest(false)
                    }
                )
                DropdownMenuItem(
                    modifier = Modifier.background(if(this@MainActivity.getString(string.actionPDF) == selectedMenu) selectedBackground else defaultBackground),
                    text = { Text(this@MainActivity.getString(string.actionPDF), style = TextStyle.Default) },
                    trailingIcon = { Icon(Icons.AutoMirrored.Filled.ArrowRight, contentDescription = null) },
                    //colors = MenuItemColors,
                    onClick = {
                        showPdfMenu = true
                        selectedMenu = this@MainActivity.getString(string.actionPDF)
                    }
                )
                DropdownMenuItem(
                    modifier = Modifier.background(if(this@MainActivity.getString(string.action_dataManagement) == selectedMenu) selectedBackground else defaultBackground),
                    text = { Text(this@MainActivity.getString(string.action_dataManagement), style = TextStyle.Default) },
                    trailingIcon = { Icon(Icons.AutoMirrored.Filled.ArrowRight, contentDescription = null) },
                    onClick = {
                        showDataMenu = true
                        selectedMenu = this@MainActivity.getString(string.action_dataManagement)
                    }
                )
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.action_profiles), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.Person, contentDescription = null) },
                    onClick = {
                        val intent = Intent(this@MainActivity, ProfilesActivity::class.java)
                        this@MainActivity.startActivity(intent)
                        onDismissRequest(false)
                    }
                )
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.action_about), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.Info, contentDescription = null) },
                    onClick = {
                        val intent = Intent(this@MainActivity, AboutActivity::class.java)
                        this@MainActivity.startActivity(intent)
                        //Toast.makeText(context, getString(string.action_about), Toast.LENGTH_SHORT).show()
                        onDismissRequest(false)
                    }
                )

                if (showCsvMenu) CsvMenu(onDismissRequest = {
                    showCsvMenu = false // Close submenu
                    onDismissRequest(false) // Close Overflow menu
                    }, expanded = true)
                if (showPdfMenu) PdfMenu(onDismissRequest = {
                    showPdfMenu = false // Close submenu
                    onDismissRequest(false) // Close Overflow menu
                    }, true)
                if (showDataMenu) DataMenu(onDismissRequest = {
                    showDataMenu = false // Close submenu
                    onDismissRequest(false) // Close Overflow menu
                    }, true)
            }
        }
    }

    @Composable
    fun CsvMenu(
        onDismissRequest: (Boolean) -> Unit,
        expanded: Boolean
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.TopEnd)
        ) {
            DropdownMenu(
                expanded = expanded,
                { onDismissRequest(false) },
            ) {
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.actionOpenCSV), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.OpenInBrowser, contentDescription = null) },
                    onClick = {
                        //if (viewModel.getSize(true) == 0) Toast.makeText(this, getString(string.noDataToExport), Toast.LENGTH_LONG).show()
                        openFile(IntentTypes.CSV)
                        onDismissRequest(false)
                    }
                )
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.actionSendCSV), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.AutoMirrored.Outlined.Send, contentDescription = null) },
                    onClick = {
                        //if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) Toast.makeText(this, getString(string.noDataToExport), Toast.LENGTH_LONG).show()
                        sendCsvFile()
                        onDismissRequest(false)
                    }
                )
            }
        }
    }

    @Composable
    fun PdfMenu(
        onDismissRequest: (Boolean) -> Unit,
        expanded: Boolean
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.TopEnd)
        ) {
            DropdownMenu(expanded = expanded, { onDismissRequest(false) }) {
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.actionOpenPDF), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.OpenInBrowser, contentDescription = null) },
                    onClick = {
                        //if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) Toast.makeText(this, getString(string.noDataToExport), Toast.LENGTH_LONG).show()
                        openFile(IntentTypes.PDF)
                        onDismissRequest(false)
                    }
                )
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.actionSendPDF), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.AutoMirrored.Outlined.Send, contentDescription = null) },
                    onClick = {
                        //if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) Toast.makeText(this, getString(string.noDataToExport), Toast.LENGTH_LONG).show()
                        sendPdfFile(IntentTypes.PDF)
                        onDismissRequest(false)
                    }
                )
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.actionSendZIP), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.FolderZip, contentDescription = null) },
                    onClick = {
                        //if (viewModels[binding.tabLayout.selectedTabPosition].getSize(true) == 0) Toast.makeText(this, getString(string.noDataToExport), Toast.LENGTH_LONG).show()
                         val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                         if (zipPassword.isNullOrEmpty()) getSendZipPassword()
                         else sendPdfFile(IntentTypes.ZIP, zipPassword)
                         onDismissRequest(false)
                    }
                )
            }
        }
    }

    @Composable
    fun DataMenu(
        onDismissRequest: (Boolean) -> Unit,
        expanded: Boolean
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.TopEnd)
        ) {
            DropdownMenu(
                expanded = expanded,
                { onDismissRequest(false) }
            ) {
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.action_restore), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.OpenInBrowser, contentDescription = null) },
                    onClick = {
                        val intent = Intent(ACTION_GET_CONTENT)
                        intent.type = "application/zip"
                        restoreLauncher.launch(createChooser(intent, getString(string.selectFile)))
                        onDismissRequest(false)
                    }
                )
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.action_backup), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.AutoMirrored.Outlined.Send, contentDescription = null) },
                    onClick = {
                        if (viewModel.count() == 0) {
                            //Toast.makeText(this, getString(string.noDataToExport), Toast.LENGTH_LONG).show()
                            onDismissRequest(false)
                        }

                        // Pick backup folder
                        val intent = Intent(ACTION_OPEN_DOCUMENT_TREE)
                        intent.addCategory(CATEGORY_DEFAULT)
                        intent.addFlags(FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                        intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
                        intent.addFlags(FLAG_GRANT_WRITE_URI_PERMISSION)

                        backupLauncher.launch(createChooser(intent, getString(string.selectDirectory)))
                        onDismissRequest(false)
                    }
                )
                DropdownMenuItem(
                    text = { Text(this@MainActivity.getString(string.action_import), style = TextStyle.Default) },
                    leadingIcon = { Icon(Icons.Outlined.FolderZip, contentDescription = null) },
                    onClick = {
                        // Pick import folder
                        val intent = Intent(ACTION_GET_CONTENT)
                        intent.type = "text/*"
                        importLauncher.launch(createChooser(intent, getString(string.selectFile)))
                        onDismissRequest(false)
                    }
                )
            }
        }
    }


//*****************************************************************************
    companion object {
        lateinit var medilogApplicationContext: MediLog
        lateinit var viewModel: DataViewModel

        lateinit var weightViewModel: WeightViewModel
        lateinit var bloodPressureViewModel: BloodPressureViewModel
        lateinit var diaryViewModel: DiaryViewModel
        lateinit var fluidViewModel: FluidViewModel
        lateinit var glucoseViewModel: GlucoseViewModel
        lateinit var temperatureViewModel: TemperatureViewModel
        lateinit var oximetryViewModel: OximetryViewModel
        lateinit var documentsViewModel: DocumentsViewModel

        lateinit var profilesViewModel: ProfilesViewModel
        lateinit var textTemplatesViewModel: TextTemplatesViewModel
        lateinit var tagsViewModel: TagsViewModel
        lateinit var settingsViewModel: SettingsViewModel

        var authenticated = mutableStateOf(false)

        val availableTabs = ArrayList<DataTab>() // Holds all known tabs, active and inactive
        var activeProfileId = UNSET
        var activeTabRoute = "" //settingsViewModel.getValue(key = "activeTab", default = "weight_route")

        lateinit var feedbackString: MutableState<String>  // Global variable so we can set it in multiple places

        const val DEBUGTAG: String = "MediLog-Debug-Tag"

        object DatePattern {
            const val DATE_TIME = "yyyy-MM-dd HH:mm:ss"
            const val DATE = "yyyy-MM-dd"
        }

        object Delimiter {
            const val THRESHOLD = "-"
            const val STRING = '"'
            const val TAG = ":"
        }

        const val NOT_SET = -1
        val AMBER = Color(0xFFFF8500) //Color(0xFFADFF01)
        const val LINK_COLOR = 0xff64B5F6
        const val decimalSeparators = ".," // Allow both , and . as input

        object Tabs {
            const val WEIGHT = 1
            const val BLOODPRESSURE = 2
            const val DIARY = 3
            const val FLUID = 4
            const val GLUCOSE = 5
            const val TEMPERATURE = 6
            const val OXIMETRY = 7
            const val DOCUMENTS = 8
            const val LAST_TAB = 8
            const val ALL_TABS = "$WEIGHT,$BLOODPRESSURE,$DIARY,$FLUID,$GLUCOSE,$TEMPERATURE,$OXIMETRY,$DOCUMENTS"
        }

        object TextSize {
            const val MIN = "10"
            const val MAX = "40"
        }

        object IntentTypes {
            const val ZIP = "application/zip"
            const val CSV = "application/txt"
            const val PDF = "application/pdf"
        }

        object Filter {
            const val OFF = 0
            const val STATIC = 1
            const val ROLLING = 2
            const val NO_TAGS = ""
        }

    // Constants to check against if an entry is valid
        object Threshold {
            const val NO_THRESHOLD_FLOAT = Float.MAX_VALUE
            const val NO_THRESHOLD_INT = Int.MAX_VALUE
            const val MIN_WEIGHT = 20f
            const val MAX_WEIGHT = 999f
            const val MAX_BODYFAT = 99f
            const val MAX_KETONE = 99f // Todo: No idea
            const val MAX_OXIMETRY = 99
            const val MAX_PULSE = 999
            const val MAX_FLUID = 9999
            const val MAX_TEMPERATURE = 999f
            const val MAX_GLUCOSE_MG = 999
            const val MAX_GLUCOSE_MMOL = 99F
        }


        data class TableFields(val n: String) {
            var name = n
            var pos = NOT_SET
        }

        // Initialise table vars
        val dataFields = arrayListOf(
            TableFields("timestamp"),
            TableFields("profile_id"),
            TableFields("tags"),
            TableFields("category_id"), // Can go once DB schema is updated
            TableFields("type"),
            TableFields("value1"),
            TableFields("value2"),
            TableFields("value3"),
            TableFields("value4"),
            TableFields("comment"),
            TableFields("attachment")
        )

        val templateFields = arrayListOf(
            TableFields("_id"),
            TableFields("type"),
            TableFields("template")
        )

        val tagFields = arrayListOf(
            TableFields("_id"),
            TableFields("type"),
            TableFields( "tag"),
            TableFields(    "color")
        )

        val profileFields = arrayListOf(
            TableFields("_id"),
            TableFields("name"),
            TableFields("description")
        )

        val settingsFields = arrayListOf(
            TableFields("profile_id"),
            TableFields("_key"),
            TableFields("value")
        )

        // Retrieve decimal separator for current language
        val decimalSeparator = DecimalFormatSymbols.getInstance().decimalSeparator
        val modifyDecimalSeparator = (decimalSeparator.compareTo('.') != 0)

 //     // Initialize, real value is set in mainActivity
        var userFeedbackLevel = 0

        var dynamicColorOn = false
        //var blockScreenShots = false

        fun resetReAuthenticationTimer(context: Context?) {
            if (context == null) return

            // Reset Stopwatch, after all we are still in MediLog
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)

            val editor = preferences.edit()
            editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
            editor.apply()
        }

        fun userFeedback(context: Context?):String {
            if (context == null) return ""

            val versionCode = getVersionCode(context)

            val delimiter = ":"
            var tabs = "" //delimiter
            for (tab in availableTabs) {
                tabs += viewModel.getSize(tab.id).toString() + delimiter
            }
            val sql = "SELECT count(*) FROM data WHERE attachment != ''"
            val attachments = viewModel.getInt(sql)

            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val devID = preferences.getString("DEVICE_ID", randomUUID().toString())
            val data = "uuid=" + devID + delimiter + "build=" + versionCode + delimiter + "tabs=" + tabs + attachments //tabs.substring(0, tabs.length-1) // Cut off trailing delimiter

            val reqParam = URLEncoder.encode(data, "UTF-8")
            val mURL = URL( context.getString(string.userFeedbackUrl)) // + reqParam)

            thread {
                var localFeedbackString: String
                val https = mURL.openConnection() as HttpURLConnection
                https.requestMethod = "POST"
                https.doOutput = true

                val editor = preferences.edit()
                try {
                    val wr = OutputStreamWriter(https.outputStream)
                    wr.write(reqParam)
                    wr.flush()
                    val ret = https.responseCode // Required to close the POST request
                    editor.putString("DEVICE_ID", devID)
                    editor.putLong("LAST_FEEDBACK_RUN", Date().time)
                    localFeedbackString = if (ret == 200) {
                        editor.putString("USER_FEEDBACK_STRING", data)
                        data
                    } else {
                        // Save error message
                        editor.putString("USER_FEEDBACK_STRING", "Error: $ret")
                        ret.toString()
                    }
                  wr.close()
                }
                catch (e: Exception) {
                    // Save error message
                    editor.putString("USER_FEEDBACK_STRING", "Exception: $e")
                    localFeedbackString = e.toString()
                    //Log.d("Return: Exception", e.toString())
                }
                editor.apply()
                if (::feedbackString.isInitialized) feedbackString.value = localFeedbackString // Only initialized if Feedback UI is shown, ignore otherwise
            }
            return data
        }

        fun isDarkThemeOn(context: Context) = when (context.resources.configuration.uiMode and
            Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> true
            Configuration.UI_MODE_NIGHT_NO -> false
            Configuration.UI_MODE_NIGHT_UNDEFINED -> false
            else -> false
        }
    }
}

