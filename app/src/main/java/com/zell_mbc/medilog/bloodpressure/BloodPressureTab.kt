/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.dialogs.MultipleMeasurementsDialog
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.MedilogTheme
import com.zell_mbc.medilog.support.SnackbarDelegate
import java.util.Date

class BloodPressureTab(context: Context, snackbarDelegate: SnackbarDelegate): BaseTab(context, snackbarDelegate) {

    override var editActivityClass: Class<*> = BloodPressureEditActivity::class.java
    override var infoActivityClass: Class<*> = BloodPressureInfoActivity::class.java
    override var chartActivityClass: Class<*> = BloodPressureChartActivity::class.java

    private var logHeartRhythm = false
    // -------------------------------
    private var bpHelper: BloodPressureHelper = BloodPressureHelper(context)
    private var heartRhythmIssue = true // will be set to default=false by mutableStateOf definition
    private var colorBad = Color.Black
    private var colorGood = Color.Black

    lateinit var multipleMeasurementsDialog: MultipleMeasurementsDialog
    var openMultipleMeasurementsDialog = mutableStateOf(false)

    init {
        logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, context.getString(R.string.LOG_HEARTRHYTHM_DEFAULT).toBoolean())
        itemUnit        = preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_UNIT, context.getString(R.string.BLOODPRESSURE_UNIT_DEFAULT)).toString()
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_HIGHLIGHT_VALUES, context.getString(R.string.BLOODPRESSURE_HIGHLIGHT_VALUES_DEFAULT).toBoolean())
        showTime        = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_SHOWTIME, context.getString(R.string.SHOWTIME_DEFAULT).toBoolean())
    }

    @Composable
    override fun ShowContent(padding: PaddingValues) {
        super.ShowContent(padding)
        showComment.value = comment.value.isNotEmpty() // For some reason the app will crash if this is in super?

        colorBad = MaterialTheme.colorScheme.error
        colorGood = MaterialTheme.colorScheme.primary
        var iconColor by remember { mutableStateOf(setIcon()) }

        if (value1Width == 0.dp) MeasureValue1String("999")

        MedilogTheme {
            LazyColumn(state = listState, horizontalAlignment = Alignment.Start, modifier = Modifier
                .fillMaxWidth()
                .padding(start = 8.dp, end = 8.dp)) {
                item {
                    if (quickEntry) {             // Only show DataEntryRow if quick entry is enabled
                        //val focusManager = LocalFocusManager.current
                        val (textField1, focusDia, focusPulse, focusComment) = remember { FocusRequester.createRefs() }
                        Row(Modifier.fillMaxWidth()) {
                            TextField(
                                value = value1.value,
                                onValueChange = { it ->
                                    val tmp = it.filter  { it.isDigit() }
                                    updateValue1(tmp)
                                    if (value1.value.isNotEmpty()) {
                                        // Jump if starts with 1 and is 3 digits or starts with !1 and is 2 digits
                                        val crit1 = value1.value.length == 3
                                        val crit2 = (value1.value[0] != '1' && value1.value.length == 2)
                                        if (crit1 || crit2) focusManager?.moveFocus(FocusDirection.Next) //focusDia.requestFocus()
                                    }
                                },
                                colors = textFieldColors,
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                                textStyle = TextStyle(),
                                label = {
                                    Text(
                                        text = stringResource(id = R.string.systolic) + "*",
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                },
                                placeholder = { Text(text = stringResource(id = R.string.bpEntryHint) ) },
                                modifier = Modifier
                                    .weight(1f)
                                    .focusRequester(textField1)
                                    .padding(end = 10.dp),
                                keyboardActions = KeyboardActions(onDone = { addItem() })
                            )

                            // Dia
                            TextField(
                                value = value2.value,
                                colors = textFieldColors,
                                onValueChange = {
                                    val tmp = it.filter  { it.isDigit() }
                                    updateValue2(tmp)
                                    if (value2.value.isNotEmpty()) {
                                        // Jump if starts with 1 and is 3 digits or starts with !1 and is 2 digits
                                        val crit1 = value2.value.length == 3
                                        val crit2 = (value2.value[0] != '1' && value2.value.length == 2)
                                        if (crit1 || crit2) focusManager?.moveFocus(FocusDirection.Next) //focusPulse.requestFocus()
                                    }
                                },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Decimal
                                ),
                                textStyle = TextStyle(),
                                label = {
                                    Text(
                                        text = stringResource(id = R.string.diastolic) + "*",
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                },
                                placeholder = { Text(text = stringResource(id = R.string.bpEntryHint)) },
                                modifier = Modifier
                                    .weight(1f)
                                    .focusRequester(focusDia)
                                    .padding(end = 10.dp),
                                keyboardActions = KeyboardActions(onDone = { addItem() })
                            )
                            // Pulse
                            TextField(
                                value = value3.value,
                                colors = textFieldColors,
                                onValueChange = {
                                    val tmp = it.filter { it.isDigit() }
                                    updateValue3(tmp)
                                    if (value3.value.isNotEmpty()) {
                                        showComment.value = true
                                        // Jump if field starts with 1 and is 3 digits or starts with !1 and is 2 digits
                                        val crit1 = value3.value.length == 3
                                        val crit2 = (value3.value[0] != '1' && value3.value.length == 2)
                                        if (crit1 || crit2) focusManager?.moveFocus(FocusDirection.Next)
                                    }
                                },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Decimal
                                ),
                                textStyle = TextStyle(),
                                label = {
                                    Text(
                                        text = stringResource(id = R.string.pulse),
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                },
                                placeholder = { Text(text = stringResource(id = R.string.bpEntryHint)) },
                                modifier = Modifier
                                    .weight(1f)
                                    .focusRequester(focusPulse)
                                    .padding(end = 10.dp),
                                keyboardActions = KeyboardActions(onDone = { addItem() })
                            )

                            if (logHeartRhythm) {
                                Image(painter = painterResource(R.drawable.ic_baseline_warning_24), contentDescription = "Heart Rhythm State",
                                    modifier = Modifier
                                        .weight(0.5f)
                                        .padding(start = 16.dp)
                                        .align(Alignment.CenterVertically)
                                        .clickable { iconColor = setIcon() },
                                    colorFilter = ColorFilter.tint(iconColor),
                                )
                            }
                        } // Row

                        if (showComment.value) {
                            // Comment
                            TextField(
                                value = comment.value,
                                colors = textFieldColors,
                                onValueChange = { updateComment(it) },
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Text,
                                    capitalization = KeyboardCapitalization.Sentences),
                                singleLine = true,
                                //maxLines = 2,
                                textStyle = TextStyle(),
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .focusRequester(focusComment),
                                label = { Text(text = stringResource(id = R.string.comment)) },
                                trailingIcon = { IconButton(onClick = {
                                    openTextTemplatesDialog.value = true })
                                { Icon(Icons.Outlined.Abc,contentDescription = null) } },
                                keyboardActions = KeyboardActions(onDone = { addItem() }))
                        }
                        SideEffect {
                            // Set cursor to first field after addItem completed
                            if (activateFirstField) {
                                textField1.requestFocus()
                                activateFirstField = false }
                        }

                        // Dialog section
                        if (openTextTemplatesDialog.value) {
                            textTemplateDialog.ShowDialog(setShowDialog = { openTextTemplatesDialog.value = it }) {
                                comment.value += it
                                focusComment.requestFocus()
                            }
                            ShowKeyboard() // Todo: Not working reliably
                        }
                        if (openMultipleMeasurementsDialog.value) multipleMeasurementsDialog.ShowDialog(setShowDialog = { openMultipleMeasurementsDialog.value = it })
                        //if (viewModel.openSearchDialog.value) SearchDialog(setShowDialog = { viewModel.openSearchDialog.value = it })
                    }
                }
                items(dataList.value) { item ->
                    HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, thickness = 1.dp) // Lines starting from the top
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(IntrinsicSize.Min)
                            .width(IntrinsicSize.Min)
                            .clickable { selection.value = item },
                        verticalAlignment = Alignment.CenterVertically)
                    { ShowRow(item) }

                    if (selection.value != null) {
                        ItemClicked(selection.value!!._id)
                        selection.value = null
                    }
                }
            }
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        Text(formatDateString(item.timestamp),
            Modifier
                .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
                .width(dateColumnWidth),
            color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        var fontStyle = FontStyle.Normal
        var textColor = MaterialTheme.colorScheme.primary
        var fontWeight = FontWeight.Normal

        // A diary item blended in
        if (item.type != viewModel.dataType) {
            fontStyle = FontStyle.Italic
            item.value1 = ""
            item.value2 = ""
            item.value3 = ""
            // Comment
            val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
            Text(s, Modifier.padding(start = cellPadding.dp), fontWeight = fontWeight, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
            return
        }

        if (logHeartRhythm) {
            // Check value
            var heartRhythm = -1
            if (item.value4.isNotEmpty()) {
                heartRhythm = try {
                    item.value4.toInt()
                } catch (_: NumberFormatException) {
                    0
                }
            }
            // Show image only if value > 0
            val s = if (heartRhythm > 0) stringResource(id = R.string.warningSign)
            else "   "
            Text(s, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)
            VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier
                .width(1.dp)
                .fillMaxHeight()) // Vertical separator
        }
        // Sys
        if (item.value1.isNotEmpty() && highlightValues) {
            when (bpHelper.sysGrade(item.value1)) {
                bpHelper.hyperGrade3 -> {
                    textColor = MaterialTheme.colorScheme.error
                    fontWeight = FontWeight.Bold
                }
                bpHelper.hyperGrade2 -> textColor = MaterialTheme.colorScheme.error
                bpHelper.hyperGrade1 -> textColor = MainActivity.AMBER
                bpHelper.hyperGrade4 -> textColor = MainActivity.AMBER
            }
        }
        Text(item.value1,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(value1Width), textAlign = TextAlign.Center, fontWeight = fontWeight, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Dia
        textColor = MaterialTheme.colorScheme.primary
        fontWeight = FontWeight.Normal
        if (item.value2.isNotEmpty() && highlightValues) {
            when (bpHelper.diaGrade(item.value2)) {
                bpHelper.hyperGrade3 -> {
                    textColor = MaterialTheme.colorScheme.error
                    fontWeight = FontWeight.Bold
                }
                bpHelper.hyperGrade2 -> textColor = MaterialTheme.colorScheme.error
                bpHelper.hyperGrade1 -> textColor = MainActivity.AMBER
                bpHelper.hyperGrade4 -> textColor = MainActivity.AMBER
            }
        }
        Text(item.value2,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(value1Width), textAlign = TextAlign.Center, fontWeight = fontWeight, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)

        textColor = MaterialTheme.colorScheme.primary
        fontWeight = FontWeight.Normal
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Pulse
        Text(item.value3,
            Modifier
                .padding(start = cellPadding.dp, end = cellPadding.dp)
                .width(value1Width), textAlign = TextAlign.Center, fontWeight = fontWeight, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()) // Vertical separator

        // Comment
        val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
        Text(s, Modifier.padding(start = cellPadding.dp), fontWeight = fontWeight, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle,
            style = TextStyle(lineHeight = TextUnit.Unspecified, platformStyle = PlatformTextStyle(includeFontPadding = false)) // style is for removing the padding between multiline text
        )
    }

    override fun addItem() {
        // If standard entry is active create a data element and launch the editActivity
        if (!quickEntry) {
            startEditing(-1) // Indicate that this is about a new item
            return
        }

        // ###########################
        // Checks
        // ###########################
        // Close keyboard so errors are visible
        hideKeyboard()

        // Safely convert string values
        if (stringToInt(value1.value, context.getString(R.string.sysMissing)) < 0) return
        if (stringToInt(value2.value, context.getString(R.string.diaMissing)) < 0) return
        if (value3.value.isNotEmpty() && stringToInt(value3.value, context.getString(R.string.pulseMissing)) < 0) return

        val item = Data(0, timestamp = Date().time, comment = comment.value, type = viewModel.dataType, value1 = value1.value, value2 = value2.value, value3 = value3.value, value4 = if (heartRhythmIssue) "1" else "0", "", tags = NO_TAGS, category_id = -1, profile_id = activeProfileId)
        viewModel.upsertBlocking(item)

        val multipleMeasurementsTimeWindow = "" + preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW, context.getString(R.string.BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW_DEFAULT))
        if (multipleMeasurementsTimeWindow.toInt() > 0) {
            multipleMeasurementsDialog = MultipleMeasurementsDialog(context, viewModel as BloodPressureViewModel)
            openMultipleMeasurementsDialog.value = true
        }

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) snackbarDelegate.showSnackbar(context.getString(R.string.filteredOut))

        cleanUpAfterAddItem()
    }

    // Helper functions
    private fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch (_: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) snackbarDelegate.showSnackbar(errorMessage)

        return valueInt
    }

    fun setIcon(): Color {
        heartRhythmIssue = !heartRhythmIssue
        return if (heartRhythmIssue) colorBad else colorGood
    }
}