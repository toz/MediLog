/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.bloodpressure

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.Tabs.BLOODPRESSURE
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.text.DateFormat
import java.util.ArrayList
import java.util.Calendar

class BloodPressureHelper(val context: Context) {
    var hyperGrade3Sys = 0
    var hyperGrade3Dia = 0
    var hyperGrade2Sys = 0
    var hyperGrade2Dia = 0
    var hyperGrade1Sys = 0
    var hyperGrade1Dia = 0
    private var hypotensionSys = 0
    private var hypotensionDia = 0

    val hyperGrade1 = 1
    val hyperGrade2 = 2
    val hyperGrade3 = 3
    val hyperGrade4 = 4

    private val gradeSettingsDivider = "/"
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    init {
        // Check values
        var sTmp = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade1, context.getString(R.string.BLOODPRESSURE_GRADE1_VALUES)).toString()
        if (sTmp.contains(",")) {
            migrateThresholds() // Convert legacy (pre v3.0) settings
            sTmp = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade1, context.getString(R.string.BLOODPRESSURE_GRADE1_VALUES)).toString()
        }

        try {
            val grade = sTmp.split(gradeSettingsDivider).toTypedArray()
            hyperGrade1Sys = grade[0].toInt()
            hyperGrade1Dia = grade[1].toInt()
        } catch (e: Exception) {
            Toast.makeText(context, context.getString(R.string.grade1Error) + " $sTmp", Toast.LENGTH_LONG).show()
        }
        sTmp = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade2, context.getString(R.string.BLOODPRESSURE_GRADE2_VALUES)).toString()
        try {
            val grade = sTmp.split(gradeSettingsDivider).toTypedArray()
            hyperGrade2Sys = grade[0].toInt()
            hyperGrade2Dia = grade[1].toInt()
        } catch (e: Exception) {
            Toast.makeText(context, context.getString(R.string.grade2Error) + " $sTmp", Toast.LENGTH_LONG).show()
        }
        sTmp = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade3, context.getString(R.string.BLOODPRESSURE_GRADE3_VALUES)).toString()
        try {
            val grade = sTmp.split(gradeSettingsDivider).toTypedArray()
            hyperGrade3Sys = grade[0].toInt()
            hyperGrade3Dia = grade[1].toInt()
        } catch (_: Exception) {
            Toast.makeText(context, context.getString(R.string.grade3Error) + " $sTmp", Toast.LENGTH_LONG).show()
        }
        sTmp = preferences.getString(SettingsActivity.KEY_PREF_HYPOTENSION, context.getString(R.string.BLOODPRESSURE_HYPOTENSION_VALUES)).toString()
        try {
            val grade = sTmp.split(gradeSettingsDivider).toTypedArray()
            hypotensionSys = grade[0].toInt()
            hypotensionDia = grade[1].toInt()
        } catch (e: Exception) {
            Toast.makeText(context, context.getString(R.string.hypotensionError) + " $sTmp", Toast.LENGTH_LONG).show()
        }
    }

    fun sysGrade(systolic: String): Int {
        val sys = try { systolic.toInt() }
        catch  (e: NumberFormatException) { 0 }

        if (sys >= hyperGrade3Sys) return hyperGrade3
        if (sys >= hyperGrade2Sys) return hyperGrade2
        if (sys >= hyperGrade1Sys) return hyperGrade1
        if (sys <= hypotensionSys) return hyperGrade4
        return 0
    }

    fun diaGrade(diastolic: String): Int {
        val dia = try { diastolic.toInt() }
        catch  (e: NumberFormatException) { 0 }

        if (dia >= hyperGrade3Dia) return hyperGrade3
        if (dia >= hyperGrade2Dia) return hyperGrade2
        if (dia >= hyperGrade1Dia) return hyperGrade1
        if (dia <= hypotensionDia) return hyperGrade4
        return 0
    }

    // Migrate , based settings of pre v3.0 settings to more logical | separator
    private fun migrateThresholds() {
        val sTmp1 = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade1, context.getString(R.string.BLOODPRESSURE_GRADE1_VALUES)).toString()
        val sTmp2 = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade2, context.getString(R.string.BLOODPRESSURE_GRADE2_VALUES)).toString()
        val sTmp3 = preferences.getString(SettingsActivity.KEY_PREF_hyperGrade3, context.getString(R.string.BLOODPRESSURE_GRADE3_VALUES)).toString()
        val sTmp4 = preferences.getString(SettingsActivity.KEY_PREF_HYPOTENSION, context.getString(R.string.BLOODPRESSURE_HYPOTENSION_VALUES)).toString()

        val editor = preferences.edit()
        editor.putString(SettingsActivity.KEY_PREF_hyperGrade1, sTmp1.replace(",",gradeSettingsDivider))
        editor.putString(SettingsActivity.KEY_PREF_hyperGrade2, sTmp2.replace(",",gradeSettingsDivider))
        editor.putString(SettingsActivity.KEY_PREF_hyperGrade3, sTmp3.replace(",",gradeSettingsDivider))
        editor.putString(SettingsActivity.KEY_PREF_HYPOTENSION, sTmp4.replace(",",gradeSettingsDivider))
        editor.apply()
    }
}

fun collectData(activity: Activity, arr: ArrayList<String>): ArrayList<String>  {
    if (viewModel.getSize(false) == 0) return arr

    // Measurements
    val totalMeasurements = viewModel.getSize(false)

    val now = Calendar.getInstance().time
    val aYearAgo = now.time - 31557600000

    val aMonthAgo = now.time - 2629800000
    var sql = "SELECT count(*) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
    val monthlyMeasurements = viewModel.getInt(sql)
    val montlyDataExist = (monthlyMeasurements > 0)

    sql = "SELECT count(*) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    val annualMeasurements = viewModel.getInt(sql)

    arr.add(activity.getString(R.string.measurementLabel) + "," +
            monthlyMeasurements.toString() + "," +
            annualMeasurements.toString() + "," +
            totalMeasurements.toString()
    )

    arr.add(" , , , ,") // Blank row

    // Systolic min
    var m = ""

    if (montlyDataExist) {
        sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
        m = viewModel.getInt(sql).toString()
    }
    sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    var y: String = viewModel.getInt(sql).toString()

    sql = "SELECT MIN(CAST(value1 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    var t: String = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.systolic) + " " + activity.getString(R.string.min) + ",$m,$y,$t")

    // Systolic max
    m = ""
    if (montlyDataExist) {
        sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
        m = viewModel.getInt(sql).toString()
    }
    sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT MAX(CAST(value1 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    t = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.systolic) + " " + activity.getString(R.string.max) + ",$m,$y,$t")

    // Systolic avg
    sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
    m = viewModel.getInt(sql).toString()

    sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT AVG(CAST(value1 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    t = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.systolic) + " " + activity.getString(R.string.avg) + ",$m,$y,$t")
    arr.add(" , , , ,") // Blank row

    // Diastolic min
    m = ""
    if (montlyDataExist) {
        sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
        m = viewModel.getInt(sql).toString()
    }
    sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT MIN(CAST(value2 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    t = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.diastolic) + " " + activity.getString(R.string.min) + ",$m,$y,$t")

    // Diastolic max
    m = ""
    if (montlyDataExist) {
        sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
        m = viewModel.getInt(sql).toString()
    }
    sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT MAX(CAST(value2 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    t = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.diastolic) + " " + activity.getString(R.string.max) + ",$m,$y,$t")

    // Diastolic avg
    sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
    m = viewModel.getInt(sql).toString()

    sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT AVG(CAST(value2 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    t = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.diastolic) + " " + activity.getString(R.string.avg) + ",$m,$y,$t")
    arr.add(" , , , ,") // Blank row

    // Pulse min
    m = ""
    if (montlyDataExist) {
        sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE} and value3 <> ''"
        m = viewModel.getInt(sql).toString()
    }
    sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE} and value3 <> ''"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT MIN(CAST(value3 as int)) FROM data WHERE type=${BLOODPRESSURE} and value3 <> ''"
    t = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.pulse) + " " + activity.getString(R.string.min) + ",$m,$y,$t")

    // Pulse max
    m = ""
    if (montlyDataExist) {
        sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
        m = viewModel.getInt(sql).toString()
    }
    sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT MAX(CAST(value3 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    t =viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.pulse) + " " + activity.getString(R.string.max) + ",$m,$y,$t")

    // Pulse avg
    sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE timestamp >= $aMonthAgo and type=${BLOODPRESSURE}"
    m = viewModel.getInt(sql).toString()

    sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE timestamp >= $aYearAgo and type=${BLOODPRESSURE}"
    y = viewModel.getInt(sql).toString()

    sql = "SELECT AVG(CAST(value3 as int)) FROM data WHERE type=${BLOODPRESSURE}"
    t = viewModel.getInt(sql).toString()

    arr.add(activity.getString(R.string.pulse) + " " + activity.getString(R.string.avg) + ",$m,$y,$t")

    return arr
}