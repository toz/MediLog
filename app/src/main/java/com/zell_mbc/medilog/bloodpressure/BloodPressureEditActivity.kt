/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Tabs.BLOODPRESSURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.EditActivity
import com.zell_mbc.medilog.dialogs.MultipleMeasurementsDialog
import com.zell_mbc.medilog.preferences.SettingsActivity

class BloodPressureEditActivity: EditActivity() {
    override val dataType = BLOODPRESSURE

    private var heartRhythmIssue = false
    private var logHeartRhythm = false

    private var colorBad = androidx.compose.ui.graphics.Color.Black
    private var colorGood = androidx.compose.ui.graphics.Color.Black

    lateinit var multipleMeasurementsDialog: MultipleMeasurementsDialog
    var openMultipleMeasurementsDialog = mutableStateOf(false)

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this)[BloodPressureViewModel::class.java]
        super.onCreate(savedInstanceState)

        logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, true)

        if (logHeartRhythm) {
            //standardColors =  binding.etSys.currentHintTextColor //  textColors.defaultColor

            // Invert result because setHeartRhythm inverst again
//            binding.btHeartRhythm.setText(warning) // = (editItem.value4 != "1")
            heartRhythmIssue = (editItem.value4 != "1")
        }
        // All preparation done, start Compose
        setContent { StartCompose() }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(start = 8.dp, end = 8.dp)) {
            DateTimeBlock()

            colorBad = MaterialTheme.colorScheme.error
            colorGood = MaterialTheme.colorScheme.primary
            var iconColor by remember { mutableStateOf(setIcon()) }

            val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = androidx.compose.ui.graphics.Color.Transparent, focusedContainerColor = androidx.compose.ui.graphics.Color.Transparent)
            val focusManager = LocalFocusManager.current
            val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }

            Row(Modifier.fillMaxWidth()) {
                // Weight
                TextField(
                    value = value1String,
                    colors = textFieldColors,
                    onValueChange = {
                        value1String = it
                        if (((it.length == 4) && (!it.endsWith(decimalSeparator))) || (it.length >= 5))
                            focusManager.moveFocus(FocusDirection.Next)
                    },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                    singleLine = true,
                    textStyle = TextStyle(),
                    label = { Text(text = stringResource(id = R.string.systolic) + "*", maxLines = 1) },
                    placeholder = { Text(text = value1Hint) },
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 20.dp)
                        .focusRequester(textField1),
                )

                // Diastolic
                TextField(
                    value = value2String,
                    onValueChange = {
                        value2String = it
                        if (it.length > 3) focusManager.moveFocus(FocusDirection.Next) //textField3.requestFocus()
                    },
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 20.dp)
                        .focusRequester(textField2),
                    colors = textFieldColors,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                    singleLine = true,
                    textStyle = TextStyle(),
                    label = { Text(text = stringResource(id = R.string.diastolic) + "*", maxLines = 1) },
                    )
                // Pulse
                TextField(
                    value = value3String,
                    onValueChange = {
                        value3String = it
                        if (it.length > 3) focusManager.moveFocus(FocusDirection.Next) //textField3.requestFocus()
                    },
                    modifier = Modifier
                        .weight(1f)
                        .focusRequester(textField2)
                        .padding(end = 20.dp)
                        .focusRequester(textField3),
                    colors = textFieldColors,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                    singleLine = true,
                    textStyle = TextStyle(),
                    label = { Text(text = stringResource(id = R.string.pulse)) },
                )

                if (logHeartRhythm) {
                    Image(painter = painterResource(R.drawable.ic_baseline_warning_24), contentDescription = "Heart Rhythm State",
                        modifier = Modifier
                            .weight(0.5f)
                            .padding(start = 16.dp)
                            .align(Alignment.CenterVertically)
                            .clickable { iconColor = setIcon() },
                        colorFilter = ColorFilter.tint(iconColor),
                    )
                }
            }
            Row {
                // Comment
                TextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .focusRequester(textField3),
                    value = commentString,
                    colors = textFieldColors,
                    onValueChange = { commentString = it
                        //  commentString = it
                    },
                    singleLine = false,
                    label = { Text(text = stringResource(id = R.string.comment)) },
                    trailingIcon = {
                        IconButton(onClick = {
                            showTextTemplatesDialog = true
                        })
                        { Icon(Icons.Outlined.Abc, contentDescription = null) }
                    },
                )
            }
            Text("")

            Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                if (editItem.attachment.isNotEmpty()) AttachmentBlock()
            }
        }

        // Dialogs
        if (showTextTemplatesDialog)
            //TextTemplatesDialog(activity, setShowDialog = { showTextTemplatesDialog = it }) { commentString += it } // TextField Content
            textTemplateDialog.ShowDialog(setShowDialog = { showTextTemplatesDialog = it }) { commentString += it }

        if (showDatePickerDialog) OpenDatePickerDialog()
        if (showTimePickerDialog) OpenTimePickerDialog()
        if (openMultipleMeasurementsDialog.value) multipleMeasurementsDialog.ShowDialog(setShowDialog = {
            openMultipleMeasurementsDialog.value = it
            finish() // Close current window / activity
        })
    }

    //Tab specific checks
    override fun saveItem() {
        hideKeyboard()
        // Safely convert string values
        if (stringToInt(value1String, getString(R.string.sysMissing)) < 0) return
        if (stringToInt(value2String, getString(R.string.diaMissing)) < 0) return
        if (value3String.isNotEmpty()) // Pulse can e blank, check if it has got a value
            if (stringToInt(value3String, getString(R.string.pulseMissing)) < 0) return

        value4String = if (heartRhythmIssue) "1" else "0"

        super.saveItem()

        val multipleMeasurementsTimeWindow = "" + preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW, getString(R.string.BLOODPRESSURE_MULTI_MEASUREMENTS_WINDOW_DEFAULT))
        if (multipleMeasurementsTimeWindow.toInt() > 0) {
            multipleMeasurementsDialog = MultipleMeasurementsDialog(this, MainActivity.Companion.viewModel as BloodPressureViewModel)
            openMultipleMeasurementsDialog.value = true
        }
        else finish() // Close current window / activity

    }

    private fun setIcon(): androidx.compose.ui.graphics.Color {
        heartRhythmIssue = !heartRhythmIssue
        return if (heartRhythmIssue) colorBad else colorGood
    }
}
