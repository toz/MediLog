/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.bloodpressure

import android.app.Activity
import android.graphics.Color
import android.graphics.pdf.PdfDocument
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Tabs.BLOODPRESSURE
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.util.Calendar

class BloodPressurePdf(activity: Activity): BasePdf(activity) {
    // Base class overrides
    override var itemName = activity.getString(R.string.bloodPressure)

    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_BLENDINITEMS, activity.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())
    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_LANDSCAPE, activity.getString(R.string.BLOODPRESSURE_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_PAPER_SIZE, activity.getString(R.string.BLOODPRESSURE_PAPER_SIZE_DEFAULT))
    override var printDaySeparatorLines = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_PDF_DAYSEPARATOR, activity.getString(R.string.DAY_SEPARATOR_LINE_DEFAULT).toBoolean())
    override var highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_HIGHLIGHT_VALUES, false)

    lateinit var viewModel: DataViewModel
    //var filterData = ArrayList<String>(0)
    //var totalData = ArrayList<String>(0)

    // BloodPressure specific
    private val warningSign = activity.getString(R.string.warningSign)

    private var section1 = 0f
    private var section2 = 0f
    private var section3 = 0f
    private var warningSignWidth = 0f
    private var timeTab = 0f
    private var sysTab = 0f
    private var diaTab = 0f
    private var pulseTab = 0f

    private val MORNING = 0
    private val AFTERNOON = 1
    private val EVENING = 2

    // Stats page values
    val placeholder = 999
    private var measurementsTotal = 0
    private var measurementsYear = 0
    private var measurementsMonth = 0

    // Total
    var minSysTotal = placeholder
    var maxSysTotal = 0
    var minDiaTotal = placeholder
    var maxDiaTotal = 0
    var minPulseTotal = placeholder
    var maxPulseTotal = 0

    // Year
    var minSysYear = placeholder
    var maxSysYear = 0
    var minDiaYear = placeholder
    var maxDiaYear = 0
    var minPulseYear = placeholder
    var maxPulseYear = 0

    // Month
    var minSysMonth = placeholder
    var maxSysMonth = 0
    var minDiaMonth = placeholder
    var maxDiaMonth = 0
    var minPulseMonth = placeholder
    var maxPulseMonth = 0

    // Report timeframes
    // Morning, Afternoon, Evening
    // t0 - t1, t1 - t2,   t2 - t0
    private var t0 = 0
    private var t1 = 12
    private var t2 = 18


    val logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, true)


    override fun createPdfDocument(): PdfDocument? {
        // Check here to be able to return right away in case of error
        if (MainActivity.viewModel.getSize(true) == 0) {
            Toast.makeText(activity, activity.getString(R.string.bloodPressure) + ": " + activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        super.createPdfDocument()
        commentTab = 60f

        val bpHelper = BloodPressureHelper(activity)

        viewModel = MainActivity.viewModel


        var currentDay = ""
        var lastPeriod = -1
        var commentPrinted = false

        if (logHeartRhythm) {  // Need more space if warningSign is shown
            section2 += warningSignWidth
            section3 += (warningSignWidth * 2)
            commentTab += (warningSignWidth * 3)
            timeTab += warningSignWidth
            sysTab += warningSignWidth
            diaTab += warningSignWidth
            pulseTab += warningSignWidth
        } // Need more space if warningSign is shown


        setTimezones()
        setColumns()

        initDataPage()
        var f = pdfDataTop + pdfLineSpacing
        commentWidth = measureColumn(pdfRightBorder - commentTab)

        val aMonthAgo = Calendar.getInstance()
        aMonthAgo.add(Calendar.DAY_OF_MONTH, -30)
        val aYearAgo = Calendar.getInstance()
        aYearAgo.add(Calendar.DAY_OF_MONTH, -365)
        //val aMonthAgo = now.time - 2629800000

        // Make sure they are reset
        measurementsTotal = 0
        measurementsYear = 0
        measurementsMonth = 0

        //currentDay = toStringDate(pdfItems[0].timestamp)

        for (item in pdfItems) {
            val dayPeriod = dayPeriod(item.timestamp)
            var activeSection = section1
            if (dayPeriod == AFTERNOON) activeSection = section2
            if (dayPeriod == EVENING) {
                // Special case where an early morning measurement should be treated like the previous day
                val cal = Calendar.getInstance()
                cal.timeInMillis = item.timestamp
                if (t0 > 0 && cal[Calendar.HOUR_OF_DAY] < t0) {
                    cal.add(Calendar.DAY_OF_YEAR, -1)
                    item.timestamp = cal.timeInMillis
                }
                activeSection = section3
            }

            // New line if a different day or (a second measurement in the same period) or (if a comment has been printed that day and a second one comes round)
            val newDay = (currentDay != toStringDate(item.timestamp))
            if (newDay or (lastPeriod == dayPeriod) or (commentPrinted and item.comment.isNotEmpty())) {
                f += pdfLineSpacing
                currentDay = toStringDate(item.timestamp)
            }
            lastPeriod = dayPeriod
            commentPrinted = item.comment.isNotEmpty()

            if (printDaySeparatorLines && newDay) {
                f -= (pdfLineSpacing - pdfDaySeparatorLineSpacer) // Go back one line and add half a daySeparatorLineHeight
                canvas.drawLine(pdfLeftBorder, f, pdfRightBorder, f, pdfPaintDaySeparator)
                f += (pdfLineSpacing - pdfDaySeparatorLineSpacer) + pdfPaintDaySeparator.strokeWidth
            }

            // Start new page
            if (f > pdfDataBottom) {

                document.finishPage(page)
                createPage()
                drawHeader()
                f = pdfDataTop + (pdfLineSpacing * 2)
            }

            // Date
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder, f, pdfPaint)

            // If this is a blended item, draw comment and be done
            if (item.type != BLOODPRESSURE) {
                f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
                continue
            }

            // HeartRhythm
            if (logHeartRhythm) {
                pdfPaint.color = if (item.value4 == "1") Color.BLACK else Color.WHITE
                canvas.drawText(warningSign, activeSection, f, pdfPaint)
                pdfPaint.color = Color.BLACK
            }

            // Time
            canvas.drawText(toStringTime(item.timestamp), activeSection + timeTab, f, pdfPaintSmall)

            if (highlightValues) {
                when(bpHelper.sysGrade(item.value1)) {
                    bpHelper.hyperGrade3,
                    bpHelper.hyperGrade2 -> {
                        pdfPaint.isFakeBoldText = true
                        pdfPaint.isUnderlineText = true
                    }
                    bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                    bpHelper.hyperGrade4 -> pdfPaint.isFakeBoldText = true
                    else                 -> pdfPaint.isFakeBoldText = false
                }
                canvas.drawText(item.value1, activeSection + sysTab, f, pdfPaint)
                pdfPaint.isFakeBoldText = false
                pdfPaint.isUnderlineText = false

                when(bpHelper.diaGrade(item.value2)) {
                    bpHelper.hyperGrade3,
                    bpHelper.hyperGrade2 -> {
                        pdfPaint.isFakeBoldText = true
                        pdfPaint.isUnderlineText = true
                    }
                    bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                    bpHelper.hyperGrade4 -> pdfPaint.isFakeBoldText = true
                    else                 -> pdfPaint.isFakeBoldText = false
                }
                canvas.drawText(item.value2, activeSection + diaTab, f, pdfPaint)
                pdfPaint.isFakeBoldText = false
                pdfPaint.isUnderlineText = false
            }
            else {  // Don't highlight values
                canvas.drawText(item.value1, activeSection + sysTab, f, pdfPaint)
                canvas.drawText(item.value2, activeSection + diaTab, f, pdfPaint)
            }
            canvas.drawText(item.value3, activeSection+ pulseTab, f, pdfPaint)
            if (item.comment.isNotBlank()) f = multipleLines(item.comment, f)
            //f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
            //canvas.drawText(item.comment, commentTab, f, pdfPaint)

            // Keep min/max values for stats page
            try {
                measurementsTotal += 1

                val sys = item.value1.toInt()
                val dia = item.value2.toInt()
                val pulse = item.value3.toInt()

                // Total sys values
                if (sys < minSysTotal) minSysTotal = sys
                if (sys > maxSysTotal) maxSysTotal = sys

                // Total dia values
                if (dia < minDiaTotal) minDiaTotal = dia
                if (dia > maxDiaTotal) maxDiaTotal = dia

                // Total pulse values
                if (pulse < minPulseTotal) minPulseTotal = pulse
                if (pulse > maxPulseTotal) maxPulseTotal = pulse

                // Annual values
                if (item.timestamp >= aYearAgo.timeInMillis) {
                    measurementsYear += 1

                    if (sys < minSysYear) minSysYear = sys
                    if (sys > maxSysYear) maxSysYear = sys

                    if (dia < minDiaYear) minDiaYear = dia
                    if (dia > maxDiaYear) maxDiaYear = dia

                    if (pulse < minPulseYear) minPulseYear = pulse
                    if (pulse > maxPulseYear) maxPulseYear = pulse

                    // Monthly values
                    if (item.timestamp >= aMonthAgo.timeInMillis) {
                        measurementsMonth += 1

                        if (sys < minSysMonth) minSysMonth = sys
                        if (sys > maxSysMonth) maxSysMonth = sys

                        if (dia < minDiaMonth) minDiaMonth = dia
                        if (dia > maxDiaMonth) maxDiaMonth = dia

                        if (pulse < minPulseMonth) minPulseMonth = pulse
                        if (pulse > maxPulseMonth) maxPulseMonth = pulse
                    }
                }
            } catch (e: NumberFormatException) { /* Do nothing */ }
        }
        // finish the page
        document.finishPage(page)

        // Add statistics page
        createPage()
        drawStatsPage()
        document.finishPage(page)

        /* Add chart page
        createPage()
        addChartPage()
        document.finishPage(page) */

        return document
    }

    override fun setColumns() {
        val sysDiaWidth = pdfPaintHighlight.measureText(activity.getString(R.string.sysShort))

        if (logHeartRhythm) {
            warningSignWidth = pdfPaintHighlight.measureText(activity.getString(R.string.warningSign))
            timeTab = warningSignWidth + space
        }
        else timeTab = 0f

        sysTab = timeTab + timeWidth + space
        diaTab = sysTab + sysDiaWidth + space
        pulseTab = diaTab + sysDiaWidth + space

        val subHeaderWidth = pulseTab + pdfPaintHighlight.measureText(activity.getString(R.string.pulse))

        section1 = pdfLeftBorder + dateWidth + padding
        section2 = section1 + subHeaderWidth + padding
        section3 = section2 + subHeaderWidth + padding
        commentTab = section3 + subHeaderWidth + padding
    }

    override fun initDataPage() {
        drawHeader() // Draw logo, date etc.

        // Data section
        canvas.drawText(activity.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.morning), section1, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.afternoon), section2, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.evening), section3, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.comment), commentTab, pdfDataTop, pdfPaintHighlight)

        val f = (pdfDataTop + pdfLineSpacing)
//        pdfPaint.textSkewX = -0.25f

        if (logHeartRhythm) canvas.drawText(activity.getString(R.string.warningSign), section1, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.time), section1 + timeTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.sysShort), section1 + sysTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.diaShort), section1 + diaTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.pulse), section1 + pulseTab, f, pdfPaint)

        if (logHeartRhythm) canvas.drawText(activity.getString(R.string.warningSign), section2, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.time), section2 + timeTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.sysShort), section2 + sysTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.diaShort), section2 + diaTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.pulse), section2 + pulseTab, f, pdfPaint)

        if (logHeartRhythm) canvas.drawText(activity.getString(R.string.warningSign), section3, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.time), section3 + timeTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.sysShort), section3 + sysTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.diaShort), section3 + diaTab, f, pdfPaint)
        canvas.drawText(activity.getString(R.string.pulse), section3 + pulseTab, f, pdfPaint)

        // ------------
        // Daytime separation lines
        val space = (5).toFloat()

        canvas.drawLine(section1 - space, pdfHeaderBottom, section1 - space, pdfDataBottom, pdfPaintBackground)
        canvas.drawLine(section2 - space, pdfHeaderBottom, section2 - space, pdfDataBottom, pdfPaintBackground)
        canvas.drawLine(section3 - space, pdfHeaderBottom, section3 - space, pdfDataBottom, pdfPaintBackground)
        //pdfPaint.color = Color.BLACK
        canvas.drawLine(commentTab - space, pdfHeaderBottom, commentTab - space, pdfDataBottom, pdfPaintBackground)
    }

    fun drawStatsPage() {
        super.drawStatsPage(R.drawable.ic_blood_pressure_filled)

        val timeframeLabel = activity.getString(R.string.timeframeLabel)
        val totalLabel = activity.getString(R.string.totalLabel)
        val yearLabel = activity.getString(R.string.annualLabel)
        val monthLabel = activity.getString(R.string.monthLabel)

        val space = 25
        val monthColumn =  pdfPaintHighlight.measureText(timeframeLabel) + space
        val yearColumn = monthColumn + pdfPaintHighlight.measureText(monthLabel) + space
        val totalColumn =  yearColumn + pdfPaintHighlight.measureText(yearLabel) + space

        var f = pdfDataTop + pdfLineSpacing
        canvas.drawText(activity.getString(R.string.statistics) + ":", pdfLeftBorder, f, pdfPaintHighlight)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(monthLabel, monthColumn, f, pdfPaint)
        canvas.drawText(yearLabel, yearColumn, f, pdfPaint)
        canvas.drawText(totalLabel, totalColumn, f, pdfPaint)

        var arr = collectData(activity, totalData)
        arr.forEach() {
            val row = it.split(",")
            f += pdfLineSpacing
            canvas.drawText(row[0], pdfLeftBorder, f, pdfPaint)
            canvas.drawText(row[1], monthColumn, f, pdfPaint)
            canvas.drawText(row[2], yearColumn, f, pdfPaint)
            canvas.drawText(row[3], totalColumn, f, pdfPaint)
        }

        f += pdfLineSpacing
    }

    private fun dayPeriod(timestamp: Long): Int {
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp
        val hour = cal[Calendar.HOUR_OF_DAY]

        return when (hour) {
            in t0 until t1 -> MORNING
            in t1 until t2 -> AFTERNOON
            else -> EVENING
        }
    }

    // Read timezones for Morning/Midday/Evening sections in PDF report
    private fun setTimezones() {
        //       preferences.edit().remove(SettingsActivity.KEY_PREF_BLOODPRESSURE_TIMEZONES).activityly()
        val timezones = "" + preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_TIMEZONES, activity.getString(R.string.BLOODPRESSURE_TIMEZONE_DEFAULT))
        val ta = timezones.split("-")
        // Morning, Afternoon, Evening
        // t0 - t1, t1 - t2,   t2 - t0
        try {
            t0 = ta[0].toInt()
            t1 = ta[1].toInt()
            t2 = ta[2].toInt()

            // Is each number larger than the previous one?
            if ( t0 > t1 || t1 > t2) "x".toInt() // force exception to get desired error handling
        } catch (e: NumberFormatException) {
            Toast.makeText(activity, timezones + " " + activity.getString(R.string.invalidTimezoneSetting), Toast.LENGTH_LONG).show()
            t0 = 0
            t1 = 12
            t2 = 18
        }
    }
}