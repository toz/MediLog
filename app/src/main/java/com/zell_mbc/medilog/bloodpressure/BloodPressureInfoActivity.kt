/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.InfoActivity

class BloodPressureInfoActivity: InfoActivity() {
    val column1Weight = .4f
    val column2Weight = .2f
    val column3Weight = .2f
    val column4Weight = .2f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this)[BloodPressureViewModel::class.java]

        //if ((viewModel.filterStart + viewModel.filterStart) > 0L) gatherData(true)
        totalData = collectData(this, totalData)
        setContent { StartCompose() }
    }

    @Composable
    fun RowScope.TableCell(text: String, weight: Float, color: Color = MaterialTheme.colorScheme.primary) {
        Text(text = text, Modifier.weight(weight).padding(cellPadding.dp), color = color)
    }

    @Composable
    fun ShowRow(s: String) {
        val row = s.split(",")
        Row(Modifier.fillMaxWidth()) {
            TableCell(text = row[0], weight = column1Weight)
            TableCell(text = row[1], weight = column2Weight)
            TableCell(text = row[2], weight = column3Weight)
            TableCell(text = row[3], weight = column4Weight)
        }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(8.dp)) {
            HeaderBlock(R.drawable.ic_bloodpressure)
            // Each cell of a column must have the same weight.
            val column1Weight = .4f
            val column2Weight = .2f
            val column3Weight = .2f
            val column4Weight = .2f

            Row(Modifier.background(MaterialTheme.colorScheme.secondaryContainer)) {
                TableCell(text = "", weight = column1Weight)
                TableCell(text = getString(R.string.monthLabel), weight = column2Weight)
                TableCell(text = getString(R.string.annualLabel), weight = column3Weight)
                TableCell(text = getString(R.string.totalLabel), weight = column4Weight)
            }
            LazyColumn(modifier = Modifier.fillMaxSize(1F)) {
                items(totalData) { item ->
                    ShowRow(item)
                }
            }
        }
    }
}