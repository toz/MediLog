/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import android.widget.Toast
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModelProvider
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottom
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStart
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLine
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.cartesian.rememberVicoZoomState
import com.patrykandpatrick.vico.core.cartesian.Zoom
import com.patrykandpatrick.vico.core.cartesian.axis.HorizontalAxis
import com.patrykandpatrick.vico.core.cartesian.axis.VerticalAxis
import com.patrykandpatrick.vico.core.cartesian.data.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.cartesian.data.lineSeries
import com.patrykandpatrick.vico.core.cartesian.decoration.HorizontalLine
import com.patrykandpatrick.vico.core.cartesian.layer.LineCartesianLayer
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.ChartActivity
import com.zell_mbc.medilog.base.rememberMarker
import com.zell_mbc.medilog.preferences.SettingsActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.abs
import kotlin.math.min

class BloodPressureChartActivity: ChartActivity() {
    override val filename = "BloodPressureChart.jpg"

    // Tab specific data lists
    private var sys = mutableListOf<Int>()
    private var dia = mutableListOf<Int>()
    var pulse = mutableListOf<Int>()

    lateinit var sysLinearTrendlineData: Map<Long, Float>
    lateinit var diaLinearTrendlineData: Map<Long, Float>
    var sysLinearTrendline = mutableListOf<Float>()
    var diaLinearTrendline = mutableListOf<Float>()

    // Tab specific settings
    var showPulse = false
    private var showRibbonChart = false

    private var SYS = 0
    private var DIA = 1
    private var PULSE = 2

    private var sysLine = 0
    private var sysLabel = ""
    private var diaLine = 0
    private var diaLabel = ""

    private var sysMax = 0
    private var diaMin = 999
    var pulseMin = 999
    private var minY = 999

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showLinearTrendline = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_BLOODPRESSURE_LINEAR_TRENDLINE, false)
        showPulse = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOWPULSE, false)
        showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureGrid, true)
        showLegend = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureLegend, false)
        showMovingAverage = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_SHOW_MOVING_AVERAGE, false)
        showThreshold = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureThreshold, false)
        showRibbonChart = preferences.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_RIBBON_CHART, false)

        legendText = mutableListOf(getString(R.string.systolic), getString(R.string.diastolic))
        chartColors = mutableListOf(lineColor1, lineColor2)

        if (showPulse) {
            legendText.add(getString(R.string.pulse))
            chartColors.add(lineColor3)
        }
        if (showLinearTrendline) {
            legendText.add(getString(R.string.systolic) + " " + getString(R.string.trendline))
            chartColors.add(linearTrendlineColor)
            legendText.add(getString(R.string.diastolic) + " " + getString(R.string.trendline))
            chartColors.add(linearTrendlineColor)
        }

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[BloodPressureViewModel::class.java]

        val bpHelper = BloodPressureHelper(this)

        // For the charts only native entries can be considered, no matter what showAllTabs is set to
        val tmp = viewModel.blendInItems
        viewModel.blendInItems = false
        items = viewModel.getItems("ASC", filtered = true)
        viewModel.blendInItems = tmp

        if (items.size < 2) {
            Toast.makeText(this, getString(R.string.notEnoughDataForChart), Toast.LENGTH_LONG).show()
            finish()
            return // Needs return to avoid the rest to be executed because finish() may not kill the activity fast enough
        }

        var iTmp: Int
        var sysMin = 999
        var diaMax = 0
        var i = 0

        for (item in items) {
            timestamps.add(i++)
            iTmp = try { item.value1.toInt() } catch (e: NumberFormatException) { 0 }
            sys.add(iTmp)
            if (iTmp > sysMax) sysMax = iTmp
            if (iTmp < sysMin) sysMin = iTmp

            iTmp = try { item.value2.toInt() } catch (e: NumberFormatException) { 0 }
            dia.add(iTmp)
            if (iTmp > diaMax) diaMax = iTmp
            if (iTmp < diaMin) diaMin = iTmp

            if (showPulse) {
                iTmp = try { item.value3.toInt() } catch (e: NumberFormatException) { 0 }
                if (iTmp < pulseMin) pulseMin = iTmp
                pulse.add(iTmp)
            }
        }

        // Compile linear trendline
        if (showLinearTrendline) {
            sysLinearTrendline = getLinearTrendlineInt(sys)
            diaLinearTrendline = getLinearTrendlineInt(dia)
        }

        xAxisOffset = items[0].timestamp

        // Fill data maps
        if (showMovingAverage) {
            val p = preferences.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE, getString(R.string.BLOODPRESSURE_MOVING_AVERAGE_SIZE_DEFAULT))
            if (p != null) {
                val period = try { p.toInt() } catch (e: NumberFormatException) { 0 }
                sys = getMovingAverageInt(sys, period)
                var i = 0
                for (item in items) {item.value1 = sys[i++].toString() }
                value1DataI = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value1.toInt() } catch (e: NumberFormatException) { 0 } })

                dia = getMovingAverageInt(dia, period)
                i = 0
                for (item in items) {item.value1 = dia[i++].toString() }
                value2DataI = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value2.toInt() } catch (e: NumberFormatException) { 0 } })
            }
        }
        else {
            value1DataI = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value1.toInt() } catch (e: NumberFormatException) { 0 } })
            value2DataI = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value2.toInt() } catch (e: NumberFormatException) { 0 } })

        }

        if (showPulse) value3DataI = items.associateBy({ scaledTimeStamp(it.timestamp)   }, { try { it.value3.toInt() } catch (e: NumberFormatException) { 0 } })
        if (showLinearTrendline) {
            var ii = 0
            sysLinearTrendlineData = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { sysLinearTrendline[ii++] } catch (e: NumberFormatException) { 0F } })
            ii = 0
            diaLinearTrendlineData = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { diaLinearTrendline[ii++] } catch (e: NumberFormatException) { 0F } })
        }

        // Show the grade line next to max sys value
        val sysGrades = listOf(
            abs(bpHelper.hyperGrade1Sys - sysMax),
            abs(bpHelper.hyperGrade2Sys - sysMax),
            abs(bpHelper.hyperGrade3Sys - sysMax)
        )

        sysLine = 0
        sysLabel = getString(R.string.systolic) + ", " +  getString(R.string.grade)
        when (sysGrades.min())    {
            sysGrades[0] -> {
                sysLine = bpHelper.hyperGrade1Sys
                sysLabel += " 1"
            }
            sysGrades[1] -> {
                sysLine = bpHelper.hyperGrade1Sys
                sysLabel += " 2"
            }
            else         -> {
                sysLine = bpHelper.hyperGrade1Sys
                sysLabel += " 3"
            }
        }

        val diaGrades = listOf(
            abs(bpHelper.hyperGrade1Sys - diaMax),
            abs(bpHelper.hyperGrade2Sys - diaMax),
            abs(bpHelper.hyperGrade3Sys - diaMax)
        )

        diaLine = 0
        diaLabel = getString(R.string.diastolic) + ", " +  getString(R.string.grade)
        when (diaGrades.min())    {
            diaGrades[0] -> {
                diaLine = bpHelper.hyperGrade1Dia
                diaLabel += " 1"
            }
            diaGrades[1] -> {
                diaLine = bpHelper.hyperGrade2Dia
                diaLabel += " 2"
            }
            else         -> {
                diaLine = bpHelper.hyperGrade3Dia
                diaLabel += " 3"
            }
        }
        minY = if (showPulse) min(diaMin, pulseMin) else diaMin
        showContent()
    }

    @Composable
    override fun ShowContent() {
        Box(Modifier.safeDrawingPadding()) {
            val decorationList = if (showThreshold) listOf(
                helperLine(sysLabel, sysLine.toDouble(), chartColors[SYS]),
                helperLine(diaLabel, diaLine.toDouble(), chartColors[DIA])
            ) else listOf()

            val modelProducer = remember { CartesianChartModelProducer() }
            LaunchedEffect(Unit) {
                withContext(Dispatchers.Default) {
                    modelProducer.runTransaction {
                        lineSeries {
                            series(value1DataI.keys, value1DataI.values)
                            series(value2DataI.keys, value2DataI.values)
                            if (showPulse) series(value3DataI.keys, value3DataI.values)
                            if (showLinearTrendline) {
                                series(sysLinearTrendlineData.keys, sysLinearTrendlineData.values)
                                series(diaLinearTrendlineData.keys, diaLinearTrendlineData.values)
                            }
                        }
                    }
                }
            }
            ComposeChart(modelProducer, Modifier.fillMaxHeight(), decorationList)
        }
    }

    @Composable
    private fun ComposeChart(
        modelProducer: CartesianChartModelProducer,
        modifier: Modifier, decorationList: List<HorizontalLine>) {

        val marker = rememberMarker()
        val backgroundFill = if (isSystemInDarkTheme()) Color(0xff000000) else Color(0xffffffff)

        val lineProvider = LineCartesianLayer.LineProvider.series(
            buildList {
                add(LineCartesianLayer.rememberLine(LineCartesianLayer.LineFill.single(fill = com.patrykandpatrick.vico.compose.common.fill(chartColors[SYS]))))
                if (showRibbonChart) add(
                    LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(chartColors[DIA])), areaFill = LineCartesianLayer.AreaFill.single(
                        com.patrykandpatrick.vico.compose.common.fill(
                            backgroundFill
                        )
                    )) )
                else                 add(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(chartColors[DIA]))))
                if (showPulse) add(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(chartColors[PULSE])), areaFill = null))
                if (showLinearTrendline) {
                    add(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(linearTrendlineColor)), areaFill = null))
                    add(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(com.patrykandpatrick.vico.compose.common.fill(linearTrendlineColor)), areaFill = null))
                }
            }
        )

        CartesianChartHost(
            chart = rememberCartesianChart(
                rememberLineCartesianLayer(lineProvider = lineProvider),
                startAxis = VerticalAxis.rememberStart(
                    guideline = guideline(),
                ),
                bottomAxis = HorizontalAxis.rememberBottom(
                    valueFormatter = bottomAxisValueFormatter, // Convert values back to proper dates
                    guideline = guideline()
                ),
                legend = if (showLegend) rememberLegend() else null,
                decorations = decorationList,
                marker = marker,
            ),
            modelProducer = modelProducer,
            modifier = modifier,
            zoomState = rememberVicoZoomState(zoomEnabled = true, maxZoom = Zoom.max(Zoom.static(100f), Zoom.Content), initialZoom = Zoom.min(Zoom.static(), Zoom.Content))
        )
    }
}