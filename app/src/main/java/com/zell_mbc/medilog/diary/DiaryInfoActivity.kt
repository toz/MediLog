/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.diary

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.base.InfoActivity
import java.util.Calendar

class DiaryInfoActivity: InfoActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[DiaryViewModel::class.java]

        if ((viewModel.filterStart + viewModel.filterStart) > 0L) gatherData(true)
        gatherData(false)

        setContent { StartCompose() }
    }

    @Composable
    fun RowScope.TableCell(text: String, weight: Float) {
        Text(text = text, Modifier.weight(weight).padding(cellPadding.dp), style = MaterialTheme.typography.bodySmall, color = MaterialTheme.colorScheme.primary)
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun ShowTable(arr: ArrayList<String>) {
        Text(measurementsIn, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
        Text(timeperiodIn, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)

        // Each cell of a column must have the same weight.
        val column1Weight = .4f
        val column2Weight = .2f
        val column3Weight = .2f
        val column4Weight = .2f

        LazyColumn(Modifier.fillMaxWidth().padding(16.dp)) {
            stickyHeader {
                Row(Modifier.background(MaterialTheme.colorScheme.primaryContainer)) {
                    TableCell(text = "", weight = column1Weight)
                    TableCell(text = getString(R.string.monthLabel), weight = column2Weight)
                    TableCell(text = getString(R.string.annualLabel), weight = column3Weight)
                    TableCell(text = getString(R.string.totalLabel), weight = column4Weight)
                }
            }

            items(arr) {
                val row = it.split(",")
                Row(Modifier.fillMaxWidth()) {
                    TableCell(text = row[0], weight = column1Weight)
                    TableCell(text = row[1], weight = column2Weight)
                    TableCell(text = row[2], weight = column3Weight)
                    TableCell(text = row[3], weight = column4Weight)
                }
            }
        }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(8.dp)) {
            HeaderBlock(R.drawable.ic_diary_outlined)
            if ((viewModel.filterStart + viewModel.filterStart) > 0L) {
                measurementsIn = measurementsInFilter
                timeperiodIn = timeperiodInFilter
                ShowTable(filterData)
                Text("")
                HorizontalDivider(color = MaterialTheme.colorScheme.primaryContainer, thickness = 1.dp)
                Text("")
            }
            measurementsIn = measurementsInDB
            timeperiodIn = timeperiodInDB
            ShowTable(totalData)
            ShowAttachments()
            }
    }

    private fun gatherData(filtered: Boolean) {
        val arr = if (filtered) filterData else totalData

        count = viewModel.getSize(filtered)

        var item: Data? = viewModel.getFirst(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val startDate = dateFormat.format(item.timestamp)

        item = viewModel.getLast(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val endDate = dateFormat.format(item.timestamp)

        if (filtered) {
            timeperiodInFilter = getString(R.string.timePeriod) + " $startDate - $endDate"
            measurementsInFilter = getString(R.string.measurementsInFilter) + " $count"
        }
        else {
            timeperiodInDB = getString(R.string.timePeriod) + " $startDate - $endDate"
            measurementsInDB = getString(R.string.measurementsInDB) + " $count"
        }

/*        val dayInMilliseconds: Long = 1000 * 60 * 60 * 24
        var totalDays = 0L
        val lastDay = item.timestamp
        val tmpItem = viewModel.getFirst()
        if (tmpItem != null) {
            val firstDay = tmpItem.timestamp
            val timeFrame = lastDay - firstDay
            totalDays = timeFrame / dayInMilliseconds + 1 // From ms to days
        }
*/
        // Measurements
        val totalMeasurements = viewModel.getSize(filtered)

        val now = Calendar.getInstance().time
        val aYearAgo = now.time - 31557600000

        val aMonthAgo = now.time - 2629800000
        var sql = "SELECT count(*) FROM data WHERE timestamp >= $aMonthAgo and type=${viewModel.dataType}"
        val monthlyMeasurements = viewModel.getInt(sql)

        sql = "SELECT count(*) FROM data WHERE timestamp >= $aYearAgo and type=${viewModel.dataType}"
        val yearlyMeasurements = viewModel.getInt(sql)

        arr.add(
            getString(R.string.measurementLabel) + "," +
            monthlyMeasurements.toString() + "," +
            yearlyMeasurements.toString() + "," +
            totalMeasurements.toString()
        )

        // Good
        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND timestamp >= $aMonthAgo AND cast(value2 as float) = 0"
        if (filtered) sql += viewModel.compileFilter()
        var ret = viewModel.getInt(sql)
        var m = ret.toString()

        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND timestamp >= $aYearAgo AND cast(value2 as float) = 0"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        var y = ret.toString()

        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND cast(value2 as float) = 0"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        var t = ret.toString()

        arr.add(getString(R.string.good) + "," + m  + "," + y + "," + t)

        // Not good
        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND timestamp >= $aMonthAgo AND cast(value2 as float) > 0 AND cast(value2 as float) < 2"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        m = ret.toString()

        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND timestamp >= $aYearAgo AND cast(value2 as float) > 0 AND cast(value2 as float) < 2"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        y = ret.toString()

        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND cast(value2 as float) > 0 AND cast(value2 as float) < 2"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        t = ret.toString()

        arr.add(getString(R.string.notGood) + "," + m  + "," + y + "," + t)

        // Bad
        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND timestamp >= $aMonthAgo AND cast(value2 as float) > 1"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        m = ret.toString()

        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND timestamp >= $aYearAgo AND cast(value2 as float) > 1"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        y = ret.toString()

        sql = "SELECT count(*) FROM data WHERE type=${viewModel.dataType} AND cast(value2 as float) > 1"
        if (filtered) sql += viewModel.compileFilter()
        ret = viewModel.getInt(sql)
        t = ret.toString()

        arr.add(getString(R.string.bad) + "," + m  + "," + y + "," + t)
    }
}
