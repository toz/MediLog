/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.diary

import android.content.Context
import android.graphics.Color.parseColor
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Abc
import androidx.compose.material.icons.filled.Category
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.Tabs.DIARY
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.dialogs.toHexString
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.SnackbarDelegate
import java.util.Date

class DiaryTab(context: Context, snackbarDelegate: SnackbarDelegate): BaseTab(context, snackbarDelegate) {
    override var editActivityClass: Class<*> = DiaryEditActivity::class.java
    override var infoActivityClass: Class<*> = DiaryInfoActivity::class.java
    override var chartActivityClass: Class<*> = DiaryChartActivity::class.java

    private val GOOD = 0
    private val NOT_GOOD = 1
    private val BAD = 2

    private var currentEmoji = mutableIntStateOf( R.drawable.ic_ok )
    private var goodSmiley = ""
    private var notGoodSmiley = ""
    private var badSmiley = ""

    init {
        notGoodSmiley = if (preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_SHOWNOTGOOD, true)) context.getString(R.string.notGoodSmileyEmojy) else " "
        badSmiley     = if (preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_SHOWBAD, true)) context.getString(R.string.badSmileyEmojy) else " "
        goodSmiley    = if (preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_SHOWGOOD, false)) "😀︎" else " "
        showTime      = preferences.getBoolean(SettingsActivity.KEY_PREF_DIARY_SHOWTIME, context.getString(R.string.SHOWTIME_DEFAULT).toBoolean())
    }

    @Composable
    override fun ShowContent(padding: PaddingValues) {
        super.ShowContent(padding)
        setEmoji(try { value2.value.toInt() } catch (e: NumberFormatException) { GOOD })

        val emojiState by remember { currentEmoji }
        LazyColumn(state = listState, horizontalAlignment = Alignment.Start, modifier = Modifier.fillMaxWidth().padding(start = 8.dp, end = 8.dp)) {
            item {
                if (quickEntry) {
                    val (textField1) = remember { FocusRequester.createRefs() }
                    //val backgroundColor = Color.Transparent
                    //val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = backgroundColor, focusedContainerColor = backgroundColor)
                    Row(modifier = Modifier.fillMaxWidth()) {
                        TextField(
                            leadingIcon = { Icon(
                                painter = painterResource(emojiState),
                                contentDescription = "State",
                                modifier = Modifier.clickable {  changeEmoji() }) },
                            value = comment.value,
                            onValueChange = { updateComment(it) },
                            singleLine = false,
                            maxLines = 2,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text, capitalization = KeyboardCapitalization.Sentences),
                            label = { Text(text = stringResource(id = R.string.diary) ) },
                            colors = textFieldColors,
                            modifier = Modifier.weight(1f).focusRequester(textField1),
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )
                        IconButton(onClick = { openTextTemplatesDialog.value = true }) { Icon(Icons.Default.Abc,contentDescription = null) }
                        IconButton(onClick = { showTagsDialog.value = true }) { Icon(imageVector = Icons.Default.Category, contentDescription = "Tags") }
                    } // Row
                    SideEffect {
                        // Set cursor to first field after addItem completed
                        if (activateFirstField) {
                            textField1.requestFocus()
                            activateFirstField = false }
                    }

                    // Dialog section
                    if (openTextTemplatesDialog.value)
                        textTemplateDialog.ShowDialog(setShowDialog = { openTextTemplatesDialog.value = it }) {
                            comment.value += it
                            showKeyboard()
                        }
                    if (showTagsDialog.value)
                        tagsDialog.ShowDialog(tags.value, setShowDialog = { showTagsDialog.value = it }) {
                            tags.value = it
                            showKeyboard()
                        }
                }
            }
            items(dataList.value) { item ->
                HorizontalDivider(thickness = 1.dp, color = MaterialTheme.colorScheme.secondaryContainer) // Lines starting from the top
                var colorString = ""
                // Todo: Pick color of first tag until I can think of a better way to show multiple tags
                val tagStrings = item.tags.split(Delimiter.TAG)
                if (tagStrings[0].isNotEmpty()) {
                    val tmpColorString = MainActivity.tagsViewModel.getColor(tagStrings[0])
                    if (tmpColorString != Color.Transparent.toHexString()) colorString = tmpColorString // Don't touch color if no tag color is set
                }

                val backgroundColor = if (colorString.isNotEmpty()) try { Color(parseColor(colorString)) } catch (e: Exception ) { MaterialTheme.colorScheme.background }
                else MaterialTheme.colorScheme.background // Color.Transparent

                Row(
                    modifier = Modifier.height(IntrinsicSize.Min).fillMaxWidth().background(color = backgroundColor).width(IntrinsicSize.Max).clickable { selection.value = item },
                    verticalAlignment = Alignment.CenterVertically
                )
                {
                    ShowRow(item, colorString)
                }
                if (selection.value != null) {
                    ItemClicked(selection.value!!._id)
                    selection.value = null
                }
            }
        }
    }

    @Composable
    fun ShowRow(item: Data, colorString: String) {
        // Todo: Pick color of first tag for now
        //val tagStrings = item.tags.split(",")
        //val colorString = MainActivity.tagsViewModel.getColor(tagStrings[0])
        val backgroundColor = if (colorString.isNotEmpty()) try { Color(parseColor(colorString)) } catch (e: Exception ) { MaterialTheme.colorScheme.background }
        else MaterialTheme.colorScheme.background // Color.Transparent
        val textColor = if (MainActivity.isDarkThemeOn(context) && backgroundColor != MaterialTheme.colorScheme.background) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.primary

        // Timestamp
        Text(formatDateString(item.timestamp), Modifier
            .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
            .width(dateColumnWidth), color = textColor, fontSize = fontSize.sp)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        var emoji = goodSmiley
        var fontStyle = FontStyle.Normal
        if (item.type != DIARY) { // Merged items
            fontStyle = FontStyle.Italic
        }
        else {  // Proper Diary item
            val state = try { item.value2.toInt() } catch (e: NumberFormatException) { 0 }
            emoji = when (state) {
                1 -> notGoodSmiley
                2 -> badSmiley
                else -> goodSmiley
            }
        }
        Text(emoji,
            Modifier.padding(start = cellPadding.dp, end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(15.dp),
            fontSize = fontSize.sp
        )
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator
        var s = compileAnnotatedString(item.comment + if (item.attachment.isNotEmpty())  " $PAPERCLIP" else "")
        Text(s, Modifier.padding(start = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).clickable {
            selection.value = item
            selectedField = "comment"},
            color = textColor,
            fontSize = fontSize.sp,
            fontStyle = fontStyle,
            style = TextStyle(lineHeight = TextUnit.Unspecified, platformStyle = PlatformTextStyle(includeFontPadding = false)) // style is for removing the padding between multiline text
        )
    }

    private fun changeEmoji() {
        var state = try { value2.value.toInt() } catch (e: NumberFormatException) { GOOD }
        if (state == BAD) state = GOOD
        else state++
        updateValue2(state.toString())
    }

    private fun setEmoji(state: Int) {
        currentEmoji.intValue = when (state) {
            NOT_GOOD -> R.drawable.ic_neutral
            BAD      -> R.drawable.ic_not_ok
            else     -> R.drawable.ic_ok
        }
        updateValue2(state.toString()) // Make sure this field is populated
    }

    override fun addItem() {
        if (!quickEntry) {
            startEditing(-1) // Indicate that this is about a new item
            return
        }

        // Close keyboard after entry is done
        hideKeyboard()

        // If value is empty, replace with default text based on state
        if (comment.value.isEmpty()) {
            snackbarDelegate.showSnackbar(context.getString(R.string.diaryMissing))
            return
        }

        if (tags.value.isEmpty()) {
            tags.value = NO_TAGS
        }

        val item = Data(0, Date().time, comment.value, viewModel.dataType, "", value2.value,"","", "", tags = tags.value, category_id = -1, profile_id = activeProfileId)
        viewModel.upsert(item)
        if ((viewModel.filterEnd > 0 ) && (viewModel.filterEnd < item.timestamp)) snackbarDelegate.showSnackbar(context.getString(R.string.filteredOut))
//            userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)
        // ### Clean up ###
        // setEmoji moves to next step, hence state = BAD so setEmoji can set it to good again
        //state = BAD
        value2.value = BAD.toString()
        setEmoji(GOOD)
        cleanUpAfterAddItem()
    }
}