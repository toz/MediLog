/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.diary

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.EditActivity
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.tags.TagRows
import com.zell_mbc.medilog.tags.TagsViewModel

class DiaryEditActivity : EditActivity() {
    override val dataType = Tabs.DIARY
    lateinit var tagsViewModel: TagsViewModel

    var radioOptions = listOf("")

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel     = ViewModelProvider(this)[DiaryViewModel::class.java]
        tagsViewModel = ViewModelProvider(this)[TagsViewModel::class.java]

        super.onCreate(savedInstanceState)

        radioOptions = listOf(getString(R.string.goodSmileyEmojy),getString(R.string.notGoodSmileyEmojy),getString(R.string.badSmileyEmojy))

        // All preparation done, start Compose
        setContent {
            StartCompose() }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier
            .padding(start = 8.dp, end = 8.dp)
            .imePadding()) {
            DateTimeBlock()
            val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent, focusedContainerColor = Color.Transparent)
            var selectedOption by remember { mutableStateOf(posToChar(value2String)) }

            Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Text(getString(R.string.state) + ": ", Modifier.padding(start = 16.dp))
                radioOptions.forEach { item ->
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        RadioButton(
                            selected = (item == selectedOption),
                            onClick = {
                                selectedOption = item
                                value2String = charToPos(item)
                            }
                        )
                        Text(
                            text = item,
                            style = MaterialTheme.typography.bodyMedium,
                            modifier = Modifier.padding(start = 0.dp, end = 24.dp)
                        )
                    }
                }
            }
            Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Column(modifier = Modifier.padding(start = 16.dp)) {
                    TagRows(activity, tagsViewModel, tagIds, setTagIds = { tagIds = it }, setShowTagsDialog = { showTagsDialog = it })
                }
            }
            Row(Modifier.fillMaxWidth()) {
                // Comment
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = commentString,
                    colors = textFieldColors,
                    onValueChange = { commentString = it
                        //  commentString = it
                    },
                    singleLine = false,
                    label = { Text(text = stringResource(id = R.string.diary), style = MaterialTheme.typography.bodyMedium) },
                    trailingIcon = {
                        IconButton(onClick = {
                            showTextTemplatesDialog = true
                        })
                        { Icon(Icons.Outlined.Abc, contentDescription = null) }
                    },
                )
            }
            Text("")

            if (attachmentPresent()) {
                Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    AttachmentBlock()
                }
            }
        }
  //      }

        // Dialogs
        if (showTagsDialog)
            tagsDialog.ShowDialog(selectedTags = tagIds, setShowDialog = { showTagsDialog = it }) {
                tagIds = it
            }

        if (showTextTemplatesDialog)
            textTemplateDialog.ShowDialog(setShowDialog = { showTextTemplatesDialog = it }) { commentString += it }

        if (showDatePickerDialog) OpenDatePickerDialog()
        if (showTimePickerDialog) OpenTimePickerDialog()    }

    // Return the position of the corresponding char in the radioOptions array
    fun charToPos(item: String) = radioOptions.indexOf(item).toString()

    // Return the corresponding char in the radioOptions array
    fun posToChar(pos: String): String {
        var posInt = try { pos.toInt() } catch (_: NumberFormatException) { 0 }
        if (posInt > 2) posInt = 0 // Should never happen
        return radioOptions[posInt]
    }

    override fun saveItem() {
        // Check empty variables
        if (commentString.isEmpty()) {
            hideKeyboard()
            snackbarDelegate.showSnackbar(getString(R.string.diaryMissing))
            return
        }
        super.saveItem()
        finish() // Close current window / activity
    }
}