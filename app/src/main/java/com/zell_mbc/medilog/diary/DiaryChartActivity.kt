/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.diary

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.ChartActivity
import java.time.DayOfWeek
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month
import java.time.temporal.ChronoUnit
import com.kizitonwose.calendar.compose.CalendarLayoutInfo
import com.kizitonwose.calendar.compose.HeatMapCalendar
import com.kizitonwose.calendar.compose.heatmapcalendar.HeatMapCalendarState
import com.kizitonwose.calendar.compose.heatmapcalendar.HeatMapWeek
import com.kizitonwose.calendar.compose.heatmapcalendar.rememberHeatMapCalendarState
import com.kizitonwose.calendar.core.CalendarDay
import com.kizitonwose.calendar.core.CalendarMonth
import com.kizitonwose.calendar.core.firstDayOfWeekFromLocale
import com.kizitonwose.calendar.core.yearMonth
import java.time.YearMonth
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.format.TextStyle
import java.util.Locale
import kotlin.math.max

val NO_VALUE = 0
val GOOD = 1

val colorsLight = arrayOf(Color(0xFFEBEDF0), Color.Green, MainActivity.AMBER, Color.Red)
val colorsDark = arrayOf(Color.Gray, Color.Green, MainActivity.AMBER, Color.Red)

@SuppressLint("NewApi")
class DiaryChartActivity: ChartActivity() {
    override val filename = "DiaryChart.jpg"
    private var daySize = 28.dp
    var colorArray = emptyArray<Color>()

    data class DayClass(
        var state: Int,
        var comment: String,
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[DiaryViewModel::class.java]

        // For the charts only native diary entries can be considered, no matter what showAllTabs is set to
        val tmp = viewModel.blendInItems
        viewModel.blendInItems = false
        items = viewModel.getItems("ASC", filtered = true)
        viewModel.blendInItems = tmp

        showContent()
    }

    private fun compileTimeframe(startDate: LocalDate, endDate: LocalDate): Map<LocalDate, DayClass> {

        // Build day hashmap
        val days = (0..ChronoUnit.DAYS.between(startDate, endDate))
            .associateTo(hashMapOf()) { count ->
                startDate.plusDays(count) to DayClass(state = NO_VALUE, comment = "")
        }
        // Add item data to hashmap
        for (item in items) {
            val itemDay = LocalDate.from(LocalDateTime.ofInstant(Instant.ofEpochMilli(item.timestamp), ZoneId.systemDefault())) // ZoneOffset.UTC))
            val day = days[itemDay]
            if (day != null) {
                day.comment = if (day.comment.isNotEmpty()) day.comment + "\n" + item.comment else item.comment
                // Make sure the "worst value" wins
                // Increase value2 by 1 reflect that good in value2=0
                val tmpState = if (item.value2.isEmpty()) GOOD else try { item.value2.toInt()+1 } catch (e: NumberFormatException) { NO_VALUE }
                day.state = max(tmpState, day.state)
            }
        }
        return days
    }

    @Composable
    override fun ShowContent() {
        val startDate = LocalDate.from(LocalDateTime.ofInstant(Instant.ofEpochMilli(items[0].timestamp), ZoneId.systemDefault()))
        val endDate   = LocalDate.now()
        colorArray = if (isSystemInDarkTheme()) colorsDark else colorsLight

        val data = remember { mutableStateOf<Map<LocalDate, DayClass>>(emptyMap()) }
        var selection by remember { mutableStateOf<Pair<LocalDate, DayClass>?>(null) }
        data.value = compileTimeframe(startDate, endDate)

        Box(modifier = Modifier.verticalScroll(rememberScrollState()).safeDrawingPadding()) {
            Column(modifier = Modifier.fillMaxSize()) {
                val state = rememberHeatMapCalendarState(
                    startMonth = startDate.yearMonth,
                    endMonth = endDate.yearMonth,
                    firstVisibleMonth = endDate.yearMonth,
                    firstDayOfWeek = firstDayOfWeekFromLocale(),
                )
                HeatMapCalendar(
                    modifier = Modifier.padding(vertical = 10.dp),
                    state = state,
                    contentPadding = PaddingValues(end = 6.dp),
                    dayContent = { day, week ->
                        Day(
                            day = day,
                            startDate = startDate,
                            endDate = endDate,
                            week = week,
                           // level = data.value[day.date]?.level ?: Level.NoValue,
                            state = data.value[day.date]?.state ?: NO_VALUE
                        ) { clicked ->
                            selection = Pair(clicked, data.value[clicked]?: DayClass(state = NO_VALUE, comment = getString(R.string.stateNoValue)))
                        }
                    },
                    weekHeader = { WeekHeader(it) },
                    monthHeader = { MonthHeader(it, endDate, state) },
                )
                CalendarInfo(modifier = Modifier.fillMaxWidth().padding(horizontal = 40.dp, vertical = daySize))
                BottomContent(modifier = Modifier.fillMaxWidth().padding(20.dp), selection = selection)
            }
        }
    }


    private val formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)

    @Composable
    private fun BottomContent(
        modifier: Modifier = Modifier,
        selection: Pair<LocalDate, DayClass>? = null) {
        Column(modifier = modifier) {
            if (selection != null) {
                val color = colorArray[selection.second.state]
                Row(modifier = Modifier.align(Alignment.CenterHorizontally), verticalAlignment = Alignment.CenterVertically) {
                    LevelBox(color = color)
                    Text(text = formatter.format(selection.first), modifier = Modifier.padding(start = 8.dp), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                }
                val rows = selection.second.comment.split("\n") // In case of multiple entries per day
                for (row in rows)
                    Row(modifier = Modifier.align(Alignment.CenterHorizontally), verticalAlignment = Alignment.CenterVertically) {
                        Text(text = row, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                }
            }
        }
    }
    @Composable
    private fun CalendarInfo(modifier: Modifier = Modifier) {
        Column(
            modifier = modifier,
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.Start,
        ) {
            val labels = arrayOf(getString(R.string.stateNoValue), getString(R.string.stateGood),getString(R.string.stateNotGood),getString(R.string.stateBad))
            for (i in 0..3) {
                Row {
                    Text(text = labels[i], modifier = Modifier.width(80.dp), style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
                    LevelBox(colorArray[i])
                }
            }
        }
    }

    @Composable
    private fun Day(
        day: CalendarDay,
        startDate: LocalDate,
        endDate: LocalDate,
        week: HeatMapWeek,
     //   level: Level,
        state: Int,
        onClick: (LocalDate) -> Unit,
    ) {
        // We draw a transparent box on the empty spaces in the first week
        // so the items are laid out properly as the column is top to bottom.
        val weekDates = week.days.map { it.date }
        if (day.date in startDate..endDate) {
            LevelBox(colorArray[state]) { onClick(day.date) }
        } else if (weekDates.contains(startDate)) {
            LevelBox(Color.Transparent)
        }
    }

    @Composable
    private fun LevelBox(color: Color, onClick: (() -> Unit)? = null) {
        Box(
            modifier = Modifier
                .size(daySize) // Must set a size on the day.
                .padding(2.dp)
                .clip(RoundedCornerShape(2.dp))
                .background(color = color)
                .clickable(enabled = onClick != null) { onClick?.invoke() },
        )
    }

    @Composable
    private fun WeekHeader(dayOfWeek: DayOfWeek) {
        Box(
            modifier = Modifier
                .height(daySize) // Must set a height on the day of week so it aligns with the day.
                .padding(horizontal = 4.dp),
        ) {
            Text(
                text = dayOfWeek.displayText(),
                modifier = Modifier.align(Alignment.Center),
                style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary
            )
        }
    }

    @Composable
    private fun MonthHeader(
        calendarMonth: CalendarMonth,
        endDate: LocalDate,
        state: HeatMapCalendarState,
    ) {
        val density = LocalDensity.current
        val firstFullyVisibleMonth by remember {
            // Find the first index with at most one box out of bounds.
            derivedStateOf { getMonthWithYear(state.layoutInfo, daySize, density) }
        }
        if (calendarMonth.weekDays.first().first().date <= endDate) {
            val month = calendarMonth.yearMonth
            val title = if (month == firstFullyVisibleMonth) {
                month.displayText(short = true)
            } else {
                month.month.displayText()
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 1.dp, start = 2.dp),
            ) {
                Text(text = title, style = MaterialTheme.typography.bodyMedium, color = MaterialTheme.colorScheme.primary)
            }
        }
    }

    // Find the first index with at most one box out of bounds.
    private fun getMonthWithYear(
        layoutInfo: CalendarLayoutInfo,
        daySize: Dp,
        density: Density,
    ): YearMonth? {
        val visibleItemsInfo = layoutInfo.visibleMonthsInfo
        return when {
            visibleItemsInfo.isEmpty() -> null
            visibleItemsInfo.count() == 1 -> visibleItemsInfo.first().month.yearMonth
            else -> {
                val firstItem = visibleItemsInfo.first()
                val daySizePx = with(density) { daySize.toPx() }
                if (
                // Ensure the Month + Year text can fit.
                    firstItem.size < daySizePx * 3 ||
                    // Ensure the week row size - 1 is visible.
                    firstItem.offset < layoutInfo.viewportStartOffset &&
                    (layoutInfo.viewportStartOffset - firstItem.offset > daySizePx)
                ) {
                    visibleItemsInfo[1].month.yearMonth
                } else {
                    firstItem.month.yearMonth
                }
            }
        }
    }

    fun YearMonth.displayText(short: Boolean = false): String {
        return "${this.month.displayText(short = short)} ${this.year}"
    }

    fun Month.displayText(short: Boolean = true): String {
        val style = if (short) TextStyle.SHORT else TextStyle.FULL
        return getDisplayName(style, Locale.getDefault())
    }

    fun DayOfWeek.displayText(uppercase: Boolean = false, narrow: Boolean = false): String {
        val style = if (narrow) TextStyle.NARROW else TextStyle.SHORT
        return getDisplayName(style, Locale.getDefault()).let { value ->
            if (uppercase) value.uppercase(Locale.getDefault()) else value
        }
    }
}
