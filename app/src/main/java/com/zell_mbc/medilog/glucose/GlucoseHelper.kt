/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.glucose

// Calculate glucose Ketone Index
fun getGKI(glucose: String, ketone: String, mg_dl: Boolean = false): String {
    // GKI
    var gkiValue: Float
    var gkiString = ""

    if (glucose.isNotEmpty() && ketone.isNotEmpty()) {
        val g = glucose.replace(",",".")
        val k = ketone.replace(",",".")
        try {
            val gv = g.toFloat()
            val kv = k.toFloat()
            if (kv > 0 && gv > 0) {
                gkiValue = (gv / kv)
                if (mg_dl) gkiValue /= 18

                gkiString = gkiValue.toString()
                if (gkiString.length > 4) gkiString = gkiString.substring(0,4)
            }
        } catch (_: Exception) { gkiString = "" }
    }
    return gkiString
}