/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.glucose

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.R.string
import com.zell_mbc.medilog.base.EditActivity
import com.zell_mbc.medilog.preferences.SettingsActivity

class GlucoseEditActivity: EditActivity() {
    override val dataType = Tabs.GLUCOSE

    private var mg_dl = false
    private var logKetone = false

    var radioOptions = listOf("")

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this)[GlucoseViewModel::class.java]
        super.onCreate(savedInstanceState)

        radioOptions = listOf(getString(string.fasting),getString(string.preMeal),getString(string.postMeal))
        logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, getString(string.LOG_KETONE_DEFAULT).toBoolean())

        // mg_dl = integers, mmol_L = decimals and < 10
        mg_dl = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(string.GLUCOSE_UNIT_MMOL_L)) == getString(string.GLUCOSE_UNIT_MG_DL))

        var gh = (if (mg_dl) getString(string.glucoseHintUS) else getString(string.glucoseHint))
        if (modifyDecimalSeparator) {
            gh = gh.replace('.', decimalSeparator)
            if (logKetone) value2Hint = getString(string.ketoneEntryHint).replace('.', decimalSeparator)
        }
        value1Hint = gh

        // All preparation done, start Compose
        setContent { StartCompose() }
    }


    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(start = 8.dp, end = 8.dp)) {
            DateTimeBlock()

            val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent, focusedContainerColor = Color.Transparent)
            val focusManager = LocalFocusManager.current
            val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }
            var selectedOption by remember { mutableStateOf(setRadioPosition(value3String)) }
            //selectedOption = value3String

            Row(Modifier.fillMaxWidth()) {
                // Weight
                TextField(
                    value = value1String,
                    colors = textFieldColors,
                    onValueChange = {
                        value1String = it.filter { it.isDigit() }
                        if (value1String.isNotEmpty()) {
                            value1String = it
                            if (((it.length == 4) && (!it.endsWith(decimalSeparator))) || (it.length >= 5))
                                focusManager.moveFocus(FocusDirection.Next)
                        }
                    },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                    singleLine = true,
                    textStyle = TextStyle(),
                    label = { Text(text = stringResource(id = string.glucose) + "*", maxLines = 1) },
                    placeholder = { Text(text = "$value1Hint $value1Unit") },
                    modifier = Modifier.padding(end = 20.dp).focusRequester(textField1).weight(1f),
                )

                // Body fat
                if (logKetone) {
                    TextField(
                        value = value2String,
                        onValueChange = {
                            value2String = it
                            if (it.length > 1) focusManager.moveFocus(FocusDirection.Next) //textField3.requestFocus()
                        },
                        placeholder = { Text(text = "$value2Hint $value2Unit") },
                        modifier = Modifier.weight(1f).focusRequester(textField2),
                        colors = textFieldColors,
                        singleLine = true,
                        label = { Text(text = stringResource(id = string.ketone)) },
                    )
                }
            }
            if (logKetone) {
                Spacer(modifier = Modifier.height(20.dp))
                Row { Text(getString(string.glucoseKetoneIndex) + " " + getGKI(value1String, value2String, mg_dl), Modifier.padding(start = 8.dp)) }
            }

            Text("")
            Row {
                Text(getString(string.tag), Modifier.padding(start = 16.dp))
            }
            Row {
                Text("")
                radioOptions.forEach { item ->
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        RadioButton(
                            selected = (item == selectedOption),
                            onClick = {
                                selectedOption = item
                                value3String = getRadioPosition(item)
                            }
                        )
                        Text(
                            text = item,
                            style = MaterialTheme.typography.bodyMedium,
                            modifier = Modifier.padding(start = 16.dp)
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(20.dp) )
            Row {
                // Comment
                TextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .focusRequester(textField3),
                    value = commentString,
                    colors = textFieldColors,
                    onValueChange = { commentString = it
                        //  commentString = it
                    },
                    singleLine = false,
                    label = { Text(text = stringResource(id = string.comment)) },
                    trailingIcon = {
                        IconButton(onClick = {
                            showTextTemplatesDialog = true
                        })
                        { Icon(Icons.Outlined.Abc, contentDescription = null) }
                    },
                )
            }
            Text("")

            if (attachmentPresent()) {
                Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    AttachmentBlock()
                }
            }
        }

        // Dialogs
        if (showTextTemplatesDialog)
            textTemplateDialog.ShowDialog(setShowDialog = { showTextTemplatesDialog = it }) { commentString += it }

        if (showDatePickerDialog) OpenDatePickerDialog()
        if (showTimePickerDialog) OpenTimePickerDialog()
    }

    fun getRadioPosition(item: String) = radioOptions.indexOf(item).toString()
    fun setRadioPosition(pos: String): String {
        var i = try { pos.toInt() } catch (_: NumberFormatException)  { 0 }
        if (i > 2) i = 0 // Should never happen, but…
        return radioOptions[i]
    }

    //Tab specific pre-save checks
    override fun saveItem() {
        // Close keyboard so error is visible
        hideKeyboard()

        if (value1String.isEmpty()) {
            snackbarDelegate.showSnackbar(this.getString(string.valuesMissing))
            return
        }
        value1String = value1String.replace(decimalSeparator, '.') // Make sure no , slips through

        // Check for Float if European / Integer if US
        var value1ValueF = 0f
        var value1ValueI = 0
        if (mg_dl) value1ValueI = viewModel.validValueI(value1String, Threshold.MAX_GLUCOSE_MG)
        else value1ValueF = viewModel.validValueF(value1String, Threshold.MAX_GLUCOSE_MMOL)

        if (value1ValueI < 0 || value1ValueF < 0f) {
            snackbarDelegate.showSnackbar(this.getString(string.invalid) + " " + this.getString(string.glucose) + " " + this.getString(string.value) + " $value1String")
            return
        }
        if (value1ValueF > 10) {
            MaterialAlertDialogBuilder(this).setTitle(getString(string.attention)).setMessage(getString(string.glucoseWrongUnit)).setPositiveButton(getString(string.ok)) { _, _ -> finish() }.show()
            return
        }

        // Valid Ketone?
        value2String = value2String.replace(decimalSeparator, '.')
        if (logKetone && value2String.isNotEmpty() ) {
            val value2Value = viewModel.validValueF(value2String)
            if (value2Value <= 0) {
                snackbarDelegate.showSnackbar(this.getString(string.invalid) + " " + this.getString(string.ketone) + " " + this.getString(string.value) + " $value2String")
                return
            }
            if (!value2String.substring(0,1).isDigitsOnly()) value2String = "0$value2String"  // If string starts with , or . add a 0
        }

        super.saveItem()
        finish() // Close current window / activity
    }

}
