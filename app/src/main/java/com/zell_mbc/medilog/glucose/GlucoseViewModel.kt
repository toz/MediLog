/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.glucose

import android.app.Application
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity

class GlucoseViewModel(application: Application): DataViewModel(application, Tabs.GLUCOSE) {
    override val filterStartPref = "GLUCOSEFILTERSTART"
    override val filterEndPref = "GLUCOSEFILTEREND"
    override val timeFilterModePref =  "GLUCOSE_FILTER_MODE"
    override val rollingFilterValuePref = "GLUCOSE_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "GLUCOSE_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.glucose)
    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_BLENDINITEMS, app.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())

    val logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, app.getString(R.string.LOG_KETONE_DEFAULT).toBoolean())
}
