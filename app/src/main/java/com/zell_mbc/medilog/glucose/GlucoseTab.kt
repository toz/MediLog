/*
 *     This file is part of MediLog.
 *
 *     MediLog is free softwar_: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.glucose

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Filter
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.R.string
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.SnackbarDelegate
import com.zell_mbc.medilog.support.checkThresholds
import java.util.Date

class GlucoseTab(context: Context, snackbarDelegate: SnackbarDelegate): BaseTab(context, snackbarDelegate) {

    override var editActivityClass: Class<*> = GlucoseEditActivity::class.java
    override var infoActivityClass: Class<*> = GlucoseInfoActivity::class.java
    override var chartActivityClass: Class<*> = GlucoseChartActivity::class.java

    private var glucoseHint = ""
    private var mg_dl = false
    private var logKetone = false

    init {
        //val itemUnit = preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, context.getString(R.string.GLUCOSE_UNIT_MMOL_L))

        // mg_dl = integers, mmol_L = decimals and < 10
        mg_dl = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, context.getString(string.GLUCOSE_UNIT_MMOL_L)) == context.getString(string.GLUCOSE_UNIT_MG_DL))
        var itemUnit = "" + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT,"")
        if (itemUnit.isEmpty()) {
            itemUnit = when (context.resources.configuration.locales.get(0).toString()) {
                "DE",
                "CH",
                "AU" -> context.getString(string.GLUCOSE_UNIT_MMOL_L)
                else -> context.getString(string.GLUCOSE_UNIT_MG_DL)
            }
        }

        glucoseHint = (if (mg_dl) context.getString(string.glucoseHintUS) else context.getString(string.glucoseHint)) + " " + itemUnit
        showTime        = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_SHOWTIME, context.getString(string.SHOWTIME_DEFAULT).toBoolean())
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_HIGHLIGHT_VALUES, context.getString(string.HIGHLIGHT_VALUES_DEFAULT).toBoolean())
        logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, context.getString(string.LOG_KETONE_DEFAULT).toBoolean())

        val defaultThresholds = if(mg_dl) context.getString(string.GLUCOSE_THRESHOLDS_DEFAULT_MG_DL) else context.getString(string.GLUCOSE_THRESHOLDS_DEFAULT_MMOL_L)
        val s = "" + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS, defaultThresholds)
        val th = checkThresholds(context, s, defaultThresholds, string.glucose)
        lowerThreshold = try { th[0].toFloat() } catch  (_: NumberFormatException) { 0f }
        upperThreshold = try { th[1].toFloat() } catch  (_: NumberFormatException) { 0f }
    }

    @Composable
    override fun ShowContent(padding: PaddingValues) {
        super.ShowContent(padding)
        showComment.value = comment.value.isNotEmpty() // For some reason the app will crash if this is in super?

        MeasureColumnWidths()
        //val textFieldColors: TextFieldColors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent, focusedContainerColor = Color.Transparent)

        // Spacer(modifier = Modifier.height(5.dp))
        LazyColumn(state = listState, horizontalAlignment = Alignment.Start, modifier = Modifier.fillMaxWidth().padding(start = 8.dp, end = 8.dp)) {
            item {
                if (quickEntry) {
                    // Glucose
                    val focusManager = LocalFocusManager.current
                    val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }
                    Row(Modifier.fillMaxWidth()) {
                        TextField(
                            value = value1.value,
                            colors = textFieldColors,
                            onValueChange = {
                                if (!logKetone) showComment.value = true // only activate comment field if logKetone is not visible
                                updateValue1(it)
                                if (mg_dl) {
                                    // 3 digit value expected
                                    val l = if (it.isNotEmpty() && it.substring(0,1) == "1") 2 else 1
                                    if (it.length > l) focusManager.moveFocus(FocusDirection.Next)
                                }
                                else {
                                    if (it.length > 2) focusManager.moveFocus(FocusDirection.Next)
                                }
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Decimal
                            ),
                            textStyle = TextStyle(),
                            label = {
                                Text(text = stringResource(id = string.glucose) + "*", maxLines = 1, overflow = TextOverflow.Ellipsis)
                            },
                            placeholder = { Text(text = glucoseHint) },
                            modifier = Modifier.weight(1f).focusRequester(textField1).padding(end = 10.dp),
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )
                        if (logKetone)
                            TextField(
                                value = value2.value,
                                colors = textFieldColors,
                                onValueChange = {
                                    updateValue2(it)
                                    if (showComment.value && it.length > 2) focusManager.moveFocus(FocusDirection.Next)
                                    showComment.value = true
                                },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                                textStyle = TextStyle(),
                                label = { Text(text = stringResource(id = string.ketone), maxLines = 1, overflow = TextOverflow.Ellipsis)},
                                placeholder = { Text(text = stringResource(id = string.ketoneEntryHint) + " " + stringResource(string.KETON_UNIT)
                                ) },
                                modifier = Modifier.weight(1f).focusRequester(textField2).padding(end = cellPadding.dp),
                                keyboardActions = KeyboardActions( onDone = { addItem() })
                            )
                    } // Row

                    if (showComment.value)
                        TextField(
                            value = comment.value,
                            colors = textFieldColors,
                            onValueChange = {
                                updateComment(it)
                            },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            singleLine = true,
                            textStyle = TextStyle(),
                            modifier = Modifier.fillMaxWidth().focusRequester(textField3),
                            label = { Text(text = stringResource(id = string.comment)) },
                            trailingIcon = {
                                IconButton(onClick = {
                                    openTextTemplatesDialog.value = true
                                })
                                { Icon(Icons.Outlined.Abc, contentDescription = null) }
                            },
                            keyboardActions = KeyboardActions(onDone = {
                                keyboardController?.hide()
                                addItem()
                            })
                        )
                    SideEffect {
                        // Set cursor to first field after addItem completed
                        if (activateFirstField) {
                            textField1.requestFocus()
                            activateFirstField = false }
                    }

                    // Dialog section
                    if (openTextTemplatesDialog.value) {
                        textTemplateDialog.ShowDialog(setShowDialog = { openTextTemplatesDialog.value = it }) {
                            comment.value += it
                            textField3.requestFocus()
                        }
                    }
                } // End quick entry
            } // item
            items(dataList.value) { item ->
                // Lines starting from the top
                HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, thickness = 1.dp)
                Row(modifier = Modifier.fillMaxWidth().height(IntrinsicSize.Min).clickable { selection.value = item }, verticalAlignment = Alignment.CenterVertically) {
                    ShowRow(item)
                }
                if (selection.value != null) {
                    ItemClicked(selection.value!!._id)
                    selection.value = null
                }
            }
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(formatDateString(item.timestamp),
            Modifier.padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp).width(dateColumnWidth),color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)

        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        var fontStyle = FontStyle.Normal
        var textColor = MaterialTheme.colorScheme.primary
        var newValue1String: String

        // A diary item blended in
        if (item.type != viewModel.dataType) {
            fontStyle = FontStyle.Italic
            val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
            Text(s, Modifier.padding(start = cellPadding.dp), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp, fontStyle = fontStyle)
            return
        }

        val newValue1Value = try { item.value1.toFloat() } catch (_: NumberFormatException) { 0f }

        // If unit cut off trailing .0
        newValue1String = if (mg_dl) newValue1Value.toInt().toString() else newValue1Value.toString()
        if (MainActivity.modifyDecimalSeparator) newValue1String = newValue1String.replace('.', MainActivity.decimalSeparator)
        if (highlightValues && ((newValue1Value < lowerThreshold) || (newValue1Value > upperThreshold))) textColor = MaterialTheme.colorScheme.error

        // Glucose
        Text(newValue1String, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp).width(value1Width),
            textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Ketone
        textColor = MaterialTheme.colorScheme.primary
        if (logKetone) {
            var newValue2String = ""
            if (newValue1String.isNotEmpty()) {
                newValue2String = if (MainActivity.modifyDecimalSeparator) item.value2.replace('.', MainActivity.decimalSeparator) else item.value2
            }
            Text(newValue2String,
                Modifier.padding(start = cellPadding.dp, end = cellPadding.dp).width(value2Width),
                textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
            VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

            textColor = MaterialTheme.colorScheme.primary
            if (newValue2String.isNotEmpty()) {
                val gki = getGKI(item.value1, item.value2, mg_dl)
                newValue2String = if (MainActivity.modifyDecimalSeparator) gki.replace('.', MainActivity.decimalSeparator) else gki
            }
            Text(newValue2String, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp).width(value3Width),
                textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
            VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator
        }

        // Comment
        Spacer(modifier = Modifier.width(cellPadding.dp))
        val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
        Text(s, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle,
            style = TextStyle(lineHeight = TextUnit.Unspecified, platformStyle = PlatformTextStyle(includeFontPadding = false)) // style is for removing the padding between multiline text
        )
    }


    // Helper functions
    @Composable
    fun MeasureColumnWidths() {
        val sampleText = if (mg_dl) "999" else "9.9"
        if (value1Width == 0.dp) MeasureValue1String(sampleText)
        if (value2Width == 0.dp) MeasureValue2String("9.9")
        if (value3Width == 0.dp) MeasureValue3String("99.9")
    }

    override fun addItem() {
        if (!quickEntry) {
            startEditing(-1) // Indicate that this is about a new item
            return
        }

        // Check empty variables
        if (value1.value.isEmpty()) {
            snackbarDelegate.showSnackbar(context.getString(string.valuesMissing))
            return
        }

        // Close keyboard after entry is done
        hideKeyboard()

        value1.value = value1.value.replace(MainActivity.decimalSeparator, '.')
        if(mg_dl) {
            var value = viewModel.validValueI(value1.value, Threshold.MAX_GLUCOSE_MG)
            if (value < 0) {
                snackbarDelegate.showSnackbar(context.getString(string.valuesMissing))
                return
            }
        }
        else {
            var value = viewModel.validValueF(value1.value, Threshold.MAX_GLUCOSE_MMOL)
            if (value < 0) {
                snackbarDelegate.showSnackbar(context.getString(string.valuesMissing))
                return
            }
            else if (value > 10) MaterialAlertDialogBuilder(context).setTitle(context.getString(string.attention)).setMessage(context.getString(string.glucoseWrongUnit)).setPositiveButton(context.getString(string.ok)) { _, _ -> }.show()
        }

        // Valid ketone?
        if (value2.value.isNotBlank()) {
            value2.value = value2.value.replace(MainActivity.decimalSeparator, '.')
            val bodyFatValue = viewModel.validValueF(value2.value, Threshold.MAX_KETONE)
            if (bodyFatValue <= 0f) {
                snackbarDelegate.showSnackbar(context.getString(string.invalid) + " " + context.getString(string.ketone) + " " + context.getString(string.value) + " ${value2.value}")
                return
            }
        }

        val item = Data(_id = 0, timestamp = Date().time, comment = comment.value, type = viewModel.dataType, value1 = value1.value, value2 = value2.value, "", "", "", tags = Filter.NO_TAGS, category_id = -1, profile_id = activeProfileId) // Start with empty item
        viewModel.upsert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp))
            snackbarDelegate.showSnackbar(context.getString(string.filteredOut))

        cleanUpAfterAddItem()
    }
}