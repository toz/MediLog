/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.glucose

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.base.InfoActivity
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.math.RoundingMode
import java.text.DecimalFormat

class GlucoseInfoActivity: InfoActivity() {
    var mmol_l = false
    var df = DecimalFormat("#.##") // European as default

    val tableRows = 5
    val tableColumns = 4
    val tableContent = Array(tableRows) { _ -> Array(tableColumns) { ""} }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[GlucoseViewModel::class.java]

        itemUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT_MMOL_L))
        mmol_l = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, getString(R.string.GLUCOSE_UNIT_MMOL_L)) == getString(R.string.GLUCOSE_UNIT_MG_DL))

        if (mmol_l) df = DecimalFormat("###")
        df.roundingMode = RoundingMode.HALF_EVEN

        setContent { StartCompose() }
    }

    @Composable
    fun RowScope.TableCell(text: String, weight: Float) {
        Text(text = text, Modifier.weight(weight).padding(cellPadding.dp), style = MaterialTheme.typography.bodySmall, color = MaterialTheme.colorScheme.primary)
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun ShowTable() {
        Text(measurementsIn, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)
        Text(timePeriod, modifier = Modifier.padding(start = leftPadding.dp), color = MaterialTheme.colorScheme.primary)

        // Each cell of a column must have the same weight.
        val column1Weight = .4f
        val column2Weight = .2f
        val column3Weight = .2f
        val column4Weight = .2f
        LazyColumn(Modifier.fillMaxWidth().padding(16.dp)) {
            // Here is the header
            stickyHeader {
                    Row(Modifier.background(Color.Gray)) {
                        TableCell(text = getString(R.string.timeframeLabel), weight = column1Weight)
                        TableCell(text = getString(R.string.avg), weight = column2Weight)
                        TableCell(text = getString(R.string.min), weight = column3Weight)
                        TableCell(text = getString(R.string.max), weight = column4Weight)
                }
            }
                // Here are all the lines of your table.
            items(tableContent) {
                val (_, _) = it
                Row(Modifier.fillMaxWidth()) {
                    TableCell(text = it[0], weight = column1Weight)
                    TableCell(text = it[1], weight = column2Weight)
                    TableCell(text = it[2], weight = column3Weight)
                    TableCell(text = it[3], weight = column4Weight)
                }
            }
        }
    }

    @Composable
    override fun ShowContent() {
        Column(modifier = Modifier.padding(8.dp)) {
            HeaderBlock(R.drawable.ic_glucose)
            if ((viewModel.filterStart + viewModel.filterStart) > 0L) {
                gatherData(true)
                ShowTable()
                Text("")
                HorizontalDivider(color = MaterialTheme.colorScheme.secondary, thickness = 1.dp)
                Text("")
            }
            gatherData(false)
            ShowTable()
            ShowAttachments()
        }
    }

    private fun gatherData(filtered: Boolean) {
        count = viewModel.getSize(filtered)

        var item: Data? = viewModel.getFirst(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val startDate = dateFormat.format(item.timestamp)

        item = viewModel.getLast(filtered)
        if (item == null) {
            Toast.makeText(application, getString(R.string.noDataToShow), Toast.LENGTH_LONG).show()
            return
        }
        val endDate = dateFormat.format(item.timestamp)
        timePeriod = getString(R.string.timePeriod) + " $startDate - $endDate"

        measurementsIn = if (!filtered) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"

        var timeframe = 7
        var avg = viewModel.getAvgFloat("value1", timeframe)
        var min = viewModel.getMinValue1( timeframe)
        var max = viewModel.getMaxValue1( timeframe)
        tableContent[0][0] = getString(R.string.days7)
        tableContent[0][1] = df.format(avg)
        tableContent[0][2] = df.format(min)
        tableContent[0][3] = df.format(max)

        timeframe = 14
        avg = viewModel.getAvgFloat("value1", timeframe)
        min = viewModel.getMinValue1( timeframe)
        max = viewModel.getMaxValue1( timeframe)
        tableContent[1][0] = getString(R.string.days14)
        tableContent[1][1] = df.format(avg)
        tableContent[1][2] = df.format(min)
        tableContent[1][3] = df.format(max)

        avg = viewModel.getAvgFloat("value1",21)
        min = viewModel.getMinValue1( 21)
        max = viewModel.getMaxValue1( 21)
        tableContent[2][0] = getString(R.string.days21)
        tableContent[2][1] = df.format(avg)
        tableContent[2][2] = df.format(min)
        tableContent[2][3] = df.format(max)

        avg= viewModel.getAvgFloat("value1",31)
        min = viewModel.getMinValue1( 31)
        max = viewModel.getMaxValue1( 31)
        tableContent[3][0] = getString(R.string.days31)
        tableContent[3][1] = df.format(avg)
        tableContent[3][2] = df.format(min)
        tableContent[3][3] = df.format(max)

        avg = viewModel.getAvgFloat("value1",filtered)
        min = viewModel.getMinValue1(filtered)
        max = viewModel.getMaxValue1( filtered)
        tableContent[4][0] = getString(R.string.totalLabel)
        tableContent[4][1] = df.format(avg)
        tableContent[4][2] = df.format(min)
        tableContent[4][3] = df.format(max)

        if (modifyDecimalSeparator) {
            for (i in tableContent) {
                i[1] = i[1].replace('.', decimalSeparator)
                i[2] = i[2].replace('.', decimalSeparator)
                i[3] = i[3].replace('.', decimalSeparator)
            }
        }
    }

}
