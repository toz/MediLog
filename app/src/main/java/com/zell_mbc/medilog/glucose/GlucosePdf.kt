/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.glucose

import android.app.Activity
import android.graphics.pdf.PdfDocument
import android.widget.Toast
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.checkThresholds

class GlucosePdf(activity: Activity): BasePdf(activity) {
    // Base class overrides
    override var itemName = activity.getString(R.string.weight)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_LANDSCAPE, activity.getString(R.string.GLUCOSE_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_PAPER_SIZE, activity.getString(R.string.GLUCOSE_PAPER_SIZE_DEFAULT))
    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_BLENDINITEMS, activity.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())
    override var printDaySeparatorLines = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_PDF_DAYSEPARATOR, activity.getString(R.string.DAY_SEPARATOR_LINE_DEFAULT).toBoolean())
    override var highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_HIGHLIGHT_VALUES, false)

    // Glucose specific
    val logKetone = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_KETONE, activity.getString(R.string.LOG_KETONE_DEFAULT).toBoolean())
    private val warningSign = activity.getString(R.string.warningSign)
    private val unit = (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, activity.getString(R.string.GLUCOSE_UNIT_MG_DL)) == activity.getString(R.string.GLUCOSE_UNIT_MG_DL))

    private var warningTab = 0f
    private var ketoneTab = 0f
    private var gkiTab = 0f
    private var categoryTab = 0f

    private val FASTING = "0"
    private val PRE_MEAL = "1"
    private val POST_MEAL = "2"

    override fun createPdfDocument(): PdfDocument? {
        // Check here to be able to return right away in case of error
        if (viewModel.getSize(true) == 0) {
            Toast.makeText(activity, activity.getString(R.string.bloodPressure) + ": " + activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        super.createPdfDocument()

        val defaultThresholds = if(unit) activity.getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT_MG_DL) else activity.getString(R.string.GLUCOSE_THRESHOLDS_DEFAULT_MMOL_L)
        var s = "" + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS, defaultThresholds)
        val th = checkThresholds(activity, s, defaultThresholds, R.string.glucose)

        val lowerThreshold = try { th[0].toFloat() } catch  (_: NumberFormatException) { 0f }
        val upperThreshold = try { th[1].toFloat() } catch  (_: NumberFormatException) { 0f }

        setColumns()
        initDataPage()
        var f = pdfDataTop + pdfLineSpacing
        commentWidth = measureColumn(pdfRightBorder - commentTab)

        var currentDay = toStringDate(pdfItems[0].timestamp)

        for (item in pdfItems) {
            if (printDaySeparatorLines) {
                val itemDay = toStringDate(item.timestamp)
                if (currentDay != itemDay) {
                    f -= (pdfLineSpacing - pdfDaySeparatorLineSpacer) // Go back one line and add half a daySeparatorLineHeight
                    canvas.drawLine(pdfLeftBorder, f, pdfRightBorder, f, pdfPaintDaySeparator)
                    f += (pdfLineSpacing - pdfDaySeparatorLineSpacer)
                    currentDay = itemDay
                }
            }
            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)

            // If this is a blended item, draw comment and be done
            if (item.type != viewModel.dataType) {
                f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
                continue
            }

            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)
            s = when (item.value3) {
                FASTING -> activity.getString(R.string.fasting)
                PRE_MEAL -> activity.getString(R.string.preMeal)
                POST_MEAL -> activity.getString(R.string.postMeal)
                else -> ""
            }
            canvas.drawText(s, categoryTab + space, f, pdfPaint)

            val value = try { item.value1.toFloat() } catch  (_: NumberFormatException) { 0f }
            if (value > upperThreshold || value < lowerThreshold) canvas.drawText(warningSign, warningTab + space, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            if(logKetone) {
                canvas.drawText(item.value2, ketoneTab, f, pdfPaint)
                canvas.drawText(getGKI(item.value1, item.value2, unit), gkiTab, f, pdfPaint)
            }
            f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
        }
        // finish the page
        document.finishPage(page)

        /* Add chart page
        createPage()
        addChartPage()
        document.finishPage(page) */

        return document
    }

/*    fun addChartPage() {
        // Draw individual chart
        val chart = GlucoseChart(activity)
        //…and let super do the bitmap handling
        super.addChartPage(chart)
    }*/

    override fun setColumns() {
        val warningSignWidth = pdfPaintHighlight.measureText(activity.getString(R.string.warningSign))
        val glucoseWidth = pdfPaintHighlight.measureText(activity.getString(R.string.glucose))
        val ketoneWidth = pdfPaintHighlight.measureText(activity.getString(R.string.ketone))
        val gkiWidth = pdfPaintHighlight.measureText(activity.getString(R.string.glucoseKetoneIndex))
        val categoryWidth = pdfPaintHighlight.measureText(activity.getString(R.string.postMeal))

        categoryTab = pdfLeftBorder + dateTabWidth + space
        warningTab = categoryTab + categoryWidth + space + space
        dataTab = warningTab + warningSignWidth + space
        if(logKetone) {
            ketoneTab = dataTab + glucoseWidth + space
            gkiTab = ketoneTab + ketoneWidth + space
            commentTab = gkiTab + gkiWidth + padding
        }
        else commentTab = dataTab + glucoseWidth + space
    }

    override fun initDataPage() {
        drawHeader()

        // Data section
        canvas.drawText(activity.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.tag), categoryTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.glucose), dataTab, pdfDataTop, pdfPaintHighlight)
        if (logKetone) {
            canvas.drawText(activity.getString(R.string.ketone), ketoneTab, pdfDataTop, pdfPaintHighlight)
            canvas.drawText(activity.getString(R.string.glucoseKetoneIndex), gkiTab, pdfDataTop, pdfPaintHighlight)
        }
        canvas.drawText(activity.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(categoryTab, pdfHeaderBottom, categoryTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(warningTab, pdfHeaderBottom, warningTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }
}