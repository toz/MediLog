/*
 *     This file is part of MediLog.
 *
 *     MediLog is free softwar_: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.glucose

import android.os.Bundle
import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottom
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStart
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLine
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.cartesian.rememberVicoZoomState
import com.patrykandpatrick.vico.compose.common.ProvideVicoTheme
import com.patrykandpatrick.vico.compose.common.fill
import com.patrykandpatrick.vico.compose.m3.common.rememberM3VicoTheme
import com.patrykandpatrick.vico.core.cartesian.Zoom
import com.patrykandpatrick.vico.core.cartesian.axis.HorizontalAxis
import com.patrykandpatrick.vico.core.cartesian.axis.VerticalAxis
import com.patrykandpatrick.vico.core.cartesian.data.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.cartesian.data.CartesianLayerRangeProvider
import com.patrykandpatrick.vico.core.cartesian.data.lineSeries
import com.patrykandpatrick.vico.core.cartesian.layer.LineCartesianLayer
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.ChartActivity
import com.zell_mbc.medilog.base.rememberMarker
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.checkThresholds
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GlucoseChartActivity: ChartActivity() {
    override val filename = "GlucoseChart.jpg"

    var upperThresholdLine = 0
    var lowerThresholdLine = 0
    var minY = 1000.0 // Initialize with dummy large value
    var maxY = 0.0 // Initialize with dummy large value

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        legendText = mutableListOf(getString(R.string.glucose))

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_SHOW_GRID, true)
        showLegend = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_SHOW_LEGEND, false)
        showThreshold = preferences.getBoolean(SettingsActivity.KEY_PREF_GLUCOSE_SHOW_THRESHOLDS, false)
        if (showThreshold) {
            val s = "" + preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_THRESHOLDS, "")
            val defaultUnit = if (preferences.getString(SettingsActivity.KEY_PREF_GLUCOSE_UNIT, "") == getString(R.string.GLUCOSE_UNIT_MMOL_L)) getString(R.string.GLUCOSE_UNIT_MMOL_L) else getString(R.string.GLUCOSE_UNIT_MG_DL)
            val th = checkThresholds(this, s, defaultUnit, R.string.glucose)

            lowerThresholdLine = try { th[0].toInt() } catch  (_: NumberFormatException) { 0 }
            upperThresholdLine = try { th[1].toInt() } catch  (_: NumberFormatException) { 0 }
        }

        // Activities need to be self sufficient because MainActivity might get killed by OS
        viewModel = ViewModelProvider(this)[GlucoseViewModel::class.java]

        // For the charts only native diary entries can be considered, no matter what showAllTabs is set to
        val tmp = viewModel.blendInItems
        viewModel.blendInItems = false
        items = viewModel.getItems("ASC", filtered = true)
        viewModel.blendInItems = tmp

        if (items.size < 2) {
            Toast.makeText(this, getString(R.string.notEnoughDataForChart), Toast.LENGTH_LONG).show()
            finish()
            return // Needs return to avoid the rest to be executed because finish() may not kill the activity fast enough
        }

        // Collect minimal y value
        for (item in items) {
            val y = try { item.value1.toDouble() } catch (_: NumberFormatException) { 0.0 }
            if (y < minY) minY = y
            if (y > maxY) maxY = y
        }

        minY -= 1
        maxY += 1

        // Fill data maps
        xAxisOffset = items[0].timestamp
        value1Data = items.associateBy({ scaledTimeStamp(it.timestamp) }, { try { it.value1.toFloat() } catch (_: NumberFormatException) { 0F } })
        showContent()
    }

    @Composable
    override fun ShowContent() {
        Box(Modifier.safeDrawingPadding()) {
            chartColors = mutableListOf(Color(0xffb983ff))

            val modelProducer = remember { CartesianChartModelProducer() }
            LaunchedEffect(Unit) {
                withContext(Dispatchers.Default) {
                    modelProducer.runTransaction {
                        lineSeries { series(value1Data.keys, value1Data.values) }
                    }
                }
            }
            ProvideVicoTheme(rememberM3VicoTheme()) {
                ComposeChart(modelProducer, Modifier.fillMaxHeight())
            }
        }
    }

    @Composable
    private fun ComposeChart(modelProducer: CartesianChartModelProducer, modifier: Modifier) {
        val marker = rememberMarker()
        val lineProvider = LineCartesianLayer.LineProvider.series(LineCartesianLayer.rememberLine(fill = LineCartesianLayer.LineFill.single(fill(chartColors[0]))))

        CartesianChartHost(
            chart = rememberCartesianChart(
                rememberLineCartesianLayer(lineProvider = lineProvider, rangeProvider = CartesianLayerRangeProvider.fixed(minY = minY, maxY = maxY)),
                //rememberLineCartesianLayer(lineProvider = lineProvider),
                startAxis = VerticalAxis.rememberStart(
                    guideline = guideline(),
                ),
                bottomAxis = HorizontalAxis.rememberBottom(
                    valueFormatter = bottomAxisValueFormatter,
                    guideline = guideline(),
                ),
                legend = if (showLegend) rememberLegend() else null,
                decorations = if (showThreshold) listOf(helperLine("", upperThresholdLine.toDouble(), chartColors[0]), helperLine("", lowerThresholdLine.toDouble(), chartColors[0])) else listOf(),
                marker = marker,
            ),
            modelProducer = modelProducer,
            modifier = modifier,
            zoomState = rememberVicoZoomState(zoomEnabled = true, maxZoom = Zoom.max(Zoom.static(100f), Zoom.Content), initialZoom = Zoom.min(Zoom.static(), Zoom.Content)),
        )
    }
}
