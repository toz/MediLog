/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.oximetry

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Abc
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Filter.NO_TAGS
import com.zell_mbc.medilog.MainActivity.Companion.Threshold
import com.zell_mbc.medilog.MainActivity.Companion.activeProfileId
import com.zell_mbc.medilog.MainActivity.Companion.viewModel
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BaseTab
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.preferences.SettingsActivity
import com.zell_mbc.medilog.support.SnackbarDelegate
import java.util.Date

class OximetryTab(context: Context, snackbarDelegate: SnackbarDelegate): BaseTab(context, snackbarDelegate) {

    override var editActivityClass: Class<*> = OximetryEditActivity::class.java
    override var infoActivityClass: Class<*> = OximetryInfoActivity::class.java
    override var chartActivityClass: Class<*> = OximetryChartActivity::class.java
    private var oximetryHint = ""

    init {
        // -------------------------------
        val itemUnit = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_UNIT, context.getString(R.string.OXIMETRY_UNIT))
        oximetryHint = context.getString(R.string.oximetryHint) + " " + itemUnit

        showTime        = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_SHOWTIME, context.getString(R.string.SHOWTIME_DEFAULT).toBoolean())
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES, context.getString(R.string.OXIMETRY_HIGHLIGHT_VALUES_DEFAULT).toBoolean())
        val thresholdString = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_THRESHOLDS, context.getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT))
        if (highlightValues && thresholdString != null)
            lowerThreshold = try { thresholdString.toFloat() } catch (e: NumberFormatException) { 0f }
    }

    @Composable
    override fun ShowContent(padding: PaddingValues) {
        super.ShowContent(padding)
        showComment.value = comment.value.isNotEmpty() // For some reason the app will crash if this is in super?
        if (value1Width == 0.dp) MeasureValue1String("99")
        if (value2Width == 0.dp) MeasureValue2String("999")

        LazyColumn(state = listState, horizontalAlignment = Alignment.Start, modifier = Modifier.fillMaxWidth().padding(start = 8.dp, end = 8.dp)) {
            item {
                if (quickEntry) {
                    // Oximetry
                    val focusManager = LocalFocusManager.current
                    val (textField1, textField2, textField3) = remember { FocusRequester.createRefs() }
                    Row(Modifier.fillMaxWidth()) {
                        // Oximetry
                        TextField(
                            value = value1.value,
                            colors = textFieldColors,
                            onValueChange = { it ->
                                val tmp = it.filter  { it.isDigit() }
                                updateValue1(tmp)
                                if (it.length > 1) textField2.requestFocus()
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Decimal
                            ),
                            textStyle = TextStyle(),
                            label = { Text(text = stringResource(id = R.string.oximetry) + "*", maxLines = 1, overflow = TextOverflow.Ellipsis) },
                            placeholder = { Text(text = oximetryHint) },
                            modifier = Modifier.weight(1f).focusRequester(textField1).padding(end = 10.dp),
                            keyboardActions = KeyboardActions(onDone = { addItem() })
                        )
                        // Bpm
                        TextField(
                            value = value2.value,
                            colors = textFieldColors,
                            onValueChange = {
                                val tmp = it.filter { it.isDigit() }
                                updateValue2(tmp)
                                if (value2.value.isNotEmpty()) {
                                    showComment.value = true
                                    // Jump if field starts with 1 and is 3 digits or starts with !1 and is 2 digits
                                    val crit1 = value2.value.length == 3
                                    val crit2 = (value2.value[0] != '1' && value2.value.length == 2)
                                    if (crit1 || crit2) focusManager.moveFocus(FocusDirection.Next)
                                }
                            },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                            textStyle = TextStyle(),
                            label = { Text(text = stringResource(id = R.string.pulse), maxLines = 1, overflow = TextOverflow.Ellipsis)},
                            placeholder = { Text(text = stringResource(id = R.string.PULSE_HINT) + " " + stringResource(R.string.pulseUnit)
                            ) },
                            modifier = Modifier.weight(1f).focusRequester(textField2).padding(end = cellPadding.dp),
                            keyboardActions = KeyboardActions( onDone = { addItem() })
                        )
                    } // Row

                    if (showComment.value)
                        TextField(
                            value = comment.value,
                            colors = textFieldColors,
                            onValueChange = { updateComment(it) },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                capitalization = KeyboardCapitalization.Sentences
                            ),
                            singleLine = true,
                            textStyle = TextStyle(),
                            modifier = Modifier
                                .fillMaxWidth()
                                .focusRequester(textField3),
                            label = { Text(text = stringResource(id = R.string.comment)) },
                            trailingIcon = {
                                IconButton(onClick = {
                                    openTextTemplatesDialog.value = true
                                })
                                { Icon(Icons.Outlined.Abc, contentDescription = null) }
                            },
                            keyboardActions = KeyboardActions(onDone = {
                                keyboardController?.hide()
                                addItem()
                            })
                        )
                    SideEffect {
                        // Set cursor to first field after addItem completed
                        if (activateFirstField) {
                            textField1.requestFocus()
                            activateFirstField = false }
                    }

                    // Dialog section
                    if (openTextTemplatesDialog.value) {
                        textTemplateDialog.ShowDialog(setShowDialog = { openTextTemplatesDialog.value = it }) {
                            comment.value += it
                            textField3.requestFocus()
                        }
                    }
                } // End quick entry
            } // item
            items(dataList.value) { item ->
                // Lines starting from the top
                HorizontalDivider(color = MaterialTheme.colorScheme.secondaryContainer, thickness = 1.dp)
                Row(modifier = Modifier.fillMaxWidth().height(IntrinsicSize.Min).clickable { selection.value = item }, verticalAlignment = Alignment.CenterVertically) {
                    ShowRow(item)
                }
                if (selection.value != null) {
                    ItemClicked(selection.value!!._id)
                    selection.value = null
                }
            }
        }
    }

    @Composable
    fun ShowRow(item: Data) {
        // Timestamp
        Text(formatDateString(item.timestamp), Modifier
            .padding(end = cellPadding.dp, top = rowPadding.dp, bottom = rowPadding.dp)
            .width(dateColumnWidth),color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        var fontStyle = FontStyle.Normal
        var textColor = MaterialTheme.colorScheme.primary
        val value1String: String
        val value2String: String

        // A diary item blended in
        if (item.type != viewModel.dataType) {
            fontStyle = FontStyle.Italic
            val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
            Text(s, Modifier.padding(start = cellPadding.dp), color = MaterialTheme.colorScheme.primary, fontSize = fontSize.sp, fontStyle = fontStyle)
            return
        }

        val value1 = try { item.value1.toInt() } catch (e: NumberFormatException) { 0 }
        val value2 = try { item.value2.toInt() } catch (e: NumberFormatException) { 0 }

        // If usStyle cut off trailing .0
        value1String = value1.toString()
        if (highlightValues && (value1 < lowerThreshold)) textColor = MaterialTheme.colorScheme.error
        value2String = value2.toString()

        // Oximetry
        Text(value1String, Modifier.padding(start = cellPadding.dp, end = cellPadding.dp).width(value1Width),textAlign = TextAlign.Center,
            color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Pulse
        textColor = MaterialTheme.colorScheme.primary
        Text(value2String,
            Modifier.padding(start = cellPadding.dp, end = cellPadding.dp).width(value2Width),
            textAlign = TextAlign.Center, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle)
        VerticalDivider(color = MaterialTheme.colorScheme.secondaryContainer, modifier = Modifier.width(1.dp).fillMaxHeight()) // Vertical separator

        // Comment
        Spacer(modifier = Modifier.width(cellPadding.dp))
        val s = item.comment + if (item.attachment.isNotEmpty()) " $PAPERCLIP" else ""
        Text(s, color = textColor, fontSize = fontSize.sp, fontStyle = fontStyle,
            style = TextStyle(lineHeight = TextUnit.Unspecified, platformStyle = PlatformTextStyle(includeFontPadding = false)) // style is for removing the padding between multiline text
        )
    }


    override fun addItem() {
        if (!quickEntry) {
            startEditing(-1) // Indicate that this is about a new item
            return
        }

        // Close keyboard after entry is done
        hideKeyboard()

        // Check empty variables
        if (value1.value.isEmpty()) {
            snackbarDelegate.showSnackbar(context.getString(R.string.valuesMissing))
//            Toast.makeText(requireContext(), getString(R.string.weightMissing), Toast.LENGTH_LONG).show()
            return
        }

        value1.value = value1.value.replace(MainActivity.decimalSeparator, '.')
        val oximetryValue = viewModel.validValueI(value1.value, Threshold.MAX_OXIMETRY)
        if (oximetryValue <= 0) {
            snackbarDelegate.showSnackbar(context.getString(R.string.invalid) + " " + context.getString(R.string.oximetry) + " " + context.getString(R.string.value) + " $value1")
            return
        }

        // Valid pulse?
        if (value2.value.isNotBlank()) {
            //value2Value.value = value2Value.value.replace(MainActivity.decimalSeparator, '.')
            val pulseValue = viewModel.validValueI(value2.value, Threshold.MAX_PULSE)
            if (pulseValue <= 0) {
                snackbarDelegate.showSnackbar(context.getString(R.string.invalid) + " " + context.getString(R.string.pulse) + " " + context.getString(R.string.value) + " $value2")
                return
            }
        }

        val item = Data(_id = 0, timestamp = Date().time, comment = comment.value, type = viewModel.dataType, value1 = value1.value, value2 = value2.value, value3 = "", value4 = "", "", tags = NO_TAGS, category_id = -1, profile_id = activeProfileId) // Start with empty item
        viewModel.upsert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp))
            snackbarDelegate.showSnackbar(context.getString(R.string.filteredOut))

        cleanUpAfterAddItem()
    }
}