/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.oximetry

import android.app.Application
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.DataViewModel
import com.zell_mbc.medilog.preferences.SettingsActivity

class OximetryViewModel(application: Application): DataViewModel(application, Tabs.OXIMETRY) {
    override val filterStartPref = "OXIMETRYFILTERSTART"
    override val filterEndPref = "OXIMETRYFILTEREND"
    override val timeFilterModePref =  "OXIMETRY_FILTER_MODE"
    override val rollingFilterValuePref = "OXIMETRY_ROLLING_FILTER_VALUE"
    override val rollingFilterTimeframePref = "OXIMETRY_ROLLING_FILTER_TIMEFRAME"

    override var itemName = app.getString(R.string.oximetry)

    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_BLENDINITEMS, false)
}
