/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.oximetry

import android.app.Activity
import android.graphics.pdf.PdfDocument
import android.widget.Toast
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.BasePdf
import com.zell_mbc.medilog.preferences.SettingsActivity
import java.text.DateFormat

class OximetryPdf(activity: Activity): BasePdf(activity) {
    // Base class overrides
    override var itemName = activity.getString(R.string.oximetry)

    override var landscape = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_LANDSCAPE, activity.getString(R.string.OXIMETRY_LANDSCAPE_DEFAULT).toBoolean())
    override var pageSize = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_PAPER_SIZE, activity.getString(R.string.OXIMETRY_PAPER_SIZE_DEFAULT))
    override var blendInItems = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_BLENDINITEMS, activity.getString(R.string.BLENDINITEMS_DEFAULT).toBoolean())
    override var printDaySeparatorLines = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_PDF_DAYSEPARATOR, activity.getString(R.string.DAY_SEPARATOR_LINE_DEFAULT).toBoolean())
    override var highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES, false)

    // Oximetry specific
    private val itemUnit = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_UNIT, activity.getString(R.string.OXIMETRY_UNIT))

    private val warningSign = activity.getString(R.string.warningSign)
    private var warningTab = 0f
    private var pulseTab = 0f
    private var filtered = true


    override fun createPdfDocument(): PdfDocument? {
        // Check here to be able to return right away in case of error
        if (MainActivity.viewModel.getSize(true) == 0) {
            Toast.makeText(activity, activity.getString(R.string.bloodPressure) + ": " + activity.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }
        val dataType = MainActivity.viewModel.dataType
        super.createPdfDocument()

        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_OXIMETRY_HIGHLIGHT_VALUES, activity.getString(R.string.OXIMETRY_HIGHLIGHT_VALUES_DEFAULT).toBoolean())
        val thresholdString = preferences.getString(SettingsActivity.KEY_PREF_OXIMETRY_THRESHOLDS, activity.getString(R.string.OXIMETRY_THRESHOLDS_DEFAULT))
        var oximetryLowerThreshold = 0
        if (highlightValues && thresholdString != null)
            oximetryLowerThreshold = try { thresholdString.toInt() } catch (e: NumberFormatException) { 0 }

        setColumns()
        initDataPage()
        var f = pdfDataTop + pdfLineSpacing
        commentWidth = measureColumn(pdfRightBorder - commentTab)

        var currentDay = toStringDate(pdfItems[0].timestamp)

        for (item in pdfItems) {
            if (printDaySeparatorLines) {
                val itemDay = toStringDate(item.timestamp)
                if (currentDay != itemDay) {
                    f -= (pdfLineSpacing - pdfDaySeparatorLineSpacer) // Go back one line and add half a daySeparatorLineHeight
                    canvas.drawLine(pdfLeftBorder, f, pdfRightBorder, f, pdfPaintDaySeparator)
                    f += (pdfLineSpacing - pdfDaySeparatorLineSpacer)
                    currentDay = itemDay
                }
            }
            f = checkForNewPage(f)
            val dtString = toStringDate(item.timestamp) + "  " + toStringTime(item.timestamp)
            canvas.drawText(dtString, pdfLeftBorder, f, pdfPaint)

            // If this is a blended item, draw comment and be done
            if (item.type != dataType) {
                f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
                continue
            }

            val value = try { item.value1.toInt() } catch  (e: NumberFormatException) { 0 }
            if (value < oximetryLowerThreshold)
                canvas.drawText(warningSign, warningTab, f, pdfPaint)
            canvas.drawText(item.value1, dataTab, f, pdfPaint)
            canvas.drawText(item.value2, pulseTab, f, pdfPaint)
            f = if (item.comment.isNotBlank()) multipleLines(item.comment, f) else f + pdfLineSpacing
        }
        document.finishPage(page)

        // Add statistics page
        createPage()
        drawStatsPage()
        document.finishPage(page)

        /* Add chart page
        createPage()
        addChartPage()
        document.finishPage(page) */

        return document
    }

/*    fun addChartPage() {
        // Draw individual chart
        val chart = OximetryChart(activity, chartWidth = pageWidth)
        //…and let super do the bitmap handling
        super.addChartPage(chart)
    }*/

    override fun setColumns() {
        val warningSignWidth = pdfPaintHighlight.measureText(activity.getString(R.string.warningSign))
        val oximetryWidth = pdfPaintHighlight.measureText(activity.getString(R.string.oximetry))
        val pulseWidth = pdfPaintHighlight.measureText(activity.getString(R.string.pulse))

        warningTab = pdfLeftBorder + dateTabWidth + padding
        dataTab = warningTab + warningSignWidth + space
        pulseTab = dataTab + oximetryWidth + space
        commentTab = pulseTab + pulseWidth + padding
    }

    fun drawStatsPage() {
        super.drawStatsPage(0)

        var f = pdfDataTop + pdfLineSpacing
        canvas.drawText(activity.getString(R.string.statistics) + ":", pdfLeftBorder, f, pdfPaintHighlight)

        var totalMeasurements = MainActivity.viewModel.getSize(false)
        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(activity.getString(R.string.measurementsInDB) + " $totalMeasurements", pdfLeftBorder, f, pdfPaint)

        if (filtered) {
            totalMeasurements = MainActivity.viewModel.getSize(true)
            f += pdfLineSpacing
            canvas.drawText(activity.getString(R.string.measurementsInFilter) + " $totalMeasurements", pdfLeftBorder, f, pdfPaint)
        }

        val avg = MainActivity.viewModel.getAvgFloat("value1",filtered)
        var tmp = avg.toString()
        if (tmp.length > 4) tmp = tmp.substring(0,4)
        if (MainActivity.modifyDecimalSeparator) tmp = tmp.replace('.', MainActivity.decimalSeparator)
        val avgString = activity.getString(R.string.average) + ": $tmp $itemUnit"

        val min = MainActivity.viewModel.getMinValue1(filtered)
        val max = MainActivity.viewModel.getMaxValue1(filtered)
        var minMaxString = activity.getString(R.string.minMaxValues) + " $min - $max $itemUnit"
        if (MainActivity.modifyDecimalSeparator) minMaxString = minMaxString.replace('.', MainActivity.decimalSeparator)

        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(avgString, pdfLeftBorder, f, pdfPaint)
        f += pdfLineSpacing
        canvas.drawText(minMaxString, pdfLeftBorder, f, pdfPaint)

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate    = dateFormat.format(lastDay)
        val startDate  = dateFormat.format(firstDay)
        val timePeriod = activity.getString(R.string.timePeriod) + " $startDate - $endDate"
        f += pdfLineSpacing
        f += pdfLineSpacing
        canvas.drawText(timePeriod, pdfLeftBorder, f, pdfPaint)

        drawFilter(f)
    }


    override fun initDataPage() {
        drawHeader()
        
        // Data section
        canvas.drawText(activity.getString(R.string.date), pdfLeftBorder, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.oximetry), dataTab, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.pulse), pulseTab, pdfDataTop, pdfPaintHighlight)
        canvas.drawText(activity.getString(R.string.comment), commentTab + space, pdfDataTop, pdfPaintHighlight)
        canvas.drawLine(warningTab, pdfHeaderBottom, warningTab, pdfDataBottom, pdfPaint)  // Line before weight
        canvas.drawLine(commentTab, pdfHeaderBottom, commentTab, pdfDataBottom, pdfPaint)  // Line before comment
    }
}