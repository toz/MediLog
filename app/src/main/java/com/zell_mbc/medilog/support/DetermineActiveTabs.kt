/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

package com.zell_mbc.medilog.support

import com.zell_mbc.medilog.MainActivity.Companion.Tabs
import com.zell_mbc.medilog.MainActivity.Companion.profilesViewModel
import com.zell_mbc.medilog.MainActivity.Companion.settingsViewModel
import com.zell_mbc.medilog.MainActivity.Companion.weightViewModel
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.Profiles

// Set activeTabs based on imported data per profile
fun determineAvailableTabs(items: ArrayList<Data>, profiles: ArrayList<Profiles>) {
    val tabPresent = arrayOfNulls<Boolean>(Tabs.LAST_TAB)

    for (p in profiles) {
        // Identify tabs to activate
        for (i in tabPresent.indices) tabPresent[i] = false
        items.forEach { if (it.type > 0 && it.profile_id == p._id) tabPresent[it.type - 1] = true }

        // Capture activeTabs string
        var activeTabs = ""
        for (i in tabPresent.indices) if (tabPresent[i] == true) activeTabs += (i + 1).toString() + ","
        if (activeTabs.endsWith(",")) activeTabs = activeTabs.substring(0, activeTabs.length - 1) // Remove trailing ,

        settingsViewModel.setValue(profile_id = p._id, key = "activeTabs", value = activeTabs)
        profilesViewModel.upsert(p)
    }
}

// Set activeTabs based on profile
fun determineAvailableTabs(profileId: Int): String {
    val tabPresent = arrayOfNulls<Boolean>(Tabs.LAST_TAB)

    for (i in tabPresent.indices) {
        val string = "SELECT * from data WHERE profile_id = $profileId AND type = $i LIMIT 1"
        val sampleItem = weightViewModel.getItem(string) // Any viewModel will do
        tabPresent[i] =  (sampleItem != null)
    }

    var activeTabs = ""
    for (i in tabPresent.indices) if (tabPresent[i] == true) activeTabs += (i).toString() + ","
    if (activeTabs.endsWith(",")) activeTabs = activeTabs.substring(0, activeTabs.length - 1) // Remove trailing ,

    return activeTabs
}
