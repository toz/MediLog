/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.support

import android.content.Context
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE
import com.zell_mbc.medilog.R

class BiometricHelper internal constructor(var context: Context) {
    private var biometricManager: BiometricManager = BiometricManager.from(context)
    //private var userOutputService: UserOutputService = UserOutputServiceImpl(context,null)
    fun canAuthenticate(notify: Boolean): Int {
       // val ret = -9999 // Unknown error
        when (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK)) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
 //               Log.d(context.getString(R.string.app_name), "App can authenticate using biometrics.")
                return 0
            }
            BIOMETRIC_ERROR_NO_HARDWARE -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.noBiometricHardware))
                if (notify) {
                    run {
                        Toast.makeText(context, context.getString(R.string.noBiometricHardware), Toast.LENGTH_LONG).show()
                        //userOutputService.showAndHideMessageForLong(context.getString(R.string.noBiometricHardware))
                    }
                }
                return -1
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
//                Log.e(context.getString(R.string.app_name), context.getString(R.string.biometricHardwareUnavailable))
                run {
                    Toast.makeText(context, context.getString(R.string.biometricHardwareUnavailable), Toast.LENGTH_LONG).show()
                    //userOutputService.showAndHideMessageForLong(context.getString(R.string.biometricHardwareUnavailable)) 7
                    }
                return -2
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
 //               Log.e(context.getString(R.string.app_name), context.getString(R.string.noCredentials))
                if (notify) {
                    run {
                        Toast.makeText(context, context.getString(R.string.noCredentials), Toast.LENGTH_LONG).show()
                        //userOutputService.showAndHideMessageForLong(context.getString(R.string.noCredentials))
                    }
                }
                return -3
            }
            else -> return -4
        }
    }

    fun hasHardware(notify: Boolean): Boolean {
        when (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK)) {
            BIOMETRIC_ERROR_NO_HARDWARE -> {
                if (notify) {
                    Toast.makeText(context, context.getString(R.string.noBiometricHardware), Toast.LENGTH_LONG).show()
                    //userOutputService.showAndHideMessageForLong(context.getString(R.string.noBiometricHardware))
                return false
                }
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                if (notify) {
                    Toast.makeText(context, context.getString(R.string.biometricHardwareUnavailable), Toast.LENGTH_LONG).show()
                    //userOutputService.showAndHideMessageForLong(context.getString(R.string.biometricHardwareUnavailable))
                }
                return false
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                return true
            }
            BiometricManager.BIOMETRIC_SUCCESS -> {
                return true
            }
            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> {}
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> {}
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> {}
        }
        return true
    }

}