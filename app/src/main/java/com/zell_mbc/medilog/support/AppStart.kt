/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.support

import android.content.Context
import android.util.Log
import androidx.preference.PreferenceManager

enum class AppStart {
    FIRST_TIME, FIRST_TIME_VERSION, NORMAL
}

/**
 * The app version code (not the version name!) that was used on the last
 * start of the app.
 */
private const val LAST_APP_VERSION = "last_app_version"

/**
 * Finds out started for the first time (ever or in the current version)
 *
 * Note: This method is **not idempotent** only the first call will
 * determine the proper result. Any subsequent calls will only return
 * [AppStart.NORMAL] until the app is started again. So you might want
 * to consider caching the result!
 *
 * @return the type of app start
 */
fun checkAppStart(c: Context): AppStart {
    val preferences = PreferenceManager.getDefaultSharedPreferences(c)
    val currentVersionCode = getVersionCode(c).toInt()

    val lastVersionCode = preferences.getInt(LAST_APP_VERSION, -1)

    // Update version in preferences
    preferences.edit().putInt(LAST_APP_VERSION, currentVersionCode).apply()

    return when {
        lastVersionCode == -1 -> {
            AppStart.FIRST_TIME
        }
        lastVersionCode < currentVersionCode -> {
            AppStart.FIRST_TIME_VERSION
        }
        lastVersionCode > currentVersionCode -> {
            Log.w("Warning", "Current version code ($currentVersionCode) is less then the one recognized on last startup ($lastVersionCode$). Defensively assuming normal app start.")
            AppStart.NORMAL
        }
        else -> {
            AppStart.NORMAL
        }
    }
}
