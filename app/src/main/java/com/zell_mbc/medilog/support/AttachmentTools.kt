/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */


package com.zell_mbc.medilog.support

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKey
import com.zell_mbc.medilog.R
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption

const val ATTACHMENT_LABEL = "MediLog_"

// Creates Uri from string
fun getAttachmentUri(context: Context, attachment: String): Uri? {
    // If a path is present this is an unsaved and unencrypted(!) image
    val file = File(getAttachmentFolder(context), attachment)

    if (file.exists()) return FileProvider.getUriForFile(context, context.packageName + ".provider", file)
    else { // Should not happen…
        Toast.makeText(context, "Error: Attachment not found!", Toast.LENGTH_LONG).show()
        return null
    }
}

fun onAttachmentClick(context: Context, attachment: String) {
    val uri = getAttachmentUri(context, attachment)
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = uri
    intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
    try { context.startActivity(intent) } catch (_: Exception) { Toast.makeText(context, context.getString(R.string.eShareError), Toast.LENGTH_LONG).show() }
}

// Receives a Uri pointing to an encrypted file, returns a Uri pointing to an unencrypted file in application cache
fun decryptAttachment(activity: Activity, encryptedUri: Uri?): Uri? {
    if (encryptedUri == null) return null

    val attachmentFolder = getAttachmentFolder(activity)
    var uri = encryptedUri

    // Create temporary file
    val filePath = File(activity.cacheDir, "")
    val fileName = encryptedUri.lastPathSegment + ""
    val tempFile = File(filePath, fileName)
    uri = FileProvider.getUriForFile(activity,activity.applicationContext.packageName + ".provider", tempFile)

    // Decrypt source file into temp file
    val masterKey = MasterKey.Builder(activity, MasterKey.DEFAULT_MASTER_KEY_ALIAS).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
    val encryptedFile = EncryptedFile.Builder(activity, File(attachmentFolder, fileName), masterKey, EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB).build()
    val inputStream = encryptedFile.openFileInput()
    try {
        Files.copy(inputStream, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
    } catch (e: Exception) {
        val s = activity.getString(R.string.errorDuringAttachmentDecryption)
        Toast.makeText(activity, s, Toast.LENGTH_LONG).show()
        uri = encryptedUri // Try source file = not encrypted
    }

    return uri
}

// Receives a Uri pointing to an encrypted file, returns a Uri pointing to an unencrypted file in application cache
fun decryptAttachment(context: Context, fileName: String): File? {
    if (fileName.isBlank()) return null

    val attachmentFolder = getAttachmentFolder(context)
    val unencryptedFile: File?

    // Create temporary file
    val filePath = File(context.cacheDir, "")
    unencryptedFile = File(filePath, fileName)

    // Decrypt source file into temp file
    val masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
    val encryptedFile = EncryptedFile.Builder(context, File(attachmentFolder, fileName), masterKey, EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB).build()
    val inputStream = encryptedFile.openFileInput()
    try {
        Files.copy(inputStream, unencryptedFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
    } catch (e: Exception) {
        val s = context.getString(R.string.errorDuringAttachmentDecryption)
        Toast.makeText(context, s, Toast.LENGTH_LONG).show()
    }
    // Return the source file instead
    //else unencryptedFile = File(attachmentFolder, fileName)

    return unencryptedFile
}

// One location to define the attachment folder
fun getAttachmentFolder(context: Context): String {
    val subFolder = context.filesDir.toString() + "/attachments"
    if (!File(subFolder).exists())
        File(subFolder).mkdir()
    //Files.createDirectories(Paths.get(subFolder))
    return subFolder
}

// Counts attachments based on files in attachment folder
fun countAttachments(context: Context, filter: String): Int {
    //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return 0
    //val list = listAttachments(context, filter)
    val list = listAttachments(context, filter)
    return list.count()
}

fun listAttachments(context: Context, filter: String): MutableList<String> {
    val list = mutableListOf<String>()
    File(getAttachmentFolder(context)).walk().forEach { if (it.name.contains(filter)) list.add(it.name) }
    return list
}

// Returns size of files in bytes
fun attachmentSize(context: Context, filter: String): Long {
    var total = 0L
    val f = getAttachmentFolder(context)
    File(f).walk().forEach {
        if (it.name.contains(filter)) total += it.length()
    }
    return total
}

// Delete application cache to not leave unencrypted files behind
fun emptyAttachmentFolder(context: Context): Int {
    var count = 0
    for (file:File in File(getAttachmentFolder(context), "").listFiles()!!) {
        file.delete()
        //Log.d("FilesDir:", file.name)
        count++
    }
    return count
}

// Delete application cache to not leave unencrypted files behind
fun deleteApplicationCache(context: Context): Int {
    // Clean up cache to limit exposure of unencrypted files
    var count = 0
    for (file: File in context.cacheDir.listFiles()!!) {
        file.delete()
        //Log.d("Cache:", file.name + " " + ret.toString())
        count++
    }
    return count
}
