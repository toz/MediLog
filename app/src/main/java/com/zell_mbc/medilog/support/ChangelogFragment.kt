/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import androidx.fragment.app.DialogFragment
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.ChangelogBinding
import com.zell_mbc.medilog.preferences.SettingsActivity
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.withLink
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.MainActivity.Companion.LINK_COLOR

class ChangelogFragment: DialogFragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: ChangelogBinding? = null
    private val binding get() = _binding!!
    private var changelog = ""
    var defaultFontSize = 0
    var fw = FontWeight.Normal
    var fs = 0

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ChangelogBinding.inflate(inflater, container, false)
        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @Composable
    fun ShowRow(line: String) {
        val linkStart = line.indexOf("https://")
        val lineHeight = defaultFontSize + 4
        if (linkStart < 0)
            Text(line, color = MaterialTheme.colorScheme.primary, lineHeight = lineHeight.sp , fontSize = fs.sp, fontFamily = FontFamily.Default, fontWeight = fw)
        else {
            Text(line.substring(0, linkStart), lineHeight = lineHeight.sp, color = MaterialTheme.colorScheme.primary, fontSize = fs.sp, fontFamily = FontFamily.Default, fontWeight = fw)
            Text(buildAnnotatedString {
                append("")
                withLink(LinkAnnotation.Url(url = line.substring(linkStart), TextLinkStyles(style = SpanStyle(color = Color(LINK_COLOR))))) { append(line.substring(linkStart)) }
            }, style = MaterialTheme.typography.bodySmall, textDecoration = TextDecoration.Underline)
            //Text("")
        }
    }


    // Poor man's Markdown scanner
    @Composable
    fun ShowContent() {
        val scrollState = rememberScrollState()
        var skipLine = false

        Column(horizontalAlignment = Alignment.Start,
            modifier = Modifier
                .verticalScroll(state = scrollState)
                .padding(start = 16.dp, end = 16.dp, top = 16.dp, bottom = 8.dp)) {
            //MarkdownText(markdown = changelog, fontSize = 12.sp, color =  MaterialTheme.colors.primary)
            fs = defaultFontSize + 4
            Text("ChangeLog", color = MaterialTheme.colorScheme.primary, fontSize = fs.sp, fontFamily = FontFamily.Default, fontWeight = FontWeight.Bold)
            //Text("")
            for (line in changelog.lines()){
                if (!skipLine) {
                    var l = ""
                    fw = FontWeight.Normal
                    fs = defaultFontSize

                    if (line.indexOf("###", 0) >= 0) {
                        fs = defaultFontSize
                        fw = FontWeight.Bold
                        l = line.substring(4)
                        skipLine = true
                    }
                    else if (line.indexOf("##", 0) >= 0) {
                        fs = defaultFontSize +2
                        fw = FontWeight.Bold
                        l = line.substring(3)
                        skipLine = true
                    }
                    else if (line.indexOf("# ", 0) >= 0) { // get rid of # Changelog line
                        skipLine = true
                    }
                    else l = line

                    ShowRow(l)
                }
                else skipLine = false
           }
                        }
            Column(horizontalAlignment = Alignment.End) {
                IconButton( onClick = { dialog?.dismiss() }, modifier = Modifier.padding(top = 0.dp)) {
                    Icon(painter = painterResource(R.drawable.ic_baseline_close_24),"ChangeLog", tint = MaterialTheme.colorScheme.primary) }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())

        changelog = resources.openRawResource(R.raw.changelog).bufferedReader().use { it.readText() }

        defaultFontSize = getString(R.string.TEXT_SIZE_DEFAULT).toInt() // Set defined value in case the below fails
        val checkValue = preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, getString(R.string.TEXT_SIZE_DEFAULT))
        if (!checkValue.isNullOrEmpty()) {
            var tmp: Int
            try {
                tmp = checkValue.toInt()
            } catch (e: NumberFormatException) {
                tmp = getString(R.string.TEXT_SIZE_DEFAULT).toInt()
                Toast.makeText(requireContext(), "Invalid Font Size value: $checkValue", Toast.LENGTH_LONG).show()
            }
            if (tmp > 0) defaultFontSize = tmp
        }

        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT) // Full screen
        DialogProperties(dismissOnBackPress = true, dismissOnClickOutside = false)
        binding.changelogView.setContent { MedilogTheme { ShowContent() } }
    }

}