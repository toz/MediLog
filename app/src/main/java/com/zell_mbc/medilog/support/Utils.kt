/*
 *     This file is part of MediLog.
 *
 *     MediLog is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation.
 *
 *     MediLog is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with MediLog.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2018 - 2024 by Zell-MBC.com
 */

/*
 */

package com.zell_mbc.medilog.support

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter
import com.zell_mbc.medilog.MainActivity.Companion.Delimiter.THRESHOLD
import com.zell_mbc.medilog.R
import java.text.DateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.Locale



fun checkThresholds(context: Context, value: String, default: String, type: Int): Array<String> {
    //val userOutputService: UserOutputService = UserOutputServiceImpl(context,null)
    var lowerThreshold = 0f
    var upperThreshold = 99999f

    // Check if value is empty, if yes set to default
    var checkValue = value.ifEmpty { default }

    // Handle weight legacy setting where there is only an upper limit
    if (type == R.string.weight && !value.contains("-")) checkValue = "-" + value

    // Is only the lower limit supplied, if yes add fake upper threshold
    if (checkValue[checkValue.length-1].toString() == THRESHOLD)
        checkValue += upperThreshold.toString()

    // Is only the upper limit supplied, if yes add fake lower threshold
    if (checkValue[0].toString() == THRESHOLD)
        checkValue = lowerThreshold.toString() + checkValue

    if (checkValue.isNotEmpty()) {
        val minMax = checkValue.split(THRESHOLD)
        // Check if really 2 values were present
        if ( minMax.size == 2 ) {
            try {
                lowerThreshold = minMax[0].toFloat()
            } catch (e: NumberFormatException) {
                Toast.makeText(context, context.getString(type) + ", " + context.getString(R.string.invalidLowerThreshold) + ": $checkValue", Toast.LENGTH_LONG).show()
                //userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidLowerThreshold) + ": $checkValue")
                checkValue = default
            }
            try {
                upperThreshold = minMax[1].toFloat()
            } catch (e: NumberFormatException) {
                Toast.makeText(context,context.getString(type) + ", " + context.getString(R.string.invalidUpperThreshold) + ": $checkValue", Toast.LENGTH_LONG).show()
                //userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidUpperThreshold) + ": $checkValue")
                checkValue = default
            }

            if (upperThreshold <= lowerThreshold) {
                Toast.makeText(context,context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue", Toast.LENGTH_LONG).show()
                //userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue")
                checkValue = default
            }
        }
        else {
            Toast.makeText(context,context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue", Toast.LENGTH_LONG).show()
            //userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue")
            checkValue = default
        }
    }
    else checkValue = default

    val finalMinMax = checkValue.split(Delimiter.THRESHOLD)
    val arr = try {
        arrayOf(finalMinMax[0], finalMinMax[1])
    } catch (e: IndexOutOfBoundsException) {
        // This exception can only happen if the default value is wrong -> Should never happen…
        Toast.makeText(context,context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue", Toast.LENGTH_LONG).show()
        //userOutputService.showAndHideMessageForLong(context.getString(type) + ", " + context.getString(R.string.invalidThresholds) + ": $checkValue")
        arrayOf("0","0")
    }
    return arr
}

fun getMemoryConsumption(): Long {
    val usedSize = try {
        val info = Runtime.getRuntime()
        val freeSize = info.freeMemory()
        val totalSize = info.totalMemory()
        totalSize - freeSize
    } catch (e: Exception) {
        -1L
    }
    return usedSize
}

fun getCorrectedDateFormat(c: Context): DateFormat {
    // For some reason en_DE does not set the correct date format?
//    val devLocale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) c.resources.configuration.locales.get(0) else c.resources.configuration.locale
    val devLocale = c.resources.configuration.locales.get(0)
    return if (devLocale.language == "en" && devLocale.country == "DE")
        DateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMANY)
    else DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
}

fun millisecondsToLocalDate(m: Long): LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(m), ZoneOffset.UTC)
fun localDateToMilliseconds(localDate: LocalDate) = localDate.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli()

// Handle deprecation error
fun getVersionCode(context: Context): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) getLongVersionCode(context).toString()
    else getIntVersionCode(context).toString()
}

// Method to retrieve version code using PackageManager
@Suppress("DEPRECATION")
fun getIntVersionCode(context: Context): Int {
    try {
        val packageManager = context.packageManager
        val packageName = context.packageName
        val packageInfo = packageManager.getPackageInfo(packageName, 0)
        return packageInfo.versionCode
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        return -1 // Error occurred, return -1 or handle it accordingly
    }
}

@RequiresApi(Build.VERSION_CODES.P)
fun getLongVersionCode(context: Context): Long {
    try {
        val packageManager = context.packageManager
        val packageName = context.packageName
        val packageInfo = packageManager.getPackageInfo(packageName, 0)
        return packageInfo.longVersionCode
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        return -1L // Error occurred, return -1 or handle it accordingly
    }
}

// Method to retrieve version name using PackageManager
fun getVersionName(context: Context): String {
    try {
        val packageManager = context.packageManager
        val packageName = context.packageName
        val packageInfo = packageManager.getPackageInfo(packageName, 0)
        return packageInfo.versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        return "" // Error occurred, return empty string or handle it accordingly
    }
}
