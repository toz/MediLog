Datenschutzfreundliches Protokollieren von Blutdruck, Gewicht, täglicher Trinkmenge, Blutzucker (experimentell) sowie ein einfaches Tagebuch. Inklusive Diagramme und der Möglichkeit die vertraulichen Daten sicher weiterzuleiten.

 * Unterstützt Biometrie (Fingerabdruck) zur Anmeldung
 * Diagramme mit Trends und Schwellwerten
 * CSV und PDF Export in passwortgeschützter ZIP Datei 
