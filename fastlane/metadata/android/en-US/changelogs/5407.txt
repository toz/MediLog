## v2.4.1, build 5407
New 

- BloodPressure: Allow to change the timezones for morning-midday-evening in PDF report
- BloodPressure: Added posibility to show only trendlines and no actual data values in chart, new setting ShowValues
- Added support for CSV export (Open + Send CSV file)
- Added setting to specify the minutes of inactivity after which reauthentication is required
- Added **Opt-In** user feedback setting

Fixed/Changed

- Fixed crash when creating a pdf reported with empty table
- Fixed data duplication in PDF report when report is opened multiple times: https://gitlab.com/toz12/medilog/-/issues/209
- Fixed dark mode color issues, https://gitlab.com/toz12/medilog/-/issues/207
- Entries sometimes don't get emptied after hitting enter, https://gitlab.com/toz12/medilog/-/issues/192


Known issues

- A few new strings not yet translated
