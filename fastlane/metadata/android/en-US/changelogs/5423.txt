## v2.5.7, build 5423

### New 

- Nothing

### Fixed/Changed

- Export: Fixed visual glitch, https://codeberg.org/toz/MediLog/issues/288
- Glucose: Stats don't show in dark mode, https://codeberg.org/toz/MediLog/issues/296
- Glucose: Fixed distorted PDF output, https://codeberg.org/toz/MediLog/issues/297
- Water: Fixed a bug when filtering data for the PDF report
- Some language updates

### Known issues

- None
