## v2.4.0, build 5405
New 

- Diary: Possibility to quickly capture overall health state in Diary tab by just hitting add-button, automatically filling in "Good, ok, bad" dependent on the health icon state
- Diary: Totally redesigned chart, requires API > 26
- Weight: Added configurable tare which is automatically subtracted from every measurement to support use cases such as assisted measurements or wheelchairs.
- Added ability to change text size in PDF reports
- Added rolling filter feature, allows to always show the last x years/months/weeks


Fixed/Changed

- Switched core UI parts to JetPack Compose
- Some UI cleanup/improvements
    + Moved delete record to edit dialog
    + Hiding the + Button in quickEntry mode whenever data only is entered. + Button is required when comment field is visible to allow multiline entries
- Changed logic of backup check, never warn if DB is empty (= less than 5 entries)
- Support for line breaks in PDF documents added
- Support for line breaks in comment field in data tabs 
- Automatically calculate width of columns based on font size and locale
- Removed rowHeight setting, now automatically calculated
- Added rowPadding setting
- About Dialog content is scrollable for phones with low resolution


Known issues

- Screen refresh after editing/deleting values is a little clunky
- Screen refresh after changing filter value not working, needs selecting a different tab
- A few new strings not yet translated
- Entries sometimes don't get emptied after hitting enter https://gitlab.com/toz12/medilog/-/issues/192
