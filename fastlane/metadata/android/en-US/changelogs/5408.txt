## v2.4.2, build 5408
New 

- Weight: Added posibility to show only trendlines and no actual data in chart, new setting ShowValues yes/no
- Glucose: Lots of smaller changes, added Ketone as a second measurement

Fixed/Changed

- Tab and data sometimes out of sync, https://gitlab.com/toz12/medilog/-/issues/200
- Removed "change-tab" animation to support the fix above
- Lots of updates to Polish language

Known issues

- None
