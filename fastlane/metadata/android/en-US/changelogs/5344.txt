## v2.3.0, build 5344
New 

- Totally revamped data import screen
- BloodPressure: Added support for highlighting low blood pressure
- BloodPressure: Added statistics to PDF and info screen
- Additional stats in Info screen and PDF report
- Diary: Switched from warning sign to a more approbriate Emoji for health state 
- Diary: Added experimental chart to reflect health state. Not sure if it will stay.
- Added Chinese Simplified translation
- Some language updates and minor bug fixes here and there


Fixed/Changed

- Warning signs shown as Emoji instead of proper text, https://gitlab.com/toz12/medilog/-/issues/168
- Write Access for Nextcloud lost after App restart, https://gitlab.com/toz12/medilog/-/issues/149

Known issues

- Sometimes the first Edit field (E.g. Systolic or Weight) in a tab loses focus as soon as the first digit is entered

