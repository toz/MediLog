
## v2.0.1, build 5327
New 

- No code changes

Fixed

- Crash of F-Droid signed app during launch. Changed AndroidStudio minifiedEnabled compiler flag to false.

Known issues

- None
