## v2.6.3, build 5443

### New 

- Nothing

### Fixed/Changed

- BloodPressure: Removed average from statistics screen as it's a potentially misleading number with no real value
- BloodPressure: Fixed an issue in PDF report where the statistics screen showed Systolic values twice
- Some Turkish language updates by @abfreeman
- Targeting Android 14 SDK (SDK Version = 34)

