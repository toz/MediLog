# MediLog
[<img src="./ic_launcher-playstore.png"
     alt="MediLog Logo"
     height="80">](./ic_launcher-playstore.png)

[![Status: Active](./badges/status.svg)](#)
[![Status: Active](./badges/release.svg)](#)
[![API Reference](./badges/license.svg)](./LICENSE)

Android App to easily capture medical data. The following values are supported at present:

- Blood Pressure
- Weight + Body Fat
- A Diary
- Glucose
- Oximetry
- Temperature
- Water intake


## Key features

The main goal of MediLog is to allow the manual capturing of medical data, basically replacing pen and paper. And to do this as quickly as possible. This means supporting smart devices is not on my roadmap.

Also, I don't plan to cover the whole universe of medical data, but rather provide a solution for types of data which have to be entered often and cases where the alternatives either don't respect privacy, are full of trackers or simply too complicated. E.g.

For example:
- Blood Pressure values and Weight might get captured multiple times a day and are a good fit for MediLog, therefore there's a dedicated tab for these values.
- PSA values get captured once or twice a year, which means a dedicated tab would be too complicated, I'd recommend storing these in MediLog's Diary tab. Search will allow to retrieve such values whenever required.


Guiding principles for MediLog are:

- Be simple. Allow to capture data as quickly as possible, don't get in the way. If you can think of ways to improve the UI, let me know.
- Be open. Export and import format is a simple CSV, if you can export CSV from your existing application it should be fairly easy to import into MediLog, and of course, import into your new application if you ever wish to move on with your data
- Be secure. PDF reports and ZIP file backups are password protected, the database is encrypted
- Be transparent. No data transfers to anyone but the one you choose. No hidden telemetry, no automatic crash logs, etc.
- In essence, offer appropriate measures to protect your data as defined by art. 9 GDPR, Processing of special categories of personal data


[![Awesome Humane Tech](https://raw.githubusercontent.com/humanetech-community/awesome-humane-tech/main/humane-tech-badge.svg?sanitize=true)](https://delightful.club/delightful-humane-design/#health)

### Installation


Download and install MediLog from
[Obtainium](https://github.com/ImranR98/Obtainium),
[IzzyOnDroid](https://apt.izzysoft.de/packages/com.zell_mbc.medilog),
[F-Droid](https://f-droid.org/packages/com.zell_mbc.medilog/)

<p float="left">
  <a href="https://android.izzysoft.de/repo/apk/com.zell_mbc.medilog">
    <img alt="Get it on IzzyOnDroid" src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png" height="100">
  </a>
  <a href="https://f-droid.org/packages/com.zell_mbc.medilog/">
    <img alt="Get it on F-Droid" src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="100">
  </a>
  <a href="https://codeberg.org/toz/MediLog/releases/">
    <img alt="Get it on Obtainium" src="./badges/obtainium.svg" height="80">
  </a>
</p>


**Important note:** Be careful when mixing downloads from F-Droid with other locations. The F-Droid version is signed with a different developer key which means you will need to delete the existing app when changing. Make sure to backup your data before you change.

### Removal
Like with all Android applications, via a long click on the app icon.


**Uninstalling the application will delete all data! Unrecoverable! Make sure you backup your data first!**

### User manual
Can be found here: https://codeberg.org/toz/MediLog/wiki/?action=_pages

I know it's missing lots of sections, feel free to contribute.


### Privacy Policy

#### Stored data
Data entered by the user is stored inside an encrypted SQLite database.

**To keep the input process as simple and fast as possible, the app/your data is not protected with an additional password. If your device has support for Biometric (Fingerprint) make sure to enable it. Otherwise, if someone is able to unlock your device they can access your health data!**

The app supports storing backups in encrypted ZIP files. Make use of it by adding a password whenever you create a file or setting a default one in the settings dialog. Don't forget to test (and remember) the password.
The app allows to send password protected files so you can share over unprotected media (eg. email) reducing the risk of your data getting in the wrong hands.


#### Required permissions

- WRITE_EXTERNAL_STORAGE : Required to export backup files
- READ_EXTERNAL_STORAGE: Required to import backup files
- USE_BIOMETRIC: Required to enable biometric logon protection
- INTERNET: Required for automated and ad-hoc feedback

#### Tracking
No advertising, no hidden tracking!
You can however let me know which tabs you use via the *user feedback* function, which I would appreciate because it's the only way for me to know if I focus my time and efforts on areas which are actually used by people out there.
As of version 3.0 there is also the possibility to send crash logs to the developers in case of application crashes. However, crash logs will never be sent without your explicit approval. See here for more details: https://codeberg.org/toz/MediLog/wiki/Crash-logs

#### Libraries
3rd party libraries in use are the below:

- [Vico](https://patrykandpatrick.com/vico/wiki/): Most of the charts,
- [Calendar](https://github.com/kizitonwose/Calendar): Diary heatmap chart
- [Zip4J](https://github.com/srikanth-lingala/zip4j): Enable password protected ZIP files
- [SQLCipher](https://www.zetetic.net/sqlcipher/): Encrypt the SQLite database
- [ACRA](https://www.acra.ch/): Enable crash logs

## Contact & Bug reporting

Please use, whenever possible, the issue tracker at [codeberg.org](https://codeberg.org/toz/MediLog/issues) for reporting issues and/or suggestions:

Other means to contact the author are:
- Matrix: medilog:matrix.org
- Fediverse: Akkoma/Mastodon/Pleroma/etc., @thomas@social.zell-mbc.com
- eMail: medilog@zell-mbc.com

## Translations
The number of translations is growing quickly. If your language isn't covered, or if you are a native speaker and think your language could be improved, please contact me.

Translation platform: https://translate.codeberg.org/projects/medilog/

[<img src="https://translate.codeberg.org/widgets/medilog/-/multi-blue.svg">](https://translate.codeberg.org/engage/medilog/)


## Donations
If you feel like it…

[<img src="https://liberapay.com/assets/widgets/donate.svg" border="0" alt="Donate using Liberapay">](https://liberapay.com/MediLog/donate)
[<img src="https://www.paypalobjects.com/webstatic/de_DE/i/de-pp-logo-100px.png" border="0" alt="PayPal Logo">](https://www.paypal.com/paypalme/thomaszellmbc)
[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bitcoin_logo.svg/252px-Bitcoin_logo.svg.png" border="0" height="20" alt="Bitcoin Logo">](https://live.blockcypher.com/btc/address/1EUrRpjDAGgpS8J46tmsVWKbgNqAv7rWC7/)

## Screenshots
<p float="left">
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/Weight.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/WeightChart.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/WeightInfo.png" width="400" />
</p>
<p float="left">
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/BloodPressure.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/BloodPressureChart.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/BloodPressureInfo.png" width="400" />
</p>
<p float="left">
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/Diary.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/DiaryEdit.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/DiaryChart.png" width="400" />
</p>
<p float="left">
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/PDFReport.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/Settings.png" width="400" />
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/About.png" width="400" />
</p>

## Changelog
[Change log](ChangeLog.md)


